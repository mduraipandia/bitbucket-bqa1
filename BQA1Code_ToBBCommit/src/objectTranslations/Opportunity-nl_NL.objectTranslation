<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <fields>
        <label><!-- Account Representative --></label>
        <lookupFilter>
            <errorMessage><!-- The user entered is not in an Account Representative role. --></errorMessage>
        </lookupFilter>
        <name>Account_Manager__c</name>
        <relationshipLabel><!-- Opportunities --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Account Number --></label>
        <name>Account_Number__c</name>
    </fields>
    <fields>
        <label><!-- Opportunity Canvass --></label>
        <name>Account_Primary_Canvass__c</name>
        <relationshipLabel><!-- Opportunities --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Allow Upfront Payment --></label>
        <name>Allow_Upfront_Payment__c</name>
    </fields>
    <fields>
        <label><!-- Approval Status --></label>
        <name>Approval_Status__c</name>
        <picklistValues>
            <masterLabel>Approved</masterLabel>
            <translation><!-- Approved --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Rejected</masterLabel>
            <translation><!-- Rejected --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Under Review</masterLabel>
            <translation><!-- Under Review --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Auto Number --></label>
        <name>Auto_Number__c</name>
    </fields>
    <fields>
        <label><!-- BMI Quote Exists --></label>
        <name>BMI_Quote_Exists__c</name>
    </fields>
    <fields>
        <label><!-- Line Items --></label>
        <name>BigMachines__Line_Items__c</name>
    </fields>
    <fields>
        <label><!-- Billing Contact --></label>
        <lookupFilter>
            <errorMessage><!-- The Billing Contact must be attached to the Account for this Opportunity. --></errorMessage>
        </lookupFilter>
        <name>Billing_Contact__c</name>
        <relationshipLabel><!-- Opportunities --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Billing Frequency --></label>
        <name>Billing_Frequency__c</name>
        <picklistValues>
            <masterLabel>Monthly</masterLabel>
            <translation><!-- Monthly --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Single Payment</masterLabel>
            <translation><!-- Single Payment --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Billing Locked --></label>
        <name>Billing_Locked__c</name>
    </fields>
    <fields>
        <label><!-- Billing Partner --></label>
        <name>Billing_Partner__c</name>
    </fields>
    <fields>
        <label><!-- CORE Migration ID --></label>
        <name>CORE_Migration_ID__c</name>
    </fields>
    <fields>
        <label><!-- CORE Migration ID temp --></label>
        <name>CORE_Migration_ID_temp__c</name>
    </fields>
    <fields>
        <help><!-- Used for the Informatica Cancellation job. DO NOT DELETE --></help>
        <label><!-- Cancellation Letter Sent --></label>
        <name>Cancellation_Letter_Sent__c</name>
    </fields>
    <fields>
        <label><!-- Client Tier --></label>
        <name>Client_Tier__c</name>
    </fields>
    <fields>
        <label><!-- Close Date --></label>
        <name>Close_Date__c</name>
    </fields>
    <fields>
        <help><!-- Reason for Opportunity Closed Lost --></help>
        <label><!-- Closed Lost Reason --></label>
        <name>Closed_Lost_Reason__c</name>
        <picklistValues>
            <masterLabel>Client Choice</masterLabel>
            <translation><!-- Client Choice --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Disconnect</masterLabel>
            <translation><!-- Disconnect --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Expired</masterLabel>
            <translation><!-- Expired --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>NYPS</masterLabel>
            <translation><!-- NYPS --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Non- Adv Declined</masterLabel>
            <translation><!-- Non- Adv Declined --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Non-Pay</masterLabel>
            <translation><!-- Non-Pay --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Unable to Connect</masterLabel>
            <translation><!-- Unable to Connect --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- If the advertiser is planning to use Co-Op reimbursement, please indicate which ads and brands Co-Op is being used for so they can be sent the paperwork they need. --></help>
        <label><!-- Co-Op Ads and Brands (if applicable) --></label>
        <name>Co_Op_Ads_and_Brands__c</name>
    </fields>
    <fields>
        <label><!-- Converted From Lead --></label>
        <name>Converted_From_Lead__c</name>
    </fields>
    <fields>
        <label><!-- Credit Check Required --></label>
        <name>Credit_Check__c</name>
    </fields>
    <fields>
        <label><!-- DB Competitor --></label>
        <name>DB_Competitor__c</name>
        <picklistValues>
            <masterLabel>Competitor A</masterLabel>
            <translation><!-- Competitor A --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Competitor B</masterLabel>
            <translation><!-- Competitor B --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Competitor C</masterLabel>
            <translation><!-- Competitor C --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Delinquency Status --></label>
        <name>Delinquency_Status__c</name>
    </fields>
    <fields>
        <label><!-- Directory # --></label>
        <name>Directory_Number__c</name>
    </fields>
    <fields>
        <label><!-- DocuSign Received --></label>
        <name>DocuSign_Received__c</name>
    </fields>
    <fields>
        <label><!-- Enterprise Customer ID --></label>
        <name>Enterprise_Customer_ID__c</name>
    </fields>
    <fields>
        <label><!-- Guaranteed Calls --></label>
        <name>Guaranteed_Calls__c</name>
    </fields>
    <fields>
        <label><!-- Guaranteed End Date --></label>
        <name>Guaranteed_End_Date__c</name>
    </fields>
    <fields>
        <label><!-- Guaranteed Start Date --></label>
        <name>Guaranteed_Start_Date__c</name>
    </fields>
    <fields>
        <help><!-- This checkbox is used to flag down that an Opportunity that was submitted for Late Order Print Approval was Approved. If Approved, the BOTS Edition of the Opportunity Product&apos;s Directory&apos;s will be applied as opposed to the NI Edition. --></help>
        <label><!-- Late Print Order Approved --></label>
        <name>Late_Print_Order_Approved__c</name>
    </fields>
    <fields>
        <help><!-- This is the Text Area box where a Sales Rep will describe the reason, and nature, of a late print order --></help>
        <label><!-- Late Print Order Description --></label>
        <name>Late_Print_Order_Description__c</name>
    </fields>
    <fields>
        <help><!-- If the Print Order is Rejected, this will be marked as TRUE --></help>
        <label><!-- Late Print Order Rejected --></label>
        <name>Late_Print_Order_Rejected__c</name>
    </fields>
    <fields>
        <name>LeadSource</name>
        <picklistValues>
            <masterLabel>Advertisement</masterLabel>
            <translation><!-- Advertisement --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Business Card</masterLabel>
            <translation><!-- Business Card --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Competitor IYP</masterLabel>
            <translation><!-- Competitor IYP --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Competitor YP</masterLabel>
            <translation><!-- Competitor YP --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Customer Service</masterLabel>
            <translation><!-- Customer Service --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Employee Referral</masterLabel>
            <translation><!-- Employee Referral --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>External Referral</masterLabel>
            <translation><!-- External Referral --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Industry/Community Website</masterLabel>
            <translation><!-- Industry/Community Website --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Leads List</masterLabel>
            <translation><!-- Leads List --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Newspaper/Magazine</masterLabel>
            <translation><!-- Newspaper/Magazine --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>On Vehicle</masterLabel>
            <translation><!-- On Vehicle --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Other</masterLabel>
            <translation><!-- Other --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Partner</masterLabel>
            <translation><!-- Partner --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Public Relations</masterLabel>
            <translation><!-- Public Relations --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Search Engine</masterLabel>
            <translation><!-- Search Engine --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Seminar - Internal</masterLabel>
            <translation><!-- Seminar - Internal --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Seminar - Partner</masterLabel>
            <translation><!-- Seminar - Partner --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Service Order</masterLabel>
            <translation><!-- Service Order --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Trade Show</masterLabel>
            <translation><!-- Trade Show --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Web</masterLabel>
            <translation><!-- Web --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Word of mouth</masterLabel>
            <translation><!-- Word of mouth --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Main Listed City --></label>
        <name>Main_Listed_City__c</name>
    </fields>
    <fields>
        <label><!-- Main Listed Phone --></label>
        <name>Main_Listed_Phone__c</name>
    </fields>
    <fields>
        <label><!-- Main Listed State --></label>
        <name>Main_Listed_State__c</name>
    </fields>
    <fields>
        <label><!-- Main Listed Street --></label>
        <name>Main_Listed_Street__c</name>
    </fields>
    <fields>
        <label><!-- Main Listed Zip Code --></label>
        <name>Main_Listed_Zip_Code__c</name>
    </fields>
    <fields>
        <label><!-- National Staging Order Set --></label>
        <name>National_Staging_Order_Set__c</name>
        <relationshipLabel><!-- Opportunities --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Opportunity Amount --></label>
        <name>Net_Total__c</name>
    </fields>
    <fields>
        <label><!-- NotProcessed --></label>
        <name>NotProcessed__c</name>
    </fields>
    <fields>
        <label><!-- Number of Cancelled Opportunity Products --></label>
        <name>Number_of_Cancelled_Opportunity_Products__c</name>
    </fields>
    <fields>
        <label><!-- Number of Opportunity Products --></label>
        <name>Number_of_Opportunity_Products__c</name>
    </fields>
    <fields>
        <label><!-- OppLI Sell Price --></label>
        <name>OppLI_Sell_Price__c</name>
    </fields>
    <fields>
        <label><!-- Opportunity Owner Email --></label>
        <name>Opportunity_Owner_Email__c</name>
    </fields>
    <fields>
        <label><!-- Opportunity Owner Phone --></label>
        <name>Opportunity_Owner_Phone__c</name>
    </fields>
    <fields>
        <help><!-- Revenue Assurance may check this to override delinquency status --></help>
        <label><!-- Delinquency Override --></label>
        <name>Override__c</name>
    </fields>
    <fields>
        <label><!-- Price Per Lead --></label>
        <name>P4P_Price_Quote__c</name>
    </fields>
    <fields>
        <help><!-- Payment In Advance --></help>
        <label><!-- PIA --></label>
        <name>PIA__c</name>
    </fields>
    <fields>
        <label><!-- Payment Method --></label>
        <name>Payment_Method__c</name>
        <picklistValues>
            <masterLabel>ACH</masterLabel>
            <translation><!-- ACH --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Credit Card</masterLabel>
            <translation><!-- Credit Card --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Statement</masterLabel>
            <translation><!-- Statement --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Telco Billing</masterLabel>
            <translation><!-- Telco Billing --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Prepayment Required --></label>
        <name>Prepayment_Required__c</name>
    </fields>
    <fields>
        <label><!-- RAS Date --></label>
        <name>RAS_Date__c</name>
    </fields>
    <fields>
        <label><!-- RAS ID --></label>
        <name>RAS_ID__c</name>
    </fields>
    <fields>
        <help><!-- If this is checked, the Opportunity is a Late Print Order and requires Approval --></help>
        <label><!-- Requires Late Print Order Approval --></label>
        <name>Requires_Late_Print_Order_Approval__c</name>
    </fields>
    <fields>
        <help><!-- Used to determine if Informatica needs to send a Cancellation Letter. DO NOT DELETE --></help>
        <label><!-- Send Cancellation Letter --></label>
        <name>Send_Cancellation_Letter__c</name>
    </fields>
    <fields>
        <label><!-- Signing Contact&apos;s Email --></label>
        <name>Sig_Contact_Email__c</name>
    </fields>
    <fields>
        <label><!-- Signing Contact&apos;s First Name --></label>
        <name>Sig_Contact_First__c</name>
    </fields>
    <fields>
        <label><!-- Signing Contact&apos;s Name --></label>
        <name>Sig_Contact_FullName__c</name>
    </fields>
    <fields>
        <label><!-- Signing Contact&apos;s Last Name --></label>
        <name>Sig_Contact_Last__c</name>
    </fields>
    <fields>
        <label><!-- Signing Contact&apos;s Title --></label>
        <name>Sig_Contact_Title__c</name>
    </fields>
    <fields>
        <label><!-- Signing Contact --></label>
        <name>Signing_Contact__c</name>
        <relationshipLabel><!-- Opportunities (Contact) --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Signing Method --></label>
        <name>Signing_Method__c</name>
        <picklistValues>
            <masterLabel>DocuSign</masterLabel>
            <translation><!-- DocuSign --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>RAS</masterLabel>
            <translation><!-- RAS --></translation>
        </picklistValues>
    </fields>
    <fields>
        <name>StageName</name>
        <picklistValues>
            <masterLabel>Attempting to contact</masterLabel>
            <translation><!-- Attempting to contact --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Auto Renewal</masterLabel>
            <translation><!-- Auto Renewal --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Closed Decrease</masterLabel>
            <translation><!-- Closed Decrease --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Closed Lost</masterLabel>
            <translation><!-- Closed Lost --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Closed Migrated</masterLabel>
            <translation><!-- Closed Migrated --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Closed UTC</masterLabel>
            <translation><!-- Closed UTC --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Closed Won</masterLabel>
            <translation><!-- Closed Won --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Contacted</masterLabel>
            <translation><!-- Contacted --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Forecasted Net</masterLabel>
            <translation><!-- Forecasted Net --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Id. Decision Makers</masterLabel>
            <translation><!-- Id. Decision Makers --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>National Quote</masterLabel>
            <translation><!-- National Quote --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Needs Analysis</masterLabel>
            <translation><!-- Needs Analysis --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Negotiation/Review</masterLabel>
            <translation><!-- Negotiation/Review --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>No Contact</masterLabel>
            <translation><!-- No Contact --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Perception Analysis</masterLabel>
            <translation><!-- Perception Analysis --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Proposal Created</masterLabel>
            <translation><!-- Proposal Created --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Proposal/Price Quote</masterLabel>
            <translation><!-- Proposal/Price Quote --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Prospecting</masterLabel>
            <translation><!-- Prospecting --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Qualification</masterLabel>
            <translation><!-- Qualification --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Signature Pending</masterLabel>
            <translation><!-- Signature Pending --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Value Proposition</masterLabel>
            <translation><!-- Value Proposition --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Telco --></label>
        <name>Telco__c</name>
    </fields>
    <fields>
        <label><!-- TmpOppty Total Price --></label>
        <name>TmpOppty_Total_Price__c</name>
    </fields>
    <fields>
        <label><!-- Total Single Payment Amount --></label>
        <name>TotalSinglePaymentAmount__c</name>
    </fields>
    <fields>
        <label><!-- Transaction Unique ID --></label>
        <name>Transaction_Unique_ID__c</name>
    </fields>
    <fields>
        <name>Type</name>
        <picklistValues>
            <masterLabel>Existing Business</masterLabel>
            <translation><!-- Existing Business --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>New Business</masterLabel>
            <translation><!-- New Business --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Website --></label>
        <name>Website__c</name>
    </fields>
    <fields>
        <label><!-- billing defaults set --></label>
        <name>billing_defaults_set__c</name>
    </fields>
    <fields>
        <label><!-- Unit of Work --></label>
        <name>c2g__CODAUnitOfWork__c</name>
    </fields>
    <fields>
        <label><!-- cmr Number --></label>
        <name>cmr_Number__c</name>
    </fields>
    <fields>
        <label><!-- isLocked --></label>
        <name>isLocked__c</name>
    </fields>
    <fields>
        <label><!-- modifyid --></label>
        <name>modifyid__c</name>
    </fields>
    <fields>
        <help><!-- Total Amount - Payments Made --></help>
        <label><!-- Balance --></label>
        <name>pymt__Balance__c</name>
    </fields>
    <fields>
        <help><!-- PaymentConnect currency code. --></help>
        <label><!-- Currency ISO Code --></label>
        <name>pymt__Currency_ISO_Code__c</name>
        <picklistValues>
            <masterLabel>AUD</masterLabel>
            <translation><!-- AUD --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>CAD</masterLabel>
            <translation><!-- CAD --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>CHF</masterLabel>
            <translation><!-- CHF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>CZK</masterLabel>
            <translation><!-- CZK --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>DKK</masterLabel>
            <translation><!-- DKK --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>EUR</masterLabel>
            <translation><!-- EUR --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>GBP</masterLabel>
            <translation><!-- GBP --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>HKD</masterLabel>
            <translation><!-- HKD --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>HUF</masterLabel>
            <translation><!-- HUF --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>ILS</masterLabel>
            <translation><!-- ILS --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>JPY</masterLabel>
            <translation><!-- JPY --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>MXN</masterLabel>
            <translation><!-- MXN --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>NOK</masterLabel>
            <translation><!-- NOK --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>NZD</masterLabel>
            <translation><!-- NZD --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>PLN</masterLabel>
            <translation><!-- PLN --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>SEK</masterLabel>
            <translation><!-- SEK --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>SGD</masterLabel>
            <translation><!-- SGD --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>USD</masterLabel>
            <translation><!-- USD --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Recurring payments frequency --></help>
        <label><!-- Frequency --></label>
        <name>pymt__Frequency__c</name>
    </fields>
    <fields>
        <label><!-- Invoice Number --></label>
        <name>pymt__Invoice_Number__c</name>
    </fields>
    <fields>
        <help><!-- Total number of settled or completed payments related to this opportunity. Updated automatically when a payment record is added, updated or removed. --></help>
        <label><!-- Number of Payments Made --></label>
        <name>pymt__Number_of_Payments_Made__c</name>
    </fields>
    <fields>
        <help><!-- Number of recurring payment occurrences. --></help>
        <label><!-- Occurrences --></label>
        <name>pymt__Occurrences__c</name>
    </fields>
    <fields>
        <help><!-- Purchase order number.  Used when generating PaymentConnect invoices. --></help>
        <label><!-- PO Number --></label>
        <name>pymt__PO_Number__c</name>
    </fields>
    <fields>
        <help><!-- Checked by PaymentConnect when payments equal or exceed the opportunity amount (if &gt;0). --></help>
        <label><!-- Paid Off --></label>
        <name>pymt__Paid_Off__c</name>
    </fields>
    <fields>
        <help><!-- Total of settled payments related to this opportunity. This field is updated automatically when a payment record is added, updated or removed. --></help>
        <label><!-- Payments Made --></label>
        <name>pymt__Payments_Made__c</name>
    </fields>
    <fields>
        <help><!-- Recurring payments period. --></help>
        <label><!-- Period --></label>
        <name>pymt__Period__c</name>
        <picklistValues>
            <masterLabel>Day</masterLabel>
            <translation><!-- Day --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Month</masterLabel>
            <translation><!-- Month --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>SemiMonth</masterLabel>
            <translation><!-- SemiMonth --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Week</masterLabel>
            <translation><!-- Week --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Year</masterLabel>
            <translation><!-- Year --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Recurring Amount --></label>
        <name>pymt__Recurring_Amount__c</name>
    </fields>
    <fields>
        <help><!-- PaymentConnect Shipping Amount --></help>
        <label><!-- Shipping --></label>
        <name>pymt__Shipping__c</name>
    </fields>
    <fields>
        <help><!-- Date that the web-accessible quote on this opportunity expires.  Leave blank to disable the site quote entirely. --></help>
        <label><!-- SiteQuote Expiration --></label>
        <name>pymt__SiteQuote_Expiration__c</name>
    </fields>
    <fields>
        <help><!-- Controls the way recurring payments are set up on the SiteQuote page. --></help>
        <label><!-- Sites Recurring Payment Setup --></label>
        <name>pymt__SiteQuote_Recurring_Setup__c</name>
        <picklistValues>
            <masterLabel>Recurring Payments = Total Amount</masterLabel>
            <translation><!-- Recurring Payments = Total Amount --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Total Amount + Recurring Payments</masterLabel>
            <translation><!-- Total Amount + Recurring Payments --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Total Amount Includes First Recurring Payment</masterLabel>
            <translation><!-- Total Amount Includes First Recurring Payment --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Database name of Sites website or community portal to publish quote or invoice to (optional).  If listing multiple site names separate with a comma. --></help>
        <label><!-- Site Name --></label>
        <name>pymt__Site_Name__c</name>
    </fields>
    <fields>
        <help><!-- Date to start recurring or subscription payments --></help>
        <label><!-- Subscription Start Date --></label>
        <name>pymt__Subscription_Start_Date__c</name>
    </fields>
    <fields>
        <help><!-- PaymentConnect tax amount --></help>
        <label><!-- Tax --></label>
        <name>pymt__Tax__c</name>
    </fields>
    <fields>
        <help><!-- PaymentConnect total calculated amount (Opportunity amount + shipping + tax) --></help>
        <label><!-- Total Amount --></label>
        <name>pymt__Total_Amount__c</name>
    </fields>
    <layouts>
        <layout>CS Opportunity Layout</layout>
        <sections>
            <label><!-- Billing Information --></label>
            <section>Billing Information</section>
        </sections>
        <sections>
            <label><!-- Main Listed Phone and Address --></label>
            <section>Main Listed Phone and Address</section>
        </sections>
        <sections>
            <label><!-- Signing Information --></label>
            <section>Signing Information</section>
        </sections>
    </layouts>
    <layouts>
        <layout>CS Opportunity Locked Layout</layout>
        <sections>
            <label><!-- Billing Information --></label>
            <section>Billing Information</section>
        </sections>
        <sections>
            <label><!-- Late Print Order Information --></label>
            <section>Late Print Order Information</section>
        </sections>
        <sections>
            <label><!-- Main Listed Phone and Address --></label>
            <section>Main Listed Phone and Address</section>
        </sections>
        <sections>
            <label><!-- Signing Information --></label>
            <section>Signing Information</section>
        </sections>
    </layouts>
    <layouts>
        <layout>National Layout</layout>
        <sections>
            <label><!-- Billing Information --></label>
            <section>Billing Information</section>
        </sections>
        <sections>
            <label><!-- Signing Method --></label>
            <section>Signing Method</section>
        </sections>
    </layouts>
    <layouts>
        <layout>Opportunity Layout</layout>
        <sections>
            <label><!-- Billing Information --></label>
            <section>Billing Information</section>
        </sections>
        <sections>
            <label><!-- Late Print Order Information --></label>
            <section>Late Print Order Information</section>
        </sections>
        <sections>
            <label><!-- Main Listed Phone and Address --></label>
            <section>Main Listed Phone and Address</section>
        </sections>
        <sections>
            <label><!-- Pending Item Information to Create Order --></label>
            <section>Pending Item Information to Create Order</section>
        </sections>
        <sections>
            <label><!-- Signing Method --></label>
            <section>Signing Method</section>
        </sections>
    </layouts>
    <layouts>
        <layout>Opportunity Locked Layout</layout>
        <sections>
            <label><!-- Billing Information --></label>
            <section>Billing Information</section>
        </sections>
        <sections>
            <label><!-- Late Print Order Information --></label>
            <section>Late Print Order Information</section>
        </sections>
        <sections>
            <label><!-- Main Listed Phone and Address --></label>
            <section>Main Listed Phone and Address</section>
        </sections>
        <sections>
            <label><!-- Signing Information --></label>
            <section>Signing Information</section>
        </sections>
    </layouts>
    <layouts>
        <layout>RA Opportunity Locked Layout</layout>
        <sections>
            <label><!-- Billing Information --></label>
            <section>Billing Information</section>
        </sections>
        <sections>
            <label><!-- Main Listed Phone and Address --></label>
            <section>Main Listed Phone and Address</section>
        </sections>
        <sections>
            <label><!-- Signing Information --></label>
            <section>Signing Information</section>
        </sections>
    </layouts>
    <layouts>
        <layout>Win Opportunity Layout</layout>
        <sections>
            <label><!-- Billing Information --></label>
            <section>Billing Information</section>
        </sections>
        <sections>
            <label><!-- Signing Information --></label>
            <section>Signing Information</section>
        </sections>
    </layouts>
    <quickActions>
        <label><!-- DocuSign --></label>
        <name>DocuSignCustom</name>
    </quickActions>
    <recordTypes>
        <label><!-- National Opportunity --></label>
        <name>National_Opportunity</name>
    </recordTypes>
    <recordTypes>
        <label><!-- Opportunity Ready For Order --></label>
        <name>Opportunity_Assigned</name>
    </recordTypes>
    <recordTypes>
        <label><!-- Opportunity New --></label>
        <name>Opportunity_New</name>
    </recordTypes>
    <recordTypes>
        <label><!-- Opportunity Won --></label>
        <name>Opportunity_Won</name>
    </recordTypes>
    <validationRules>
        <errorMessage><!-- Account manager does&apos;t match with account value --></errorMessage>
        <name>Accountmanager_match_with_account_object</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Please select &quot;Berry&quot; as Billing Partner if &quot;Statement&quot; Payment Method is selected --></errorMessage>
        <name>Billing_Partner_Berry_If_Method_Statment</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Opportunity Canvass cannot be updated once a Quote has been created or if line items were brought in for renewal or modification. --></errorMessage>
        <name>Canvas_Do_Not_Edit_After_Quote_Created</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- If opportunity stage is closed lost, you must add a Closed Lost Reason. --></errorMessage>
        <name>Closed_Lost_Reason</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- You cannot mark this Opportunity as Requires Late Print Order Approval without providing Late Print Order Description notes. --></errorMessage>
        <name>Late_Print_Order_Requires_Description</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- The Payment Method for Windstream Communications Billing Partner must be Credit Card. --></errorMessage>
        <name>Pymt_Mthd_is_CC_IF_BP_is_Windstream</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- You do not have permissions to change the owner of the opportunity.  Click Cancel and contact your System Administrator or Manager to make this change. --></errorMessage>
        <name>Restrict_Opportunity_Owner_Change</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- You Must specify a Telco to be billed Not berry --></errorMessage>
        <name>TELCO_BILLING_MUST_NOT_BE_Berry</name>
    </validationRules>
    <webLinks>
        <label><!-- BLND_FormFactory_Opportunity_Link --></label>
        <name>BLND_FormFactory_Opportunity_Link</name>
    </webLinks>
    <webLinks>
        <label><!-- BSure_Credit_Application --></label>
        <name>BSure_Credit_Application</name>
    </webLinks>
    <webLinks>
        <label><!-- Create_Credit_Application --></label>
        <name>Create_Credit_Application</name>
    </webLinks>
    <webLinks>
        <label><!-- Credit_Application --></label>
        <name>Credit_Application</name>
    </webLinks>
    <webLinks>
        <label><!-- Estimate_Incentives_Prod --></label>
        <name>Estimate_Incentives_Prod</name>
    </webLinks>
    <webLinks>
        <label><!-- In_Person_DocuSigncustom --></label>
        <name>In_Person_DocuSigncustom</name>
    </webLinks>
    <webLinks>
        <label><!-- Inventory_Tracking --></label>
        <name>Inventory_Tracking</name>
    </webLinks>
    <webLinks>
        <label><!-- Send_with_DocuSigncustom --></label>
        <name>Send_with_DocuSigncustom</name>
    </webLinks>
    <webLinks>
        <label><!-- Signing_Contact --></label>
        <name>Signing_Contact</name>
    </webLinks>
    <webLinks>
        <label><!-- UpdateBillingPartner --></label>
        <name>UpdateBillingPartner</name>
    </webLinks>
    <webLinks>
        <label><!-- Validate_Opportunity --></label>
        <name>Validate_Opportunity</name>
    </webLinks>
    <webLinks>
        <label><!-- Win_Opportunity_Create_Order --></label>
        <name>Win_Opportunity_Create_Order</name>
    </webLinks>
    <webLinks>
        <label><!-- Estimate_Incentives_Prod --></label>
        <name>XactlyCorp__Estimate_Incentives_Prod</name>
    </webLinks>
    <webLinks>
        <label><!-- CreateInvoice --></label>
        <name>c2g__CreateInvoice</name>
    </webLinks>
    <webLinks>
        <label><!-- Send_with_DocuSign --></label>
        <name>dsfs__Send_with_DocuSign</name>
    </webLinks>
    <webLinks>
        <label><!-- AuthNet_Recurring_Billing --></label>
        <name>pymt__AuthNet_Recurring_Billing</name>
    </webLinks>
    <webLinks>
        <label><!-- Calculate_Tax --></label>
        <name>pymt__Calculate_Tax</name>
    </webLinks>
    <webLinks>
        <label><!-- PayPal_Recurring_Billing --></label>
        <name>pymt__PayPal_Recurring_Billing</name>
    </webLinks>
    <webLinks>
        <label><!-- Payment_Terminal --></label>
        <name>pymt__Payment_Terminal</name>
    </webLinks>
    <webLinks>
        <label><!-- Schedule_Payments --></label>
        <name>pymt__Schedule_Payments</name>
    </webLinks>
    <workflowTasks>
        <description><!-- Your Opportunity&apos;s Close Date has an expired date.  Please review the Opportunity and change the Close Date to a future date or change the Opportunity Stage to Closed Lost. --></description>
        <name>Open_Expired_Opportunity_Please_close</name>
        <subject><!-- Open Expired Opportunity: Please close --></subject>
    </workflowTasks>
    <workflowTasks>
        <description><!-- Please review this expired quote.  If it is still being worked, please update the close date. If this Opportunity is dead, please change the stage to Closed - Lost. --></description>
        <name>Open_Expired_Opportunity_Please_close_Account_Owner</name>
        <subject><!-- Open Expired Opportunity: Please close --></subject>
    </workflowTasks>
    <workflowTasks>
        <description><!-- The close date that has expired on this Opportunity. Please update this opportunity with a new close date if you are continuing to work this opportunity or if the deal is considered dead, please change the Opportunity stage to Closed - Lost. --></description>
        <name>X2nd_Notice_Opportunity_Expired_Date</name>
        <subject><!-- 2nd Notice: Opportunity with Expired Close Date --></subject>
    </workflowTasks>
</CustomObjectTranslation>
