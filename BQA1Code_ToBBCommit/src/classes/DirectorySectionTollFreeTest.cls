@isTest(seeAllData=True)
public class DirectorySectionTollFreeTest{
    static testmethod void DirectorySectionTollFreeTest(){
    test.starttest();
  /* Directory__c objDir = new Directory__c();
        objDir.Name = 'Wind Telco Test';
        objDir.Directory_Code__c = '103342';
        insert objDir;*/
        
     Directory__c objDir= TestMethodsUtility.createDirectory();
        
    Directory_Edition__c objDE = TestMethodsUtility.generateDirectoryEdition(objDir);
    objDE.Book_Status__c = 'NI';
    insert objDE;
    
    Account objAcc = TestMethodsUtility.generateTelcoAccount();
    Telco__c objTelco = TestMethodsUtility.createTelco(objAcc.id);
    
    Directory_Section__c objDirSec = new Directory_Section__c();
        objDirSec.Directory__c = objDir.Id;
        objDirSec.Name = 'Test Dir Section';
        objDirSec.Section_Page_Type__c = 'WP';
        objDirSec.Section_Code__c = '103342';
        insert objDirSec;      
    objDir.Toll_Free_Phrase__c = 'Test';
    objDir.Telco_Provider__c = objTelco.id;
    update objDir;
    
    map<Id,Directory__c> oldMap = new map<Id,Directory__c>();
    oldMap.put(objDir.Id,objDir);
    list<Directory__c> dirLst = new list<Directory__c>();
    dirLst.add(objDir);
    DirectorySectionTollFree.updateDirsecTollfree(dirLst,oldMap);
    
    
    test.stoptest();
    }
}