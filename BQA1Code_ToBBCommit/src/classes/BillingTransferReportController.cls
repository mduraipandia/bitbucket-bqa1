public class BillingTransferReportController 
{
    private map<Date,List<c2g__codaInvoice__c>> mapForSIN=new map<Date,List<c2g__codaInvoice__c>>(); 
    private map<Date,List<c2g__codaCreditNote__c>> mapForSCN=new map<Date,List<c2g__codaCreditNote__c>>();
    public List<SalesInvoiceWrapper> SalesInvoiceWrapperList{get;set;}
    public List<SalesCreditNoteWrapper> SalesCreditNoteWrapperList{get;set;}
    private Id accountid;
    public string ReportType{get;set;}    
    public BillingTransferReportController(ApexPages.StandardController controller) 
    {
            ReportType='text/html';
           // GetReportInfo();
    }
    public void GetReportInfo()
    {
        accountId = Apexpages.currentPage().getParameters().get('id');
        SalesInvoiceWrapperList=new List<SalesInvoiceWrapper>();
        SalesCreditNoteWrapperList=new List<SalesCreditNoteWrapper>();         
        for(Account iterator:[select id,Name,(select id,CreatedDate,c2g__InvoiceStatus__c, c2g__PaymentStatus__c,c2g__InvoiceDate__c, c2g__Opportunity__c, c2g__NetTotal__c, c2g__InvoiceTotal__c, c2g__Interval__c, c2g__Account__c, Order_Set__c,c2g__Period__c,c2g__DueDate__c ,c2g__InvoiceCurrency__c, c2g__PrintStatus__c,Name, Customer_Name__c from Sales_Invoices__r where (Transaction_Type__c='TD - Billing Transfer Invoice' OR Transaction_Type__c='FC - Frequency Change')),(select c2g__CreditNoteTotal__c,CreatedDate, Id, c2g__Invoice__c, c2g__NetTotal__c,c2g__Account__c, Name,Transaction_Type__c,c2g__CreditNoteDate__c,Case__c,c2g__OutstandingValue__c,c2g__CreditNoteStatus__c,c2g__PaymentStatus__c from Sales_Credit_Notes__r where (Transaction_Type__c='TD - Billing Transfer Invoice' OR Transaction_Type__c='FC - Frequency Change')) from Account where id=:accountID])
        {
            if(iterator.Sales_Invoices__r.size()>0)
            {
                for(c2g__codaInvoice__c invoiceIterator:iterator.Sales_Invoices__r)
                {
                    if(!mapForSIN.ContainsKey(invoiceIterator.CreatedDate.date()))
                    {
                        mapForSIN.put(invoiceIterator.CreatedDate.Date(),new List<c2g__codaInvoice__c>());
                    }
                    mapForSIN.get(invoiceIterator.CreatedDate.Date()).add(invoiceIterator);
                }
            }
            if(iterator.Sales_Credit_Notes__r.size()>0)
            {
                for(c2g__codaCreditNote__c SCNIterator:iterator.Sales_Credit_Notes__r)
                {
                    if(!mapForSCN.ContainsKey(SCNIterator.CreatedDate.Date()))
                    {
                        mapForSCN.put(SCNIterator.CreatedDate.Date(),new List<c2g__codaCreditNote__c>());
                    }
                    mapForSCN.get(SCNIterator.CreatedDate.Date()).add(SCNIterator);
                }               
            }           
        }
        
        if(mapForSIN.size()>0)
        {
            for(Date d :mapForSIN.keySet())
            {
                  SalesInvoiceWrapperList.add(new SalesInvoiceWrapper(d,mapForSIN.get(d)));              
            }
        }
        if(mapForSCN.size()>0)
        {
            for(Date d :mapForSCN.keySet())
            {
                  SalesCreditNoteWrapperList.add(new SalesCreditNoteWrapper(d,mapForSCN.get(d)));              
            }
        }                    
    }
    public void exportToExcel()
    {
       
        ReportType='application/x-excel#BillingTransferReport-'+Date.today()+'.xls';
        GetReportInfo();
    }
    
    public class SalesInvoiceWrapper
    {
        public Date SINCreatedDate{get;set;}
        public List<c2g__codaInvoice__c> SalesInvoiceList{get;set;}
        public SalesInvoiceWrapper(Date SINCreatedDate,List<c2g__codaInvoice__c> SalesInvoiceList)
        {
           this.SINCreatedDate=SINCreatedDate;
           this.SalesInvoiceList=SalesInvoiceList;
        }
    }
    public class SalesCreditNoteWrapper
    {
        public Date SCNCreatedDate{get;set;}
        public List<c2g__codaCreditNote__c> creditNoteList{get;set;}
        public SalesCreditNoteWrapper(Date SCNCreatedDate,List<c2g__codaCreditNote__c> creditNoteList)
        {
           this.SCNCreatedDate=SCNCreatedDate;
           this.creditNoteList=creditNoteList;
        }
    }    

}