global class localBillingSendToWFOneBatch implements Database.Batchable<sObject> {
	global Date dtSendTo =  null;
	global localBillingSendToWFOneBatch(Date dtSend) {
		this.dtSendTo = dtSend;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		String dtBD = String.valueOf(dtSendTo);		
		String strQuery = 'SELECT Id, Send_to_WF1__c FROM Statement_Header__c WHERE Send_to_WF1__c = false AND Statement_Date__c ='+ dtBD +'';
		system.debug(strQuery);
		return Database.getQueryLocator(strQuery);
	}
	
	global void execute(Database.BatchableContext BC, List<Statement_Header__c> lststmtHeader) {
		for(Statement_Header__c iterator : lststmtHeader) {
			iterator.Send_to_WF1__c = true;
		}
		update lststmtHeader;
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
}