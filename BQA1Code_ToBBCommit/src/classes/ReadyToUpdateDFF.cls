public with sharing class ReadyToUpdateDFF {

    //Getter and Setters
    public String dffId {
        get;
        set;
    }
    public String orderSetId {
        get;
        set;
    }
    public String mOrderSetId {
        get;
        set;
    }
    public String accountId {
        get;
        set;
    }
    public String fpId {
        get;
        set;
    }
    public String profileId {
        get;
        set;
    }
    public Integer fpCnt {
        get;
        set;
    }
    public boolean closeStatus {
        get;
        set;
    }
    public List < Digital_Product_Requirement__c > allDFFs {
        get;
        set;
    }
    public List < Digital_Product_Requirement__c > fnlLst {
        get;
        set;
    }
    public List < Digital_Product_Requirement__c > lstAllDFFs {
        get;
        set;
    }
    public Map < Id, Fulfillment_Profile__c > mapFProfile {
        get;
        set;
    }
    public Map < Id, Digital_Product_Requirement__c > UpdateDFFMap {
        get;
        set;
    }

    private Digital_Product_Requirement__c dff;
    public String apxMsg;

    public ReadyToUpdateDFF(ApexPages.StandardController controller) {
        if (!Test.isRunningTest()) {
            List < String > flds = new List < String > {
                'Id', 'Name', 'Test_PackageId__c', 'OrderLineItemID__r.Order_Group__c', 'ModificationOrderLineItem__r.Order_Group__c', 'Account__c', 'Fulfillment_Profile__c'
            };
            controller.addFields(flds);
        }
        this.dff = (Digital_Product_Requirement__c) Controller.getRecord();
        lstAllDFFs = new List < Digital_Product_Requirement__c > ();
        closeStatus = false;
    }

    public pageReference fpDffs() {
        dffId = dff.Id;
        accountId = dff.Account__c;
        fpId = dff.Fulfillment_Profile__c;

        List < DFF_RecordType_Field__c > lstDFFields = [select Name, DFF_Fields__c from DFF_RecordType_Field__c];
        Map < String, String > mapDFFFields = new Map < String, String > ();
        for (DFF_RecordType_Field__c objDFFField: lstDFFields) {
            mapDFFFields.put(objDFFField.name, objDFFField.DFF_Fields__c);
        }

        String strDFFFields;

        if (!Test.isRunningTest()) {
            orderSetId = dff.OrderLineItemID__r.Order_Group__c;
            mOrderSetId = dff.ModificationOrderLineItem__r.Order_Group__c;
        } else {
            Digital_Product_Requirement__c dprRcd = [SELECT Id, OrderLineItemID__r.Order_Group__c, ModificationOrderLineItem__r.Order_Group__c, Test_PackageId__c from Digital_Product_Requirement__c where id = : dffId];
            orderSetId = dprRcd.OrderLineItemID__r.Order_Group__c;
            mOrderSetId = dff.ModificationOrderLineItem__r.Order_Group__c;
        }

        try {
            if (String.isNotBlank(fpId)) {
                
                allDffs = CommonUtility.fpDffs(orderSetId, mOrderSetId, fpId);

                if (allDffs.size() > 0) {
                    
                    mapFProfile = CommonUtility.fpById(new Set < Id > { fpId });
                    
                    Fulfillment_Profile__c objFProfile = mapFProfile.get(fpId);
                    UpdateDFFMap = new Map < Id, Digital_Product_Requirement__c > ();

                    for (Digital_Product_Requirement__c iterator: allDffs) {
                        strDFFFields = mapDFFFields.get(iterator.DFF_RecordType_Name__c);
                        if (String.isNotBlank(strDFFFields)) {
                            Digital_Product_Requirement__c modifyDFF = cpyFpToDff(iterator, objFProfile, strDFFFields);
                            //System.debug('************DFF Updated************' + modifyDFF);
                            UpdateDFFMap.put(modifyDFF.id, modifyDFF);
                        }
                    }

                    if (UpdateDFFMap.size() > 0) {
                        apxMsg = 'Below DFFs are utilizing Fuflillment Profile: "' + objFProfile.Name + '" and may be impacted by this change. Please review & submit changes as needed. Note*: Any Migrated Dffs that has not yet completed the Data Migration process will not be impacted with this update............!';
                        CommonUtility.msgInfo(apxMsg);
                        apxMsg.remove(apxMsg);

                        lstAllDFFs.addall(UpdateDFFMap.values());
                        //System.debug('************INSIDE IF: allDFFs.size()************' + UpdateDFFMap.keyset());
                    } else {
                        CommonUtility.msgError(commonUtility.noDffsAssociated);
                    }
                }
            } else {
                CommonUtility.msgWarning(commonUtility.dffNotAssigned);
            }
        } catch (exception e) {
            CommonUtility.msgError(String.valueof(e.getMessage()));
        }
        return null;
    }

    public pageReference updateDffs() {
        if (lstAllDFFs.size() > 0) {
            String SptzBusinessUrl;
            List<Digital_Product_Requirement__c> lstSptzAddonDffs = new List<Digital_Product_Requirement__c>();
            for(Digital_Product_Requirement__c iterator: lstAllDFFs){
                if(String.isNotBlank(iterator.Bundle__c) && iterator.DFF_RecordType_Name__c == 'Spotzer Website'){
                    SptzBusinessUrl = iterator.business_url__c;
                    System.debug('************SpotzerBaseBusinessUrl************' + SptzBusinessUrl);
                } else if(String.isNotBlank(iterator.Bundle__c) && iterator.DFF_RecordType_Name__c != 'Spotzer Website'){
                    lstSptzAddonDffs.add(iterator);
                }
            }
            if(lstSptzAddonDffs.size()>0){
                for(Digital_Product_Requirement__c iterator: lstSptzAddonDffs){
                    if(iterator.business_url__c != SptzBusinessUrl){
                        iterator.Bundle__c = '';
                        System.debug('************SpotzerAddon: ' + iterator.UDAC__c + '************' + iterator.business_url__c);
                    }
                }
            }
            
            try {
                update lstAllDFFs;
                closeStatus = true;
                CommonUtility.msgInfo(CommonUtility.chngsApplied);
            } catch (exception e) {
                CommonUtility.msgError(String.valueof(e.getMessage()));
            }
        }
        return null;
    }

    public static Digital_Product_Requirement__c cpyFpToDff(Digital_Product_Requirement__c objDFF, Fulfillment_Profile__c objFProfile, String strDFFFields) {
        if (objDFF != null && objFProfile != null) {
            string[] strArrDFFFields = strDFFFields.split(',');
            for (String strField: strArrDFFFields) {
                if (objDFF.get(strField) != objFProfile.get(strField)) {
                    objDFF.put(strField, objFProfile.get(strField));
                    //System.debug('************Field From FP To DFFs************' + strField + '************Value************' + objDFF.get(strField));
                }
            }
        }
        return objDFF;
    }
}