/*
    * FinancialForce.com, inc. claims copyright in this software, its screen display designs and
    * supporting documentation. FinancialForce and FinancialForce.com are trademarks of FinancialForce.com, inc.
    * Any unauthorized use, copying or sale of the above may constitute an infringement of copyright and may
    * result in criminal or other legal proceedings.
    *
    * Copyright FinancialForce.com, inc. All rights reserved.
*/
public class CashMatchingAPIDefineAccountsBatch implements Database.Batchable<SObject>, Database.Stateful
{
    public Set<Id> accountIds = new Set<Id>();
    
    public Database.QueryLocator start(Database.BatchableContext BC) 
    {   
        String query = 
            'SELECT '+
            '    Id, '+
            '    ffps_bmatching__Account__c '+
            'FROM '+
            '    ffps_bmatching__Custom_Cash_Matching_History__c '+
            'WHERE '+
            '    ffps_bmatching__Completed__c = false LIMIT 1000';

        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<SObject> unTypedScope) 
    {
        for(SObject scopeItem : unTypedScope)
        {
            if((Id)scopeItem.get('ffps_bmatching__Account__c') != null)
                accountIds.add((Id)scopeItem.get('ffps_bmatching__Account__c'));
        }
    }
    
    public void finish(Database.BatchableContext BC) 
    {
        if(accountIds.size() > 0)
        {
            List<Id> accountIdList = new List<Id>();
            accountIdList.addAll(accountIds);
            
            CashMatchingAPIEnqueueByAccountBatch batch = new CashMatchingAPIEnqueueByAccountBatch();
            batch.accountIdsToProcess = accountIdList;
            
            Database.executeBatch(batch,1);
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses( new String[] { 'mfleming@financialforce.com' } );
            String body = 'Account Ids picked up ';
            for(Id accountId : accountIdList)
            {
                body += accountId + '\n';
            }
            mail.setPlainTextBody( body );
            mail.setSubject( 'Results of Account Staging batch' );
            
            Messaging.sendEmail( new Messaging.SingleEmailMessage[] { mail } );
        }
    }
}