global class MonthlyCronFMPWeeklyScheduler implements Schedulable {
   public Interface MonthlyCronFMPWeeklySchedulerInterface{
     void execute(SchedulableContext sc);
   }
   global void execute(SchedulableContext sc) {
    Type targetType = Type.forName('MonthlyCronFMPWeeklySchedulerHndlr');
        if(targetType != null) {
            MonthlyCronFMPWeeklySchedulerInterface obj = (MonthlyCronFMPWeeklySchedulerInterface)targetType.newInstance();
            obj.execute(sc);
        }
   
   }
}