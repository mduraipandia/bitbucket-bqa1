@isTest(SeeAllData=true)
public class TBChBTWrapperHandlerControllerTest{

    static testMethod void TBChBTWrapperHandlerControllerTestTestMethod(){
        
        Canvass__c c=TestMethodsUtility.createCanvass();
            Account objParentAcc=TestMethodsUtility.generateAccount();
            insert objParentAcc;
            Contact newParentContact = TestMethodsUtility.createContact(objParentAcc.Id);
            pymt__Payment_Method__c ParentPaymentMethod= new pymt__Payment_Method__c(pymt__Payment_Type__c='Credit Card',Name='Credit Card',pymt__Contact__c=newParentContact.Id);
            insert ParentPaymentMethod;
            
            list<Account> lstAccount1 = new list<Account>();
            lstAccount1.add(TestMethodsUtility.generateAccount('customer'));
            lstAccount1[0].Billing_Anniversary_Date__c=date.today();
            lstAccount1[0].ParentId=objParentAcc.Id;
            insert lstAccount1;  
            Account newAccount1 = new Account();
            for(Account iterator : lstAccount1) {
                if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount1 = iterator;
                }
            
            }
            
            ffps_bmatching__Account_Rollup_Staging_Table__c ARST=new ffps_bmatching__Account_Rollup_Staging_Table__c();
            ARST.ffps_bmatching__Account__c=newAccount1.Id;
            ARST.ffps_bmatching__Current_Billing_Amount__c=2000;
            ARST.ffps_bmatching__Total_Past_Due_Amount__c=200;
            ARST.ffps_bmatching__Total_Past_Due_Checkbox__c=true;
            ARST.ffps_bmatching__Current_Billing_Checkbox__c=true;
            ARST.ffps_bmatching__Credit_on_Account_Amount__c=100;
            ARST.ffps_bmatching__Credit_on_Account_Checkbox__c=true;
            insert ARST;
            
            Telco__c objTelco1 =TestMethodsUtility.createTelco(newAccount1.Id);
            objTelco1.Telco_Code__c = 'Test';
            update objTelco1;
            system.assertNotEquals(newAccount1.ID, null);
            Contact newContact1 = TestMethodsUtility.createContact(newAccount1.Id);
            
            pymt__Payment_Method__c PaymentMethod= new pymt__Payment_Method__c(pymt__Payment_Type__c='Credit Card',Name='Credit Card',pymt__Contact__c=newContact1.Id);
            insert PaymentMethod;
        
            Pricebook2 newPriceBook1 = new Pricebook2(Id = System.Label.PricebookId);
        
            Division__c objDiv1 = TestMethodsUtility.createDivision();
        
            Directory__c objDir1 = TestMethodsUtility.generateDirectory();
            objDir1.Telco_Recives_Electronice_File__c=true;
            objDir1.Telco_Provider__c = objTelco1.Id;
            objDir1.Canvass__c = newAccount1.Primary_Canvass__c;        
            objDir1.Publication_Company__c = newAccount1.Id;
            objDir1.Division__c = objDiv1.Id;
            objDir1.Directory_Code__c = '100000';
            insert objDir1;
            
            Directory_Heading__c objDH1 = TestMethodsUtility.createDirectoryHeading();
            Directory_Section__c objDS1 = TestMethodsUtility.createDirectorySection(objDir1);
            Section_Heading_Mapping__c objSHM1 = TestMethodsUtility.generateSectionHeadingMapping(objDS1.Id, objDH1.Id);
            insert objSHM1;
            
            Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir1);
            objDirEd1.New_Print_Bill_Date__c=date.today();
            objDirEd1.Bill_Prep__c=date.parse('01/01/2013');
            objDirEd1.XML_Output_Total_Amount__c=100;
            objDirEd1.Pub_Date__c =Date.today().addMonths(1);
            objDirEd1.Sales_Lockout__c=Date.today().addDays(30);
            objDirEd1.EBD__c = system.today();
             objDirEd1.Ship_date__c = System.today().addDays(-1);
             objDirEd1.Sales_Start__c = System.today().addDays(-1);
             objDirEd1.Delivery_Start_Date__c = System.today().addDays(-1);
            insert objDirEd1;
            
            Directory_Edition__c objDirE1 = new Directory_Edition__c();
            objDirE1.Name = 'Test DirE1';
            objDirE1.Directory__c = objDir1.Id;
            objDirE1.Letter_Renewal_Stage_1__c = system.today();
            objDirE1.book_status__c='NI';
            objDirE1.Pub_Date__c =Date.today().addMonths(2);
            objDirE1.Sales_Lockout__c=Date.today().addDays(30);
            objDirE1.EBD__c = system.today();
             objDirE1.Ship_date__c = System.today().addDays(-1);
             objDirE1.Sales_Start__c = System.today().addDays(-1);
             objDirE1.Delivery_Start_Date__c = System.today().addDays(-1);
            insert objDirE1;
            
            Directory_Edition__c objDirE2 = new Directory_Edition__c();
            objDirE2.Name = 'Test DirE2';
            objDirE2.Directory__c = objDir1.Id;
            objDirE2.Letter_Renewal_Stage_1__c = system.today();
            objDirE2.book_status__c='BOTS';
            objDirE2.Pub_Date__c =Date.today().addMonths(3);
            objDirE2.Sales_Lockout__c=Date.today().addDays(30);
            objDirE2.EBD__c = system.today();
             objDirE2.Ship_date__c = System.today().addDays(-1);
             objDirE2.Sales_Start__c = System.today().addDays(-1);
             objDirE2.Delivery_Start_Date__c = System.today().addDays(-1);
            insert objDirE2;     
            
            Directory__c objDirNew1 = TestMethodsUtility.generateDirectory();
            objDirNew1.Canvass__c = newAccount1.Primary_Canvass__c;        
            objDirNew1.Publication_Company__c = newAccount1.Id;
            objDirNew1.Division__c = objDiv1.Id;
            objDirNew1.Directory_Code__c = '100001';
            insert objDirNew1;
            Directory_Edition__c objDirEdNew2 = TestMethodsUtility.generateDirectoryEdition(objDirNew1);
            objDirEdNew2.XML_Output_Total_Amount__c=200;
            objDirEdNew2.Pub_Date__c =Date.today().addMonths(4);
            objDirEdNew2.Book_Status__c='NI';
            objDirEdNew2.Sales_Lockout__c=Date.today().addDays(30);
            objDirEdNew2.EBD__c = system.today();
             objDirEdNew2.Ship_date__c = System.today().addDays(-1);
             objDirEdNew2.Sales_Start__c = System.today().addDays(-1);
             objDirEdNew2.Delivery_Start_Date__c = System.today().addDays(-1);
            insert objDirEdNew2;
            
            Product2 newProduct3 = TestMethodsUtility.generateproduct();
            newProduct3.Family = 'Print';
            insert newProduct3;
            
            Product2 objProd3 = new Product2();
            objProd3.Name = 'Test';
            objProd3.Product_Type__c = 'Print';
            objProd3.ProductCode = 'WLCSH';
            objProd3.Print_Product_Type__c='Display';
            insert  objProd3;
            
            Product2 objProd4 = new Product2();
            objProd4.Name = 'Test';
            objProd4.Product_Type__c = 'Print';
            objProd4.ProductCode = 'GC50';
            objProd4.Print_Product_Type__c='Specialty';          
            insert objProd4;
            
            Opportunity newOpportunity2 = TestMethodsUtility.generateOpportunity('new');
            newOpportunity2.AccountId = newAccount1.Id;
            newOpportunity2.Pricebook2Id = newPriceBook1.Id;
            insert newOpportunity2;
            
            Order__c newOrder1 = TestMethodsUtility.createOrder(newAccount1.Id);
            
            Order_Group__c newOrderSet1 = TestMethodsUtility.createOrderSet(newAccount1, newOrder1, newOpportunity2);
            
            c2g__codaGeneralLedgerAccount__c gla=new c2g__codaGeneralLedgerAccount__c();
            gla.Name='Test Account';
            gla.c2g__BalanceSheet1__c='Balance Sheet';
            gla.c2g__Type__c='Balance Sheet';
            gla.c2g__UnitOfWork__c=2000.0;
            gla.c2g__ReportingCode__c='0909098988000';
            insert gla;
            
            c2g__codaCompany__c companyObj =TestMethodsUtility.generateCompany();
           
            c2g__codaAccountingCurrency__c accCurr=[SELECT Id from c2g__codaAccountingCurrency__c WHERE name='USD' limit 1]; 
           
            c2g__codaBankAccount__c bankacc=new c2g__codaBankAccount__c();
            bankacc.Name='Test Account';
            bankacc.c2g__BankAccountCurrency__c=accCurr.Id;
            bankacc.c2g__BankChargesGLA__c=gla.Id;
            bankacc.c2g__BankName__c='HSBC';
            bankacc.c2g__OwnerCompany__c=companyObj.Id;
            bankacc.c2g__GeneralLedgerAccount__c=gla.Id;
            bankacc.c2g__UnitOfWork__c=2000;
            bankacc.c2g__AccountName__c='Test' ;
            bankacc.c2g__AccountNumber__c='Test2011';
            bankacc.c2g__ReportingCode__c='0909098988000';
            insert bankacc;
            
            Test.StartTest();
            
            Order_Line_Items__c objOrderLineItem1= new Order_Line_Items__c(Billing_Partner__c='TBC123',Account__c=newAccount1.Id, 
            Billing_Contact__c=newContact1.id, Opportunity__c=newOpportunity2.id, Order_Group__c=newOrderSet1.id,
            Order__c=newOrder1.id,Product2__c=objProd4.Id,is_p4p__c=true,media_type__c='Print',
            Directory_Edition__c = objDirEdNew2 .Id,Directory__c=objDirNew1.Id,canvass__c=c.id,UnitPrice__c=200,Payment_Duration__c=12,
            Package_ID__c='pkgid_12',Payments_Remaining__c=10,Successful_Payments__c=2,
            Billing_Frequency__c='single',Payment_Method__c='Credit Card',Talus_Go_Live_Date__c =date.today(),Package_ID_External__c='2015-14136__2');
            insert objOrderLineItem1;
            
            Order_Line_Items__c objOrderLineItem2 = new Order_Line_Items__c(Billing_Partner__c='TBC123',
            Account__c=newAccount1.Id, Billing_Contact__c=newContact1.id, Opportunity__c=newOpportunity2.id,isCanceled__c=False, 
            Order_Group__c=newOrderSet1.id,Order__c=newOrder1.id,Product2__c=objProd4.Id,is_p4p__c=true,media_type__c='Print',
            Directory_Edition__c = objDirEdNew2.Id,Directory__c=objDirNew1.Id,canvass__c=c.id,UnitPrice__c=200,Payment_Duration__c=12,
            Package_ID__c='pkgid_12',Payments_Remaining__c=11,Successful_Payments__c=1,
            Billing_Frequency__c='single',Payment_Method__c='Credit Card',Talus_Go_Live_Date__c =date.today(),Package_ID_External__c='2015-14136__2');
            insert objOrderLineItem2;
            
            c2g__codaInvoice__c invoice =TestMethodsUtility.generateSalesInvoice(newAccount1,newOpportunity2);
            invoice.Customer_Name__c=newAccount1.id;
            invoice.c2g__InvoiceStatus__c='In Progress';
            invoice.c2g__OwnerCompany__c = companyObj.id;
            invoice.c2g__Account__c= newAccount1.id;
            invoice.Is_Statement_Generated__c=false;   
            invoice.SI_Payment_Method__c='Credit Card';
            invoice.Telco_Reversal__c=false;   
            invoice.Service_Start_Date__c=Date.today().Addmonths(-3);   
            insert invoice;
            
            c2g__codaInvoice__c invoice1 =TestMethodsUtility.generateSalesInvoice(newAccount1,newOpportunity2);
            invoice1.Customer_Name__c=newAccount1.id;
            invoice1.c2g__InvoiceStatus__c='In Progress';
            invoice1.c2g__OwnerCompany__c = companyObj.id;
            invoice1.c2g__Account__c= newAccount1.id;
            invoice1.Is_Statement_Generated__c=false;   
            invoice1.SI_Payment_Method__c='Credit Card';
            invoice1.Telco_Reversal__c=false;   
            invoice1.Service_Start_Date__c=Date.today().Addmonths(-5); 
            invoice1.c2g__InvoiceDate__c=date.today().addMonths(-1);  
            insert invoice1;
            
            c2g__codaCreditNote__c SCNP4PSTMT=TestMethodsUtility.generateSalesCreditNote(invoice, newAccount1);
            SCNP4PSTMT.Transaction_Type__c='TD - Billing Transfer Invoice';
            SCNP4PSTMT.c2g__CreditNoteStatus__c='In Progress';
            SCNP4PSTMT.Is_Statement_Generated__c=false;
            SCNP4PSTMT.Customer_Name__c=newAccount1.id;
            SCNP4PSTMT.Telco_Reversal__c  =false;
            SCNP4PSTMT.Is_Manually_Created__c =false;
            insert SCNP4PSTMT;
            
            c2g__codaDimension1__c  dimension1=TestMethodsUtility.createDimension1(objDirNew1);
            c2g__codaDimension2__c  dimension2=TestMethodsUtility.createDimension2(objProd4);
            c2g__codaDimension4__c  dimension4=TestMethodsUtility.createDimension4();
            c2g__codaDimension3__c  dimension3_ss=TestMethodsUtility.createDimension3(objOrderLineItem1);
            c2g__codaInvoiceLineItem__c invoiceLiStmt1= new c2g__codaInvoiceLineItem__c(Order_Line_Item__c=objOrderLineItem1.id,Is_Manually_Created__c = false,OffSet__c=false,Billing_Frequency__c='Single Payment',c2g__Quantity__c = 1, c2g__UnitPrice__c = 100, c2g__Invoice__c = invoice.Id, c2g__Product__c = objProd4.Id,c2g__Dimension1__c=dimension1.id,c2g__Dimension2__c=dimension2.id,c2g__Dimension3__c= dimension3_ss.id,c2g__Dimension4__c=dimension4.id);
            insert invoiceLiStmt1;
            
            c2g__codaInvoiceLineItem__c invoiceLiStmt2= new c2g__codaInvoiceLineItem__c(Order_Line_Item__c=objOrderLineItem2.id,Is_Manually_Created__c = false,OffSet__c=false,Billing_Frequency__c='Single Payment',c2g__Quantity__c = 1, c2g__UnitPrice__c = 100, c2g__Invoice__c = invoice.Id, c2g__Product__c = objProd4.Id,c2g__Dimension1__c=dimension1.id,c2g__Dimension2__c=dimension2.id,c2g__Dimension3__c= dimension3_ss.id,c2g__Dimension4__c=dimension4.id);
            insert invoiceLiStmt2;
            
            c2g__codaInvoiceLineItem__c zerodollarinvoiceLiStmt= new c2g__codaInvoiceLineItem__c(Order_Line_Item__c=objOrderLineItem2.id,Is_Manually_Created__c = false,OffSet__c=false,Billing_Frequency__c='Single Payment',c2g__Quantity__c = 1, c2g__UnitPrice__c = 0, c2g__Invoice__c = invoice.Id, c2g__Product__c = objProd4.Id,c2g__Dimension1__c=dimension1.id,c2g__Dimension2__c=dimension2.id,c2g__Dimension3__c= dimension3_ss.id,c2g__Dimension4__c=dimension4.id);
            insert zerodollarinvoiceLiStmt;
            
            list<c2g__codaInvoiceLineItem__c> lstILI=new list<c2g__codaInvoiceLineItem__c>();
            lstILI.add(zerodollarinvoiceLiStmt);
           // TBCcBillingTransferCommonMethod.checkOfZeroLineItems(lstILI);
            
            list<c2g__codaInvoiceLineItem__c> lstILINew=new list<c2g__codaInvoiceLineItem__c>();
            lstILINew.add(invoiceLiStmt1);
            lstILINew.add(invoiceLiStmt2);
            
            TBCcBillingTransferCommonMethod.checkFFPeriodDate(Date.Today().adddays(-1));
            TBCcBillingTransferCommonMethod.checkFFPeriodDate(Date.Today().adddays(1));
            Test.StopTest();
            
            c2g__codaCreditNoteLineItem__c scliStmt=TestMethodsUtility.generateSalesCreditNoteLineItem(SCNP4PSTMT,objProd4);
            scliStmt.Sales_Invoice_Line_Item__c=invoiceLiStmt1.id;
            scliStmt.c2g__Dimension1__c=dimension1.id;
            scliStmt.c2g__Dimension2__c=dimension2.id;
            scliStmt.c2g__Dimension3__c= dimension3_ss.id;
            scliStmt.c2g__Dimension4__c=dimension4.id;
            scliStmt.order_line_item__c=objOrderLineItem1.id;
            scliStmt.Billing_Frequency__c='Monthly';
            insert scliStmt;
            
            c2g__codaCreditNoteLineItem__c scliStmt1=TestMethodsUtility.generateSalesCreditNoteLineItem(SCNP4PSTMT,objProd4);
            scliStmt1.Sales_Invoice_Line_Item__c=invoiceLiStmt2.id;
            scliStmt1.c2g__Dimension1__c=dimension1.id;
            scliStmt1.c2g__Dimension2__c=dimension2.id;
            scliStmt1.c2g__Dimension3__c= dimension3_ss.id;
            scliStmt1.c2g__Dimension4__c=dimension4.id;
            scliStmt.order_line_item__c=objOrderLineItem2.id;
            scliStmt1.Billing_Frequency__c='Monthly';
            insert scliStmt1;
        
        
            pymt__PaymentX__c newPaymentX = TestMethodsUtility.generatePaymentX(invoice, newOpportunity2);
            newPaymentX.pymt__Billing_Email__c = 'test@test.test';
            newPaymentX.pymt__Status__c = 'Declined';
            newPaymentX.pymt__Amount__c = 10.00;
            newPaymentX.pymt__Payment_Type__c = 'Credit Card';
            newPaymentX.pymt__Contact__c = newContact1.Id;
            newPaymentX.pymt__Card_Type__c = 'Visa';
            newPaymentX.pymt__Last_4_Digits__c = '9876';
            newPaymentX.pymt__Log__c = 'Description';
            insert newPaymentX;
            
            objOrderLineItem1.Billing_Partner__c='THE BERRY COMPANY';
            update objOrderLineItem1;
            
            objOrderLineItem2.Billing_Partner__c='THE BERRY COMPANY';
            update objOrderLineItem2;
            
            Billing_Transfer_Status__c BillingTransferStatusObjDigital= new Billing_Transfer_Status__c(
            BTS_Account__c=newAccount1.Id,
            BTS_Billing_Partner__c='berry',
            BTS_Canvass__c=c.Id,
            BTS_Go_Live__c='golive',
            BTS_Media_Type__c='digital',
            BTS_Mode_of_Transfer__c='future',
            BTS_Type_of_Transfer__c='BillingPartnerTransfer',
            BTS_Billing_Frequency__c='single', 
            BTS_Directory_Id__c=objDirNew1.Id,
            BTS_Directory_Edition_Id__c=objDirEdNew2.Id,
            BTS_Billing_Partner_Name__c='THE BERRY COMPANY',
            BTS_Billing_Partner_ID__c = newAccount1.Id,
            BTS_Migrated_OLI__c=false);
            
            insert BillingTransferStatusObjDigital;
            
            TBChBTWrapperHandlerController tbc=new TBChBTWrapperHandlerController();
            TBChBTWrapperHandlerController.DirectoryEditionWrapper deWrapper=new TBChBTWrapperHandlerController.DirectoryEditionWrapper(objDirNew1, objDirEdNew2.Name,objDirEdNew2.Id);
            
            
            TBChBTWrapperHandlerController.OrderLineItemWrapper iter=new TBChBTWrapperHandlerController.OrderLineItemWrapper(true, objOrderLineItem1);
            List <TBChBTWrapperHandlerController.OrderLineItemWrapper > WrapperLstOLI = new List < TBChBTWrapperHandlerController.OrderLineItemWrapper > ();
            WrapperLstOLI.add(iter);
            
            TBChBTWrapperHandlerController.OrderLineItemWrapper iter1=new TBChBTWrapperHandlerController.OrderLineItemWrapper(true, objOrderLineItem2);
            List <TBChBTWrapperHandlerController.OrderLineItemWrapper > WrapperLstOLI1 = new List < TBChBTWrapperHandlerController.OrderLineItemWrapper > ();
            WrapperLstOLI1.add(iter1);
            
            List<TBChBTWrapperHandlerController.OrderLineItemByDECanvassWrapper> OliWrapperListFinal=new List<TBChBTWrapperHandlerController.OrderLineItemByDECanvassWrapper>();
            
            TBChBTWrapperHandlerController.OrderLineItemByDECanvassWrapper iterVar=new TBChBTWrapperHandlerController.OrderLineItemByDECanvassWrapper(objDirEdNew2.Id,null,WrapperLstOLI1);
            OliWrapperListFinal.add(iterVar);
               
            List<order_line_items__c> lstOLI=new List<order_line_items__c >();
            lstOLI.add(objOrderLineItem1);
            lstOLI.add(objOrderLineItem2);
            
            TBChBTWrapperHandlerController.OrderLineItemByOrderSetWrapper iterOLIOSWrapper=new TBChBTWrapperHandlerController.OrderLineItemByOrderSetWrapper(newOrderSet1.Id,newOrderSet1.Name,WrapperLstOLI1);
            
            List<TBChBTWrapperHandlerController.SubSalesInvoiceWrapper> ListSubSIWrapper=new List<TBChBTWrapperHandlerController.SubSalesInvoiceWrapper>();
            TBChBTWrapperHandlerController.SubSalesInvoiceWrapper subInvoice=new TBChBTWrapperHandlerController.SubSalesInvoiceWrapper(double.valueof(100.00),'Single','Test',newAccount1.Id,newOpportunity2.Id,newPaymentX.Id,newPaymentX.Id,'Test', system.today(),system.today().addMOnths(2),double.valueof(100.00),'In Progress',invoice.Id,objOrderLineItem1,1,lstILINew,1,11,true);
            ListSubSIWrapper.add(subInvoice);
            TBChBTWrapperHandlerController.SubSalesInvoiceWrapper subInvoice1=new TBChBTWrapperHandlerController.SubSalesInvoiceWrapper(double.valueof(100.00),'Single','Test',newAccount1.Id,newOpportunity2.Id,newPaymentX.Id,newPaymentX.Id,'Test', system.today().adddays(-5),system.today().addMOnths(2),double.valueof(100.00),'In Progress',invoice.Id,objOrderLineItem1,1,lstILINew,1,11,true);
            ListSubSIWrapper.add(subInvoice1);
            TBChBTWrapperHandlerController.SubSalesInvoiceWrapper subInvoice2=new TBChBTWrapperHandlerController.SubSalesInvoiceWrapper(double.valueof(100.00),'Single','Test',newAccount1.Id,newOpportunity2.Id,newPaymentX.Id,newPaymentX.Id,'Test', system.today().adddays(5),system.today().addMOnths(2),double.valueof(100.00),'In Progress',invoice.Id,objOrderLineItem1,1,lstILINew,1,11,true);
            ListSubSIWrapper.add(subInvoice2);
            
            List<TBChBTWrapperHandlerController.SalesInvoiceWrapperForSort> listSIWrap=new List<TBChBTWrapperHandlerController.SalesInvoiceWrapperForSort>();
            TBChBTWrapperHandlerController.SalesInvoiceWrapperForSort SIWrap1=new TBChBTWrapperHandlerController.SalesInvoiceWrapperForSort(invoice);
            listSIWrap.add(SIWrap1);
            TBChBTWrapperHandlerController.SalesInvoiceWrapperForSort SIWrap2=new TBChBTWrapperHandlerController.SalesInvoiceWrapperForSort(invoice1);
            listSIWrap.add(SIWrap1);
            listSIWrap.sort();
            
            TBChBTWrapperHandlerController.SubSalesInvoiceWrapper subInvoice3=new TBChBTWrapperHandlerController.SubSalesInvoiceWrapper();
            
            TBChBTWrapperHandlerController.SalesInvoiceWrapper SIWrapper=new TBChBTWrapperHandlerController.SalesInvoiceWrapper(ListSubSIWrapper,'Test',objDirEdNew2.Id);
    
            TBChBTWrapperHandlerController.SalesInvoiceLineItemWrapper SILIWrapper= new TBChBTWrapperHandlerController.SalesInvoiceLineItemWrapper(true,invoiceLiStmt1);
            TBChBTWrapperHandlerController.SalesInvoiceLineItemWrapper SILIWrapper1= new TBChBTWrapperHandlerController.SalesInvoiceLineItemWrapper(true,invoiceLiStmt2,true);  
            
            List<TBChBTWrapperHandlerController.SalesCreditNoteCountWrapper> LstSCNWrapper=new List<TBChBTWrapperHandlerController.SalesCreditNoteCountWrapper>();
            TBChBTWrapperHandlerController.SalesCreditNoteCountWrapper SCNWrapper1=new TBChBTWrapperHandlerController.SalesCreditNoteCountWrapper('ABC',1);
            LstSCNWrapper.add(SCNWrapper1);
            TBChBTWrapperHandlerController.SalesCreditNoteCountWrapper SCNWrapper2=new TBChBTWrapperHandlerController.SalesCreditNoteCountWrapper('Test',1);
            LstSCNWrapper.add(SCNWrapper2);
            TBChBTWrapperHandlerController.SalesCreditNoteCountWrapper SCNWrapper3=new TBChBTWrapperHandlerController.SalesCreditNoteCountWrapper('Test123',1);
            LstSCNWrapper.add(SCNWrapper3);
            LstSCNWrapper.sort();
    }
    

}