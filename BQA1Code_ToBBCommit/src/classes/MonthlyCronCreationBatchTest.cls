@isTest
private class MonthlyCronCreationBatchTest {
	@testSetup static void setup() {
		TBC_Cron__c CleanUpCron = TestMethodsUtility.createTBCCron('CleanUpCron');
		TBC_Cron__c FMPCron = TestMethodsUtility.createTBCCron('FMPCron');
		TBC_Cron__c DigitalPrintCron = TestMethodsUtility.createTBCCron('DigitalPrintCron');		
		Account acct = TestMethodsUtility.createAccount('cmr');
        Account childAcct = TestMethodsUtility.generateAccount();
        childAcct.ParentId = acct.Id;
        insert childAcct;
        Contact cnt = TestMethodsUtility.createContact(acct.Id);
        Order__c ord = TestMethodsUtility.createOrder(acct.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(acct, cnt);
        insert oppty;
        Order_Group__c og = TestMethodsUtility.createOrderSet(acct, ord, oppty);
        Order_Line_Items__c oln = TestMethodsUtility.createOrderLineItem(acct, cnt, oppty, ord, og);
        Order_Line_Items__c oln1 = TestMethodsUtility.generateOrderLineItem(acct, cnt, oppty, ord, og);
        oln1.Order_Anniversary_Start_Date__c = System.today().addMonths(-1);
        oln1.Talus_Go_Live_Date__c = System.today();
        oln1.Service_Start_Date__c = System.today().addDays(-2);
        oln1.Service_End_Date__c = System.today().addMonths(1);
        insert oln1;
	}
	
	@isTest static void monthlyCronTest() {
		Test.startTest();		
        Datetime dt = Datetime.now().addMinutes(1);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        String jobId = System.schedule('TBCscMonthlyCronScheduler', CRON_EXP, new TBCscMonthlyCronScheduler());   
        Test.stopTest();
	}
	
	@isTest static void monthlyCronTest1() {
		Test.startTest();        
        MonthlyCronCreationBatch obj = new MonthlyCronCreationBatch(System.Today().addMonths(2), null, false, false);
        Database.executeBatch(obj); 
        Test.stopTest();
	}
	
	@isTest static void monthlyCronTest2() {
		Test.startTest();
        MonthlyCronCreationBatch obj = new MonthlyCronCreationBatch(System.Today().addMonths(2), null);
        Database.executeBatch(obj); 
        Test.stopTest();
	}
}