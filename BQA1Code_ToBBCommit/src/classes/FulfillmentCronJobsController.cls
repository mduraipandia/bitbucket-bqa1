public with sharing class FulfillmentCronJobsController {

    //Constructor
    public FulfillmentCronJobsController() {

    }

    public pageReference ypcBatch() {
        try {
            String query = 'SELECT Id, Name, udac__c, udac_points__c, add_ons__c, vid_add_on__c, coupon_description__c, coupon_url__c, coupon_text_offer_1__c, coupon_text_offer_2__c, coupon_text_offer_3__c, cslt_url__c, cslt_text__c, recordType.DeveloperName, Account__r.TalusAccountId__c, Talus_Subscription_Id__c, Talus_DFF_Id__c, OrderLineItemID__r.Effective_Date__c, (SELECT Id, Name, DFF__c, DFF_Id__c, Validation__c, Effective_Date__c, Action_Type__c, coupon_description__c, coupon_url__c, cslt_text__c, cslt_url__c, coupon_text_offer_1__c, coupon_text_offer_2__c, coupon_text_offer_3__c from YPC_AddOns__r) from Digital_Product_Requirement__c where Account__r.TalusAccountId__c != null and Talus_Subscription_Id__c != null and Id IN(SELECT DFF__c from YPC_AddOn__c)';
            
            YpcAddonPatch obj = new YpcAddonPatch(query);
            ID batchprocessid = database.executebatch(obj, Integer.valueOf(Label.Cron_Batch_Size));
            
            CommonUtility.msgInfo('YPC Batch Job is Successfully Scheduled, Monitor Jobs at Apex Jobs Page');
        } catch (exception e) {
            CommonUtility.msgError('Exception Occured: ' + e.getMessage());
        }
        return null;
    }

    public pageReference ypcDMBatch() {
        try {
            String query = 'SELECT Id, Name, udac__c, udac_points__c, add_ons__c, vid_add_on__c, coupon_description__c, coupon_url__c, coupon_text_offer_1__c, coupon_text_offer_2__c, coupon_text_offer_3__c, cslt_url__c, cslt_text__c, recordType.DeveloperName, Account__r.TalusAccountId__c, Talus_Subscription_Id__c, Talus_DFF_Id__c, Core_Opportunity_Line_ID__c, OrderLineItemID__r.Effective_Date__c, (SELECT Id, Name, DFF__c, DFF_Id__c, Validation__c, Effective_Date__c, Action_Type__c, coupon_description__c, coupon_url__c, cslt_text__c, cslt_url__c, coupon_text_offer_1__c, coupon_text_offer_2__c, coupon_text_offer_3__c from YPC_AddOns__r) from Digital_Product_Requirement__c where Core_Opportunity_Line_ID__c != null and Id IN(SELECT DFF__c from YPC_AddOn__c)';
            
            YpcAddonPatchDataMigration obj = new YpcAddonPatchDataMigration(query);
            ID batchprocessid = database.executebatch(obj, Integer.valueOf(Label.Cron_Batch_Size));
            
            CommonUtility.msgInfo('YPC Batch Job for Data Migration Dffs is Successfully Scheduled, Monitor Jobs at Schedle Jobs Page');
        } catch (exception e) {
            CommonUtility.msgError('Exception Occured: ' + e.getMessage());
        }
        return null;
    }
    
    public pageReference fulmntCncl() {
        
        try {
            
            DeleteFulfillmentBatch obj = new DeleteFulfillmentBatch();
            Batch_Size__c objBatch = Batch_Size__c.getInstance('DeleteTalus');
            ID batchprocessid = database.executebatch(obj, Integer.valueOf(objBatch.Size__c));
            
            CommonUtility.msgInfo('Cancellation Jobs is Successfully Submitted, Monitor Jobs at Scheduled Jobs Page');
        } catch (exception e) {
            CommonUtility.msgError('Exception Occured: ' + e.getMessage());
        }                
        
        /*
        Map<Id, List<Order_Line_Items__c>> mapOrdSetOl = new Map<Id, List<Order_Line_Items__c>>();
        Set<Id> OlIds = new Set<Id>();
        List<Order_Line_Items__c> fnlLst = new List<Order_Line_Items__c>();
       
        try{
        //List<Order_Line_Items__c> testLst = new List<Order_Line_Items__c>();
        List<Order_Line_Items__c> lstOLICancel = [select id, Udac__c, Action_Code__c, media_type__c, Talus_Subscription_Id__c, Order_Group__c, Order_Group__r.Name, Digital_Product_Requirement__r.Bundle__c from Order_Line_Items__c where Action_Code__c = 'Cancel' and Cutomer_Cancel_Date__c < today and Status__c not in ('Cancelation Requested','Cancelled') and media_type__c = 'Digital' limit 50000];// 
        
        //testLst = DeleteTalusSubscription.parentAddonOLIs(lstOLICancel);
        
        for(Order_Line_Items__c iterator: lstOLICancel){
            if(mapOrdSetOl.containsKey(iterator.Order_Group__c)){
                mapOrdSetOl.get(iterator.Order_Group__c).add(iterator);
            }else{
                List<Order_Line_Items__c> allOls = new List<Order_Line_Items__c>();
                allOls.add(iterator);
                mapOrdSetOl.put(iterator.Order_Group__c, allOls);
            }
        }
        
        for(Id iterator: mapOrdSetOl.keySet()){
            List<Order_Line_Items__c> lstOL1 =  mapOrdSetOl.get(iterator);
            List<Order_Line_Items__c> lstOL2 = new List<Order_Line_Items__c>();
            boolean flag = false;
            for(Order_Line_Items__c ol: lstOL1){
                if(String.isBlank(ol.Digital_Product_Requirement__r.Bundle__c)){
                    fnlLst.add(ol);
                }else if(ol.Digital_Product_Requirement__r.Bundle__c.Contains('BASE')){
                    fnlLst.add(ol); 
                    flag = true; 
                } else{
                    lstOL2.add(ol);
                }
            }
            if(!flag){
                fnlLst.addAll(lstOL2);
            }        
        }
        
        CommonUtility.msgInfo('Total Number of Records to Process: ' + fnlLst.size());
        if(fnlLst.size()>0){
        for(Order_Line_Items__c iterator: fnlLst){
            CommonUtility.msgInfo('************' + iterator.Order_Group__r.Name + '************' + iterator.udac__c + '************' + iterator.Talus_Subscription_Id__c + '************' + iterator.Digital_Product_Requirement__r.Bundle__c + '************');
        }
        }else{
            CommonUtility.msgInfo('No Records Found to Process!');
        }
        
        } catch (exception e) {
            CommonUtility.msgError('Exception Occured: ' + e.getMessage());
        }
        */
        return null;
    }
}