public class CreatePaymentMethod {
public pymt__Payment_Method__c tempPaymentMethod{get;set;}
String expYear;
public String cardNumber{get;set;}
String expMonth;
public Id ContactId;
public boolean makeDefaultMethod{get;set;}
    public CreatePaymentMethod(ApexPages.StandardController controller) {
    tempPaymentMethod=new pymt__Payment_Method__c ();
    ContactId=ApexPages.currentPage().getParameters().get('conid');
    if(ContactId!=null)
    {
    PopulateContact(ContactId);
    }
    }
  
  public void PopulateContact(Id CId)
  {
    Contact con=[Select FirstName,LastName,Id,MailingStreet,MailingCity, MailingState,MailingCountry,MailingPostalCode From Contact where ID=:CId Limit 1];
    tempPaymentMethod.pymt__Contact__c=con.id;
    tempPaymentMethod.pymt__Billing_First_Name__c=con.FirstName;
    tempPaymentMethod.pymt__Billing_Last_Name__c=con.LastName;
    tempPaymentMethod.pymt__Billing_Street__c=con.MailingStreet;
    tempPaymentMethod.pymt__Billing_City__c=con.MailingCity;
    tempPaymentMethod.pymt__Billing_State__c=con.MailingState;
    tempPaymentMethod.pymt__Billing_Postal_Code__c=con.MailingPostalCode;
    tempPaymentMethod.pymt__Billing_Country__c=con.MailingCountry;
  }
  public List<SelectOption> getexpYearOptions()
  {
     List<SelectOption> options = new List<SelectOption>();
     Integer currentyear= date.today().year();
     for(integer i=currentyear;i<currentyear+15;i++)
     {
       options.add(new SelectOption(String.valueof(i),String.valueof(i)));
     }
     return options;
  }
  public String getexpYear() 
   {
            return expYear;
   }
   public void setexpYear(String expYear) 
   {
            this.expYear= expYear;
   }
  public List<SelectOption> getexpMonthOptions() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('01','Jan'));
            options.add(new SelectOption('02','Feb'));
            options.add(new SelectOption('03','Mar'));
            options.add(new SelectOption('04','Apr'));
            options.add(new SelectOption('05','May'));
            options.add(new SelectOption('06','Jun'));
            options.add(new SelectOption('07','Jul'));
            options.add(new SelectOption('08','Aug'));
            options.add(new SelectOption('09','Sep'));
            options.add(new SelectOption('10','Oct'));
            options.add(new SelectOption('11','Nov'));
            options.add(new SelectOption('12','Dec'));
            return options;
        }
   public String getexpMonth() 
   {
            return expMonth;
   }
   public void setexpMonth(String expMonth) 
   {
            this.expMonth= expMonth;
   }  
   
   public Boolean isValid(String CardType, String CardNumber)
    {
        CardNumber = CardNumber.replace(' ','');
        Boolean ret = false; 
        System.debug('@@@@@@@@@@@@'+ CardType); 
        //first check if all digits are numeric     
        if(!pattern.matches('[0-9]+',CardNumber))
        {
         return false;
        }
        else
        {
            if(CardNumber.length()==16) 
            {
            return true;
            }
            else
            {
            return false;
            }
        }
      /*  if(CardType=='MasterCard')
        {
            ret = true;
            //prefix: 51->55
            ret = ret && (checkPrefix(CardNumber, '51') 
            || checkPrefix(CardNumber, '52')
            || checkPrefix(CardNumber, '53')
            || checkPrefix(CardNumber, '55')
            || checkPrefix(CardNumber, '54'));
            //length: 16
            ret = ret && CardNumber.length()==16;
            //mod 10
            ret = ret && checkMod10(CardNumber);
        }
        if(CardType=='American Express')
        {
            ret = true;
            //prefix: 34 or 37
            ret = ret && (checkPrefix(CardNumber, '34')
            || checkPrefix(CardNumber, '37'));
            //length: 15
            ret = ret && CardNumber.length()==15;
            //mod 10
            ret = ret && checkMod10(CardNumber);
            
        }
        if(CardType=='Discover')
        {
            ret = true;
            //prefix: 6011
            ret = ret && checkPrefix(CardNumber, '6011');
            //length: 16
            ret = ret && CardNumber.length()==16;
            //mod 10
            ret = ret && checkMod10(CardNumber);
            
        }
        if(CardType=='Visa')
        {
            ret = true;
            //prefix: 4
            ret = ret && checkPrefix(CardNumber, '4');
            //length: 13 or 16
            ret = ret && (CardNumber.length()==16 || CardNumber.length()==13);
            //mod 10
            ret = ret && checkMod10(CardNumber);
            
        }*/
        return ret;
    }
    /*
    private Boolean checkPrefix(String CardNumber, String Prefix)
    {
        return CardNumber.startsWith(Prefix);
    }
    
    private Boolean checkMod10(String CardNumber)
    {
        //first check if all digits are numeric
        if(!pattern.matches('[0-9]+',CardNumber)) return false;
        
        String s = '';
        Integer digit = 0;
        for(Integer d=CardNumber.length()-1; d>=0;d--)
        {
            digit++;
            if(Mod(digit,2)==0)
            {
                Integer i = Integer.valueOf(CardNumber.substring(d,d+1))*2;
                s+=i.format();
            }
            else
            {
                Integer i = Integer.valueOf(CardNumber.substring(d,d+1));
                s+=i.format();
            }
        }
        Integer sum = getSum(s);
        system.debug('s:'+sum);
        system.debug('sum:'+s);
        return Mod(sum,10)==0;
    }
    private Integer getSum(String s)
    {
        Integer sum = 0;
        for(Integer d=0; d<s.length();d++)
        {
            sum+=Integer.valueOf(s.substring(d,d+1));
        }
        return sum;
    }
    public Integer Mod(Integer n, Integer div)
    {   
        while(n>=div)
            n-=div;
        return n;
    }*/   
  /*public boolean CCValidation(String CCNumber)
  {
            Integer sum = 0;
            Integer len = CCNumber.length();
            for(Integer i=len-1;i>=0;i--){
            Integer num = Integer.ValueOf(CCNumber.substring(i,i+1));
            if ( math.mod(i , 2) == math.mod(len, 2) )
            {
            Integer n = num * 2;
            sum += (n / 10) + ( math.mod(n, 10));
            }
            else{
            sum += num;
            }
            }
            return ( math.mod( sum, 10) == 0 );
  }
  */
  public Pagereference cancelFromPage()
  {
   return null;
  }
  public pageReference addCreditCard()
  {
   pageReference pg=null;
   if(cardNumber!=null)
   {
    if(isValid(tempPaymentMethod.pymt__Card_Type__c, cardNumber))
    {
        pymt__Payment_Method__c pymtMethod=new pymt__Payment_Method__c ();
        //pymtMethod.ccrd__Card_Number__c=cardNumber;
        pymtMethod.Name=tempPaymentMethod.pymt__Type__c;
        pymtMethod.pymt__Billing_City__c=tempPaymentMethod.pymt__Billing_City__c;
        pymtMethod.pymt__Billing_Country__c=tempPaymentMethod.pymt__Billing_Country__c;
        pymtMethod.pymt__Billing_Email__c=tempPaymentMethod.pymt__Billing_Email__c;
        pymtMethod.pymt__Billing_First_Name__c=tempPaymentMethod.pymt__Billing_First_Name__c;
        pymtMethod.pymt__Billing_Last_Name__c=tempPaymentMethod.pymt__Billing_Last_Name__c;
        pymtMethod.pymt__Billing_Phone__c=tempPaymentMethod.pymt__Billing_Phone__c;
        pymtMethod.pymt__Billing_Postal_Code__c=tempPaymentMethod.pymt__Billing_Postal_Code__c;
        pymtMethod.pymt__Billing_Salutation__c=tempPaymentMethod.pymt__Billing_Salutation__c;
        pymtMethod.pymt__Billing_State__c=tempPaymentMethod.pymt__Billing_State__c;
        pymtMethod.pymt__Billing_Street__c=tempPaymentMethod.pymt__Billing_Street__c;
        pymtMethod.pymt__Card_Type__c=tempPaymentMethod.pymt__Card_Type__c;
        pymtMethod.pymt__Contact__c=tempPaymentMethod.pymt__Contact__c;
        pymtMethod.pymt__Expiration_Month__c=expMonth;
         pymtMethod.pymt__Expiration_Year__c=expYear;
        pymtMethod.pymt__Last_4_Digits__c=tempPaymentMethod.pymt__Last_4_Digits__c;
        pymtMethod.pymt__Payment_Type__c=tempPaymentMethod.pymt__Payment_Type__c;
        pymtMethod.pymt__Processor_Connection__c=tempPaymentMethod.pymt__Processor_Connection__c;
        pymtMethod.pymt__Profile_Id__c=tempPaymentMethod.pymt__Profile_Id__c;
        pymtMethod.pymt__Type__c =tempPaymentMethod.pymt__Type__c ;
        if(makeDefaultMethod==true)
        {
        pymtMethod.pymt__Default__c=true;
        }
        else
        {
        pymtMethod.pymt__Default__c=false;
        }
        try
        {
        insert pymtMethod;
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Confirm,'Payment Method Created Successfully.');
        ApexPages.addMessage(myMsg);
        string returnURI=ApexPages.currentPage().getParameters().get('retURL');
        if(returnURI!=null)
        {
        pg=new PageReference(returnURI);
        }
        }
        catch(Exception ex)
        {
         ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'Error while creating record'+ex);
         ApexPages.addMessage(myMsg);
        }
        
       
    }
    else
    {
      ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'Credit Card Number is In-valid. Kindly enter a valid number.');
      ApexPages.addMessage(myMsg);
    }
  }
  else
  {
    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'Credit Card Number is In-valid. Kindly enter a valid number.');
     ApexPages.addMessage(myMsg);
  }
    return pg;
  }
}