@IsTest
private class DirectoryFlipBookClassTestClass {
    public static testMethod void directoryInsert(){
    
        Account acct = TestMethodsUtility.createAccount('telco');
        Telco__c telco = TestMethodsUtility.createTelco(acct.Id);
        Canvass__c objCanvss = TestMethodsUtility.generateCanvass(telco);
        objCanvss.Name = 'Test Canvass';
        objCanvss.co_brand_name__c = 'Test';
        objCanvss.IsActive__c = true;
        insert objCanvss;
    
    List<Directory__c> ObjdirLst = new List<Directory__c>();
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Name = 'Test Dir1';
        objDir.EBD__c = System.Today().addDays(1);
        objDir.DCR_Close__c = System.Today().addDays(5);
        objDir.BOC__c = System.Today().addDays(2);
        objDir.Final_Service_Order_Due_to_Berry__c = System.Today().addDays(1);
        objDir.Sales_Lockout__c = System.Today();
        objDir.Book_Extract_YP__c = System.Today().addDays(3);
        objDir.Ship_Date__c = System.Today().addDays(-2);
        objDir.Pub_Date__c = System.Today().addMonths(1);
        objDir.Directory_Code__c = 'DirCode1';
        objDir.Telco_Provider__c=telco.id;
        objDir.Sales_Start__c = System.Today().addDays(4);
        objDir.Distribution_Method__c= 'MAILED';
        objDir.Delivery_Start_Date__c= System.Today().addDays(2);
        objDir.Printer__c='DP';
        objDir.Printer_Location__c= 'Illinois';
        objDir.ALPHA_COLOR_LEVEL__c= 'K';
        objDir.CLASS_COLOR_LEVEL__c= 'K';
        ObjdirLst.add(objDir);
        
        insert objdirLst;
     
    Date pubdate = date.newInstance(2014, 1, 1);
    List<Directory_Edition__c> ObjdirEditionLst = new List<Directory_Edition__c>(); 
        Directory_Edition__c objDirE = new Directory_Edition__c();
        objDirE.Name = 'Test Directory Edition 1';
        objDirE.LSA_Directory_Version__c=String.valueof(System.Today().year());
        objDirE.Directory__c = objdirLst[0].id;
        objDirE.Book_Status__c = 'BOTS';
        objDirE.EBD__c = System.Today().addDays(1);
        objDirE.DCR_Close__c = System.Today().addDays(5);
        objDirE.BOC__c = System.Today().addDays(2);
        objDirE.Final_Service_Order_Due_to_Berry__c = System.Today().addDays(1);
        objDirE.Sales_Lockout__c = System.Today();
        objDirE.Book_Extract_YP__c = System.Today().addDays(3);
        objDirE.Ship_Date__c = Date.valueof('2013-07-28');
        objDirE.Pub_Date__c =  System.Today().addMonths(1);
        objDirE.Final_Auto_Renew_Job__c = System.Today().addDays(2);
        objDirE.EACD__c = System.Today().addDays(3);
        objDirE.Write_Away_Date__c = System.Today().addDays(1);
        objDirE.Free_Listings_Copied_To_New_Edition__c = System.Today().addDays(3);
        objDirE.National_Rates_Due__c = System.Today().addDays(6);  
        objDirE.National_Close_Date__c = System.Today().addDays(1);
        objDirE.Proof_Close__c = System.Today().addDays(4);
        objDirE.Galleys_Due_Telco__c = System.Today().addDays(3);
        objDirE.Galleys_Due_Berry__c = System.Today().addDays(1);
        objDirE.Renewal_Open_Date__c = System.Today().addDays(4);
        objDirE.Annual_Load_File_Due_Berry__c = System.Today().addDays(2);
        objDirE.Book_Extract_WP__c = System.Today().addDays(2); 
        objDirE.AD_PROOF_CHNGS_LMB__c = System.Today().addDays(-10);
        objDirE.ALPHA_GALLEY_PULL__c = System.Today().addDays(-14);
        objDirE.ALPHA_PAGES_DUE__c = System.Today().addDays(-14);
        objDirE.ALPHA_PROOFS_DUE__c = System.Today().addDays(-14);
        objDirE.ALPHA_PROOFS_DUE_BERRY__c = System.Today().addDays(-14);
        //objDirE.Amount__c - currency
        objDirE.ANNUAL_LOAD_FILE_DUE_DATALINK__c = System.Today().addDays(-14);
        //objDirE.Annual_PI__c - currency
        objDirE.Bill_Prep__c = System.Today().addDays(-14);
        objDirE.BILL_PREP_START__c = System.Today().addDays(-14);
        //objDirE.Book_Extract_YP_Extension__c - text
        objDirE.Canvass__c = objCanvss.id;
        objDirE.CH_LAST_KEYING_DATE__c = System.Today().addDays(-14);
        //objDirE.CH_Last_Keying_Date_Extension__c - Text
        objDirE.CLASS_PAGES_DUE__c = System.Today().addDays(-14);
        //objDirE.Class_Pages_Due_Extension__c -Text
        objDirE.CLASS_PROOFS_DUE__c = System.Today().addDays(-14);
        objDirE.CLASS_PROOFS_DUE_BERRY__c = System.Today().addDays(-14);
        //objDirE.Cost_Type__c - Pickist
        objDirE.Coup_Prelim_Filler__c = System.Today().addDays(-14);
        objDirE.COVER_DUE_PRINTER__c = System.Today().addDays(-14);
        objDirE.Delivery_Start_Date__c = System.Today().addDays(-14);
        //objDirE.Directory_Mapping__c - Lookup
        //objDirE.Directory_Worth_2_5_Million__c = true;
        objDirE.DPV_DUE_BERRY__c = System.Today().addDays(-14);
        objDirE.DPV_DUE_TELCO__c = System.Today().addDays(-14);
        objDirE.DSA_CLOSE__c = System.Today().addDays(-14);
        //objDirE.EACD_Extension__c = System.Today().addDays(-14);
        objDirE.EAS_CHANGE_CLOSE__c = System.Today().addDays(-14);
        objDirE.EAS_DSA_COMPLETED__c = System.Today().addDays(-14);
        objDirE.EAS_DUE_COMPOSER__c = System.Today().addDays(-14);
        objDirE.EAS_DUE_PRINTER__c = System.Today().addDays(-14);
        //objDirE.EBD_Extension__c = '1223';
        objDirE.FILES_DUE_FROM_DATALINK__c = System.Today().addDays(-14);
        objDirE.FILES_DUE_TO_DATALINK__c = System.Today().addDays(-14);
        objDirE.FINAL_QC_DUE_PAGING_WP__c = System.Today().addDays(-14);
        objDirE.FINAL_QC_DUE_PAGING_YP__c = System.Today().addDays(-14);
        objDirE.FINAL_QC_DUE_PROD_WP__c = System.Today().addDays(-14);
        objDirE.FINAL_QC_DUE_PROD_YP__c = System.Today().addDays(-14);
        objDirE.FINAL_QC_DUE_Sales_Telco_WP__c = System.Today().addDays(-14);
        objDirE.FINAL_QC_DUE_Sales_Telco_YP__c = System.Today().addDays(-14);
        objDirE.FIN_PAGINATION_EXTRACT_WP__c = System.Today().addDays(-14);
        objDirE.FIN_PAGINATION_EXTRACT_YP__c = System.Today().addDays(-14);
        objDirE.GRAPHICS_COMPLETE__c = System.Today().addDays(-14);
        objDirE.HALF_MARK__c = System.Today().addDays(-14);
        objDirE.INIT_PAGINATION_ERRORS_WP__c = System.Today().addDays(-14);
        objDirE.INIT_PAGINATION_ERRORS_YP__c = System.Today().addDays(-14);
        objDirE.INIT_PAGINATION_EXTRACT_WP__c = System.Today().addDays(-14);
        objDirE.INIT_PAGINATION_EXTRACT_YP__c = System.Today().addDays(-14);
        //objDirE.Init_Pagination_Extract_YP_Extension__c - text
        objDirE.IN_STONE_DATE__c = System.Today().addDays(-14);
        objDirE.INVOICE_DATE__c = System.Today().addDays(-14);
        objDirE.IPE_PROOF_DUE_PROD_WP__c = System.Today().addDays(-14);
        objDirE.IPE_PROOF_DUE_PROD_YP__c = System.Today().addDays(-14);
        objDirE.Letter_Renewal_Stage_1__c = System.Today().addDays(-14);
        objDirE.Letter_Renewal_Stage_2__c = System.Today().addDays(-14);
        objDirE.LLAD_LISTING_CLOSE__c = System.Today().addDays(-14);
        objDirE.LOAD_INTERFILED_CATS_FILES__c = System.Today().addDays(-14);
        objDirE.LOCAL_CORRECTIONS_KEYED_COMPLETE__c = System.Today().addDays(-14);
        objDirE.Local_Rates_Due__c = System.Today().addDays(-14);
        objDirE.MAIL_LABELS_ENVELOPE_ORDERS_DATE__c = System.Today().addDays(-14);
        objDirE.MAIL_LABELS_BERRY__c = System.Today().addDays(-14);
        objDirE.MAIL_LABELS_PRINTER__c = System.Today().addDays(-14);
        objDirE.MANUSCRIPT_DUE_BERRY__c = System.Today().addDays(-14);
        
        //objDirE.National_Billing_Date__c = date.newInstance(objDirE.Pub_Date__c.year(),objDirE.Pub_Date__c.month(),objDirE.Pub_Date__c.day());
        //objDirE.National_Billing_Date__c = System.Today().addMonths(1);
      
        objDirE.NATIONAL_CORRECTIONS_KEYED_COMPLETE__c = System.Today().addDays(-14);
        objDirE.NATIONAL_FIRST_CRITICAL_DATE__c = System.Today().addDays(-14);
        objDirE.NCR_FILE_DUE_PAGINATION__c = System.Today().addDays(-14);
        objDirE.NCR_PAGES_DUE_PRINTER__c = System.Today().addDays(-14);
        //objDirE.New_Print_Bill_Date__c = date.newInstance(objDirE.Pub_Date__c.year(),objDirE.Pub_Date__c.month(),objDirE.Pub_Date__c.day());
        objDirE.NON_COVER_SPECIALTY_ITEMS__c = System.Today().addDays(-14);
        objDirE.ONE_THIRD_MARK__c = System.Today().addDays(-14);
        objDirE.PAGINATION_END_WP__c = System.Today().addDays(-14);
        objDirE.PAGINATION_END_YP__c = System.Today().addDays(-14);
        objDirE.PAGINATION_EXTRACT__c = System.Today().addDays(-14);
        objDirE.PAGINATION_START_WP__c = System.Today().addDays(-14);
        objDirE.PAGINATION_START_YP__c = System.Today().addDays(-14);
        objDirE.PRE_PAGINATION_EXTRACT__c = System.Today().addDays(-14);
        objDirE.PRINT_REQ_DISTRIBUTED__c = System.Today().addDays(-14);
        objDirE.PRINT_REQ_DUE_PPM__c = System.Today().addDays(-14);
        objDirE.PRINT_REQ_DUE_TELCO__c = System.Today().addDays(-14);
        objDirE.PRINT_REQ_UPDATED_W_APPRVD_SCOPE_COUNTS__c = System.Today().addDays(-14);
        objDirE.RECON_COMPLETED_WP__c = System.Today().addDays(-14);
        objDirE.RECON_COMPLETED_YP__c = System.Today().addDays(-14);
        objDirE.SALES_PAPERWORK_DUE_PUBLISHING__c = System.Today().addDays(-14);
        objDirE.Sales_Start__c = System.Today().addDays(-14);
        objDirE.Sold_Ad_Renew_Job_1__c = System.Today().addDays(-14);
        objDirE.Sold_Ad_Renew_Job_2__c = System.Today().addDays(-14);
        objDirE.SPECIALTY_ITEM_LIST_SENT_DELIVERY_CORDIN__c = System.Today().addDays(-14);
        objDirE.SPECIALTY_ITEMS_DUE_PPM__c = System.Today().addDays(-14);
        objDirE.SPECIALTY_ITEMS_NOTIFICATION_DUE_SALES1__c = System.Today().addDays(-14);
        objDirE.SPECIALTY_ITEMS_SOLD__c = System.Today().addDays(-14);
        objDirE.STAGE_1_DUE_DCS_COVER_DESIGN__c = System.Today().addDays(-14);
        objDirE.STAGE1_DUE_DCS_COVER_SPREAD__c = System.Today().addDays(-14);
        objDirE.STAGE1_DUE_DCS_PRELIMS__c = System.Today().addDays(-14);
        objDirE.STAGE1_DUE_PPM_COVER_SPREAD__c = System.Today().addDays(-14);
        objDirE.STAGE1_DUE_PPM_TELCO_COVER_SPREAD__c = System.Today().addDays(-14);
        objDirE.STAGE_1_DUE_PPM_FROM_DCS_COVER_DESIGN1__c = System.Today().addDays(-14);
        objDirE.STAGE_1_DUE_PPM_FROM_DCS_PRELIMS__c = System.Today().addDays(-14);
        objDirE.STAGE_1_DUE_TELCO_COVER_DESIGN__c = System.Today().addDays(-14);
        objDirE.STAGE1_DUE_TELCO_PRELIMS__c = System.Today().addDays(-14);
        objDirE.STAGE_2A_DUE_DCS_COVER_DESIGN__c = System.Today().addDays(-14);
        objDirE.STAGE2A_DUE_DCS_PRELIMS__c = System.Today().addDays(-14);
        objDirE.STAGE_2A_DUE_PPM_FROM_DCS_COVER_DESIG1__c = System.Today().addDays(-14);
        objDirE.STAGE_2A_DUE_PPM_FROM_DCS_PRELIMS__c = System.Today().addDays(-14);
        objDirE.STAGE_2A_DUE_PPM_FROM_TELCO_COVER_DESIG1__c = System.Today().addDays(-14);
        objDirE.STAGE_2A_DUE_PPM_FROM_TELCO_PRELIMS__c = System.Today().addDays(-14);
        objDirE.STAGE_2A_DUE_TELCO_COVER_DESIGN__c = System.Today().addDays(-14);
        objDirE.STAGE2A_DUE_TELCO_PRELIMS__c = System.Today().addDays(-14);
        objDirE.STAGE2B_DUE_DCS_PRELIMS__c = System.Today().addDays(-14);
        objDirE.STAGE2B_DUE_PPM_PRELIMS__c = System.Today().addDays(-14);
        objDirE.STAGE_2B_DUE_PPM_FROM_TELCO_COVER_DESIG1__c = System.Today().addDays(-14);
        objDirE.STAGE_2B_DUE_PPM_FROM_TELCO_PRELIMS__c = System.Today().addDays(-14);
        objDirE.STAGE2_DUE_DCS_COVER_SPREAD__c = System.Today().addDays(-14);
        objDirE.STAGE2_DUE_PPM_TELCO_COVER_SPREAD__c = System.Today().addDays(-14);
        objDirE.STAGE3_DUE_DCS_COVER_SPREAD__c = System.Today().addDays(-14);
        objDirE.STAGE3_DUE_DCS_PRELIMS__c = System.Today().addDays(-14);
        objDirE.STAGE_3_DUE_PPM_FROM_TELCO_PRELIMS__c = System.Today().addDays(-14);
        objDirE.TWO_THIRDS_MARK__c = System.Today().addDays(-14);
        
        objDirE.Setup_Cost__c = 1000;
        objDirE.Printing_Cost__c = 2000;
        objDirE.Other_Cost__c = 3000;
        
        ObjdirEditionLst.add(objDirE);
        insert ObjdirEditionLst;
        
        objDirE.EBD__c = System.Today().addDays(3);
        objDirE.DCR_Close__c = System.Today().addDays(1);
        objDirE.BOC__c = System.Today().addDays(4);
        objDirE.Final_Service_Order_Due_to_Berry__c = System.Today().addDays(2);
        objDirE.Sales_Lockout__c = System.Today().addDays(2);
        objDirE.Book_Extract_YP__c = System.Today().addDays(4);
        objDirE.Ship_Date__c = Date.valueof('2013-08-24');
        objDirE.Pub_Date__c = System.Today().addMonths(2);
        objDirE.Final_Auto_Renew_Job__c = System.Today().addDays(4);
        objDirE.EACD__c = System.Today().addDays(2);
        objDirE.Write_Away_Date__c = System.Today().addDays(5);
        objDirE.Free_Listings_Copied_To_New_Edition__c = System.Today().addDays(4);
        objDirE.National_Rates_Due__c = System.Today().addDays(4);  
        objDirE.National_Close_Date__c = System.Today().addDays(4);
        objDirE.Proof_Close__c = System.Today().addDays(3);
        objDirE.Galleys_Due_Telco__c = System.Today().addDays(2);
        objDirE.Galleys_Due_Berry__c = System.Today().addDays(2);
        objDirE.Renewal_Open_Date__c = System.Today().addDays(1);
        objDirE.Annual_Load_File_Due_Berry__c = System.Today().addDays(5);
        objDirE.Book_Extract_WP__c = System.Today().addDays(4); 
        objDirE.AD_PROOF_CHNGS_LMB__c = System.Today().addDays(-10);
        objDirE.ALPHA_GALLEY_PULL__c = System.Today().addDays(-14);
        objDirE.ALPHA_PAGES_DUE__c = System.Today().addDays(-14);
        objDirE.ALPHA_PROOFS_DUE__c = System.Today().addDays(-14);
        objDirE.ALPHA_PROOFS_DUE_BERRY__c = System.Today().addDays(-14);
        //objDirE.Amount__c - currency
        objDirE.ANNUAL_LOAD_FILE_DUE_DATALINK__c = System.Today().addDays(-14);
        //objDirE.Annual_PI__c - currency
        objDirE.Bill_Prep__c = System.Today().addDays(-14);
        objDirE.BILL_PREP_START__c = System.Today().addDays(-14);
        //objDirE.Book_Extract_YP_Extension__c - text
        objDirE.Canvass__c = objCanvss.id;
        objDirE.CH_LAST_KEYING_DATE__c = System.Today().addDays(-14);
        //objDirE.CH_Last_Keying_Date_Extension__c - Text
        objDirE.CLASS_PAGES_DUE__c = System.Today().addDays(-14);
        //objDirE.Class_Pages_Due_Extension__c -Text
        objDirE.CLASS_PROOFS_DUE__c = System.Today().addDays(-14);
        objDirE.CLASS_PROOFS_DUE_BERRY__c = System.Today().addDays(-14);
        //objDirE.Cost_Type__c - Pickist
        objDirE.Coup_Prelim_Filler__c = System.Today().addDays(-14);
        objDirE.COVER_DUE_PRINTER__c = System.Today().addDays(-14);
        objDirE.Delivery_Start_Date__c = System.Today().addDays(-14);
        //objDirE.Directory_Mapping__c - Lookup
        //objDirE.Directory_Worth_2_5_Million__c = true;
        objDirE.DPV_DUE_BERRY__c = System.Today().addDays(-14);
        objDirE.DPV_DUE_TELCO__c = System.Today().addDays(-14);
        objDirE.DSA_CLOSE__c = System.Today().addDays(-14);
        //objDirE.EACD_Extension__c = System.Today().addDays(-14);
        objDirE.EAS_CHANGE_CLOSE__c = System.Today().addDays(-14);
        objDirE.EAS_DSA_COMPLETED__c = System.Today().addDays(-14);
        objDirE.EAS_DUE_COMPOSER__c = System.Today().addDays(-14);
        objDirE.EAS_DUE_PRINTER__c = System.Today().addDays(-14);
        //objDirE.EBD_Extension__c = '1223';
        objDirE.FILES_DUE_FROM_DATALINK__c = System.Today().addDays(-14);
        objDirE.FILES_DUE_TO_DATALINK__c = System.Today().addDays(-14);
        objDirE.FINAL_QC_DUE_PAGING_WP__c = System.Today().addDays(-14);
        objDirE.FINAL_QC_DUE_PAGING_YP__c = System.Today().addDays(-14);
        objDirE.FINAL_QC_DUE_PROD_WP__c = System.Today().addDays(-14);
        objDirE.FINAL_QC_DUE_PROD_YP__c = System.Today().addDays(-14);
        objDirE.FINAL_QC_DUE_Sales_Telco_WP__c = System.Today().addDays(-14);
        objDirE.FINAL_QC_DUE_Sales_Telco_YP__c = System.Today().addDays(-14);
        objDirE.FIN_PAGINATION_EXTRACT_WP__c = System.Today().addDays(-14);
        objDirE.FIN_PAGINATION_EXTRACT_YP__c = System.Today().addDays(-14);
        objDirE.GRAPHICS_COMPLETE__c = System.Today().addDays(-14);
        objDirE.HALF_MARK__c = System.Today().addDays(-14);
        objDirE.INIT_PAGINATION_ERRORS_WP__c = System.Today().addDays(-14);
        objDirE.INIT_PAGINATION_ERRORS_YP__c = System.Today().addDays(-14);
        objDirE.INIT_PAGINATION_EXTRACT_WP__c = System.Today().addDays(-14);
        objDirE.INIT_PAGINATION_EXTRACT_YP__c = System.Today().addDays(-14);
        //objDirE.Init_Pagination_Extract_YP_Extension__c - text
        objDirE.IN_STONE_DATE__c = System.Today().addDays(-14);
        objDirE.INVOICE_DATE__c = System.Today().addDays(-14);
        objDirE.IPE_PROOF_DUE_PROD_WP__c = System.Today().addDays(-14);
        objDirE.IPE_PROOF_DUE_PROD_YP__c = System.Today().addDays(-14);
        objDirE.Letter_Renewal_Stage_1__c = System.Today().addDays(-14);
        objDirE.Letter_Renewal_Stage_2__c = System.Today().addDays(-14);
        objDirE.LLAD_LISTING_CLOSE__c = System.Today().addDays(-14);
        objDirE.LOAD_INTERFILED_CATS_FILES__c = System.Today().addDays(-14);
        objDirE.LOCAL_CORRECTIONS_KEYED_COMPLETE__c = System.Today().addDays(-14);
        objDirE.Local_Rates_Due__c = System.Today().addDays(-14);
        objDirE.MAIL_LABELS_ENVELOPE_ORDERS_DATE__c = System.Today().addDays(-14);
        objDirE.MAIL_LABELS_BERRY__c = System.Today().addDays(-14);
        objDirE.MAIL_LABELS_PRINTER__c = System.Today().addDays(-14);
        objDirE.MANUSCRIPT_DUE_BERRY__c = System.Today().addDays(-14);
        //objDirE.National_Billing_Date__c = date.newInstance(objDirE.Pub_Date__c.year(),objDirE.Pub_Date__c.month(),objDirE.Pub_Date__c.day());
        //objDirE.National_Billing_Date__c =System.Today().addMonths(2);
        objDirE.NATIONAL_CORRECTIONS_KEYED_COMPLETE__c = System.Today().addDays(-14);
        objDirE.NATIONAL_FIRST_CRITICAL_DATE__c = System.Today().addDays(-14);
        objDirE.NCR_FILE_DUE_PAGINATION__c = System.Today().addDays(-14);
        objDirE.NCR_PAGES_DUE_PRINTER__c = System.Today().addDays(-14);
        //objDirE.New_Print_Bill_Date__c = date.newInstance(objDirE.Pub_Date__c.year(),objDirE.Pub_Date__c.month(),objDirE.Pub_Date__c.day());
        objDirE.NON_COVER_SPECIALTY_ITEMS__c = System.Today().addDays(-14);
        objDirE.ONE_THIRD_MARK__c = System.Today().addDays(-14);
        objDirE.PAGINATION_END_WP__c = System.Today().addDays(-14);
        objDirE.PAGINATION_END_YP__c = System.Today().addDays(-14);
        objDirE.PAGINATION_EXTRACT__c = System.Today().addDays(-14);
        objDirE.PAGINATION_START_WP__c = System.Today().addDays(-14);
        objDirE.PAGINATION_START_YP__c = System.Today().addDays(-14);
        objDirE.PRE_PAGINATION_EXTRACT__c = System.Today().addDays(-14);
        objDirE.PRINT_REQ_DISTRIBUTED__c = System.Today().addDays(-14);
        objDirE.PRINT_REQ_DUE_PPM__c = System.Today().addDays(-14);
        objDirE.PRINT_REQ_DUE_TELCO__c = System.Today().addDays(-14);
        objDirE.PRINT_REQ_UPDATED_W_APPRVD_SCOPE_COUNTS__c = System.Today().addDays(-14);
        objDirE.RECON_COMPLETED_WP__c = System.Today().addDays(-14);
        objDirE.RECON_COMPLETED_YP__c = System.Today().addDays(-14);
        objDirE.SALES_PAPERWORK_DUE_PUBLISHING__c = System.Today().addDays(-14);
        objDirE.Sales_Start__c = System.Today().addDays(-14);
        objDirE.Sold_Ad_Renew_Job_1__c = System.Today().addDays(-14);
        objDirE.Sold_Ad_Renew_Job_2__c = System.Today().addDays(-14);
        objDirE.SPECIALTY_ITEM_LIST_SENT_DELIVERY_CORDIN__c = System.Today().addDays(-14);
        objDirE.SPECIALTY_ITEMS_DUE_PPM__c = System.Today().addDays(-14);
        objDirE.SPECIALTY_ITEMS_NOTIFICATION_DUE_SALES1__c = System.Today().addDays(-14);
        objDirE.SPECIALTY_ITEMS_SOLD__c = System.Today().addDays(-14);
        objDirE.STAGE_1_DUE_DCS_COVER_DESIGN__c = System.Today().addDays(-14);
        objDirE.STAGE1_DUE_DCS_COVER_SPREAD__c = System.Today().addDays(-14);
        objDirE.STAGE1_DUE_DCS_PRELIMS__c = System.Today().addDays(-14);
        objDirE.STAGE1_DUE_PPM_COVER_SPREAD__c = System.Today().addDays(-14);
        objDirE.STAGE1_DUE_PPM_TELCO_COVER_SPREAD__c = System.Today().addDays(-14);
        objDirE.STAGE_1_DUE_PPM_FROM_DCS_COVER_DESIGN1__c = System.Today().addDays(-14);
        objDirE.STAGE_1_DUE_PPM_FROM_DCS_PRELIMS__c = System.Today().addDays(-14);
        objDirE.STAGE_1_DUE_TELCO_COVER_DESIGN__c = System.Today().addDays(-14);
        objDirE.STAGE1_DUE_TELCO_PRELIMS__c = System.Today().addDays(-14);
        objDirE.STAGE_2A_DUE_DCS_COVER_DESIGN__c = System.Today().addDays(-14);
        objDirE.STAGE2A_DUE_DCS_PRELIMS__c = System.Today().addDays(-14);
        objDirE.STAGE_2A_DUE_PPM_FROM_DCS_COVER_DESIG1__c = System.Today().addDays(-14);
        objDirE.STAGE_2A_DUE_PPM_FROM_DCS_PRELIMS__c = System.Today().addDays(-14);
        objDirE.STAGE_2A_DUE_PPM_FROM_TELCO_COVER_DESIG1__c = System.Today().addDays(-14);
        objDirE.STAGE_2A_DUE_PPM_FROM_TELCO_PRELIMS__c = System.Today().addDays(-14);
        objDirE.STAGE_2A_DUE_TELCO_COVER_DESIGN__c = System.Today().addDays(-14);
        objDirE.STAGE2A_DUE_TELCO_PRELIMS__c = System.Today().addDays(-14);
        objDirE.STAGE2B_DUE_DCS_PRELIMS__c = System.Today().addDays(-14);
        objDirE.STAGE2B_DUE_PPM_PRELIMS__c = System.Today().addDays(-14);
        objDirE.STAGE_2B_DUE_PPM_FROM_TELCO_COVER_DESIG1__c = System.Today().addDays(-14);
        objDirE.STAGE_2B_DUE_PPM_FROM_TELCO_PRELIMS__c = System.Today().addDays(-14);
        objDirE.STAGE2_DUE_DCS_COVER_SPREAD__c = System.Today().addDays(-14);
        objDirE.STAGE2_DUE_PPM_TELCO_COVER_SPREAD__c = System.Today().addDays(-14);
        objDirE.STAGE3_DUE_DCS_COVER_SPREAD__c = System.Today().addDays(-14);
        objDirE.STAGE3_DUE_DCS_PRELIMS__c = System.Today().addDays(-14);
        objDirE.STAGE_3_DUE_PPM_FROM_TELCO_PRELIMS__c = System.Today().addDays(-14);
        objDirE.TWO_THIRDS_MARK__c = System.Today().addDays(-14);
        
        objDirE.Setup_Cost__c = 100;
        objDirE.Printing_Cost__c = 200;
        objDirE.Other_Cost__c = 300;
        
        Update ObjdirEditionLst;
      
        Billing_Settlement__c objBS = new Billing_Settlement__c();
        objBS.Name = 'Test Billing Settlement';
        objBS.Directory__c = objDir.Id;
        objBS.Directory_Edition__c = objDirE.Id;
        objBS.Telco_Cost_Share__c = 10;
        Insert objBS;
        
       Set<Id> Id = new set<Id>();
       Id.add(objDirE.Id);
       
       try{
        BillbackInvoiceSetllementController Billback = new BillbackInvoiceSetllementController();
        Billback.billbackInsert(Id );
        } catch(exception e){}
        
        try{
        DirectoryFlipBookCommonMethods.doDirectoryFlipBooKProcess(objdirLst);
        DirectoryFlipBookCommonMethods.updateBookstatus(ObjdirEditionLst);
        } catch(exception e){}
        
        ApexPages.StandardController sc1 = new ApexPages.standardController(new Directory__c()); 
        DirectoryFlipbookRun dirFlip = new DirectoryFlipbookRun(sc1);
        dirFlip.userinputName='Test';
        dirFlip.doRun();
        dirFlip.doSearch();
        
    }
}