@isTest(seeAllData = true)
public class CreateTalusAccountTest {
    public static testmethod void createTalusacctest() {
        List < Id > AccountIds = new List < Id > ();
        Id accMngrId = System.Label.TestUserPerformanceAdvisorId;

        Account a1 = new Account(name = 'TestingHttp', Phone = '(999) 999-9999', Billing_Phone__c = '(999) 999-9999', Is_Active__c = true,
            BillingStreet = 'TestBillingStreet', BillingState = 'AZ', BillingPostalCode = '850015656',
            BillingCountry = 'US', Account_Manager__c = accMngrId);

        insert a1;
        AccountIds.add(a1.Id);

        Account a2 = new Account(name = 'Test Name 2', Phone = '(999) 999-9999', Billing_Phone__c = '(999) 999-9999', Is_Active__c = true,
            BillingStreet = 'TestBillingStreet', BillingState = 'AZ', BillingPostalCode = '850015656',
            BillingCountry = 'US');

        insert a2;
        AccountIds.add(a2.Id);

        Account a3 = new Account(name = 'Test Name 2', Phone = '(999) 999-9999', Billing_Phone__c = '(999) 999-9999', Is_Active__c = true,
            BillingStreet = 'TestBillingStreet', BillingState = 'AZ', BillingPostalCode = '850015656',
            BillingCountry = 'US', Account_Manager__c = accMngrId);

        insert a3;
        AccountIds.add(a3.Id);

        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('AccountTestJSON');
        mock.setStatusCode(201);
        mock.setHeader('Content-Type', 'application/json');

        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();
        CreateTalusAccount.createAccTalus(AccountIds, accMngrId);
        Test.stopTest();
    }

    public static testmethod void createTalusacctest1() {
        List < Id > AccountIds = new List < Id > ();
        Id accMngrId = System.Label.TestUserPerformanceAdvisorId;

        Account a1 = new Account(name = 'TestingHttp', Phone = '(999) 999-9999', Billing_Phone__c = '(999) 999-9999', Is_Active__c = true,
            BillingStreet = 'TestBillingStreet', BillingState = 'AZ', BillingPostalCode = '850015656',
            BillingCountry = 'US', Account_Manager__c = accMngrId);

        insert a1;
        AccountIds.add(a1.Id);

        Account a2 = new Account(name = 'Test Name 2', Phone = '(999) 999-9999', Billing_Phone__c = '(999) 999-9999', Is_Active__c = true,
            BillingStreet = 'TestBillingStreet', BillingState = 'AZ', BillingPostalCode = '850015656',
            BillingCountry = 'US');

        insert a2;
        AccountIds.add(a2.Id);

        Account a3 = new Account(name = 'Test Name 2', Phone = '(999) 999-9999', Billing_Phone__c = '(999) 999-9999', Is_Active__c = true,
            BillingStreet = 'TestBillingStreet', BillingState = 'AZ', BillingPostalCode = '850015656',
            BillingCountry = 'US', Account_Manager__c = accMngrId);

        insert a3;
        AccountIds.add(a3.Id);

        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('AccountTestJSON');
        mock.setStatusCode(201);
        mock.setHeader('Content-Type', 'application/json');

        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();
        ApexPages.currentPage().getParameters().put('Id', a1.id);
        ApexPages.StandardController sc1 = new ApexPages.StandardController(a1);
        CreateTalusAccount1Controller ITP1 = new CreateTalusAccount1Controller(sc1);
        ITP1.crtAccnt();
        ApexPages.currentPage().getParameters().put('Id', a2.id);
        ApexPages.StandardController sc2 = new ApexPages.StandardController(a2);
        CreateTalusAccount1Controller ITP2 = new CreateTalusAccount1Controller(sc2);
        ITP2.crtAccnt();
        ApexPages.currentPage().getParameters().put('Id', a3.id);
        ApexPages.StandardController sc3 = new ApexPages.StandardController(a3);
        CreateTalusAccount1Controller ITP3 = new CreateTalusAccount1Controller(sc3);
        ITP3.crtAccnt();
        Test.stopTest();
    }
}