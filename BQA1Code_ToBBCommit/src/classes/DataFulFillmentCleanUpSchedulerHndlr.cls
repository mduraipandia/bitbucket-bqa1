global class DataFulFillmentCleanUpSchedulerHndlr implements DataFulFillmentCleanUpScheduler.DataFulFillmentCleanUpSchedulerInterface {
   // Script to execute DataFulFillmentCleanUpScheduler from Developer Console
    
    
    
   /* DataFulFillmentCleanUpScheduler datacleanup = new DataFulFillmentCleanUpScheduler ();

    String jobID = system.schedule('Data Fulfillment job',  DataFulFillmentCleanUpScheduler.getScheduleString() , datacleanup);

    System.debug('***'+ jobID);  */


 global void execute(SchedulableContext sc) 
 {
    /*try 
        {
            //Abort the existing schedule as this is a 1 time scheduled
            //job and should not run again
            CronTrigger ct = [SELECT id FROM CronTrigger WHERE id = :sc.getTriggerId()];
            if (ct != null)
            {
                System.abortJob(ct.id);
            }
        } 
        catch (Exception e) 
        {
            System.debug('There are no jobs currently scheduled.'); 
        }*/
        DataFulFillmentCleanUpBatchImpl datafullfilment = new DataFulFillmentCleanUpBatchImpl();
        datafullfilment.query= 'SELECT Id, LastModifiedDate, ModificationOrderLineItem__c,OrderLineItemID__c  FROM Digital_Product_Requirement__c WHERE LastModifiedDate < LAST_90_DAYS  and  OrderLineItemID__c = null and ModificationOrderLineItem__c = null';
        ID batchprocessid =Database.executeBatch(datafullfilment,2000);
        System.debug('Returned batch process ID: ' + batchProcessId);
    
  
 }
 
 public static String getScheduleString() {
        Datetime datet = Datetime.now();
        datet = datet.addHours(0);
        datet = datet.addMinutes(0);
        datet = datet.addSeconds(5);
        String scheduleString = '';
        scheduleString += '' + datet.second();
        scheduleString += ' '+ datet.minute();
        scheduleString += ' '+ datet.hour();
        scheduleString += ' '+ datet.day();
        scheduleString += ' '+ datet.month();
        scheduleString += ' ?';
        scheduleString += ' '+ datet.year();
        return scheduleString;
    }

   
}