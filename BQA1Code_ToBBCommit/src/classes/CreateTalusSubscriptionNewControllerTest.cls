@isTest(seeAllData = true)
public class CreateTalusSubscriptionNewControllerTest {
    static testmethod void createTSDff_OL() {

        Map <String, String> mapPrdt = new Map<String, String>{'LocalVox' => Label.LocalVox_RT_Id, 'Yodle SEM' => Label.Yodle_SEM_RT_Id, 'Marketing Messenger' => Label.Marketing_Messenger_RT_Id, 'iYP Bronze' => Label.YPC_Record_Type_Id};
        List<Product2> lstPrdts = new List<Product2>();
        List<Order_Line_Items__c> lstOLns = new List<Order_Line_Items__c>();
        List<Digital_Product_Requirement__c> lstDffs = new List<Digital_Product_Requirement__c>();
        
        Package_ID__c pkg = CommonUtility.createPkgTest();
        insert pkg;

        Package_Product__c pkPrdt = CommonUtility.createPkgPrdtTest(pkg);
        insert pkPrdt;

        instPrpts(pkPrdt.Id);

        Product2 prdt = CommonUtility.createPrdtTest();
        prdt.Product_Type__c = 'Marketing Messenger';
        insert prdt;

        Account acct = TestMethodsUtility.createAccount('telco');
        Telco__c telco = TestMethodsUtility.createTelco(acct.Id);
        Canvass__c c = TestMethodsUtility.createCanvass(telco);

        Account a = CommonUtility.createAccountTest(c);
        a.TalusAccountId__c = 'afdfd132323';
        insert a;

        Contact cnt = CommonUtility.createContactTest(a);
        insert cnt;

        Opportunity oppty = CommonUtility.createOpptyTest(a);
        insert oppty;

        Order__c ord = CommonUtility.createOrderTest(a);
        insert ord;

        Order_Group__c ordGrp = CommonUtility.createOGTest(a, ord, oppty);
        insert ordGrp;

        Order_Line_Items__c OLI = CommonUtility.createOLITest(c, a, cnt, oppty, ord, ordGrp);
        OLI.Product2__c = prdt.Id;
        OLI.Effective_Date__c = system.Today();
        OLI.Parent_ID__c = TestMethodsUtility.generateRandomString();
        insert OLI;

        Digital_Product_Requirement__c dff = CommonUtility.createDffTest();
        dff.UDAC__c = 'TMM10';
        dff.Talus_Subscription_Id__c = '';
        dff.Talus_DFF_Id__c = '';
        dff.recordTypeId = Label.Marketing_Messenger_RT_Id;
        dff.OrderLineItemID__c = OLI.Id;
        dff.Account__c = a.Id;
        dff.DFF_Product__c = prdt.Id;
        dff.Status__c = '';
        dff.hours__c = '00:30,03:00;01:00,10:30;04:30,10:30;01:30,08:30;02:00,09:30;null;null';
        insert dff;

        //callPgRef(dff);

        Order_Group__c ordGrp1 = CommonUtility.createOGTest(a, ord, oppty);
        insert ordGrp1;

        for(String s: mapPrdt.keySet()){

            Product2 prdt1 = CommonUtility.createPrdtTest();
            prdt1.Product_Type__c = s;
            lstPrdts.add(prdt1);

        }

        insert lstPrdts;

        for(Product2 p: lstPrdts){

            Order_Line_Items__c Oln1 = CommonUtility.createOLITest(c, a, cnt, oppty, ord, ordGrp1);
            Oln1.Product2__c = p.Id;
            Oln1.Product_Type__c = p.Product_Type__c;
            Oln1.Effective_Date__c = system.Today();
            Oln1.Parent_ID__c = TestMethodsUtility.generateRandomString();
            lstOLns.add(Oln1);

        }

        insert lstOLns;

        for(Order_Line_Items__c o: lstOLns){

            Digital_Product_Requirement__c dff1 = CommonUtility.createDffTest();
            dff1.Talus_Subscription_Id__c = '';
            dff1.Talus_DFF_Id__c = '';
            dff1.recordTypeId = mapPrdt.get(o.Product_Type__c);
            dff1.OrderLineItemID__c = o.Id;
            dff1.Account__c = a.Id;
            dff1.DFF_Product__c = o.Product2__c;
            dff1.Status__c = 'Complete';
            dff1.hours__c = '00:30,03:00;01:00,10:30;04:30,10:30;01:30,08:30;02:00,09:30;null;null';
            lstDffs.add(dff1);            

        }

        insert lstDffs;

        Test.startTest();
        System.assertEquals('Complete', lstDffs[0].Status__c);
        callPgRef(lstDffs[0]);

        Test.stopTest();

    }

    static testmethod void createTSDff_MOL() {
        /*
        Map <String, String> mapPrdt = new Map<String, String>{'LocalVox' => Label.LocalVox_RT_Id, 'Yodle SEM' => Label.Yodle_SEM_RT_Id, 'Marketing Messenger' => Label.Marketing_Messenger_RT_Id, 'iYP Bronze' => Label.YPC_Record_Type_Id};
        List<Product2> lstPrdts = new List<Product2>();
        List<Order_Line_Items__c> lstOLns = new List<Order_Line_Items__c>();
        List<Modification_Order_Line_Item__c> lstMOlns = new List<Modification_Order_Line_Item__c>();
        List<Digital_Product_Requirement__c> lstMDffs = new List<Digital_Product_Requirement__c>();

        Test.startTest();
        
        Package_ID__c pkg = CommonUtility.createPkgTest();
        insert pkg;

        Package_Product__c pkPrdt = CommonUtility.createPkgPrdtTest(pkg);
        insert pkPrdt;

        instPrpts(pkPrdt.Id);

        Product2 prdt = CommonUtility.createPrdtTest();
        prdt.Product_Type__c = 'Marketing Messenger';
        insert prdt;

        Canvass__c c = CommonUtility.createCanvasTest();
        insert c;

        Account a = CommonUtility.createAccountTest(c);
        a.TalusAccountId__c = 'afdfd132323';
        insert a;

        Contact cnt = CommonUtility.createContactTest(a);
        insert cnt;

        Opportunity oppty = CommonUtility.createOpptyTest(a);
        insert oppty;

        Order__c ord = CommonUtility.createOrderTest(a);
        insert ord;

        Order_Group__c ordGrp = CommonUtility.createOGTest(a, ord, oppty);
        insert ordGrp;

        Order_Line_Items__c OLI = CommonUtility.createOLITest(c, a, cnt, oppty, ord, ordGrp);
        OLI.Product2__c = prdt.Id;
        OLI.Effective_Date__c = system.Today();
        OLI.Parent_ID__c = TestMethodsUtility.generateRandomString();
        insert OLI;

        Modification_Order_Line_Item__c mOLI = CommonUtility.createMOLITest(c, a, cnt, oppty, ord, ordGrp);
        mOLI.Order_Line_Item__c = OLI.Id;
        insert mOLI;

        Digital_Product_Requirement__c dff = CommonUtility.createDffTest();
        dff.UDAC__c = 'TMM10';
        dff.Talus_Subscription_Id__c = '';
        dff.Talus_DFF_Id__c = '';
        dff.recordTypeId = Label.Marketing_Messenger_RT_Id;
        dff.ModificationOrderLineItem__c = mOLI.Id;
        dff.Account__c = a.Id;
        dff.DFF_Product__c = prdt.Id;
        dff.Status__c = '';
        dff.hours__c = '00:30,03:00;01:00,10:30;04:30,10:30;01:30,08:30;02:00,09:30;null;null';
        insert dff;

        callPgRef(dff);

        Order_Group__c ordGrp1 = CommonUtility.createOGTest(a, ord, oppty);
        insert ordGrp1;

        for(String s: mapPrdt.keySet()){

            Product2 prdt1 = CommonUtility.createPrdtTest();
            prdt1.Product_Type__c = s;
            lstPrdts.add(prdt1);

        }

        insert lstPrdts;

        for(Product2 p: lstPrdts){

            Order_Line_Items__c Oln1 = CommonUtility.createOLITest(c, a, cnt, oppty, ord, ordGrp1);
            Oln1.Product2__c = p.Id;
            Oln1.Product_Type__c = p.Product_Type__c;
            Oln1.Effective_Date__c = system.Today();
            Oln1.Parent_ID__c = TestMethodsUtility.generateRandomString();
            lstOLns.add(Oln1);

        }

        insert lstOLns;

        for(Order_Line_Items__c o: lstOLns){

            Modification_Order_Line_Item__c mOLI1 = CommonUtility.createMOLITest(c, a, cnt, oppty, ord, ordGrp1);
            mOLI.Product2__c = o.Product2__c;
            mOLI1.Product_Type__c = o.Product_Type__c;
            mOLI1.Order_Line_Item__c = o.Id;
            lstMOlns.add(mOLI1);           

        }

        insert lstMOlns;

        for(Modification_Order_Line_Item__c mOln: lstMOlns){

            Digital_Product_Requirement__c dff1 = CommonUtility.createDffTest();
            dff1.Talus_Subscription_Id__c = '';
            dff1.Talus_DFF_Id__c = '';
            dff1.recordTypeId = mapPrdt.get(mOln.Product_Type__c);
            dff1.ModificationOrderLineItem__c = mOLn.Id;
            dff1.Account__c = a.Id;
            dff1.DFF_Product__c = moln.Product2__c;
            dff1.Status__c = 'Complete';
            dff1.hours__c = '00:30,03:00;01:00,10:30;04:30,10:30;01:30,08:30;02:00,09:30;null;null';
            lstMDffs.add(dff1);

        }

        insert lstMDffs;

        callPgRef(lstMDffs[0]);

        Test.stopTest();
        */
    }

    public static void callPgRef(Digital_Product_Requirement__c dff){

        ApexPages.StandardController sc = new ApexPages.StandardController(dff);
        CreateTalusSubscriptionNewController CTS = new CreateTalusSubscriptionNewController(sc);
        CTS.prpJSON();
        CTS.rtnDff();
        CTS.backToSummary();     

    }

    public static void instPrpts(Id pkPrdt) {

        List < Product_Properties__c > prptsLst = new List < Product_Properties__c > ();
        Product_Properties__c prdtPrpt = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'business_name', Type__c = 'string');
        prptsLst.add(prdtPrpt);
        Product_Properties__c prdtPrpt1 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'affiliations', Type__c = 'stringarray');
        prptsLst.add(prdtPrpt1);
        Product_Properties__c prdtPrpt2 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'hours', Type__c = 'hoursofoperation');
        prptsLst.add(prdtPrpt2);
        Product_Properties__c prdtPrpt3 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'years_in_business', Type__c = 'int');
        prptsLst.add(prdtPrpt3);
        Product_Properties__c prdtPrpt4 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'seniority_date', Type__c = 'date');
        prptsLst.add(prdtPrpt4);
        Product_Properties__c prdtPrpt5 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'graphic_pao', Type__c = 'jsonobj');
        prptsLst.add(prdtPrpt5);
        Product_Properties__c prdtPrpt6 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'coupons', Type__c = 'jsonobjarray');
        prptsLst.add(prdtPrpt6);
        Product_Properties__c prdtPrpt7 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'business_email', Type__c = 'email');
        prptsLst.add(prdtPrpt7);
        Product_Properties__c prdtPrpt8 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'show_location_on_lp', Type__c = 'bool');
        prptsLst.add(prdtPrpt8);
        Product_Properties__c prdtPrpt9 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'cost', Type__c = 'decimal');
        prptsLst.add(prdtPrpt9);
        Product_Properties__c prdtPrpt10 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'graphic_banner', Type__c = 'jsonobj');
        prptsLst.add(prdtPrpt10);
        Product_Properties__c prdtPrpt11 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'graphic_logo', Type__c = 'jsonobj');
        prptsLst.add(prdtPrpt11);
        Product_Properties__c prdtPrpt12 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'additional_tns', Type__c = 'jsonobjarray');
        prptsLst.add(prdtPrpt12);
        Product_Properties__c prdtPrpt13 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'additional_urls', Type__c = 'jsonobjarray');
        prptsLst.add(prdtPrpt13);
        Product_Properties__c prdtPrpt14 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'after_hours_tns', Type__c = 'jsonobjarray');
        prptsLst.add(prdtPrpt14);
        Product_Properties__c prdtPrpt15 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'emergency_tns', Type__c = 'jsonobjarray');
        prptsLst.add(prdtPrpt15);
        Product_Properties__c prdtPrpt16 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'fax_numbers', Type__c = 'jsonobjarray');
        prptsLst.add(prdtPrpt16);
        Product_Properties__c prdtPrpt17 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'mobile_tns', Type__c = 'jsonobjarray');
        prptsLst.add(prdtPrpt17);
        Product_Properties__c prdtPrpt18 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'pager_tns', Type__c = 'jsonobjarray');
        prptsLst.add(prdtPrpt18);
        Product_Properties__c prdtPrpt19 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'toll_free_tns', Type__c = 'jsonobjarray');
        prptsLst.add(prdtPrpt19);
        Product_Properties__c prdtPrpt20 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'vanity_tns', Type__c = 'jsonobjarray');
        prptsLst.add(prdtPrpt20);
        Product_Properties__c prdtPrpt21 = new Product_Properties__c(Package_Product__c = pkPrdt, Name__c = 'additional_emails', Type__c = 'jsonobjarray');
        prptsLst.add(prdtPrpt21);

        insert prptsLst;

    }
}