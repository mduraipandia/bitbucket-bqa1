/*****************************************************************
Apex Class to send cancellations for Talus, Manual, and Yodle Dffs
Created By: Reddy(reddy.yellanki@theberrycompany.com)
Updated Date: 08/14/2015
$Id$
******************************************************************/
public class DeleteTalusSubscription {

    //Get nigel endpoint url from custom Label
    public static final string url = Label.Talus_Nigel_Url;

    public static String fUrl;
    public static String Responsestring;
    public static List < Order_Line_Items__c > lstOLN = new List < Order_Line_Items__c > ();
    public static List < Digital_Product_Requirement__c > lstDFF = new List < Digital_Product_Requirement__c > ();
    public static List < ErrorLog__c > eLog = new List < ErrorLog__c > ();

    public static List < Order_Line_Items__c > flmntOLs = new List < Order_Line_Items__c > ();
    public static List < Order_Line_Items__c > mnlOLs = new List < Order_Line_Items__c > ();
    public static List < Order_Line_Items__c > ydlOLs = new List < Order_Line_Items__c > ();
    public static List < Order_Line_Items__c > prntAdnOLs = new List < Order_Line_Items__c > ();

    //All Fulfillment realted RecordTypes
    public static Set < String > talusRcdTyp = CommonUtility.talusDffRT();
    public static Set < String > gnralRcdTyp = CommonUtility.manualDffRT();
    public static Set < String > yodleRcdTyp = CommonUtility.yodleDffRT();
    public static Set < String > milesRcdTyp = CommonUtility.milesDffRT();

    //List to hold all Yodle Cancllation emails
    public static List < Messaging.SingleEmailMessage > lstEmails = new List < Messaging.SingleEmailMessage > ();

    //Future method to send Subscription/Product Cancellations Invoked from OLI Trigger
    @future(callout = true)
    public static void deleteSubscriptionFuture(Set < Id > ordLns) {

        List < Order_Line_Items__c > fnlLst = new List < Order_Line_Items__c > ();

        List < Order_Line_Items__c > allOLns = [SELECT id, Parent_ID__c, Cutomer_Cancel_Date__c, Cancellation__c, Order__r.Account__r.TalusAccountId__c, Digital_Product_Requirement__r.Id, Digital_Product_Requirement__r.OwnerId, Digital_Product_Requirement__r.Final_Status__c, Digital_Product_Requirement__r.Talus_Subscription_Id__c,
            Digital_Product_Requirement__r.Submit_Yodle_Cancel__c, Digital_Product_Requirement__r.Contact__c, Digital_Product_Requirement__r.Fulfillment_Submit_Status__c, Digital_Product_Requirement__r.Customer_Cancel_Date__c, Digital_Product_Requirement__r.Bundle__c, Spotzer_Bundle__c, Action_Code__c,
            Digital_Product_Requirement__r.Enterprise_Customer_ID__c, Digital_Product_Requirement__r.UDAC__c, Digital_Product_Requirement__r.business_name__c, Digital_Product_Requirement__r.business_email__c, Digital_Product_Requirement__r.business_url__c, Digital_Product_Requirement__r.business_phone_number_office__c,
            Digital_Product_Requirement__r.business_address1__c, Digital_Product_Requirement__r.business_city__c, Digital_Product_Requirement__r.business_state__c, Digital_Product_Requirement__r.business_postal_code__c, Digital_Product_Requirement__r.CreatedById, Billing_Contact__c, Digital_Product_Requirement__r.Effective_Date__c,
            Digital_Product_Requirement__r.Talus_DFF_Id__c, Status__c, Digital_Product_Requirement__c, Digital_Product_Requirement__r.Name, Digital_Product_Requirement__r.RecordTypeId, Order_Group__r.oli_count__c, Digital_Product_Requirement__r.RecordType.DeveloperName, Digital_Product_Requirement__r.Fulfillment_Type__c from Order_Line_Items__c where id IN: ordLns
            and Cutomer_Cancel_Date__c <= today and Status__c not in ('Cancelation Requested', 'Cancelled') and media_type__c = 'Digital'
            and Product_Type__c Not In('General Add-on')
            limit 50000
        ];

        //Passing Queried List to parentAddonOLI method to seperate records based on Bundle and Non-Bundle logic
        fnlLst = DeleteTalusSubscription.parentAddonOLIs(allOLns);

        if (fnlLst.size() > 0) {
            olRcds(fnlLst);
        }

    }

    //Non-future method to send Subscription/Product Cancellation Invoked from MOLI Batch Class
    public static void deleteSubscriptionNonFuture(List < Order_Line_Items__c > LstOlns) {

        if (lstOlns.size() > 0) {
            olRcds(LstOlns);
        }

    }

    public static void olRcds(List < Order_Line_Items__c > allOlns) {

        for (Order_Line_Items__c iterator: allOLns) {
            talusRcdTyp.addAll(milesRcdTyp);
            if (talusRcdTyp.contains(iterator.Digital_Product_Requirement__r.RecordType.DeveloperName) && String.isNotBlank(iterator.Digital_Product_Requirement__r.Talus_Subscription_Id__c) && iterator.Digital_Product_Requirement__r.Fulfillment_Type__c == 'Talus' && String.isNotBlank(iterator.Digital_Product_Requirement__r.Talus_DFF_Id__c)) {
                flmntOLs.add(iterator);
            }
            if (gnralRcdTyp.contains(iterator.Digital_Product_Requirement__r.RecordType.DeveloperName) && iterator.Digital_Product_Requirement__r.Fulfillment_Type__c == 'Manual') {
                mnlOLs.add(iterator);
            }
            if (yodleRcdTyp.contains(iterator.Digital_Product_Requirement__r.RecordType.DeveloperName) && iterator.Digital_Product_Requirement__r.Fulfillment_Type__c == 'Yodle') {
                ydlOLs.add(iterator);
            }
        }

        System.debug('************TalusDffs: ' + flmntOLs + '************ManualDffs: ' + mnlOLs + '************YodleDffs: ' + ydlOLs);

        //Calling flmntOLICancel method, if Colony Logic related records exists
        if (flmntOLs.size() > 0) {
            flmntOLICancel(flmntOLs);
        }

        //Calling manualOLICancel method, if Manula related records exists
        if (mnlOLs.size() > 0) {
            manualOLICancel(mnlOLs);
        }

        //Calling yodleOLICancel mehtod, if Yodle related records exists
        if (ydlOLs.size() > 0) {
            yodleOLICancel(ydlOLs);
        }

        if (!Test.isRunningTest()) {
            //Database.SaveResult[] svRslt = Database.update(lstDFF, false);
            Database.update(lstDFF, false);
            Database.update(lstOLN, false);

            if (lstEmails.size() > 0) {
                Messaging.sendEmail(lstEmails);
            }

            if (eLog.size() > 0) {
                insert eLog;
            }
        }

    }

    //Method to send Cancellations for Colony Logic Fulfilled Products
    public static void flmntOLICancel(List < Order_Line_Items__c > oLI) {

        for (Order_Line_Items__c iterator: oLI) {

            if (iterator.Spotzer_Bundle__c != true) {

                if (iterator.Cutomer_Cancel_Date__c <= system.today()) {

                    System.debug('************Infor-Sending Delete************');

                    if (String.isNotBlank(iterator.Parent_ID__c)) {
                        fUrl = url + iterator.Order__r.Account__r.TalusAccountId__c + '/subscriptions/' + iterator.Digital_Product_Requirement__r.Talus_Subscription_Id__c + '/';
                    } else {
                        fUrl = url + iterator.Order__r.Account__r.TalusAccountId__c + '/subscriptions/' + iterator.Digital_Product_Requirement__r.Talus_Subscription_Id__c + '/subscription_products/' + iterator.Digital_Product_Requirement__r.Talus_DFF_Id__c + '/';
                    }
                    //System.debug('************FURL************' + fUrl);
                    try {
                        HttpResponse res = new HttpResponse();
                        res = TalusRequestUtilityDelete.initiateRequest(fUrl, iterator.Id);
                        //Responsestring = TalusRequestUtilityDelete.initiateRequest(fUrl, iterator.Id);
                        System.debug('**********ResponseCode************' + res.getStatusCode() + '************ResponseString************' + res.getBody());

                        if (res.getStatusCode() == 204) {
                            iterator.Status__c = 'Cancelation Requested';
                            lstOLN.add(iterator);
                        } else {
                            eLog.add(new ErrorLog__c(Error_Message__c = res.getBody(), FailedSFDCID__c = iterator.Id, ResponseHeader__c = Integer.valueof(res.getStatusCode()) + ' ************ Failed in Dff Cancellation'));
                            //ErrorLog.CreateErrorLog(res.getBody(), iterator.Digital_Product_Requirement__c, Integer.valueof(res.getStatusCode()), 'DFF Cancellation');
                        }
                    } catch (exception e) {
                        eLog.add(new ErrorLog__c(Error_Message__c = e.getmessage(), FailedSFDCID__c = iterator.Id, ResponseHeader__c = 'NULL ************ Exception in Dff Cancellation'));
                        //ErrorLog.CreateErrorLog(e.getmessage(), iterator.Digital_Product_Requirement__c, NULL, 'Exception Found in DFF Cancellation');
                    }
                }
                /*
                else {

                    System.debug('************Infor-Sending Patch************');

                    fUrl = url + iterator.Order__r.Account__r.TalusAccountId__c + '/subscriptions/' + iterator.Digital_Product_Requirement__r.Talus_Subscription_Id__c + '/subscription_products/' + iterator.Digital_Product_Requirement__r.Talus_DFF_Id__c + '/patch/';

                    List < Track_DFF_Update__c > trkDffs = new List < Track_DFF_Update__c > ();

                    JSONGenerator jsonGen = JSON.createGenerator(true);
                    String newJsonString;

                    //Starting JSON Generator
                    jsonGen.writeStartObject();
                    jsonGen.writeStringField('patch_notes', 'SFDC wants to update below fields infrormation');
                    jsonGen.writeFieldName('patch_update');
                    jsonGen.writeStartObject();

                    Datetime myDate = date.newinstance(iterator.Cutomer_Cancel_Date__c.year(), iterator.Cutomer_Cancel_Date__c.month(), iterator.Cutomer_Cancel_Date__c.day());
                    String newDate2 = String.valueof(myDate.format('yyyy-MM-dd', 'UTC'));

                    jsonGen.writeStringField('scheduled_end_date', newDate2);
                    jsonGen.writeEndObject();

                    //This field is to show user on what values have been changed on DFF
                    String trackInfo = 'scheduled_end_date: ' + newDate2;

                    //Ending JSON Generator
                    jsonGen.writeEndObject();

                    //Retrieve formed JSON data in a string format, and make sure to use this method only once
                    newJsonString = jsonGen.getAsString();

                    system.debug('************PATCH JSON DATA FINAL************' + newJsonString);

                    if (String.isNotBlank(newJsonString)) {

                        HttpResponse newRes = CommonUtility.postUtil(newJsonString, fUrl, String.valueof('POST'));

                        String newResponsestring = newRes.getBody();

                        system.debug('************Response************' + newResponsestring + '----------' + newRes.getStatusCode() + '-----' + newRes.getStatus());

                        String JSONContent = newResponsestring;

                        JSONParser parser = JSON.createParser(JSONContent);
                        parser.nextToken();
                        parser.nextValue();
                        String fieldName = parser.getCurrentName();
                        String fieldValue = parser.getText();

                        try {

                            if (Test.isRunningTest()) {
                                newRes.setStatusCode(202);
                            }
                            if (newRes != NULL && (newRes.getStatusCode() == 202 || newRes.getStatusCode() == 200)) {

                                if (newRes.getStatusCode() == 202) {

                                    //Create TrackDFFUpdate record for user to track each update
                                    Track_DFF_Update__c tDff = new Track_DFF_Update__c();
                                    tDff.Data_Fulfillment_Form__c = iterator.Digital_Product_Requirement__c;
                                    tDff.Sent_Date__c = system.today();
                                    tDff.Status__c = 'In Progress';
                                    tDff.Record_Id__c = fieldValue;
                                    tDff.Updated_Info__c = trackInfo;

                                    trkDffs.add(tDff);

                                    //System.debug('************TrackDFF************' + trkDffs);

                                    iterator.Status__c = 'Cancellation Requested';

                                    lstOLN.add(iterator);

                                } else {

                                    //create TrackDFFUpdate record for user to track each update
                                    Track_DFF_Update__c tDff = new Track_DFF_Update__c();
                                    tDff.Data_Fulfillment_Form__c = iterator.Digital_Product_Requirement__c;
                                    tDff.Sent_Date__c = system.today();
                                    tDff.Status__c = 'Applied';
                                    tDff.Updated_Info__c = trackInfo;

                                    trkDffs.add(tDff);

                                    //System.debug('************TrackDFF************' + trkDffs);
                                    iterator.Status__c = 'Cancellation Requested';
                                    lstOLN.add(iterator);

                                }

                            }

                        } catch (exception e) {

                            ErrorLog.CreateErrorLog(e.getmessage(), iterator.Digital_Product_Requirement__c, newRes.getStatusCode(), 'DFF Cancelation');

                        }

                    }

                    if (trkDffs.size() > 0) {
                        insert trkDffs;
                    }
                }
                */
            }
        }

    }

    //Method to send Cancellation for Manual Fulfilled Products
    public static void manualOLICancel(List < Order_Line_Items__c > oLI) {

        //Get Queue Ids from custom settings
        Map < String, DFF_Manual_Queue_Ids__c > dffMnlQId = DFF_Manual_Queue_Ids__c.getAll();
        List < DFF_Manual_Queue_Ids__c > lstVals = dffMnlQId.Values();
        Map < String, String > mpVals = new Map < String, String > ();
        for (DFF_Manual_Queue_Ids__c iterator: lstVals) {
            mpVals.put(iterator.Name, iterator.queue_Id__c);
        }

        System.debug('************InFor-ManualCancel************');

        for (Order_Line_Items__c iterator: oLI) {

            if (iterator.Digital_Product_Requirement__r.Fulfillment_Submit_Status__c != 'Complete') {

                //Update Order Line Item status to Cancelled if Dff is not Submitted for initial fulfillment 
                iterator.Status__c = 'Cancelled';

            } else {

                //Update Dff status to cancellation requested
                iterator.Digital_Product_Requirement__r.Final_Status__c = 'Cancelation Requested';

                //Assigning owner to the queue
                if (mpVals.containsKey(iterator.Digital_Product_Requirement__r.RecordType.DeveloperName)) {
                    iterator.Digital_Product_Requirement__r.OwnerId = mpVals.get(iterator.Digital_Product_Requirement__r.RecordType.DeveloperName);
                }

                lstDff.add(iterator.Digital_Product_Requirement__r);

                //Update Order Line Item status to Cancellatoin Requested if Dff is already submitted for initial fulfillment
                iterator.Status__c = 'Cancelation Requested';

            }
            //System.debug('************OLIStatus************' + iterator.Status__c);
            lstOln.add(iterator);

        }

    }

    //Method to send Cancellations for Yodle Fulfilled Products
    public static void yodleOLICancel(List < Order_Line_Items__c > oLI) {
        String mailBody;
        System.debug('************INFor-YodleCancel************');

        for (Order_Line_Items__c iterator: oLI) {

            if (iterator.Digital_Product_Requirement__r.Fulfillment_Submit_Status__c != 'Complete') {

                //Update Order Line Item status to Cancelled if Dff is not Submitted for initial fulfillment 
                iterator.Status__c = 'Cancelled';
                lstOln.add(iterator);

            } else {

                //Update Dff for Yodle related fulfillment cancellation
                if (iterator.Digital_Product_Requirement__r.Submit_Yodle_Cancel__c != true) {
                    iterator.Digital_Product_Requirement__r.OwnerId = Label.Yodle_Queue_Id;
                    iterator.Digital_Product_Requirement__r.Submit_Yodle_Cancel__c = true;
                    lstDff.add(iterator.Digital_Product_Requirement__r);
                }

                //Update Order Line Item status to Cancellation Requested
                if (iterator.Status__c != 'Cancellation Requested') {
                    iterator.Status__c = 'Cancelation Requested';
                    lstOln.add(iterator);
                }
                //System.debug('************OLIStatus************' + iterator.Status__c);

                List < String > yodleEmails = Label.Yodle_Email_Ids.split(',');
                List < String > yodleEmailsExternal = Label.Yodle_Email_Ids_External.split(',');

                //Email formation to send Yodle Cancellations
                for (String trgtObjId: yodleEmails) {
                    lstEmails.add(CommonUtility.emailUsingTargetObjectId(trgtObjId, 'Yodle Fulfillment Cancellation Information for' + iterator.Digital_Product_Requirement__r.Name, SubmitToYodleaAndGeneral.ydlCnclBody(iterator.Digital_Product_Requirement__r)));
                }

                lstEmails.add(CommonUtility.emailUsingEmailId(yodleEmailsExternal, 'Yodle Fulfillment Cancellation Information for' + iterator.Digital_Product_Requirement__r.Name, SubmitToYodleaAndGeneral.ydlCnclBody(iterator.Digital_Product_Requirement__r)));
                /*
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                List < String > yodleEmails = Label.Yodle_Email_Ids.split(',');
                mail.setToAddresses(yodleEmails);
                mail.setToAddresses(yodleEmails);
                mail.setSubject('Yodle Fulfillment Cancellation Information for' + iterator.Digital_Product_Requirement__r.Name);
                mailBody = SubmitToYodleaAndGeneral.ydlCnclBody(iterator.Digital_Product_Requirement__r);
                mail.setHtmlBody(mailBody);
                //Add all Yodle emails in a List
                lstEmails.add(mail);
                //Messaging.sendEmail(new Messaging.SingleEmailMessage[] {
                //mail
                //});
                */
            }

        }

    }

    //Method to filter Parent and Add-on OLIs based on Bundle logic
    public static List < Order_Line_Items__c > parentAddonOLIs(List < Order_Line_Items__c > oLs) {

        Map < Id, List < Order_Line_Items__c >> mapOrdSetOl = new Map < Id, List < Order_Line_Items__c >> ();
        Set < Id > OlIds = new Set < Id > ();
        List < Order_Line_Items__c > fnlLst = new List < Order_Line_Items__c > ();

        //Separate Order Line Items by OrderGroup
        for (Order_Line_Items__c iterator: oLs) {
            if (mapOrdSetOl.containsKey(iterator.Order_Group__c)) {
                mapOrdSetOl.get(iterator.Order_Group__c).add(iterator);
            } else {
                List < Order_Line_Items__c > allOls = new List < Order_Line_Items__c > ();
                allOls.add(iterator);
                mapOrdSetOl.put(iterator.Order_Group__c, allOls);
            }
        }

        //For each OrderGroup, separate Order Line Items with Bundle and Non-Bundle logic
        for (Id iterator: mapOrdSetOl.keySet()) {
            List < Order_Line_Items__c > lstOL1 = mapOrdSetOl.get(iterator);
            List < Order_Line_Items__c > lstOL2 = new List < Order_Line_Items__c > ();
            boolean flag = false;
            for (Order_Line_Items__c ol: lstOL1) {
                if (String.isBlank(ol.Digital_Product_Requirement__r.Bundle__c)) {
                    fnlLst.add(ol);
                } else if (ol.Digital_Product_Requirement__r.Bundle__c.Contains('BASE')) {
                    fnlLst.add(ol);
                    flag = true;
                } else {
                    lstOL2.add(ol);
                }
            }
            if (!flag) {
                fnlLst.addAll(lstOL2);
            }
        }

        return fnlLst;

    }

}