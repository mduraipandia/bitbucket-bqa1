public class PortalHomePageController{

public Boolean  termsAndConditions ;
public User userInformation;

public PortalHomePageController()
{
    termsAndConditions =false;
}

public PageReference portalHome()
{
     String uid = UserInfo.getUserId();
     String uname = UserInfo.getUsername();
     System.debug('User====='+uid);
     if(uid != null && uid != ''){
         userInformation  = [SELECT u.Id,u.name, u.Terms_And_Conditions_Agreed__c FROM User u WHERE u.Id =: uid  LIMIT 1];
         System.debug('userInformation  ====='+userInformation  );
         termsAndConditions = userInformation.Terms_And_Conditions_Agreed__c ;
                 
         System.debug('termsAndConditions====='+termsAndConditions);
          
     }
     if(termsAndConditions != true )
     {
        //Redirect to the T&C Page
        PageReference reference=new PageReference('/apex/Terms_and_Conditions?ContactId='+ uid);
        reference.setRedirect(true);
        return reference;
     }
     else
     {
         return null;
     }
}
 
}