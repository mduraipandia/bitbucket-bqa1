@IsTest
public class DirectoryEditionSOQLMethodsTest {
    @isTest static void DirectoryEditionSOQLMethodsCoverage() {
        set<id> setId = new set<id>();
        set<id> dirId = new set<id>();
        set<id>directoryIds = new set<id>();
        set<string> setLSACode = new set<string>();
        Date nextBillingDate, pubDate;
        
        //create directoryedition from utility
        Canvass__c newCanvass = TestMethodsUtility.createCanvass();
        Account newAccount = TestMethodsUtility.generateAccount('customer', false);
        newAccount.Primary_Canvass__c = newCanvass.Id;
        insert newAccount;

        Directory__c newDirectory1 = TestMethodsUtility.createDirectory();        
        Directory_Mapping__c newDirectoryMapping = TestMethodsUtility.createDirectoryMapping();
        
        Telco__c newTelco = TestMethodsUtility.createTelco(newAccount.Id);
        Directory_Edition__c newDirectoryEdition = TestMethodsUtility.generateDirectoryEdition(newDirectory1.Id, newAccount.Primary_Canvass__c, newTelco.Id, newDirectoryMapping.Id);        
        
        newDirectoryEdition.Book_Status__c='NI';        
        insert newDirectoryEdition;        
        setId.add(newDirectoryEdition.Id);
        System.assertNotEquals(null,DirectoryEditionSOQLMethods.getCurrentDEByDirectory(setId));
        System.assertNotEquals(null,DirectoryEditionSOQLMethods.getDirEditionByDirIds(directoryIds));
        System.assertNotEquals(null,DirectoryEditionSOQLMethods.getDirEditionByLSACode(setLSACode));
        System.assertNotEquals(null,DirectoryEditionSOQLMethods.getBOTSDEByDirectory(setId));
        DirectoryEditionSOQLMethods.getDirEdByDirPubCodeAndEdCode('unittest','unittest');
        DirectoryEditionSOQLMethods.getDirEditionByLSACodeDirNo('unittest', '0000');
        DirectoryEditionSOQLMethods.getDEMapByIds(setId);
    }
}