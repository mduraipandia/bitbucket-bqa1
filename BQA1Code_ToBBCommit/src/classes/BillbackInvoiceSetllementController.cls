public class BillbackInvoiceSetllementController{
    
    public void billbackInsert(set<Id> deID){
         List<Directory_Edition__c> objDirELst = new List<Directory_Edition__c>();
         objDirELst = [Select Id,Name,Setup_Cost__c,Printing_Cost__c,Other_Cost__c,(select Id,Name,Telco_Cost_Share__c from Billing_Settlements__r limit 1) from Directory_Edition__c where Id IN : deId];
         system.debug('******DEList ******'+objDirELst);
         List<Directory_Edition__c> UpdateobjDirELst = new List<directory_Edition__c>();
         for(Directory_Edition__c objDE : objDirELst){
           for(Billing_Settlement__c objBS : objDE.Billing_Settlements__r){
                system.debug('*****Telco Cost share******'+objBS.Telco_Cost_Share__c);
                if(objDE.setup_Cost__c != null && objBS.Telco_Cost_Share__c != null){
                    objDE.Setup_Cost__c = (objDE.Setup_Cost__c * objBS.Telco_Cost_Share__c)/100;
                     system.debug('******setup cost ******'+objDE.Setup_Cost__c );
                   }
                if(objDE.Printing_Cost__c!= null && objBS.Telco_Cost_Share__c != null){
                    objDE.Printing_Cost__c = (objDE.Printing_Cost__c* objBS.Telco_Cost_Share__c)/100;
                   }
                if(objDE.Other_Cost__c != null && objBS.Telco_Cost_Share__c != null){
                    objDE.Other_Cost__c = (objDE.Other_Cost__c * objBS.Telco_Cost_Share__c)/100;
                   }
        }
         UpdateobjDirELst.add(objDE);
         system.debug('******Update List ******'+UpdateobjDirELst );
    }
        if(UpdateobjDirELst.size()>0){
            upsert UpdateobjDirELst ; 
        } 
   }
}