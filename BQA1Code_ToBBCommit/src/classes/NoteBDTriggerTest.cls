/****************************
Created By : Sathishkumar Periyasamy (speriyasamy2@csc.com)
Created Date : 11/29/2012
Last Modified By : Sathishkumar Periyasamy (speriyasamy2@csc.com)
Last Modidied Date : 11/29/2012 
Orignal Trigger/Controller Name : Note_BD(Trigger)
*****************************/

@IsTest
public class NoteBDTriggerTest {
    static testMethod void PrevenDeleteNoteTest (){    	
    	User u = TestMethodsUtility.generateStandardUser();
        System.runAs(u) {
        	list<Account> lstAccount = new list<Account>();
			lstAccount.add(TestMethodsUtility.generateAccount('customer'));
			insert lstAccount;			
			list<Note> lstNote = new list<Note>();
			for(Account iterator : lstAccount) {
				system.assertNotEquals(iterator.Id, null);
				lstNote.add(new Note(ParentId = iterator.Id, Title = 'Test Note', Body = 'Test Note Body'));
			}
			insert lstNote;
			try {
	            //Delete a Note
	            delete lstNote;
	        }
	        catch (DmlException e) {
	            //Checking the Assert wheather process has passed or not.
	            System.assert( e.getMessage().contains('You do not have rights to delete Account related Notes'), e.getMessage() );
	        }
        }
    }
}