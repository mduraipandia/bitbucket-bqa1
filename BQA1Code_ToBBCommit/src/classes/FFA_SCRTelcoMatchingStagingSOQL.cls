/*
    * FinancialForce.com, inc. claims copyright in this software, its screen display designs and
    * supporting documentation. FinancialForce and FinancialForce.com are trademarks of FinancialForce.com, inc.
    * Any unauthorized use, copying or sale of the above may constitute an infringement of copyright and may
    * result in criminal or other legal proceedings.
    *
    * Copyright FinancialForce.com, inc. All rights reserved.
*/

public with sharing class FFA_SCRTelcoMatchingStagingSOQL 
{
    public static string getStartQuery(Set<id> accountIds, Date matchingDate)
    {
        String query =  
            'SELECT '+
            '   Id, '+
            '   Name, '+
            '   ffps_bmatching__OLI_Line_Number__c, '+
            '   ffps_bmatching__OLI_Product_Name__c, '+
            '   c2g__Product__c, '+
            '   ffps_bmatching__Line_Amount_Paid__c, '+
            '   ffps_bmatching__Line_Reference__c, '+
            '   c2g__DocumentValue__c, '+
            '   c2g__DocumentOutstandingValue__c, '+
            '   c2g__DocumentTaxTotal__c, '+
            '   ffps_bmatching__Line_Outstanding__c, '+
            '   ffps_bmatching__Balance_Due__c, '+
            '   c2g__Transaction__c, '+
            '   c2g__Transaction__r.Name, '+
            '   c2g__Transaction__r.Customer_Name__c, '+
            '   c2g__Account__c, '+
            '   c2g__Account__r.Name, '+
            '   c2g__Transaction__r.c2g__TransactionType__c, '+
            '   c2g__Transaction__r.c2g__DocumentOutstandingTotal__c '+
            'FROM '+
            '   c2g__codaTransactionLineItem__c '+
            'WHERE '+
            '   c2g__LineType__c = \'Analysis\'  '+
            'AND '+
            '   c2g__Transaction__r.c2g__TransactionType__c = \'Credit Note\' '+
            'AND '+
            '   c2g__Account__r.RecordType.Name = :recordTypes '+
            'AND '+
            '   c2g__Transaction__r.c2g__TransactionDate__c <= :matchingDate '+
            'AND '+
            '   c2g__Transaction__r.c2g__DocumentOutstandingTotal__c != 0 '+
            'AND '+
            '   ffps_bmatching__Line_Matching_Status__c = \'Available\' '+         
            'AND '+
            '   ffps_bmatching__Has_Order_Line_Item__c = \'Yes\' ';
            if(!accountIds.isEmpty()) query += 'AND c2g__Account__c IN :accountIds ';
            query += 'Order By c2g__Account__c ASC, c2g__Transaction__r.c2g__TransactionDate__c ASC, c2g__Transaction__r.c2g__DocumentNumber__c ASC, ffps_bmatching__OLI_Line_Number__c ASC ';
        
        return query;   
    }

    public static Map<Id, List<c2g__codaTransactionLineItem__c>> getTransAnalysisLines(Set<String> orderLineNumbers, Set<Id> accountIds, Set<id> customerNameIds, Date matchingDate)
    {
        Map<Id, List<c2g__codaTransactionLineItem__c>> rtnMap = new Map<Id, List<c2g__codaTransactionLineItem__c>>();

        String query = 
            'SELECT '+
            '   Id, '+
            '   Name, '+
            '   c2g__Product__c, '+
            '   c2g__Account__c, '+ 
            '   c2g__DocumentOutstandingValue__c, '+
            '   ffps_bmatching__OLI_Line_Number__c, '+
            '   ffps_bmatching__OLI_Product_Name__c, '+
            '   c2g__DocumentValue__c, '+
            '   ffps_bmatching__Line_Reference__c, '+
            '   c2g__DocumentTaxTotal__c, '+
            '   c2g__Transaction__r.c2g__Period__c, '+
            '   ffps_bmatching__Balance_Due__c, '+
            '   ffps_bmatching__Line_Outstanding__c, '+
            '   ffps_bmatching__Line_Amount_Paid__c, '+
            '   c2g__Transaction__c, '+ 
            '   c2g__Transaction__r.Name, '+
            '   c2g__Transaction__r.c2g__TransactionType__c, '+ 
            '   c2g__Transaction__r.c2g__DocumentOutstandingTotal__c '+
            'FROM '+
            '   c2g__codaTransactionLineItem__c '+
            'WHERE '+
            '   c2g__LineType__c = \'Analysis\' '+
            'AND '+
            '   c2g__Transaction__r.c2g__TransactionType__c = \'Invoice\' '+
            'AND '+
            '   OLI_Line_Number__c IN :orderLineNumbers  '+
            'AND '+
            '   c2g__Account__c IN :accountIds '+
            'AND '+
            '   c2g__Transaction__r.Customer_Name__c IN :customerNameIds '+
            'AND '+
            '   c2g__Transaction__r.c2g__TransactionDate__c <= :matchingDate '+
            'AND '+
            '   c2g__Transaction__r.c2g__DocumentOutstandingTotal__c != 0 '+ 
            'AND '+
            '   ffps_bmatching__Line_Matching_Status__c = \'Available\' ';
            query += 'Order By c2g__Account__c ASC, c2g__Transaction__r.c2g__TransactionDate__c ASC, c2g__Transaction__r.c2g__DocumentNumber__c ASC, ffps_bmatching__OLI_Line_Number__c ASC ';
            query += 'LIMIT 20000 ';

        for(c2g__codaTransactionLineItem__c line :Database.query(query))
        {
            if(rtnMap.containsKey(line.c2g__Transaction__c))
            {
                rtnMap.get(line.c2g__Transaction__c).add(line);
            }
            else
            {
                rtnMap.put(line.c2g__Transaction__c, new List <c2g__codaTransactionLineItem__c> { line });
            }
        }

        return rtnMap;
    }

    public static Map<Id, c2g__codaTransactionLineItem__c> getTransactionInvoiceAccLines(Set<id> accountIds, Set<id> customerNameIds, Set<id> transIds, Date matchingDate)
    {
        Map<Id, c2g__codaTransactionLineItem__c> rtnMap = new Map<Id, c2g__codaTransactionLineItem__c>();

        String query = 
            'SELECT '+
            '   Id, '+
            '   Name, '+
            '   c2g__Product__c, '+
            '   c2g__Account__c, '+ 
            '   c2g__DocumentOutstandingValue__c, '+
            '   ffps_bmatching__OLI_Line_Number__c, '+
            '   ffps_bmatching__OLI_Product_Name__c, '+
            '   c2g__DocumentValue__c, '+
            '   ffps_bmatching__Line_Reference__c, '+
            '   c2g__DocumentTaxTotal__c, '+
            '   c2g__Transaction__r.c2g__Period__c, '+
            '   ffps_bmatching__Balance_Due__c, '+
            '   ffps_bmatching__Line_Outstanding__c, '+
            '   ffps_bmatching__Line_Amount_Paid__c, '+
            '   c2g__Transaction__c, '+ 
            '   c2g__Transaction__r.Name, '+
            '   c2g__Transaction__r.c2g__TransactionType__c, '+ 
            '   c2g__Transaction__r.c2g__DocumentOutstandingTotal__c '+
            'FROM '+
            '   c2g__codaTransactionLineItem__c '+
            'WHERE '+
            '   c2g__LineType__c = \'Account\' '+
            'AND '+
            '   c2g__Transaction__r.c2g__TransactionType__c = \'Invoice\' '+
            'AND '+
            '   c2g__Account__c IN :accountIds '+
            'AND '+
            '   c2g__Transaction__r.Customer_Name__c IN :customerNameIds '+
            'AND '+
            '   c2g__Transaction__r.c2g__TransactionDate__c <= :matchingDate '+
            'AND '+
            '   c2g__Transaction__c IN :transIds ';
            query += 'Order By c2g__Account__c ASC, c2g__Transaction__r.c2g__TransactionDate__c ASC, c2g__Transaction__r.c2g__DocumentNumber__c ASC ';

        for(c2g__codaTransactionLineItem__c line :Database.query(query))
        {
            rtnMap.put(line.c2g__Transaction__c, line);
        }

        return rtnMap;
    }

    public static Map<id, c2g__codaTransactionLineItem__c> getSCRTransactionAccountLines(Set<id> transIds)
    {
        Set<String> lineTypes = new Set<String>{'Account'};
        Map<Id, c2g__codaTransactionLineItem__c> rtnMap = new Map<Id, c2g__codaTransactionLineItem__c>();

        String query = 
            'SELECT '+
            '   Id, '+
            '   Name, '+
            '   ffps_bmatching__OLI_Line_Number__c, '+
            '   c2g__LineType__c, '+
            '   ffps_bmatching__OLI_Product_Name__c, '+
            '   c2g__Product__c, '+
            '   ffps_bmatching__Line_Amount_Paid__c, '+
            '   c2g__DocumentValue__c, '+
            '   ffps_bmatching__Line_Reference__c, '+
            '   c2g__DocumentOutstandingValue__c, '+
            '   c2g__DocumentTaxTotal__c, '+
            '   ffps_bmatching__Line_Outstanding__c, '+
            '   ffps_bmatching__Balance_Due__c, '+
            '   c2g__Transaction__c, '+
            '   c2g__Transaction__r.Name, '+
            '   c2g__Account__c, '+
            '   c2g__Account__r.Name, '+
            '   c2g__Transaction__r.c2g__TransactionType__c, '+
            '   c2g__Transaction__r.c2g__DocumentOutstandingTotal__c '+
            'FROM '+
            '   c2g__codaTransactionLineItem__c '+
            'WHERE '+
            '   c2g__LineType__c IN :lineTypes '+
            'AND '+
            '   c2g__Transaction__r.c2g__TransactionType__c = \'Credit Note\' '+
            'AND '+
            '   c2g__Transaction__c = :transIds ';

        for(c2g__codaTransactionLineItem__c so :Database.query(query))
        {
            rtnMap.put(so.c2g__Transaction__c, so);
        }

        return rtnMap;
    }

    public static string getStartQuery(Id accountId, Id customerName, Date matchingDate)
    {
        String query =  
            'SELECT '+
            '   Id, '+
            '   Name, '+
            '   ffps_bmatching__OLI_Line_Number__c, '+
            '   ffps_bmatching__OLI_Product_Name__c, '+
            '   c2g__Product__c, '+
            '   ffps_bmatching__Line_Amount_Paid__c, '+
            '   ffps_bmatching__Line_Reference__c, '+
            '   c2g__DocumentValue__c, '+
            '   c2g__DocumentOutstandingValue__c, '+
            '   c2g__DocumentTaxTotal__c, '+
            '   ffps_bmatching__Line_Outstanding__c, '+
            '   ffps_bmatching__Balance_Due__c, '+
            '   c2g__Transaction__c, '+
            '   c2g__Transaction__r.Name, '+
            '   c2g__Transaction__r.Customer_Name__c, '+
            '   c2g__Account__c, '+
            '   c2g__Account__r.Name, '+
            '   c2g__Transaction__r.c2g__TransactionType__c, '+
            '   c2g__Transaction__r.c2g__DocumentOutstandingTotal__c '+
            'FROM '+
            '   c2g__codaTransactionLineItem__c '+
            'WHERE '+
            '   c2g__LineType__c = \'Analysis\'  '+
            'AND '+
            '   c2g__Transaction__r.c2g__TransactionType__c = \'Credit Note\' '+
            'AND '+
            '   c2g__Transaction__r.c2g__TransactionDate__c <= :matchingDate '+
            'AND '+
            '   c2g__Transaction__r.c2g__DocumentOutstandingTotal__c != 0 '+
            'AND '+
            '   ffps_bmatching__Line_Matching_Status__c = \'Available\' '+          
            'AND '+
            '   ffps_bmatching__Has_Order_Line_Item__c = \'Yes\' '+
            'AND '+
            '   c2g__Account__c = :accountId '+
            'AND '+
            '   c2g__Transaction__r.Customer_Name__c = :customerName ';
            query += 'Order By c2g__Account__c ASC, c2g__Transaction__r.c2g__TransactionDate__c ASC, c2g__Transaction__r.c2g__DocumentNumber__c ASC, ffps_bmatching__OLI_Line_Number__c ASC ';
            query += 'LIMIT 50000 ';
        
        return query;   
    }
}