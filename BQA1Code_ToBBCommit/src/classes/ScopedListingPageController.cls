public with sharing class ScopedListingPageController {
    public Multi_Scoping_Header__c objMSH {get;set;}
    public List<string> DirectorySection {get;set;}
    public List<SelectOption> listDirSectionOption {get;set;}
    public List<SelectOption> listSelectedDirSectionOption {get;set;}
    public List<string> listSelectedDirSection {get;set;}
    public string areaCode {get;set;}
    public string exchangeCode {get;set;}
    public boolean includeFlag {get;set;}
    public String areaAndExchangeCode {get;set;}
    private string strDirectoryCode;
    private string strSectionCode;
    private map<string,string> mapDirectoryCode;
    private map<string,string> mapSectionCode;
    private string reportParamter;    
    public List<SelectOption> listDirSecSelOpt {get;set;}
    public List<SelectOption> listDirSecAvaOpt {get;set;}
    public List<String> listDirSecSelVal {get;set;}
    public List<String> listDirSecAvaVal {get;set;}
    
    public ScopedListingPageController() {
        listDirSectionOption = new List<SelectOption>();
        listSelectedDirSectionOption = new List<SelectOption>();
        DirectorySection = new List<string>();
        listSelectedDirSection = new List<string>();    
        includeFlag=true;  
        listDirSecSelVal = new List<String>();
        listDirSecAvaVal = new List<String>();
        listDirSecAvaOpt = new List<SelectOption>();
        listDirSecSelOpt = new List<SelectOption>();
        objMSH = new Multi_Scoping_Header__c();
    }
    
    public void fetchDirSection() {
        listDirSecAvaOpt = new List<SelectOption>();
        listDirSecSelOpt = new List<SelectOption>();
        mapDirectoryCode = new map <string,string>();
        mapSectionCode = new map<string,string>();
        list<Directory_Section__c> listDirSec = [select id,name,Directory__r.Directory_Code__c,Section_Code__c from Directory_Section__c where Directory__c =: objMSH.MSH_Directory__c];
        for(Directory_Section__c objDirSec:listDirSec) {
            listDirSecAvaOpt.add(new SelectOption(objDirSec.Name, objDirSec.Name));
            if(string.isNotblank(objDirSec.Directory__r.Directory_Code__c)){
                mapDirectoryCode.put(objDirSec.Name,objDirSec.Directory__r.Directory_Code__c);}
           if(string.isNotblank(objDirSec.Section_Code__c)){            
                mapSectionCode.put(objDirSec.Name,objDirSec.Section_Code__c);}
        }
        listDirSecAvaOpt.sort();
    }   
    
    public void addDirSec() {
        if(listDirSecAvaVal.size() > 0) {
            for(String str : listDirSecAvaVal) {
                for(Integer i = 0; i < listDirSecAvaOpt.size(); i++) {
                    if(listDirSecAvaOpt.get(i).getValue() == str) {
                        listDirSecSelOpt.add(listDirSecAvaOpt.get(i));
                        listDirSecAvaOpt.remove(i);
                        break;
                    }
                }
            }
            listDirSecSelOpt.sort();
        }
    }
    
    public void addAllDirSec() {
        if(listDirSecAvaOpt.size() > 0) {
            listDirSecSelOpt.addAll(listDirSecAvaOpt);
            listDirSecAvaOpt.clear();
            listDirSecSelOpt.sort();
        }
    }
    
    public void removeDirSec() {
        if(listDirSecSelVal.size() > 0) {
            for(String str : listDirSecSelVal) {
                for(Integer i = 0; i < listDirSecSelOpt.size(); i++) {
                    if(listDirSecSelOpt.get(i).getValue() == str) {
                        listDirSecAvaOpt.add(listDirSecSelOpt.get(i));
                        listDirSecSelOpt.remove(i);
                        break;
                    }
                }
            }
            listDirSecAvaOpt.sort();
        }
    }
    
    public void removeAllDirSec() {
        listDirSecAvaOpt.addAll(listDirSecSelOpt);
        listDirSecSelOpt.clear();
        listDirSecAvaOpt.sort();
    }
    
    public PageReference validateAndRun() {
    	if(validate()) {
	        SaleofListing__c csSOL;        
	        reportParamter='';
	        strDirectoryCode='';
	        strSectionCode='';
	        set<String> setDirCodes = new set<String>();
	                 
            for(SelectOption dirSection:listDirSecSelOpt) {
                if(mapDirectoryCode.containsKey(dirSection.getValue())) {
                    setDirCodes.add(mapDirectoryCode.get(dirSection.getValue())); 
                }
                if(mapSectionCode.containsKey(dirSection.getValue())) {
                    strSectionCode+=mapSectionCode.get(dirSection.getValue()) + ',';
                }
            } 
            
            for(String str : setDirCodes) {
            	strDirectoryCode += str + ',';
            } 
                    
            if(string.isNotBlank(strDirectoryCode)) {
                strDirectoryCode = strDirectoryCode.removeEnd(',');
            }
            
            if(string.isNotBlank(strSectionCode)) {
            	strSectionCode = strSectionCode.removeEnd(',');     
            }  
	            
	        if(String.isNotBlank(strDirectoryCode) && String.isNotBlank(strSectionCode)) {
	        	reportParamter = '?pv0=' + strDirectoryCode + '&pv1=' + strSectionCode;
	        }
	        if(String.isBlank(areaAndExchangeCode)) {
	        	csSOL = SaleofListing__c.getValues('Sale_of_Listing_TEMPLATE_Dir_Code_Sec');
	        } else {
	        	if(includeFlag == true) {
	        		csSOL = SaleofListing__c.getValues('Sale_of_Listing_TEMPLATE_With_Area_Ex');
	        	} else {
	        		csSOL = SaleofListing__c.getValues('Sale_of_Listing_TEMPLATE_Area_Ex_Excl');
	        	}
	        	reportParamter += '&pv2=' + areaAndExchangeCode; 
	        }
	        
	        PageReference ReturnPage = new PageReference('/' + csSOL.Report_ID__c + reportParamter);
	        ReturnPage.setRedirect(true); 
	        return ReturnPage;
    	} else {
    		return null;
    	}
    }
    
    private Boolean validate() {
    	Boolean bFlag = true;
    	if(String.isBlank(objMSH.MSH_Directory__c)) {
    		CommonMethods.addError('Please select Directory');
    		bFlag = false;
    	}
    	if(listDirSecSelOpt.size() == 0) {
             CommonMethods.addError('Please select Directory section');
             bFlag = false;
    	}
    	return bFlag;
    }
}