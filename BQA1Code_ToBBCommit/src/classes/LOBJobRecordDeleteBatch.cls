global class LOBJobRecordDeleteBatch Implements Database.Batchable <sObject> {
	Id selectedDirId;
    Id selectedDirEdId; 
    List<String> listSelBillingPartners = new List<String>();
	
	global LOBJobRecordDeleteBatch(Id selectedDirId, Id selectedDirEdId, List<String> listSelBillingPartners) {
        this.selectedDirId = selectedDirId;
        this.selectedDirEdId = selectedDirEdId;
        this.listSelBillingPartners = listSelBillingPartners;
    }
    	
    global Database.queryLocator start(Database.BatchableContext bc) {
        String SOQL = 'SELECT Id FROM List_Of_Business_Job__c WHERE LBJ_Directory_Edition_Id__c =: selectedDirEdId AND LBJ_Directory_Id__c =: selectedDirId';
        return Database.getQueryLocator(SOQL);
    }

    global void execute(Database.BatchableContext bc, List<List_Of_Business_Job__c> listLOBJob) {
		delete listLOBJob;
		Database.emptyRecycleBin(listLOBJob);  
    }

    global void finish(Database.BatchableContext bc) {
    	LOBJobCreationBatch LOBJob = new LOBJobCreationBatch(selectedDirId, selectedDirEdId, listSelBillingPartners);
		Database.executeBatch(LOBJob);
    }
}