public class FAQs
{
	public String selectedCategory{get;set;}
	public string strExcepMessage;
   	public string strStackMessage;
   	public boolean IsExceptionOccured = false;
   	public list<CategoryWrapper> categoryWapperList{get;set;}
   	public FAQs()
   	{
   		getFAQs();
   	}
   	public List<SelectOption> getCategories() {     
  		List<SelectOption> options = new List<SelectOption>();
  		List<FAQ__c> lstCategory =  [Select id, Category__c From FAQ__c order by Category__c];
  		options.add(new SelectOption('--Select--', '--Select--'));
  		
  		String strCategory = '';
 		for(FAQ__c c : lstCategory )
 		{
 			if(strCategory!=null && !strCategory.equalsIgnoreCase(c.Category__c))
 			{
 				options.add(new  SelectOption(c.Category__c, c.Category__c));
 				strCategory=c.Category__c;
 			}
 		}
 		
 		return options;
 	}
   	
	public PageReference fetchCategoryDetails() {
     try
        {
            getFAQs();
        }
        catch(exception ex)
        {
            IsExceptionOccured=true;
            strExcepMessage = ex.getmessage();
            strStackMessage = ex.getStackTraceString();
        }   
        
        return null;
    }
  
  	public List<FAQ__c> getFAQs()
  	{
  		system.debug('selectedCategory============'+selectedCategory);
  		String faqQuery='Select Id,Name,Category__c,Question__c,Answer__c,Active__c,QuestionOrder__c from FAQ__c where Active__c=true ';
  		if(selectedCategory!=null && selectedCategory!='' && selectedCategory!='--Select--')
  		{
  			faqQuery+= ' AND Category__c =\''+ selectedCategory +'\'';
  		}
  		faqQuery+=' Order By Category__c ASC';
		List<FAQ__c> lstFaw = Database.query(faqQuery);
		if(lstFaw != null && lstFaw.size()>0){
			map<String,List<QwtAnsWrapper>> categoryMap= new map<String,List<QwtAnsWrapper>>();
			
			for(FAQ__c li:lstFaw){
				if(categoryMap.containsKey(li.Category__c)){
					List<QwtAnsWrapper> exLi=categoryMap.get(li.Category__c);
					QwtAnsWrapper wrapper= new QwtAnsWrapper();
					String strQuestion = li.Question__c;
					wrapper.question=strQuestion;
					wrapper.answer=li.Answer__c;
					wrapper.active=li.Active__c;
					wrapper.QuestionOrder=integer.valueOf(li.QuestionOrder__c);
					exLi.add(wrapper);
				}else{
					List<QwtAnsWrapper> newLi=new List<QwtAnsWrapper>();
					QwtAnsWrapper wrapper= new QwtAnsWrapper();
					String strQuestion = li.Question__c;
					wrapper.question=strQuestion;
					wrapper.answer=li.Answer__c;
					wrapper.active=li.Active__c;
					wrapper.QuestionOrder=integer.valueOf(li.QuestionOrder__c);
					newLi.add(wrapper);
					categoryMap.put(li.Category__c,newLi);
				}
			}
			Set<String> categorySet=categoryMap.keySet();
			List<String> listCategory = new List<String>();
			 listCategory.addall(categorySet);
			 listCategory.sort();
			categoryWapperList = new List<CategoryWrapper>();
			
			/*for(String str:listCategory){
				CategoryWrapper obj= new CategoryWrapper();
				obj.category=str;
				system.debug('categoryMap.get(str)==========='+categoryMap.get(str));
				obj.questions=categoryMap.get(str);
				
				categoryWapperList.add(obj);
			}
			*/
			
	        
	        for(String str:listCategory){
				CategoryWrapper obj= new CategoryWrapper();
				obj.category=str;
				system.debug('categoryMap.get(str)==========='+categoryMap.get(str));
				
				
				map<string,QwtAnsWrapper> entries = new map<string,QwtAnsWrapper>();
				map<String,string> rankToNameMap = new map<String,string>();
				for(QwtAnsWrapper entry : categoryMap.get(str))
		        {
		            String strQues = String.valueOf(entry.QuestionOrder);
		            entries.put(strQues,entry);
		            rankToNameMap.put(strQues,strQues);
		        }
		        
		        list<String> ranksList = new list<String>();
		        ranksList.addAll(rankToNameMap.keySet());
		
		        //now sort them
		        ranksList.sort();
				
				List<QwtAnsWrapper>  lstqwt = new List<QwtAnsWrapper>();
		        for(String rank : ranksList)
		        {
		        	String thisEntryName = rankToNameMap.get(rank);
		            system.debug('entries.get(thisEntryName)============='+entries.get(thisEntryName));
		           
		            lstqwt.add(entries.get(thisEntryName));
		            
		        }
		        obj.questions=lstqwt;
				
				//obj.questions=categoryMap.get(str);
				
				categoryWapperList.add(obj);
			}
			
		}
      	return lstFaw;
  	}
  	
  	
  	public class CategoryWrapper
	{
		 public String category {get; set;}
		 public List<QwtAnsWrapper> questions {get;set;}
	}
	
	public class QwtAnsWrapper
	{
		 public String question {get; set;}
		 public String answer {get; set;}
		 public Boolean active {get;set;}
		 public Integer QuestionOrder{get;set;}
		 	 
	}
  	
}