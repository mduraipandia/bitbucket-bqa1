global class CCExpireSchedule implements Schedulable {
   public Interface CCExpireScheduleInterface{
     void execute(SchedulableContext sc);
   }
   global void execute(SchedulableContext sc) {
    Type targetType = Type.forName('CCExpireScheduleHndlr');
        if(targetType != null) {
            CCExpireScheduleInterface obj = (CCExpireScheduleInterface)targetType.newInstance();
            obj.execute(sc);
        }
   
   }
}