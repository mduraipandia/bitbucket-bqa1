public class NationalStagingOrderSetDetail_V4 {
    string nationalTransUniquID;
    public String nationalStagingOrderId {get;set;}
    public National_Staging_Order_Set__c nationalStagingOrderSet {get; set;}
    public list<National_Staging_Line_Item__c> lstNationalNewTrans {get; set;}
    public list<wrapperClass> lstWrapperClass {get;set;}
    public list<National_History_Order_Set__c> lstNationalHistory {get;set;}
    public map<Date,list<National_Staging_Line_Item__c>> MapDateNationSLI{get;set;}
    public Set<Id> selectedNSLIId;
    list<National_Staging_Line_Item__c> lstNationalLineItems = new list<National_Staging_Line_Item__c>();
    
    public NationalStagingOrderSetDetail_V4(ApexPages.StandardController controller) {
        nationalStagingOrderId = controller.getId();        
    }
    
    public void onLoad() {
        selectedNSLIId=new Set<Id>();
        lstNationalHistory = new list<National_History_Order_Set__c>();
        headerData();
        transactionData();
        standingOrderData();
        nationalHistoryData();
    }
    
    private void headerData() {
        list<National_Staging_Order_Set__c> lstNSO = NationalStagingOrderSetSOQLMethods.getNationalStagingOrderSetById(nationalStagingOrderId);
        system.debug('Testingg 4 NS OS ID nationalStagingOSId1 : '+ nationalStagingOrderId);
        if(lstNSO.size() > 0) {
            nationalStagingOrderSet = lstNSO.get(0);
            lstNationalLineItems = nationalStagingOrderSet.National_Staging_Line_Items__R;
            nationalTransUniquID = nationalStagingOrderSet.Transaction_Unique_ID__c;
        }
    }
    
    private void transactionData() {
         if(nationalStagingOrderSet.TRANS_Code__c=='T' || nationalStagingOrderSet.TRANS_Code__c=='N' || nationalStagingOrderSet.TRANS_Code__c=='X' || nationalStagingOrderSet.TRANS_Code__c=='H'){
            lstNationalNewTrans = NationalStagingLineItemSOQLMethods.getNSLIForTNXHTransactionByNSOSId(nationalStagingOrderId,nationalStagingOrderSet.TRANS_Code__c);
         }
         else{
             lstNationalNewTrans = NationalStagingLineItemSOQLMethods.getNationalStagingLIListForTransactionByNSOSId(nationalStagingOrderId);
         }
         MapDateNationSLI=new map<Date,list<National_Staging_Line_Item__c>>();
         if(lstNationalNewTrans.size()>0){
             for(National_Staging_Line_Item__c iterator : lstNationalNewTrans) {
                     if(!MapDateNationSLI.containskey(iterator.createdDate.date())){
                        MapDateNationSLI.put(Date.valueof(iterator.createdDate.date()), new list<National_Staging_Line_Item__c>());
                     }
                     MapDateNationSLI.get(Date.valueof(iterator.createdDate.date())).add(iterator);
             }
         }
    }
    
    private void standingOrderData() {
        lstWrapperClass = new list<wrapperClass>();
        list<National_Staging_Line_Item__c> lstNSLI=new list<National_Staging_Line_Item__c>();
        if(nationalStagingOrderSet.TRANS_Code__c=='T' || nationalStagingOrderSet.TRANS_Code__c=='N' || nationalStagingOrderSet.TRANS_Code__c=='X' || nationalStagingOrderSet.TRANS_Code__c=='H'){
           lstNSLI = NationalStagingLineItemSOQLMethods.getNSLIForStadingOrderTNXHByNSOSId(nationalStagingOrderId,nationalStagingOrderSet.TRANS_Code__c);
         }
         else{
            lstNSLI = NationalStagingLineItemSOQLMethods.getNationalStagingLIListForStadingOrderByNSOSId(nationalStagingOrderId);
         }
        
        for(National_Staging_Line_Item__c iterator : lstNSLI) {
            lstWrapperClass.add(new wrapperClass(false,iterator.National_Staging_Header__c, iterator.National_Staging_Header__r.name, iterator.id, iterator.name, iterator.Line_Number__c, iterator.Action__c,iterator.UDAC__c, iterator.BAS__c, 
            iterator.DAT__c, iterator.SPINS__c, iterator.Advertising_Data__c,iterator.Sales_Rate__c,iterator.Line_Error_Description__c, iterator.Is_Processed__c, null, null, null, false, false, true,iterator.Needs_Review__c));
            for(Advice_Query__c iteratorAQ : iterator.Advice_Querys__r) {
                lstWrapperClass.add(new wrapperClass(false,iterator.National_Staging_Header__c, iterator.National_Staging_Header__r.name, iteratorAQ.id, iteratorAQ.name, '00000', null, null,null, null, null,null, null,null,
                false, iteratorAQ.Frequent_Comments_Questions__c, iteratorAQ.Advice_Query_Options__c, iteratorAQ.Free_text__c, iteratorAQ.Send_to_Elite__c, true, false,false));
            }
        }
    }
    
    private void nationalHistoryData() {
        lstNationalHistory = NationalHistoryOrderSetSOQLMethods.getNHOSByUniquID(nationalTransUniquID);
    }
    
    public pagereference deleteLateOrder()
    {
       PageReference pg=null;
       if(nationalStagingOrderSet!=null){
       delete nationalStagingOrderSet;
       pg=new Pagereference('/a3X');
       }
       return pg;
       
    }
    public void updateNewTransaction() {        
        if(nationalStagingOrderSet!=null) {
            if(nationalStagingOrderSet.TRANS_Code__c=='T' || nationalStagingOrderSet.TRANS_Code__c=='N'){
                nationalStagingOrderSet.Is_Converted__c=true;
            }
            update nationalStagingOrderSet;
        }   
            
        list<National_Staging_Line_Item__c> lstUpdateNSLI =new list<National_Staging_Line_Item__c>();
        if(MapDateNationSLI.size()>0){
        
           for(Date dtIterator : MapDateNationSLI.keyset()){
               if(nationalStagingOrderSet.TRANS_Code__c=='T' || nationalStagingOrderSet.TRANS_Code__c=='N'){
                    for(National_Staging_Line_Item__c iterator: MapDateNationSLI.get(dtIterator)){
                        iterator.Is_Processed__c=true;
                        iterator.Ready_for_Processing__c=true;
                        lstUpdateNSLI.add(iterator);
                    }
               }
               else{
                lstUpdateNSLI.addAll(MapDateNationSLI.get(dtIterator));
               }
           }
           if(lstUpdateNSLI.size()>0){
            update lstUpdateNSLI;
             transactionData();
            standingOrderData();
           }
           
        }
        
    }
    public pagereference backtoOrderSet()
    {
      PageReference pg=new PageReference('/'+nationalStagingOrderId);
      return pg;
    }
    
    
    public void setCookiesNSLIIDs() {
        Cookie NSLICookie= ApexPages.currentPage().getCookies().get(nationalStagingOrderId);
        if(lstWrapperClass!=null && lstWrapperClass.size()>0){
                String NSLIDs='';
                for(wrapperClass iter : lstWrapperClass){
                    if(iter.ischecked){
                       NSLIDs+=iter.Id+',';
                    }
                }
                
              // remove last additional comma from string
              if(NSLIDs.length()>0) NSLIDs=NSLIDs.subString(0,NSLIDs.length()-1); 
              System.debug(nationalStagingOrderId+'###NSLIDs###'+NSLIDs);
              if (NSLICookie == null && NSLIDs.length()>0) {
                 NSLICookie = new Cookie(nationalStagingOrderId,NSLIDs,null,-1,false);
                 // Set the new cookie for the page
                 ApexPages.currentPage().setCookies(new Cookie[]{NSLICookie});
              }
              else{
                NSLICookie = new Cookie(nationalStagingOrderId,NSLIDs,null,-1,false); // Note the 0 to delete the cookie
                ApexPages.currentPage().setCookies(new Cookie[]{NSLICookie});
              }
        }
    }
    
    public void updateTransFlagOnNSLIs() {
    	if(lstNationalLineItems != null && lstNationalLineItems.size() > 0)	{
    		for(National_Staging_Line_Item__c NSLI : lstNationalLineItems) {
    			NSLI.New_Transaction__c = false;
    		}
    		update lstNationalLineItems;
    		nationalStagingOrderSet.Is_Converted__c = true;
    		update nationalStagingOrderSet;
    	}
    }
    
    public class wrapperClass {
        public boolean ischecked{get;set;}
        public string lineNo {get;set;}
        public string name {get;set;}
        public id id {get;set;}
        public id nsosId {get;set;}
        public string nsosName {get;set;}
        public string action {get;set;}
        public string UDAC {get;set;}
        public string BAS {get;set;}
        public string DAT {get;set;}
        public string SPINS {get;set;}
        public string advData {get;set;}
        public string LineError{get;set;}
        public Decimal fullRate {get;set;}
        public Decimal salesRate {get;set;}
        public string description {get;set;}
        public string queryAdvice {get;set;}
        public string freqComment {get;set;}
        public string freeText {get;set;}
        public boolean sendToElite {get;set;}
        public boolean showSelect {get;set;}
        public boolean showAddButton {get;set;}
        public boolean newTrans {get;set;}
        public boolean isProcessed {get;set;}
        public boolean standingOrder {get;set;}
        public boolean isChanged {get;set;}
        public DateTime createdDate {get;set;}
        public boolean NeedsReview{get;set;}
        //Transaction Tab Wrapper constructor
        public wrapperClass(boolean ischecked,id nsosId, string nsosName, ID id, string name, string lineNo,string action , string UDAC, string BAS, string DAT, string SPINS, string advData,Decimal salesRate,string LineError, boolean isProcessed, 
                            string queryAdvice, string freqComment, string freeText, boolean sendToElite, boolean showSelect, boolean showAddButton,boolean NeedsReview) {
            this.ischecked=ischecked;
            this.nsosId= nsosId;
            this.action =action ;
            this.isProcessed= isProcessed;
            this.name= name;
            this.id = id;
            this.lineNo = lineNo;
            this.nsosName= nsosName;
            this.UDAC = UDAC;
            this.BAS = BAS;
            this.DAT = DAT;
            this.SPINS = SPINS;
            this.advData = advData;
            this.salesRate=salesRate;
            this.LineError=LineError;
            this.createdDate = createdDate;
            this.description = description;
            this.queryAdvice = queryAdvice;
            this.freqComment = freqComment;                             
            this.freeText = freeText;
            this.sendToElite = sendToElite;
            this.showSelect = showSelect;
            this.showAddButton = showAddButton;
            this.NeedsReview=NeedsReview;
        }
    }
}