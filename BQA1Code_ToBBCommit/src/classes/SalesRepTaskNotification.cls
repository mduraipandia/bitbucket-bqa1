public with sharing class SalesRepTaskNotification 
{
        public pagereference RunBatch() {
        database.executebatch(new  SalesRepTaskNotificationBatchController(),2000);
        return (new pagereference('/apex/SalesRepTaskNotification').setredirect(true));
    }
}