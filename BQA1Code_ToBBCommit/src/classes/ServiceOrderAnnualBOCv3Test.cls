@isTest(seealldata=true)
private Class ServiceOrderAnnualBOCv3Test {
   static testmethod void TestServiceOrderAnnualBOCBatchControllerNewDupeLogic() {
        Database.BatchableContext bc;
        Directory__c objDir = TestMethodsUtility.createDirectory();
        Directory_Section__c objDirSec = TestMethodsUtility.createDirectorySection(objDir);
        Lead_Assignment_Rules__c objLAR = new Lead_Assignment_Rules__c(Name='Test LAR',Area_code__c=170,Exchange__c=444,Directory__c=objDir.id,Directory_Section__c=objDirSec.id);
        insert objLAR;
        List<service_order_stage__c> objSOLst = new List<service_order_stage__c>();
        service_order_stage__c objSO = TestMethodsUtility.generateSOrecord('Annual Listings');
        insert objSO;
        service_order_stage__c objSODupe = objSO.clone(false);
        insert objSODupe;
        set<Id> setSOId = new set<Id>();
        setSOId.add(objSO.Id);
        setSOId.add(objSODupe.Id);
        objSOLst = [SELECT strDLcombo__c,strSOSCombination__c,StrBOCDisconnect__c,SOS_LastName_BusinessName__c,StrSOLstMatch__c,Action__c,Area_code__c,Associated_to_Dir_Listing__c,Associated_to_Listing__c,BEX__c,BOID__c,Bus_Res_Gov_Indicator__c,Caption_Display_Text__c,Caption_Header__c,Caption_Member__c,CLEC_Provider__c,Cross_Reference_Indicator__c,Cross_Reference_Text__c,Data_Feed_Type__c,Data_Feed_Type_2__c,Designation__c,Directory_Heading__c,Directory_Section__c,Directory_Section__r.Section_Page_Type__c,Directory__c,Disconnected__c,
                    Disconnect_Reason__c,Effective_Date__c,Exchange__c,Fallout__c,First_Name__c,Honorary_Title__c,Id,Indent_Level__c,Indent_Order__c,Initial_Match_on_DL__c,Initial_Match_on_Listing__c,Is_Lead__c,Lead_Account_Source__c,Lead_Owner__c,Lead_Record_Type__c,Lead_Source__c,Lead_Status__c,Left_Telephone_Phrase__c,Lineage_Title__c,Line__c,Listing_City__c,Listing_Country__c,Listing_Postal_Code__c,Listing_PO_Box__c,Listing_State__c,Listing_Street_Number__c,Listing_Street__c,Listing_Type__c,
                    Manual_Sort_As_Override__c,Name,Normalized_Designation__c,Normalized_First_Name__c,Normalized_Honorary_Title__c,Normalized_Last_Name_Business_Name__c,Normalized_Lineage_Title__c,Normalized_Listing_City__c,Normalized_Listing_Postal_Code__c,Normalized_Listing_PO_Box__c,Normalized_Listing_Street_Name__c,Normalized_Listing_Street_Number__c,Normalized_Phone__c,Normalized_Secondary_Surname__c,Omit_Address_OAD__c,Phone_Override_Indicator__c,Phone_Override__c,Phone_Type__c,Phone__c,
                    Primary_Canvass__c,RecordTypeId,Region__c,Right_Telephone_Phrase__c,Secondary_Surname__c,Service_Order__c, Batch_ID__c, 
                    Telco_Provider__c,Under_Caption__c,Under_caption__r.Directory_Heading__c,Under_caption__r.Directory__c,Under_Caption__r.Associated_to_Dir_Listing__c,Under_Sub_Caption__c,Under_Sub_Caption__r.Associated_to_Dir_Listing__c,Under_caption__r.Directory_Section__c,Under_Caption__r.Fallout__c,Under_Caption__r.Associated_to_Listing__c,Under_caption__r.Caption_Member__c,Year__c,
                    Scheduled_Process_Complete__c,Createddate,OUT_LAST_NAME_BUSINESS_NAME__c,OUT_FIRST_NAME__c,OUT_PHONE_NUMBER__c,OUT_STREET_NUMBER__c,OUT_STREET_NAME__c,OUT_PO_BOX__c,OUT_CITY__c,OUT_STATE__c,OUT_POSTAL_CODE__c,Telco_Sort_Order__c,Do_Not_Create_YP__c,Omit_Phone_OTN__c,SO_OAD__c,OTN__c FROM Service_Order_Stage__c where Id IN: setSOId];
        
        Test.StartTest();
            ServiceOrderAnnualBOCBatchController_v3 objSOBatch = new ServiceOrderAnnualBOCBatchController_v3(objDir.id);
            objSOBatch.start(bc);
            objSOBatch.execute(bc,objSOLst);
        Test.StopTest();
   }
      
  static testMethod void soMathcingBatch() {
        Database.BatchableContext bc;
        List<service_order_stage__c> objSOmatchingLst = new List<service_order_stage__c>();
        Recordtype rttelcoId = TestMethodsUtility.getRecordType('Telco__c','Telco Provider');
        Account objAcc = TestMethodsUtility.generateTelcoAccount();
        insert objAcc;
        Telco__c objTelco = TestMethodsUtility.generateTelco(objAcc.Id);
        objTelco.RecordTypeId = rttelcoId.id;
        insert objTelco;
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c=objTelco.Id;
        insert objDir;
        Directory_Section__c objDirSec = TestMethodsUtility.createDirectorySection(objDir);
        Directory_Section__c objDirSecYP = TestMethodsUtility.generateDirectorySection(objDir);
        objDirSecYP.Section_Page_Type__c = 'YP';
        objDirSecYP.Section_Code__c = TestMethodsUtility.generateRandomNumberForLen(8);
        insert objDirSecYP;
        Recordtype rtId = TestMethodsUtility.getRecordType('Service_order_stage__c','Annual Listings');
        Lead_Assignment_Rules__c objLAR = new Lead_Assignment_Rules__c(Name='Test LAR',Area_code__c=170,Exchange__c=444,Directory__c=objDir.id,Directory_Section__c=objDirSec.id);
        insert objLAR;
        Directory_Heading__c objDirH =TestMethodsUtility.createDirectoryHeading();
        listing__c objLst = new listing__c(Name ='TestLstNameExist',Lst_Last_Name_Business_Name__c='TestLstNameExist',Phone__c='(937)856-2244',Listing_State__c='TX',Telco_Provider__c=objTelco.Id,recordtypeId=system.label.TestListingMainRT);
        insert objLst;
        directory_listing__c objdirLst = new directory_listing__c(Name ='TestLstNameExist',SL_Last_Name_Business_Name__c='TestLstNameExist',Phone_Number__c='(937)856-2244',Listing_State__c='TX',Telco_Provider__c=objTelco.Id,recordtypeId=system.label.TestScopedListingNonOverrideRT,listing__c=objLst.Id,Directory__c = objDir.Id,Directory_Section__c = objDirSec.Id,Created_by_Batch__c=true);
        insert objdirLst;
        directory_listing__c objdirLstYP = new directory_listing__c(Name ='TestLstNameExist',SL_Last_Name_Business_Name__c='TestLstNameExist',Phone_Number__c='(937)856-2244',Listing_State__c='TX',Telco_Provider__c=objTelco.Id,recordtypeId=system.label.TestScopedListingNonOverrideRT,listing__c=objLst.Id,Directory__c = objDir.Id,Directory_Section__c = objDirSecYP.Id,Created_by_Batch__c=true);
        insert objdirLstYP;
        Service_order_stage__c objSO = new Service_order_stage__c(Name ='TestLstNameExist',SOS_LastName_BusinessName__c='TestLstNameExist',Action__c = 'New',Phone__c = '(937)856-2244',Directory__c = objDir.Id,Listing_State__c='TX',Bus_Res_Gov_Indicator__c = 'Business',
                                        Directory_Section__c = objDirSec.Id,Directory_Heading__c = objDirH.Id,Telco_Provider__c = objTelco.Id,recordtypeId = rtId.Id,Listing_Type__c = 'Main',Scheduled_Process_Complete__c = false,Fallout__c= false);
        
        insert objSO;
        Service_order_stage__c objSONew = new Service_order_stage__c(Name ='TestLstNameNew',SOS_LastName_BusinessName__c='TestLstNameNew',Action__c = 'New',Phone__c = '(938)856-2244',Directory__c = objDir.Id,Listing_State__c='TX',Bus_Res_Gov_Indicator__c = 'Business',
                                        Directory_Section__c = objDirSec.Id,Directory_Heading__c = objDirH.Id,Telco_Provider__c = objTelco.Id,recordtypeId = rtId.Id,Listing_Type__c = 'Main',Scheduled_Process_Complete__c = false,Fallout__c= false);
        
        insert objSONew;
        objSOmatchingLst = [SELECT strDLcombo__c,strSOSCombination__c,SOS_LastName_BusinessName__c,Action__c,Area_code__c,Associated_to_Dir_Listing__c,Associated_to_Dir_Listing__r.Disconnected__c,Associated_to_Listing__c,BEX__c,BOID__c,Bus_Res_Gov_Indicator__c,Caption_Display_Text__c,Caption_Header__c,Caption_Member__c,CLEC_Provider__c,Cross_Reference_Indicator__c,Cross_Reference_Text__c,CreatedDate,Data_Feed_Type__c,Designation__c,
                            Directory__r.BOTS_Edition_Publication_Date__c,Directory__r.SO_NI_Blackout_Period_Begin_Date__c,Directory__r.SO_NI_Blackout_Period_End_Date__c,Directory_Heading__c,Directory_Section__c,Directory_Section__r.Section_Page_Type__c,Directory__c,Disconnected__c,Disconnect_Reason__c,Effective_Date__c,Effective_Date_During_Blackout_Period__c,Exchange__c,Fallout__c,First_Name__c,Honorary_Title__c,
                            Id,Indent_Level__c,Indent_Order__c,Initial_Match_on_DL__c,Initial_Match_on_Listing__c,Is_Lead__c,Lead_Account_Source__c,Lead_Owner__c,Lead_Record_Type__c,Lead_Source__c,Lead_Status__c,Left_Telephone_Phrase__c,Lineage_Title__c,Line__c,Listing_City__c,Listing_Country__c,Listing_Postal_Code__c,Listing_PO_Box__c,Listing_State__c,Listing_Street_Number__c,Listing_Street__c,Listing_Type__c,
                            Manual_Sort_As_Override__c,Name,Normalized_Designation__c,Normalized_First_Name__c,Normalized_Honorary_Title__c,Normalized_Last_Name_Business_Name__c,Normalized_Lineage_Title__c,Normalized_Listing_City__c,Normalized_Listing_Postal_Code__c,Normalized_Listing_PO_Box__c,Normalized_Listing_Street_Name__c,Normalized_Listing_Street_Number__c,Normalized_Phone__c,Normalized_Secondary_Surname__c,
                            Omit_Address_OAD__c,On_Hold_Due_to_Blackout__c,Phone_Override_Indicator__c,Phone_Override__c,Phone_Type__c,Phone__c,Primary_Canvass__c,RecordTypeId,Region__c,Right_Telephone_Phrase__c,Secondary_Surname__c,Service_Order__c,Telco_Provider__c,Under_Caption__c,Under_Caption__r.Associated_to_Dir_Listing__c,Under_Sub_Caption__c,Under_Sub_Caption__r.Associated_to_Dir_Listing__c,
                            Under_Caption__r.Associated_to_Listing__c,Under_Caption__r.Fallout__c,Year__c,Scheduled_Process_Complete__c, Batch_ID__c,
                            OUT_LAST_NAME_BUSINESS_NAME__c,OUT_FIRST_NAME__c,OUT_PHONE_NUMBER__c,OUT_STREET_NUMBER__c,OUT_STREET_NAME__c,OUT_PO_BOX__c,OUT_CITY__c,OUT_STATE__c,OUT_POSTAL_CODE__c,Telco_Sort_Order__c,StrBOCDisconnect__c,StrSOLstMatch__c,Do_Not_Create_YP__c,Data_Feed_Type_2__c,Omit_Phone_OTN__c,SO_OAD__c,OTN__c
                            FROM Service_Order_Stage__c where Id=: objSO.id];
                            

        Test.startTest();
            ServiceOrderAnnualBOCBatchController_v3 objSOBatch = new ServiceOrderAnnualBOCBatchController_v3(objDir.id);
            objSOBatch.start(bc);
            objSOBatch.execute(bc, objSOmatchingLst);
            objSOBatch.finish(bc);
        Test.stopTest();
       
    }
}