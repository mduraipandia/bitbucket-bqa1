public with sharing class NationalHistoryOrderSetSOQLMethods {          
    public static list<National_History_Order_Set__c> getNHOSByUniquID(String nationalUniquId) {
        return [Select Directory_Edition_Number__c ,Client_Number__c,Directory_Number__c,Transaction_Unique_ID__c, CMR_Number__c,
        Transaction_ID__c,Late_Order__c , Id, CreatedDate, TRANS_Code__c, Name From National_History_Order_Set__c where
         Transaction_Unique_ID__c != null AND Transaction_Unique_ID__c =:nationalUniquId ORDER BY CreatedDate DESC];
    }
}