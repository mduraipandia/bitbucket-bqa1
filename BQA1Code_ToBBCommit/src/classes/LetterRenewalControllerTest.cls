@IsTest(SeeAllData=true)
public class LetterRenewalControllerTest {
     static testMethod void letterRenewalTest() {
        Test.startTest();
        Profile p = TestMethodsUtility.getSysAdminProfile();
        User Us = TestMethodsUtility.generateUser(p.Id,null);
        Us.LastName='Renewal';
        Us.FirstName ='Letter';
        Us.CommunityNickname ='LR';
        insert Us;
        
        Id RecordTypeId = CommonMethods.getRedordTypeIdByName(CommonMessages.opportunityLockRTName, CommonMessages.opportunityObjectName);    
        Database.BatchableContext bc;
        //system.runas(objUser){
            Account newAccountLR = TestMethodsUtility.generateAccount('customer');
            newAccountLR.Open_claim__c = false;
            newAccountLR.OwnerId = Us.Id;
            newAccountLR.Delinquency_Indicator__c= false;
            newAccountLR.Letter_Renewal_Sequence__c = '1';
            insert newAccountLR;
           
            
            Contact newContactLR = TestMethodsUtility.generateContact(newAccountLR.Id);        
            insert newContactLR;
          
            Opportunity newOpportunityLR = TestMethodsUtility.createOpportunity(newAccountLR, newContactLR);
            insert newOpportunityLR;
            map<Id,opportunity> mapnewobjOpp = new map<Id,opportunity>();
            mapnewobjOpp.put(newOpportunityLR.Id,newOpportunityLR);
            
            Directory__c newDirectory = TestMethodsUtility.createDirectory();
            
            List<Directory_Edition__c> lstDE = new List<Directory_Edition__c>();
            
            Directory_Edition__c newDE = TestMethodsUtility.generateDirectoryEdition(newDirectory.Id, newAccountLR.Primary_Canvass__c, null, null);
            newDE.Book_Status__c = 'NI';
            newDE.Letter_Renewal_Stage_1__c = System.today();
            newDE.Pub_Date__c = system.today().adddays(2);
            newDE.Bill_Prep__c= system.today().adddays(8);
            newDE.New_Print_Bill_Date__c= system.today().adddays(2);
            insert newDE;
            lstDE.add(newDE);
            
            Directory_Edition__c newDE1= TestMethodsUtility.generateDirectoryEdition(newDirectory.Id, newAccountLR.Primary_Canvass__c, null, null);
            newDE1.Book_Status__c = 'BOTS';
            insert newDE1;
            
            Product2 newProduct = TestMethodsUtility.generateproduct();
            newProduct.Print_Product_Type__c = 'Test';
            insert newProduct;
            
            Directory_Product_Mapping__c objDPM = new Directory_Product_Mapping__c();
            objDPM.Directory__c = newDirectory.Id;
            objDPM.Product2__c = newProduct.Id;
            objDPM.FullRate__c = 10;
            insert objDPM;
            
            Order__c newOrder = TestMethodsUtility.createOrder(newAccountLR.Id);
            
            Order_Group__c newOG = TestMethodsUtility.createOrderSet(newAccountLR, newOrder, newOpportunityLR);
            set<Id> setOrdersetId = new set<Id>();
            setOrdersetId.add(newOG.Id);
            
            List<Order_Line_Items__c> lstOLI = new List<Order_Line_Items__c>();
            Order_Line_Items__c newOLI1LR = TestMethodsUtility.generateOrderLineItem(newAccountLR, newContactLR, newOpportunityLR, newOrder, newOG);
            newOLI1LR.Product2__c=newProduct.Id;
            newOLI1LR.is_p4p__c=false;
            newOLI1LR.Directory_Edition__c = newDE1.Id;
            newOLI1LR.UnitPrice__c=100;
            newOLI1LR.Media_type__c = 'Print';
            newOLI1LR.Order_Anniversary_Start_Date__c = date.today();
            newOLI1LR.Last_Billing_Date__c = system.today();
            newOLI1LR.Is_Handled__c = false;
            newOLI1LR.Cutomer_Cancel_Date__c=null;
            newOLI1LR.Opportunity__c=newOpportunityLR.id;
            insert newOLI1LR;
            
        
            lstOLI=[SELECT Id,Order_Anniversary_Start_Date__c,Billing_Close_Date__c,Billing_Contact__c,Billing_End_Date__c,Billing_Frequency__c,Billing_Partner_Account__c,Billing_Partner__c,
                                                Billing_Start_Date__c,Cancelation_Fee__c,Cancellation__c,Canvass__c,Category__c,checked__c,Continious_Billing__c,Current_Billing_Period_Days2__c,
                                                Current_Billing_Period_Days__c,Current_Daily_Prorate__c,Current_Rate__c,Cutomer_Cancel_Date__c,Date_of_Transfer__c,Days_B_W__c,Description__c,Directory__c,Directory_Edition__c,
                                                Directory_Edition__r.Sales_Start__c,Discount__c,Effective_Date__c,Media_Type__c,RecordTypeId,Is_Handled__c,Anchor_Listing_Caption_Header__c,Trademark_Finding_Line_OLI__c,
                                                Fulfilled_on__c,Geo_Code__c,Geo_Type__c,Is_Billing_Cycle__c,Is_P4P__c,Last_successful_settlement__c,Line_Number__c,Listing__c,ListPrice__c,Scoped_Caption_Header__c,
                                                UnitPrice__c,Name,Next_Billing_Date__c,Opportunity__c,Order_Billing_Date_Changed__c,Order__c,OriginalBillingDate__c,Package_ID__c,Account__r.BillingStreet,
                                                Account__r.BillingCity,Account__r.BillingState,Account__r.BillingPostalCode,Account__r.Delinquency_Indicator__c,Account__r.Open_Claim__c,Account__r.Phone,
                                                Product2__r.Print_Product_Type__c,Billing_Contact__r.Name,Billing_Contact__r.Email,
                                                Listing__r.Phone__c,Listing__r.Name,Directory__r.Name,Directory_Edition__r.Letter_Renewal_Stage_1__c,Directory_Edition__r.Letter_Renewal_Stage_2__c,Account__r.Name,Account__r.Letter_Renewal_Sequence__c,Product2__r.Name,
                                                Parent_ID__c,Parent_Line_Item__c,Payments_Remaining__c,Payment_Duration__c,Payment_Method__c,Previous_End_Date__c,Previous_Start_Date__c,Product2__c,ProductCode__c,Product_Type__c,
                                                Prorate_Credit_Days__c,Prorate_Credit__c,Prorate_Stored_Value__c,Quote_Signed_Date__c,Quote_signing_method__c,Reason_for_Transfer__c,Scope__c, Directory_Section__c,Sent_to_fulfillment_on__c,
                                                Service_End_Date__c,Service_Start_Date__c,Order_Line_Total__c,Statement_Suppression__c,Status__c,Subscription_ID__c,Successful_Payments__c,Talus_Cancel_Date__c,Directory_Edition__r.Book_Status__c,
                                                Directory_Edition__r.Bill_Prep__c,Directory_Edition__r.New_Print_Bill_Date__c,Order_Group__c,Account__c,Directory_Heading__c, Directory_Heading__r.Name, Directory_Heading__r.code__c, Quantity__c,
                                                Line_Status__c,Last_Billing_Date__c,Directory__r.Pub_Date__c,Directory_Edition__r.Pub_Date__c,Seniority_Date__c,Directory_Edition__r.Name,
                                                Talus_DFF_Id__c,Talus_Fulfillment_Date__c,Talus_Go_Live_Date__c,Telco_Invoice_Date__c,Telco__c,Total_Prorate__c,User_who_initiated_the_transfer__c,Vendor_Product_Key__c,Directory_Edition__r.Directory__c
                                                FROM Order_Line_Items__c where ID=:newOLI1LR.Id];
           system.assertequals(null,lstOLI[0].Cutomer_Cancel_Date__c);
            
            map<Id, list<Order_Line_Items__c>> mapTempOLI = new map<Id, list<Order_Line_Items__c>>();
           
            LetterRenewalBatchController batchLR = new LetterRenewalBatchController();
            batchLR.start(bc);
            batchLR.execute(bc,lstDE);
            
            /*LetterRenewalReportScheduler lrRS= new LetterRenewalReportScheduler();
            schedulableContext sc;
            lrRS.execute(sc);
            */
            LetterRenewalController_v1.createNewOrderSet(setOrdersetId,mapnewobjOpp);
            LetterRenewalController_v1.generateHeaderPDFString(newAccountLR);
            LetterRenewalController_v1.generateDirectoryEditionString(lstOLI);
            LetterRenewalController_v1.generateDirectoryEditionAccountString(lstOLI);
            LetterRenewalController_v1.generateDirectoryEditionAccountString(lstOLI);
            LetterRenewalController_v1.generateDirOlitemsString(lstOLI[0]);
            LetterRenewalController_v1.generateTotalPrintInvestmentString(lstOLI);
            LetterRenewalController_v1.createNewOLI(lstOLI);
            LetterRenewalController_v1.createNewOpty(mapnewobjOpp.keyset());
            //LetterRenewalController_v1.createNewOLI(lstOLI);
            LetterRenewalController_v1.letterRenewal(lstOLI);
            DirectoryEditionLetterRenewal.DELetterrenewl(newDE1.Id);
            
            Test.stopTest();
          //}
    }
  
}