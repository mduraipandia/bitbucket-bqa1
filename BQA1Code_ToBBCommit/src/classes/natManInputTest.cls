@isTest(seeAlldata=true)
public class natManInputTest
{
    public static testmethod void natManInputMethod()
    {
        Recordtype rcforNSOS=[select id,name from recordType where developername='Manual_NS_RT' and SobjectType = 'National_Staging_Order_Set__c' limit 1];
        //UserRole role=[SELECT Id,Name FROM UserRole where name='Account Manager' limit 1];
        //User u = [SELECT Id,Name FROM User WHERE UserRoleId = :role.id limit 1];
        Recordtype rcforAcc=[select id,name from recordType where name='National Account' limit 1];
        Id clientRTID = CommonMethods.getRedordTypeIdByName(CommonMessages.accountNationalActRT, CommonMessages.accountObjectName);
        
        Account telcoAcct = TestMethodsUtility.createAccount('telco');
        Telco__c telco = TestMethodsUtility.createTelco(telcoAcct.Id);
        Canvass__c c = TestMethodsUtility.createCanvass(telco);
        Account a = new Account(Client_Number__c='2222',CMR_Number__c='333',Name='Test Account', TalusAccountId__c='222222', TalusPhoneId__c='2222', Primary_Canvass__c=c.id, Phone='(425)533-3099',RecordTypeId=rcforAcc.id);
        insert a;
        Account a1 = new Account(Client_Number__c='2222',CMR_Number__c='333',Name='Test Account', TalusAccountId__c='222222', TalusPhoneId__c='2222', Primary_Canvass__c=c.id, Phone='(425)533-3099',RecordTypeId=label.TestAccountCMRRT);
        insert a1;
        Account a2 = new Account(Parent = a1, Client_Number__c='2222',CMR_Number__c='333',Name='Test Account', TalusAccountId__c='222222', TalusPhoneId__c='2222', Primary_Canvass__c=c.id, Phone='(425)533-3099',RecordTypeId=clientRTID);
        insert a2;
        /*Directory__c directory = TestMethodsUtility.generateDirectory();
        directory.Months_for_National__c = 12;
        insert directory;*/
        Directory__c directory =TestMethodsUtility.createDirectory();
        Directory_Edition__c directoryEdition=new Directory_Edition__c(Name='Test Diretory Edition',Book_Status__c='NI',Directory__c=directory.id,LSA_Directory_Version__c='4444');     
        insert directoryEdition;
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.ProductCode = 'FP';
        newProduct.IsActive = true;
        insert newProduct;
        National_Staging_Order_Set__c no=new National_Staging_Order_Set__c();
        no.CMR_Name__c=a.id;
        no.Client_Name__c=a.id;
        no.Client_Name_Text__c='Test Account';
        no.Client_Number__c='2222';
        no.CMR_Number__c='333';
        no.Directory_Edition_Number__c='4444';
        no.RecordTypeId=rcforNSOS.id; 
        no.Directory__c = directory.Id; 
        insert no; 
        Test.startTest();
        PageReference pageRef = Page.natManInput_V1;
        Test.setCurrentPageReference(pageRef); 
        pageRef.getParameters().put('RecordType',rcforNSOS.Id);
        pageRef.getParameters().put('index','0');
        ApexPages.StandardController controller = new ApexPages.StandardController(no);
        natManInput nObj=new natManInput(controller);
        System.debug('^^^^^^^^^^^^^^^^ '+rcforNSOS.Id);
        nObj.UDAC = 'FP';
        nObj.NatHdrObj.CMR_Name__c=a.id;
        nObj.NatHdrObj.Client_Name__c=a.id;
        nObj.NatHdrObj.Client_Name_Text__c='Test Account';
        nObj.NatHdrObj.Client_Number__c='2222';
        nObj.NatHdrObj.CMR_Number__c='333';
        nObj.NatHdrObj.Directory_Edition_Number__c='4444';
        nObj.save();
        nObj.Cancel();
        nobj.addLineItems();
        nobj.createNewAccount(nObj.NatHdrObj);
        nobj.getNatList();
        nobj.getToFromList();
        nobj.getTransList();
        nobj.fetchCMRbyNumber();
        nobj.fetchClientbyNumber();
        nobj.fetchDirectoryByCode();
        nobj.NatHdrObj.Directory__c=null;
        nobj.fetchDEByLSANo();
        nobj.removeEntry();
        nobj.productByUDAC();
        
        natManInput nObj2=new natManInput(controller);
        nObj.NatHdrObj.CMR_Name__c=a1.id;
        nObj.NatHdrObj.Client_Name__c=a1.id;
        nObj.NatHdrObj.Client_Name_Text__c='Test Account';
        nObj.NatHdrObj.Client_Number__c='2222';
        nObj.NatHdrObj.CMR_Number__c='333';
        nObj.NatHdrObj.Directory_Edition_Number__c='4444';
        nobj.fetchCMRbyNumber();
        Test.stopTest();        
    }
    public static Testmethod void natManInputMethod1()
    {
        //UserRole role=[SELECT Id,Name FROM UserRole where name='Account Manager' limit 1];
        //User u = [SELECT Id,Name FROM User WHERE UserRoleId = :role.id limit 1];
        Account telcoAcct = TestMethodsUtility.createAccount('telco');
        Telco__c telco = TestMethodsUtility.createTelco(telcoAcct.Id);
        Canvass__c c = TestMethodsUtility.createCanvass(telco);
        Account a = new Account(Name='Test Account', TalusAccountId__c='222222', TalusPhoneId__c='2222', Primary_Canvass__c=c.id, Phone='(425)533-3099');
        insert a; 
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.ProductCode = 'FP';
        newProduct.IsActive = true;
        insert newProduct;
        Test.startTest();   
        National_Staging_Order_Set__c obj=new National_Staging_Order_Set__c();
        ApexPages.StandardController controller = new ApexPages.StandardController(obj);
        natManInput nObj=new natManInput(controller);
        nObj.UDAC = 'FP';
        nObj.Cancel();
        nobj.addLineItems();
        nobj.Save();

        Test.stopTest();
        
    }      
    
    public static testmethod void natManInputMethodForEdit()
    {
        Recordtype rcforNSOS=[select id,name from recordType where developername='Manual_NS_RT' and SobjectType = 'National_Staging_Order_Set__c' limit 1];
        //UserRole role=[SELECT Id,Name FROM UserRole where name='Account Manager' limit 1];
        //User u = [SELECT Id,Name FROM User WHERE UserRoleId = :role.id limit 1];
        Recordtype rcforAcc=[select id,name from recordType where name='National Account' limit 1];
        Account telcoAcct = TestMethodsUtility.createAccount('telco');
        Telco__c telco = TestMethodsUtility.createTelco(telcoAcct.Id);
        Canvass__c c = TestMethodsUtility.createCanvass(telco);
        Account a = new Account(Client_Number__c='2222',CMR_Number__c='333',Name='Test Account', TalusAccountId__c='222222', TalusPhoneId__c='2222', Primary_Canvass__c=c.id, Phone='(425)533-3099',RecordTypeId=rcforAcc.id);
        insert a;
       /* Directory__c directory = TestMethodsUtility.generateDirectory();
        directory.Months_for_National__c = 12;
        insert directory;*/
        Directory__c directory =TestMethodsUtility.createDirectory();
        Directory_Edition__c directoryEdition=new Directory_Edition__c(Name='Test Diretory Edition',Book_Status__c='NI',Directory__c=directory.id,LSA_Directory_Version__c='4444');
        insert directoryEdition;
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.ProductCode = 'FP';
        newProduct.IsActive = true;
        insert newProduct;
        Test.startTest();
        National_Staging_Order_Set__c no=new National_Staging_Order_Set__c();
        no.CMR_Name__c=a.id;
        no.Client_Name__c=a.id;
        no.Client_Name_Text__c='Test Account';
        no.Client_Number__c='2222';
        no.CMR_Number__c='333';
        no.Directory_Edition_Number__c='4444';
        no.RecordTypeId=rcforNSOS.id; 
        no.Directory__c = directory.Id; 
        insert no;    
        National_Staging_Line_Item__c nl=new National_Staging_Line_Item__c();
        nl.National_Staging_Header__c=no.id;
        //nl.UDAC__c='2002';
        nl.Line_Number__c='20045';
        insert nl;
        PageReference pageRef = Page.natManInput_V1;
        Test.setCurrentPageReference(pageRef); 
        pageRef.getParameters().put('id',no.Id);
        pageRef.getParameters().put('index','0');
        ApexPages.StandardController controller = new ApexPages.StandardController(new National_Staging_Order_Set__c());
        natManInputEdit nObj=new natManInputEdit(controller);
        System.debug('^^^^^^^^^^^^^^^^ '+rcforNSOS.Id);
        nObj.UDAC = 'FP';
        nObj.Cancel();
        nobj.addLineItems();
         nObj.save();
        nobj.createNewAccount(nObj.NatHdrObj);
        nobj.getNatList();
        nobj.getToFromList();
        nobj.getTransList();
        nobj.fetchCMRbyNumber();
        nobj.fetchClientbyNumber();
        nobj.fetchDirectoryByCode();
        nobj.removeEntry();
       // nobj.productByUDAC();  
      // nobj.NatHdrObj.Directory__c=null;
        nobj.fetchDEByLSANo();
        
        no=new National_Staging_Order_Set__c();
        no.Client_Name_Text__c='Test Account2';
        no.RecordTypeId=rcforNSOS.id;  
        no.Directory__c = directory.Id;   
        insert no;
        pageRef = Page.natManInput_V1;
        Test.setCurrentPageReference(pageRef); 
        pageRef.getParameters().put('id',no.Id);
        pageRef.getParameters().put('index','0');
        controller = new ApexPages.StandardController(new National_Staging_Order_Set__c());
        nObj=new natManInputEdit(controller);
        System.debug('^^^^^^^^^^^^^^^^ '+rcforNSOS.Id);
       
        nObj.fetchClientbyNumber();
        
        Test.stopTest();      
    }
    public static Testmethod void natManInputMethodForEdit1()
    {
         Recordtype rcforNSOS=[select id,name from recordType where developername='Manual_NS_RT' and SobjectType = 'National_Staging_Order_Set__c' limit 1];
        //UserRole role=[SELECT Id,Name FROM UserRole where name='Account Manager' limit 1];
        //User u = [SELECT Id,Name FROM User WHERE UserRoleId = :role.id limit 1];
        Recordtype rcforAcc=[select id,name from recordType where name='National Account' limit 1];
        Account telcoAcct = TestMethodsUtility.createAccount('telco');
        Telco__c telco = TestMethodsUtility.createTelco(telcoAcct.Id);
        Canvass__c c = TestMethodsUtility.createCanvass(telco);
        Account a = new Account(Client_Number__c='2222',CMR_Number__c='333',Name='Test Account', TalusAccountId__c='222222', TalusPhoneId__c='2222', Primary_Canvass__c=c.id, Phone='(425)533-3099',RecordTypeId=rcforAcc.id);
        insert a;
        /*Directory__c directory = TestMethodsUtility.generateDirectory();
        directory.Months_for_National__c = 12;
        insert directory;*/
         Directory__c directory =TestMethodsUtility.createDirectory();
        Directory_Edition__c directoryEdition=new Directory_Edition__c(Name='Test Diretory Edition',Book_Status__c='NI',Directory__c=directory.id,LSA_Directory_Version__c='4444');
        insert directoryEdition;
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.ProductCode = 'FP';
        newProduct.IsActive = true;
        insert newProduct;
        Test.startTest();
        National_Staging_Order_Set__c no=new National_Staging_Order_Set__c();
        no.Client_Number__c='2222';
        no.CMR_Number__c='333';
        no.Directory_Number__c='4444';   
        no.RecordTypeId=rcforNSOS.id;
        no.Directory__c = directory.Id; 
        insert no;    
        National_Staging_Line_Item__c nl=new National_Staging_Line_Item__c();
        nl.National_Staging_Header__c=no.id;

        insert nl;
        PageReference pageRef = Page.natManInput_V1;
        Test.setCurrentPageReference(pageRef); 
        pageRef.getParameters().put('id',no.Id);
        pageRef.getParameters().put('index','0');
        ApexPages.StandardController controller = new ApexPages.StandardController(new National_Staging_Order_Set__c());
        natManInputEdit nObj=new natManInputEdit(controller);
        System.debug('^^^^^^^^^^^^^^^^ '+rcforNSOS.Id);
        nObj.UDAC = 'FP';
        nObj.Cancel();
        nobj.addLineItems();
        nObj.save();
        Test.stopTest();
        
    }        
}