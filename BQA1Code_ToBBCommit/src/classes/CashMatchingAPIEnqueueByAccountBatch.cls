/*
    * FinancialForce.com, inc. claims copyright in this software, its screen display designs and
    * supporting documentation. FinancialForce and FinancialForce.com are trademarks of FinancialForce.com, inc.
    * Any unauthorized use, copying or sale of the above may constitute an infringement of copyright and may
    * result in criminal or other legal proceedings.
    *
    * Copyright FinancialForce.com, inc. All rights reserved.
*/

public class CashMatchingAPIEnqueueByAccountBatch implements Database.Batchable<Id>, Database.Stateful
{
    private Set<String> errorMessages;
    private Set<String> successMessages;
    
    public Integer enqueuedCount = 0;
    
    public List<Id> accountIdsToProcess {get;set;}
    
    
    public CashMatchingAPIEnqueueByAccountBatch() 
    {
        errorMessages = new Set<String>();
        successMessages = new Set<String>();
    }

    public Iterable<Id> start(Database.BatchableContext BC) 
    {
        return (Iterable<Id>)accountIdsToProcess;
    }

    public void execute(Database.BatchableContext BC, List<Id> unTypedScope) 
    {
        for(Id accountId :  (List<Id>)unTypedScope)
        {
            System.enqueueJob(new CashMatchingApiByAccountQueueable(accountId));
            enqueuedCount += 1;
        }
    }
    
    public void finish(Database.BatchableContext BC) 
    {
        successMessages.add('Account Ids kicked off to be processed' );
        for(Id accountId : accountIdsToProcess)
        {
            successMessages.add(accountId);
        }
        successMessages.add('Threads started = '+enqueuedCount);
        sendEmail('mfleming@financialforce.com',successMessages,errorMessages);
    }
    
    private void sendEmail( String address, Set<String> successMessages, Set<String> errorMessages  )
    {   
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses( new String[] { address } );

        String body = 'Results of Matching - Matching API (CashMatchingAPIEnqueueByAccountBatch):\n\n';
        
        if( errorMessages.size() > 0 )
        {
            body += 'Errors occurred.\n';
            for( String error : errorMessages )
            {
                body += error +'\n';
            }
            body += '\n';
        }
        else
        {
            body += 'No errors occurred.\n';
            body += '\n';
        }
        for( String success : successMessages )
        {
            body += success +'\n';
        }
        
        mail.setPlainTextBody( body );
        mail.setSubject( 'Results of Matching - Matching API (CashMatchingAPIEnqueueByAccountBatch)' );
        
        Messaging.sendEmail( new Messaging.SingleEmailMessage[] { mail } );
    }
}