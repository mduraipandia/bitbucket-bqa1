@isTest
public with sharing class GalleyReportPageControllerTest {
    public static testMethod void testGalleyRep() {
        Account telcoAcct = TestMethodsUtility.createAccount('telco');
        Telco__c telco = TestMethodsUtility.createTelco(telcoAcct.Id);
        Account acct = TestMethodsUtility.createAccount('cmr');
        Contact cnt = TestMethodsUtility.createContact(acct.Id);
        Order__c ord = TestMethodsUtility.createOrder(acct.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(acct, cnt);
        insert oppty;
        Order_Group__c og = TestMethodsUtility.generateOrderSet(acct, ord, oppty);
        og.selected__c = true;
        insert og;
        Listing__c listing = TestMethodsUtility.generateMainListing();
        listing.Bus_Res_Gov_Indicator__c = 'Business';
        listing.Telco_Provider_Name__c = telco.Id;
        insert listing;
        Order_Line_Items__c OLI = TestMethodsUtility.generateOrderLineItem(acct, cnt, oppty, ord, og);              
        OLI.Quote_Signed_Date__c=system.today()+1;
        OLI.Order_Anniversary_Start_Date__c=system.today()-5;
        OLI.Talus_Go_Live_Date__c=system.today()+30;
        OLI.UnitPrice__c = 300;
        OLI.Listing__c = listing.Id;
        insert OLI; 
        Canvass__c canvass = TestMethodsUtility.createCanvass();
        /*Directory__c dir = TestMethodsUtility.generateDirectory();
        dir.Telco_Provider__c = telco.Id;
        insert dir;*/
        Directory__c dir = TestMethodsUtility.createDirectory();  
        Directory_Edition__c DE = TestMethodsUtility.generateDirectoryEdition(dir);
        DE.Name = 'Test';
        DE.Canvass__c = canvass.Id;
        DE.Directory__c = dir.Id;
        DE.Book_Status__c = 'BOTS';
        DE.Pub_Date__c = system.today();
        DE.Sales_Lockout__c = system.today();
        DE.Telco__c = telco.Id;
        insert DE;
        DE = [SELECT Id, Year__c, Name FROM Directory_Edition__c WHERE Directory__c =: dir.Id ORDER BY Year__c];
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(dir);
        Directory_Pagination__c dirPag = TestMethodsUtility.generateDirectoryPagination(objDS.Id, OLI.Id, DE.Id);
        insert dirPag;
        
        GalleyReportPageController obj = new GalleyReportPageController();
        
        obj.selectedDirId = dir.Id;
        obj.fetchDirEdDirSec();
        obj.listDirEdAvaVal.add(DE.Id);
        obj.addDirEd();
        obj.removeAllDirEd();
        obj.addAllDirEd();
        obj.listDirEdSelVal.add(DE.Id);
        obj.removeDirEd();
        obj.listDirSecAvaVal.add(objDS.Id);
        obj.addDirSec();
        obj.removeAllDirSec();
        obj.addAllDirSec();
        obj.listDirSecSelVal.add(objDS.Id);
        obj.removeDirSec();
        obj.listBusTypAvaVal.add('Business');
        obj.addBusTyp();
        obj.removeAllBusTyp();
        obj.addAllBusTyp();
        obj.listBusTypSelVal.add('Business');
        obj.removeBusTyp();
        obj.listTelcoAvaVal.add(telco.Id);
        obj.addTelco();
        obj.removeAllTelco();
        obj.addAllTelco();
        obj.removeTelco();
        obj.listDirEdSelOpt.add(new SelectOption(DE.Name + '--' + DE.Id, DE.Name));
        obj.listDirSecSelOpt.add(new SelectOption(objDS.Name + '--' + objDS.Id, objDS.Name));
        obj.listTelcoSelOpt.add(new SelectOption(telco.Id, telco.Name));
        obj.listBusTypSelOpt.add(new SelectOption('Business', 'Business'));
        obj.callGalleyBatch();
        obj.fetchDirs();
        obj.pollingBatchProcess();
    }   
}