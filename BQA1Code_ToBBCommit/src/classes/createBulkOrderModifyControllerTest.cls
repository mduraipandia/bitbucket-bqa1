@isTest
public class createBulkOrderModifyControllerTest{
    static testMethod void createBulkOrderModifyController01() {
       
       
         Profile acctMgrProfile = TestMethodsUtility.getAccountManagerProfile();
        UserRole userrole= TestMethodsUtility.getPerformanceAdvisorRole();        
        User actMgr = TestMethodsUtility.createAccountManagerRoleUser(acctMgrProfile.id,userrole.id);
        
        User oppOwner = TestMethodsUtility.createUser();
        system.runAs(actMgr)
        {
         //inserting custom setting data
        
        Set<String> ObjectSet = new Set<String>{'Account','Contact','DFF','Lead','Listing','Opportunity','OpportunityLineItem','Order','OrderLineItem','OrderSet','ScopedListing'};
        
        List<SkipTriggerExecution__c> SkipTriggerExecutionsetting = new List<SkipTriggerExecution__c>();
        
        for(String obj : ObjectSet)
        {
           SkipTriggerExecutionsetting.add(new SkipTriggerExecution__c(Name=obj,InActivated__c=false));
        }
        upsert SkipTriggerExecutionsetting;
        
    
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account(Statement_Suppression__c=true);
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
            if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount = iterator;
            }
            else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
                newPubAccount = iterator;
            }
            else {
                newTelcoAccount = iterator;
            }
        }
        
        Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.Id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        
        Division__c objDiv = TestMethodsUtility.createDivision();
        
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;
        
        Directory_Heading__c objDH = TestMethodsUtility.generateDirectoryHeading();
        objDH.YPC_Heading__c = true;
        insert objDH;
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;
        
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        insert objDirEd;
        
     //   Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir);
      //  insert objDirEd1;
        
        Directory_Mapping__c objDM = TestMethodsUtility.generateDirectoryMapping(null);
        objDM.Telco__c = objDir.Telco_Provider__c;
        objDM.Canvass__c = objDir.Canvass__c;
        objDM.Directory__c = objDir.Id;
        insert objDM;
        
        list<Product2> lstProduct = new list<Product2>();      
        for(Integer x=0; x<=10;x++){
            Product2 newProduct = TestMethodsUtility.generateproduct();
            newProduct.Product_Type__c = 'SEO';
            newProduct.Vendor__c =CommonMessages.prdVendorYP;
            if(x==1) {
                newProduct.Product_Type__c = 'Online Listing Monitoring';
                newProduct.Family='Digital';
                newProduct.Addon_Type__c=CommonMessages.prdAddonTypePoints;
            }
            if(x==0){
                newProduct.Product_Type__c = 'MSA Website';
                newProduct.Vendor__c =CommonMessages.prdVendorWebCom;
            }
            if(x==2){
                newProduct.Product_Type__c = 'Spotzer Website';
                 newProduct.Addon_Type__c =CommonMessages.prdAddonTypeAddOn;
            }
             if(x==4){
                newProduct.Product_Type__c = 'Register Domain';
            }
            if(x==5){
                newProduct.Product_Type__c = 'Facebook';
            }
            if(x==6){
                newProduct.Product_Type__c = 'OPO';
            }
            if(x==7){
                newProduct.Product_Type__c = 'Online Display';
            }
            if(x==8){
                newProduct.Product_Type__c = 'Extra Pages';
                newProduct.Addon_Type__c =CommonMessages.prdAddonTypeCoupon;
            }
            if(x==9){
                newProduct.Product_Type__c = 'General Add-on';
                newProduct.Addon_Type__c =' Add On';
                newProduct.ProductCode = 'YPCP';
                newProduct.Family = 'Digital';
            }
            if(x==10){
                newProduct.Product_Type__c = 'iYP Bronze';
                newProduct.ProductCode = 'BR';
                newProduct.Family = 'Digital';
            }
            if(x==3){
                newProduct.Product_Type__c = 'General Add-on';
                newProduct.Addon_Type__c =CommonMessages.prdAddonType5Heading;
            }
            newProduct.Inventory_Tracking_Group__c = 'YP Leader Ad';
           
            lstProduct.add(newProduct);
        }
        insert lstProduct;
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Family = 'Print';
        insert newProduct;
        List<pricebookentry> lstinsertPBE = new List<pricebookentry>();
        //pricebook2 objPB = TestMethodsUtility.createpricebook();
        
        PricebookEntry pbePrint = new PricebookEntry(UnitPrice = 0, Product2Id = newProduct.ID, Pricebook2Id = newPriceBook.id, IsActive = true);
        insert pbePrint;
        
        Directory_Product_Mapping__c objDPM1 = TestMethodsUtility.generateDirectoryProductMapping(objDir);
        objDPM1.Product2__c = newProduct.Id;
        insert objDPM1;
        
        list<Directory_Product_Mapping__c> lstDPM = new list<Directory_Product_Mapping__c>();        
        for(Product2 iterator : lstProduct) {
        
        PricebookEntry pbe = new PricebookEntry(UnitPrice = 0, Product2Id = iterator.ID, Pricebook2Id = newPriceBook.id, IsActive = iterator.IsActive);
        lstinsertPBE.add(pbe);
        Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(objDir);
        objDPM.Product2__c = iterator.Id;
        }
        insert lstinsertPBE;
        insert lstDPM;
        
         //---added 
        
       
                
        list<PricebookEntry> lstPBE = [Select Id, Product2Id, product2.Product_Type__c, Product2.ProductCode  from PricebookEntry where Product2Id IN:lstProduct];
                
        Opportunity newOpportunity1 = TestMethodsUtility.generateOpportunity('new');
        newOpportunity1.AccountId = newAccount.Id;
        newOpportunity1.Pricebook2Id = newPriceBook.Id;
        newOpportunity1.Billing_Contact__c = newContact.Id;
        newOpportunity1.Signing_Contact__c = newContact.Id;
        newOpportunity1.Billing_Partner__c = objTelco.Telco_Code__c;
        newOpportunity1.Payment_Method__c = CommonMessages.telcoPaymentMethod;      
        newOpportunity1.StageName='Closed UTC';
        //newOpportunity1.Account_Manager__c = actMgr.id;
        newOpportunity1.owner=actMgr;
        insert newOpportunity1;
        
        system.debug('------owner-----'+newOpportunity1.owner.lastname);
        
         Order__c newOrder1 = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet1 = TestMethodsUtility.createOrderSet(newAccount, newOrder1, newOpportunity1);
        Order_Line_Items__c newOrLI1 = TestMethodsUtility.createOrderLineItem(newAccount, newContact, newOpportunity1, newOrder1, newOrderSet1);
        
        List<Order_Line_Items__c> listnewOrli = new List<Order_Line_Items__c>();
        listnewOrli.add(newOrLI1);
        
    List<Modification_Order_Line_Item__c> MOLIList1 = new List<Modification_Order_Line_Item__c>();
    Modification_Order_Line_Item__c mol1 = new Modification_Order_Line_Item__c(Billing_Frequency__c='Monthly',Order_Line_Item__c=newOrLI1.id);
   // mol1.Directory_Section__c = objDS.Id;
    mol1.Order_Group__c = newOrderSet1.Id;
    mol1.Parent_ID__c='2014-8235_tst11';
    mol1.Opportunity__c = newOpportunity1.id;
    mol1.Action_Code__c = CommonMessages.NewRenewalAction ;
    mol1.Product_Type__c='SEO';
    mol1.Product2__c=lstProduct[0].id;
    mol1.Parent_ID_of_Addon__c='2014-8235_tst11';
    mol1.Product_Code__c= '2345';
     Modification_Order_Line_Item__c mol2 = new Modification_Order_Line_Item__c(Billing_Frequency__c='Monthly',Order_Line_Item__c=newOrLI1.id);
   // mol1.Directory_Section__c = objDS.Id;
    mol2.Order_Group__c = newOrderSet1.Id;
    mol2.Parent_ID__c='2014-8235_tst1sgfg1';
    mol2.Opportunity__c = newOpportunity1.id;
    mol2.Action_Code__c = 'Renew' ;
    mol2.Product_Type__c='SEO';
    mol2.Product2__c=lstProduct[1].id;
    mol2.Parent_ID_of_Addon__c='2014-8235_tst1sgfg1';
    mol2.Product_Code__c= '2345567678';
     Modification_Order_Line_Item__c mol3 = new Modification_Order_Line_Item__c(Billing_Frequency__c='Monthly',Order_Line_Item__c=newOrLI1.id);
   mol3.Order_Group__c = newOrderSet1.Id;
   mol3.Opportunity__c = newOpportunity1.id;
    mol3.Action_Code__c = 'Renew' ;
    mol3.Product_Type__c='SEO';
    mol3.Product2__c=lstProduct[2].id;
   mol3.Product_Code__c= '23455676';
    mol3.Parent_ID__c='2014-8235_tst1sgfg3';
    mol3.Parent_ID_of_Addon__c='2014-8235_tst1sgfg3';
   Modification_Order_Line_Item__c mol4 = new Modification_Order_Line_Item__c(Billing_Frequency__c='Monthly',Order_Line_Item__c=newOrLI1.id);
   mol4.Order_Group__c = newOrderSet1.Id;
   mol4.Opportunity__c = newOpportunity1.id;
    mol4.Action_Code__c = 'Renew' ;
    mol4.Product_Type__c='SEO';
    mol4.Product2__c=lstProduct[3].id;
   mol4.Product_Code__c= '23455676800';
    mol4.Parent_ID__c='2014-8235_tst1sgfg2';
    mol4.Parent_ID_of_Addon__c='2014-8235_tst1sgfg2';
   Modification_Order_Line_Item__c mol5 = new Modification_Order_Line_Item__c(Billing_Frequency__c='Monthly',Order_Line_Item__c=newOrLI1.id);
   mol5.Order_Group__c = newOrderSet1.Id;
   mol5.Opportunity__c = newOpportunity1.id;
    mol5.Action_Code__c = 'Renew' ;
    mol5.Product_Type__c='SEO';
    mol5.Product2__c=lstProduct[4].id;
   mol5.Product_Code__c= '23455676802';
   Modification_Order_Line_Item__c mol6 = new Modification_Order_Line_Item__c(Billing_Frequency__c='Monthly',Order_Line_Item__c=newOrLI1.id);
   mol6.Order_Group__c = newOrderSet1.Id;
   mol6.Opportunity__c = newOpportunity1.id;
    mol6.Action_Code__c = 'Renew' ;
    mol6.Product_Type__c='SEO';
    mol6.Product2__c=lstProduct[5].id;
   mol6.Product_Code__c= '23455676820'; 
   Modification_Order_Line_Item__c mol7 = new Modification_Order_Line_Item__c(Billing_Frequency__c='Monthly',Order_Line_Item__c=newOrLI1.id);
   mol7.Order_Group__c = newOrderSet1.Id;
   mol7.Opportunity__c = newOpportunity1.id;
    mol7.Action_Code__c = 'Renew' ;
    mol7.Product_Type__c='SEO';
    mol7.Product2__c=lstProduct[6].id;
   mol7.Product_Code__c= '23455676820'; 
    Modification_Order_Line_Item__c mol8 = new Modification_Order_Line_Item__c(Billing_Frequency__c='Monthly',Order_Line_Item__c=newOrLI1.id);
   mol8.Order_Group__c = newOrderSet1.Id;
   mol8.Opportunity__c = newOpportunity1.id;
    mol8.Action_Code__c = 'Renew' ;
    mol8.Product_Type__c='SEO';
    mol8.Product2__c=lstProduct[7].id;
   mol8.Product_Code__c= '23455676820'; 
    Modification_Order_Line_Item__c mol9 = new Modification_Order_Line_Item__c(Billing_Frequency__c='Monthly',Order_Line_Item__c=newOrLI1.id);
   mol9.Order_Group__c = newOrderSet1.Id;
   mol9.Opportunity__c = newOpportunity1.id;
    mol9.Action_Code__c = 'Renew' ;
    mol9.Product_Type__c='SEO';
    mol9.Product2__c=lstProduct[8].id;
   mol9.Product_Code__c= '234556768223'; 
     mol9.Parent_ID__c='2014-8235_tst1sgfu3';
    mol9.Parent_ID_of_Addon__c='2014-8235_tst1sgfu3';
    
    MOLIList1.add(mol1);
    MOLIList1.add(mol2);
     MOLIList1.add(mol3);
      MOLIList1.add(mol4);
        MOLIList1.add(mol5);
          MOLIList1.add(mol6);
        MOLIList1.add(mol7);
          MOLIList1.add(mol8);
            MOLIList1.add(mol9);
    insert MOLIList1; 
    
    
        list<OpportunityLineItem> lstOLI1 = new list<OpportunityLineItem>();
        map<Id, PricebookEntry> mapPBE1 = new map<Id, PricebookEntry>();
        Integer i = 0;
        for(PricebookEntry iterator : lstPBE) {
            i++;
            OpportunityLineItem objOLI1 = TestMethodsUtility.generateOpportunityLineItem();
            objOLI1.PricebookEntryId = iterator.Id;
            objOLI1.Billing_Duration__c = 12;
            objOLI1.Directory__c = objDir.Id;
            objOLI1.Directory_Edition__c = objDirEd.Id;
            objOLI1.Full_Rate__c = 30.00;
            objOLI1.UnitPrice = 30.00;
            objOLI1.Package_ID__c = '123456';
            objOLI1.Billing_Partner__c = objOLI1.Id;
            objOLI1.OpportunityId = newOpportunity1.Id;
            objOLI1.Directory_Heading__c = objDH.Id;
            objOLI1.Directory_Section__c = objDS.Id;
            objOLI1.Parent_ID_of_Addon__c='2014-8235_tst1';
            //System.debug('Testinggg i '+i);
            if(iterator.product2.Product_Type__c == 'Online Listing Monitoring') {
                //System.debug('Testinggg i value in IF cond '+i);
                objOLI1.Renewals_Action__c=CommonMessages.RenewRenewalAction;
            }
            else if(iterator.product2.Product_Type__c == 'OPO') {
                //System.debug('Testinggg i value in IF cond '+i);
                objOLI1.Renewals_Action__c=CommonMessages.ModifyRenewalAction;
            }
            else {
                //System.debug('Testinggg i value in else cond '+i);
                objOLI1.Renewals_Action__c=CommonMessages.NewRenewalAction;
            }
            if(iterator.product2.ProductCode == 'YPCP') {
                objOLI1.Parent_ID_of_Addon__c='123';
            }
            else if(iterator.product2.ProductCode == 'BR') {
                objOLI1.Parent_ID_of_Addon__c=null;
                objOLI1.Parent_ID__c='123';
                objOLI1.Renewals_Action__c=CommonMessages.RenewRenewalAction;
            }
        //    objOLI1.Renewals_Action__c='Renew'; 
      
      //objOLI1.Parent_ID__c ='2014-8235_tst2';
      
      objOLI1.Effective_Date__c = System.Today().addDays(2);
      objOLI1.Original_Line_Item_ID__c = newOrLI1.id;
     
      objOLI1.P4P_Billing__c = true;
            lstOLI1.add(objOLI1);
            mapPBE1.put(iterator.Id, iterator);
        }
         insert lstOLI1;
         
         OpportunityLineItem objOLI1 = TestMethodsUtility.generateOpportunityLineItem();
            objOLI1.PricebookEntryId = pbePrint.Id;
            objOLI1.Billing_Duration__c = 12;
            objOLI1.Directory__c = objDir.Id;
            objOLI1.Directory_Edition__c = objDirEd.Id;
            objOLI1.Full_Rate__c = 30.00;
            objOLI1.UnitPrice = 30.00;
            objOLI1.Package_ID__c = '123456';
            objOLI1.Billing_Partner__c = objOLI1.Id;
            objOLI1.OpportunityId = newOpportunity1.Id;
            objOLI1.Directory_Heading__c = objDH.Id;
            objOLI1.Directory_Section__c = objDS.Id;
            objOLI1.Renewals_Action__c=CommonMessages.RenewRenewalAction;
            objOLI1.Original_Line_Item_ID__c = newOrLI1.id;
            insert objOLI1;
            
         System.debug('Testinggg lstOLI1 '+lstOLI1);
        listnewOrli[0].Opportunity_line_Item_id__c = lstOLI1[0].Id;
        pymt__PaymentX__c initialpayment = new pymt__PaymentX__c(name='Initial Payment');
        insert initialpayment;
         
        List<Digital_Product_Requirement__c> lstDFF = new List<Digital_Product_Requirement__c >();
        Digital_Product_Requirement__c DFF = TestMethodsUtility.generateDataFulfillmentForm('iYP Gold');
        DFF.Account__c = lstAccount[1].Id;
        DFF.Contact__c = newContact.Id;
        DFF.Proof_Contact__c = newContact.Id;
        DFF.UDAC__c ='88894';
        DFF.recordtypeid=Schema.SObjectType.Digital_Product_Requirement__c.getRecordTypeInfosByName().get('Multi Ad').getRecordTypeId();
        
        DFF.Heading_1__c = objDH.Id;
        DFF.Heading_2__c = objDH.Id;
        DFF.Heading_3__c = objDH.Id;
        DFF.Heading_4__c = objDH.Id;
        DFF.Heading_5__c = objDH.Id;
        DFF.OrderLineItemID__c=newOrLI1.Id;
        DFF.Talus_DFF_Id__c = '123';
        DFF.Talus_Subscription_Id__c = '123';
        DFF.OpportunityID__c=newOpportunity1.Id;
        DFF.Competitor_1__c = 'test';
        DFF.Competitor_2__c = 'test';
        DFF.Competitor_3__c = 'test';   
        DFF.Employee_1__c = 'test';
        DFF.Employee_2__c = 'test';
        DFF.Employee_3__c = 'test';
        DFF.Affiliation_1__c = 'test';
        DFF.Affiliation_2__c = 'test';   
        DFF.Affiliation_3__c = 'test';
        DFF.Affiliation_4__c = 'test';         
        DFF.Also_Known_As_aka_1__c = 'test';
        DFF.Also_Known_As_aka_2__c = 'test';
        DFF.Also_Known_As_aka_3__c = 'test';   
        DFF.Also_Known_As_aka_4__c = 'test';
        DFF.Also_Known_As_aka_5__c = 'test'; 
        DFF.CpnDesc_CstLnkUrl_CstLnkTxt__c = true; 
        DFF.add_ons__c = 'YPCP';
        DFF.coupon_description__c = 'test';
        DFF.ModificationOrderLineItem__c=MOLIList1[0].Id;
        lstDFF.add(DFF);
        insert lstDFF;
        
        
        map<id,Opportunity> mpOpp = new map<id,Opportunity>();
        mpOpp.put(newOpportunity1.id,newOpportunity1);
        
        Fulfillment_Profile__c objFP = TestMethodsUtility.createFulfillmentProfile(newAccount);
        
        map<Id, Fulfillment_Profile__c> mpFP = new map<Id, Fulfillment_Profile__c>();
        mpFP.put(newAccount.id,objFP);
        
         system.debug('------owner--1---'+newOpportunity1.owner.lastname);
        //Added some fields by Manish Jhingran for ATP-2195
        newOpportunity1 = [Select Owner.CommunityNickname, RAS_ID__c, RAS_Date__c, StageName,LastModifiedBy.Email,LastModifiedBy.ManagerId,Account.Name,Account.Billing_Anniversary_Date__c,Account.Berry_ID__c, Signing_Method__c, Owner.Name, Owner.email, Billing_Partner__c, modifyid__c, isLocked__c, Payment_Method__c, Name, Billing_Frequency__c, Billing_Contact__r.Contact_Role__c, Billing_Contact__r.Email, Billing_Contact__r.Statement_Suppression__c, Billing_Contact__r.Phone, Billing_Contact__r.MailingCountry, Billing_Contact__r.MailingPostalCode, Billing_Contact__r.MailingState, Billing_Contact__r.MailingCity, Billing_Contact__r.MailingStreet, Billing_Contact__c, Account.Delinquency_Indicator__c, Account.BillingCountry, Account.BillingPostalCode, Account.BillingState,
                             Account_Primary_Canvass__r.Billing_Entity__c,Account.BillingCity, Account.BillingStreet, AccountId, Signing_Contact__c, Account.Phone, Account.TalusAccountId__c,
                              Billing_Contact__r.TalusContactId__c, Account.Statement_Suppression__c,Account_Number__c, Account.Account_Number__c, 
                              Account_Manager__r.Email, Account_Manager__r.FirstName, Account_Manager__r.LastName, Account_Manager__c, Close_Date__c,
                               IsClosed, IsWon, DocuSign_Received__c, OwnerId, Owner.FirstName, Owner.CompanyName, Owner.Fax, Owner.Phone,Owner.lastname,
                                Owner.PostalCode, Owner.Street, Owner.city, Owner.State, Owner.Country, (Select Id, Opportunity.modifyid__c, OpportunityId, 
                                Full_Rate__c,SortOrder, PricebookEntryId, Quantity, Discount, UnitPrice, ServiceDate, Description, Listing_Id__c, Listing__c,
                                 Renewals_Action__c, Is_Anchor__c,Is_Caption__c,Is_Child__c, Canvass__c, PricebookEntry.Name, directory__c, PricebookEntry.Product2.Media_Type__c,
                                  PricebookEntry.Pricebook2Id, Original_Line_Item_ID__r.Status__c, PricebookEntry.Product2Id, PricebookEntry.ProductCode, Parent_ID__c,
                                   PricebookEntry.Product2.Product_Type__c, PricebookEntry.Product2.Addon_Type__c, Distribution_Area__c, Original_Line_Item_ID__r.Order_Group__c,
                                    Digital_Product_Requirement__c,Effective_Date__c,Billing_Duration__c,Product_Type__c,Billing_Start_Date__c, Billing_End_Date__c, 
                               isModify__c,Directory__r.Telco_Provider__c,Canvass__r.Primary_Telco__c, Package_ID__c, Parent_Line_Item__c, Geo_Type__c, Geo_Code__c, 
                               is_p4p__c,Scope__c, Directory_Section__c, Status__c,Category__c, Directory_Heading__c, P4P_Price_Per_Click_Lead__c,P4P_Billing__c ,
                                Directory_Edition__r.Bill_Prep__c, Directory_Edition__r.name, Auto_Number__c, Locality__c, Directory_Edition__c, Directory_Edition__r.New_Print_Bill_Date__c, 
                                Listing_Name__c,Pricing_Program__c, Parent_ID_of_Addon__c, Original_Line_Item_ID__c, strOppliCombo__c, Package_Item_Quantity__c, 
                                PricebookEntry.Product2.Inventory_Tracking_Group__c,PricebookEntry.Product2.Is_IBUN_Bundle_Product__c, Package_Name__c,
                                Requires_Inventory_Tracking__c,CORE_Migration_ID__c,Package_ID_External__c,Original_Line_Item_ID__r.Directory_Edition__c,
                                Display_Ad__c,PricebookEntry.Product2.Vendor__c,Original_Line_Item_ID__r.Digital_Product_Requirement__c, Original_Line_Item_ID__r.Talus_Subscription_ID__c,
                                Original_Line_Item_ID__r.Effective_Date__c,UDAC__c,Seniority_Date_Reset__c,Original_OLI_Seniority_Date__c,
                                Original_Line_Item_ID__r.P4P_Tracking_Number_ID__c,Original_Line_Item_ID__r.Talus_Go_Live_Date__c, Opportunity.Owner.CommunityNickname,
                                Original_Line_Item_ID__r.Sent_to_fulfillment_on__c, Original_Line_Item_ID__r.Digital_Product_Requirement__r.Data_Fulfillment_Form__c, 
                                Original_Line_Item_ID__r.FulfillmentDate__c,Original_Line_Item_ID__r.Directory__c, Original_Line_Item_ID__r.Listing__c, 
                                Original_Line_Item_ID__r.ProductCode__c, Original_Line_Item_ID__r.Directory_Heading__c, Original_Line_Item_ID__r.Talus_DFF_Id__c,
                                Original_Line_Item_ID__r.P4P_Tracking_Number_Status__c,BCC_Applied__c,BCC_Duration__c,BCC__c,BCC_Redeemed__c,
                                Berry_Cares_Certificate_ID__c,OPPLI_Exception_Discount__c,Original_Line_Item_ID__r.Package_ID_External__c
                                From OpportunityLineItems order by Parent_ID__c, Parent_ID_of_Addon__c) 
                                From Opportunity where id =:newOpportunity1.id];
                                
                                
                                 system.debug('------owner--2---'+newOpportunity1.owner.lastname);
        System.debug('Testinggg newOpportunity1 '+newOpportunity1);
        System.debug('Testinggg newOpportunity1.OpportunityLineItems '+newOpportunity1.OpportunityLineItems);
        Test.startTest();
        createBulkOrderModifyController CTRL= new createBulkOrderModifyController();
      //  createBulkOrderModifyController.renewMOLI=MOLIList1; 
        createBulkOrderModifyController.FindAddOnProductForDFFAndCreateDFF1(MOLIList1, mpOpp, mpFP);        
       createBulkOrderModifyController.CreateOrderAndModifyLineItem(new List<opportunity>{newOpportunity1}, new List<Order__c>{newOrder1});
       createBulkOrderModifyController.CreateNewOrderGroup(new List<opportunity>{newOpportunity1},new List<Order__c>{newOrder1});
       //Added by Manish Jhingran to improve the code coverage for ATP-2195
        createBulkOrderModifyController.createModifyOLIDFF(MOLIList1,mpFP);
        Map<id,Opportunity> mapOpp = new Map<id,Opportunity>();
        mapOpp.put(newOpportunity1.id, newOpportunity1);
        createBulkOrderModifyController.newOLIProcess(mapOpp,new List<OpportunityLineItem>{objOLI1}, new List<Order__c>{newOrder1}, new List<Order_Group__c>{newOrderSet1}, new List<Order_Line_Items__c>{});
       //Added by Manish Jhingran to improve the code coverage for ATP-2195
      //  createBulkOrderModifyController.modifyOLIProcess(mpOpp,lstOLI1,new List<Order__c>{newOrder1},new List<Order_Group__c>{newOrderSet1});
        Test.stopTest();
    }
    }
}