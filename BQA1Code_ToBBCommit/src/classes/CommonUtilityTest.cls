@isTest
public class CommonUtilityTest{

    public static testmethod void cmmnUtlty(){
        
    String jsnFld = 'additional_emails';
    String stringJSON = '[ { "error_code": "PROP:PROPERTY_VALIDATION", "external_code": "SWSLBQA1:a23Z00000019zWKIAY", "message": { "business_phone_number_office": [ "Invalid Phone number." ], "product_type": [ "This field cannot be blank." ], "product_type_id": [ "This field cannot be null." ] }, "product_sku": "WEBSITE0001_BASE" }, { "error_code": "PROP:PROPERTY_VALIDATION", "external_code": "WEBURLBQA1:a23Z00000019zWTIAY", "message": { "business_url": [ "This field cannot be blank." ] }, "product_sku": "WEBSITE0001_REGDOMAIN" }, { "error_code": "PROP:PROPERTY_VALIDATION", "external_code": "BSO5BQA1:a23Z00000019zWNIAY", "message": { "business_email": [ "Enter a valid email address." ], "business_phone_number_office": [ "Invalid Phone number." ] }, "product_sku": "WEBSITE0001_SEO" }, { "error_code": "PROP:PROPERTY_VALIDATION", "external_code": "BSPROBQA1:a23Z00000019zWSIAY", "message": { "business_email": [ "Enter a valid email address." ] }, "product_sku": "WEBSITE0001_OPM" }, { "error_code": "PROP:PROPERTY_VALIDATION", "external_code": "ODPA:a23Z00000019zWSIAY", "message": { "business_email": [ "Enter a valid email address." ], "promotion_click_url": [ "Enter a valid URL." ] }, "product_sku": "WEBSITE0001_DISPLAYADS" }, { "error_code": "PROP:PROPERTY_VALIDATION", "external_code": "FBBQA1:a23Z00000019zWSIAY", "message": { "contact_phone_number_office": [ "Invalid Phone number." ] }, "product_sku": "WEBSITE0001_FB" }, { "error_code": "PROP:PROPERTY_VALIDATION", "external_code": "SWDAP:a23Z00000019zWSIAY", "message": { "business_country": [ "Value u USA is not a valid choice." ] }, "product_sku": "WEBSITE0001_XPAGES" } ]';
    Date dt = System.today();
    Set<String> pkgId = new Set<String>{'PKG_IYP001_METAL'};
    Set<String> prdtId = new Set<String>{'IYP0001_METAL'};
    
    Test.StartTest();
    
    CommonUtility.jsonObjFlds(jsnFld);
    CommonUtility.talusDffRT();
    CommonUtility.milesDffRT();
    CommonUtility.yodleDffRT();
    CommonUtility.manualDffRT();
    CommonUtility.lookupFields();
    CommonUtility.utcDate(dt);
    CommonUtility.pkgPrdtPrpts(pkgId, prdtId);
    CommonUtility.dffReadyToSubmit();
    CommonUtility.dffReqHrs();
    CommonUtility.mockGetRspns();
    CommonUtility.dffQueryString();
    CommonUtility.fpQueryString();
    CommonUtility.msgConfirm('TestConfirm');
    CommonUtility.msgInfo('TestInfo');
    CommonUtility.msgWarning('TestWarning');
    CommonUtility.msgError('TestError');
    CommonUtility.msgFatal('TestFatal');
    CommonUtility.dffValdtnFlds(StringJSON);
    
    Test.StopTest();
    
    }

}