global class ModificationOrderLCleanUpSchedulerHndlr implements ModificationOrderLineCleanUpScheduler.ModificationOrderLineCleanUpSchedulerInterface {
    
    // Script to execute ModificationOrderLineCleanUpScheduler from Developer Console
    
    
    
   /* ModificationOrderLineCleanUpScheduler MOLIcleanup = new ModificationOrderLineCleanUpScheduler ();

    String jobID = system.schedule('Modification Order line Item job',  ModificationOrderLineCleanUpScheduler.getScheduleString() , MOLIcleanup);

    System.debug('***'+ jobID);  */


 global void execute(SchedulableContext sc) 
 {
    /*try 
        {
            //Abort the existing schedule as this is a 1 time scheduled
            //job and should not run again
            CronTrigger ct = [SELECT id FROM CronTrigger WHERE id = :sc.getTriggerId()];
            if (ct != null)
            {
                System.abortJob(ct.id);
            }
        } 
        catch (Exception e) 
        {
            System.debug('There are no jobs currently scheduled.'); 
        }*/
        ModificationOrderLineCleanUpBatchImpl MOLIobject = new ModificationOrderLineCleanUpBatchImpl();
        MOLIobject.query= 'SELECT id, LastModifiedDate, Inactive__c,Completed__c FROM Modification_Order_Line_Item__c where LastModifiedDate < LAST_90_DAYS  and (Inactive__c = true or Completed__c = true)';
        
        System.debug('Print query : ' + MOLIobject.query);
        
        ID batchprocessid =Database.executeBatch(MOLIobject,2000);
        System.debug('Returned batch process ID: ' + batchProcessId);
    
  
 }
 
 public static String getScheduleString() {
        Datetime datet = Datetime.now();
        datet = datet.addHours(0);
        datet = datet.addMinutes(0);
        datet = datet.addSeconds(5);
        String scheduleString = '';
        scheduleString += '' + datet.second();
        scheduleString += ' '+ datet.minute();
        scheduleString += ' '+ datet.hour();
        scheduleString += ' '+ datet.day();
        scheduleString += ' '+ datet.month();
        scheduleString += ' ?';
        scheduleString += ' '+ datet.year();
        return scheduleString;
    }



 
}