public with sharing class OrderSOQLMethods {
    
    public static Order__c getOrderByAccountID(set<String> accountId) {
        return [Select Name, Id, Billing_Anniversary_Date__c, Account__c, 
            (Select Name, Billing_Contact__c, Billing_End_Date__c, Billing_Frequency__c, Billing_Partner__c, 
            Billing_Start_Date__c, Canvass__c, Opportunity__c, Order_Anniversary_Start_Date__c, Payment_Duration__c, Payment_Method__c, 
            Payments_Remaining__c, Product2__c, Total_Prorated_Days_for_contract__c, Prorate_Stored_Value__c, Prorate_Credit__c, 
            Total_Prorate__c, Prorate_Credit_Days__c, Talus_Go_Live_Date__c, Successful_Payments__c, Service_End_Date__c, 
            Service_Start_Date__c, Line_Status__c, Billing_Close_Date__c, Current_Daily_Prorate__c, Next_Billing_Date__c,  
            Current_Billing_Period_Days__c, Is_Billing_Cycle__c, BillingChangeNoofDays__c, Billing_Change_Prorate_Credit__c, 
            BillingChangeProrateCreditDays__c, BillingChangesPayment__c, Order_Billing_Date_Changed__c, OriginalBillingDate__c 
            From Order_Line_Items__r where isCanceled__c = false and Talus_Go_Live_Date__c != null) From Order__c where Account__c IN :accountId];
    }
    
    public static list<Order__c> getOrderForNationalByAccountID(set<Id> accountID) {
        return [Select Id, Account__c, Talus_Account_Id__c, Billing_Anniversary_Date__c from Order__c where Account__c IN:accountID];
    }
    
    public static list<Order__c> getOrderListByAccountID(set<Id> accountID) {
        return [Select Id, Account__c, Talus_Account_Id__c, Billing_Anniversary_Date__c from Order__c where Account__c IN:accountID];
    }
}