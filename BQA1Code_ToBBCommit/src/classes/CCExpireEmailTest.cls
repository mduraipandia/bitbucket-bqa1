@isTest
public with sharing class CCExpireEmailTest {
    public static testMethod void ccExpireTest() {
        Test.StartTest();
    	Account acct = TestMethodsUtility.createAccount('cmr');
        Contact cnt = TestMethodsUtility.createContact(acct.Id);
        Order__c ord = TestMethodsUtility.createOrder(acct.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(acct, cnt);
        insert oppty;
        Order_Group__c og = TestMethodsUtility.generateOrderSet(acct, ord, oppty);
        og.selected__c = true;
        insert og;
        Order_Line_Items__c oln = TestMethodsUtility.createOrderLineItem(acct, cnt, oppty, ord, og);              
        
        List<Order_Line_Items__c> OLIList = new List<Order_Line_Items__c>();
        OLIList.add(oln);
        
        date tempDate = system.today().addMonths(2);
        string mon = string.valueOf(tempDate.month());
        string yr = string.valueOf(tempDate.year());     
        
        List<pymt__Payment_Method__c> PMList = new List<pymt__Payment_Method__c>();           
        
        pymt__Payment_Method__c PymtMethod = new pymt__Payment_Method__c(Name = 'Test', pymt__Contact__c = cnt.Id, pymt__Default__c = True,
                                                                         pymt__Expiration_Year__c = yr, pymt__Expiration_Month__c = mon);
        PMList.add(PymtMethod);    
        
        date tempDate1 = system.today().addDays(7);
        string mon1 = string.valueOf(tempDate1.month());
        string yr1 = string.valueOf(tempDate1.year());         
        
        pymt__Payment_Method__c PymtMethod1 = new pymt__Payment_Method__c(Name = 'Test', pymt__Contact__c = cnt.Id, pymt__Default__c = True,
                                                                         pymt__Expiration_Year__c = yr1, pymt__Expiration_Month__c = mon1);
        PMList.add(PymtMethod1);  
        
        date tempDate2 = system.today().addDays(7);
        string mon2 = string.valueOf(tempDate2.month());
        string yr2 = string.valueOf(tempDate2.year());         
        
        pymt__Payment_Method__c PymtMethod2 = new pymt__Payment_Method__c(Name = 'Test', pymt__Contact__c = cnt.Id, pymt__Default__c = True,
                                                                         pymt__Expiration_Year__c = yr2, pymt__Expiration_Month__c = mon2);
        PMList.add(PymtMethod2);  
         
        insert PMList;                                                                                 
        
        ccexpireEmails ccEmail = new ccexpireEmails();
        ccexpireEmails.getmail();
        ccEmail.ccExpireNotificationVF();
        ccexpireEmails.ccExpireNotification(OLIList);
        ccexpireEmails.sendingEmail(PMList, 2);
        ccexpireEmails.sendingEmail(PMList, 1);
        ccexpireEmails.sendingEmail(PMList, 7);
        Datetime dt = Datetime.now().addMinutes(1);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
		String jobId = System.schedule('CCExpireSchedule', CRON_EXP, new CCExpireSchedule() );
        /*CCExpireEmailBatch obj = new CCExpireEmailBatch();
        ID batchprocessid = Database.executeBatch(obj);*/
        Test.StopTest();
    }
}