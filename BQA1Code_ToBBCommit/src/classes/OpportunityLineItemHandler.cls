public without sharing class OpportunityLineItemHandler {
    /*****************************************************************************
    //Method called on before Insert of Opportunity Product 
    /****************************************************************************/
    public static void onBeforeInsert(List<OpportunityLineItem> opportunityLineItems) {
        set<Id> setDirectoryID = new set<Id>();
        set<Id> setOptyId = new set<Id>();
        set<String> setHeadingCode = new set<String>();
        set<String> setBCCName = new set<String>();
        Set<Id> directoryIds = new Set<Id>();
        Set<Id> prodIds = new Set<Id>();
        List<OpportunityLineItem> opptyLineItems = new List<OpportunityLineItem>();
        Set<Id> opptyLineItemsForDS = new Set<Id>();
        List<OpportunityLineItem> opptyLineItemsForSplit = new List<OpportunityLineItem>();
        
        for(OpportunityLineItem iteratorOP : opportunityLineItems) {
            Boolean bFlag = true;
            Boolean bFlag1 = true;
            if(iteratorOP.Directory__c != Null && iteratorOP.CORE_Migration_ID__c == null && iteratorOP.CreatedById != System.Label.MigrationUserID) {
                setDirectoryID.add(iteratorOP.Directory__c);
                opptyLineItems.add(iteratorOp);
            }
            if(iteratorOP.OpportunityId != null && iteratorOP.CORE_Migration_ID__c == null && iteratorOP.CreatedById != System.Label.MigrationUserID){
                setOptyId.add(iteratorOP.OpportunityId);
            }
            if(iteratorOP.Directory_Heading__c == null) {
                if(String.isNotEmpty(iteratorOP.Heading_Code__c)) {
                    setHeadingCode.add(iteratorOP.Heading_Code__c);
                    opptyLineItemsForSplit.add(iteratorOP);
                    bFlag1 = false;
                }
            }            
            if(String.isNotBlank(iteratorOP.BCC_Name__c)) {
                setBCCName.add(iteratorOP.BCC_Name__c);
                if(bFlag1) {
                    opptyLineItemsForSplit.add(iteratorOP);
                }
            }
            if(String.isNotBlank(iteratorOP.Directory__c)) {
                directoryIds.add(iteratorOP.Directory__c);
                opptyLineItemsForDS.add(iteratorOP.id);
                bFlag = false;
            }
            if(String.isNotBlank(iteratorOP.Product2Id)) {
                if(bFlag) {
                    opptyLineItemsForDS.add(iteratorOP.id);
                }
                prodIds.add(iteratorOP.Product2Id);
            }
        }
        
        if(setDirectoryID.size() > 0) {
            populateDirectoryOrDirectoryEdition(opptyLineItems, setDirectoryID, setOptyId);
        }
        
        if(setBCCName.size() > 0 || setHeadingCode.size() > 0) {
            splitBeforeOpportunityProduct(opptyLineItemsForSplit, setHeadingCode, setBCCName);
        }
        calculatePackageQuantity(opportunityLineItems);
        if(directoryIds.size() > 0 && prodIds.size() > 0) {
            populateDirectorySectionForMenu(opptyLineItemsForDS, directoryIds, prodIds, opportunityLineItems);
        }
    }
    
    /*****************************************************************************
    //Method called on before Update of Opportunity Product 
    /****************************************************************************/
    public static void onBeforeUpdate(List<OpportunityLineItem> opportunityLineItems, Map<Id, OpportunityLineItem> oldMap){ 
        set<Id> setDirectoryID = new set<Id>();
        set<Id> setOptyId = new set<Id>();
        set<String> setHeadingCode = new set<String>();
        set<String> setBCCName = new set<String>();
        Set<Id> directoryIds = new Set<Id>();
        Set<Id> prodIds = new Set<Id>();
        List<OpportunityLineItem> opptyLineItems = new List<OpportunityLineItem>();
        List<OpportunityLineItem> opptyLineItemsForPackage = new List<OpportunityLineItem>();
        Set<Id> opptyLineItemsForDS = new Set<Id>();
        List<OpportunityLineItem> opptyLineItemsForSplit = new List<OpportunityLineItem>();
        
        for(OpportunityLineItem iteratorOP : opportunityLineItems) {
            Boolean bFlag = true;
            Boolean bFlag1 = true;
            if(iteratorOP.Directory__c != oldMap.get(iteratorOP.Id).Directory__c) {
                if(iteratorOP.Directory__c != Null && iteratorOP.CORE_Migration_ID__c == null && iteratorOP.CreatedById != System.Label.MigrationUserID) {
                    setDirectoryID.add(iteratorOP.Directory__c);
                }
                if(iteratorOP.OpportunityId != null && iteratorOP.CORE_Migration_ID__c == null && iteratorOP.CreatedById != System.Label.MigrationUserID){
                    setOptyId.add(iteratorOP.OpportunityId);
                }
            }
            if(iteratorOP.Heading_Code__c != oldMap.get(iteratorOP.Id).Heading_Code__c) {
                if(iteratorOP.Directory_Heading__c == null) {
                    if(String.isNotEmpty(iteratorOP.Heading_Code__c)) {
                        setHeadingCode.add(iteratorOP.Heading_Code__c);
                        opptyLineItemsForSplit.add(iteratorOP);
                        bFlag1 = false;
                    }
                }
            }
            if(iteratorOP.BCC_Name__c != oldMap.get(iteratorOP.Id).BCC_Name__c) {
                if(String.isNotBlank(iteratorOP.BCC_Name__c)) {
                    setBCCName.add(iteratorOP.BCC_Name__c);
                    if(bFlag1) {
                        opptyLineItemsForSplit.add(iteratorOP);
                    }
                }
            }
            if(iteratorOP.Package_ID_External__c != oldMap.get(iteratorOP.Id).Package_ID_External__c) {
                opptyLineItemsForPackage.add(iteratorOp);
            }
            //if(iteratorOP.Directory__c != oldMap.get(iteratorOP.Id).Directory__c) {
                if(String.isNotBlank(iteratorOP.Directory__c)) {
                    directoryIds.add(iteratorOP.Directory__c);                  
                    opptyLineItemsForDS.add(iteratorOP.id);
                    bFlag = false;                  
                }
                if(String.isNotBlank(iteratorOP.Product2Id)) {
                	prodIds.add(iteratorOP.Product2Id);
                }
            //}
            //if(iteratorOP.Product2Id != oldMap.get(iteratorOP.Id).Product2Id) {
                if(String.isNotBlank(iteratorOP.Product2Id)) {
                    prodIds.add(iteratorOP.Product2Id);
                    if(bFlag) {
                        opptyLineItemsForDS.add(iteratorOP.id);
                    }
                    prodIds.add(iteratorOP.Product2Id);
                }
                if(String.isNotBlank(iteratorOP.Directory__c)) {
                    directoryIds.add(iteratorOP.Directory__c);                      
                }
            //}
        }
        
        if(setDirectoryID.size() > 0) {
            populateDirectoryOrDirectoryEdition(opptyLineItems, setDirectoryID, setOptyId);
        }
        
        if(setBCCName.size() > 0 || setHeadingCode.size() > 0) {
            splitBeforeOpportunityProduct(opptyLineItemsForSplit, setHeadingCode, setBCCName);
        }
        
        if(opptyLineItemsForPackage.size() > 0) {
            calculatePackageQuantity(opptyLineItemsForPackage);
        }
        
        if(directoryIds.size() > 0 && prodIds.size() > 0) {
            populateDirectorySectionForMenu(opptyLineItemsForDS, directoryIds, prodIds, opportunityLineItems);
        }
    }

    /*****************************************************************************
    //Method called on after insert of Opportunity Product 
    /****************************************************************************/
    public static void onAfterInsert(List<OpportunityLineItem> opportunityLineItems, Map<Id, OpportunityLineItem> newMap){ 
        Set<ID> OLIIds = new Set<ID>();
        
        for(OpportunityLineItem iteratorOP : opportunityLineItems) {
            if(iteratorOP.Product_Type__c != 'Print'){
                OLIIds.add(iteratorOP.Id);
            }
        }
        
        if(OLIIds.size() > 0) {
            updateDigitalProductActive(OLIIds);
        }
    }       
    
    /*****************************************************************************
    //Method called on after Update of Opportunity Product 
    /****************************************************************************/
    public static void onAfterUpdate(List<OpportunityLineItem> opportunityLineItems, Map<Id, OpportunityLineItem> oldMap){ 
        Set<ID> OLIIds = new Set<ID>();
        
        for(OpportunityLineItem iteratorOP : opportunityLineItems) {
            if(iteratorOP.Product_Type__c != oldMap.get(iteratorOP.Id).Product_Type__c) {
                if(iteratorOP.Product_Type__c != CommonMessages.oliPrintProductType){
                    OLIIds.add(iteratorOP.Id);
                }
            }
        }
        
        if(OLIIds.size() > 0) {
            updateDigitalProductActive(OLIIds);
        }
    }
    
    /*****************************************************************************
      // Populate Direcoty Heading & BCC Name on OPPLI
    /****************************************************************************/
    private static void populateDirectoryHeading(set<String> setHeadingCode, list<OpportunityLineItem> opportunityLineItems) {
        if(setHeadingCode.size() > 0) {
            list<Directory_Heading__c> lstDH = [Select code__c, Name, Directory_Heading_Name__c, Id From Directory_Heading__c where code__c IN:setHeadingCode];
            if(lstDH.size() > 0) {
                map<String, Directory_Heading__c> mapDH = new map<String, Directory_Heading__c>();
                for(Directory_Heading__c iterator : lstDH) {
                    mapDH.put(iterator.code__c, iterator);
                }
                
                if(mapDH.size() > 0) {
                    for(OpportunityLineItem iteratorOP : opportunityLineItems) {
                        if(iteratorOP.Directory_Heading__c == null) {
                            if(String.isNotEmpty(iteratorOP.Heading_Code__c)) {
                                if(mapDH.get(iteratorOP.Heading_Code__c) != null) {
                                    iteratorOP.Directory_Heading__c = mapDH.get(iteratorOP.Heading_Code__c).Id;
                                    //iteratorOP.Heading_Code__c = mapDH.get(iteratorOP.Heading__c).code__c;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
     /*****************************************************************************
      // Populate BCC Name on OPPLI
    /****************************************************************************/
    private static void populateBCC(set<String> setBCCName, list<OpportunityLineItem> opportunityLineItems) {
        if(setBCCName.size() > 0) {
            list<Berry_Cares_Certificate__c> lstBCC = [Select ID, Name From Berry_Cares_Certificate__c where Name IN:setBCCName];
            if(lstBCC.size() > 0) {
                map<String, Berry_Cares_Certificate__c> mapBCC = new map<String, Berry_Cares_Certificate__c>();
                for(Berry_Cares_Certificate__c iterator : lstBCC) {
                    mapBCC.put(iterator.Name, iterator);
                }
                
                if(mapBCC.size() > 0) {
                    for(OpportunityLineItem iteratorOP : opportunityLineItems) {
                        if(String.isNotBlank(iteratorOP.BCC_Name__c)) {
                            if(mapBCC.get(iteratorOP.BCC_Name__c) != null) {
                                iteratorOP.BCC__c = mapBCC.get(iteratorOP.BCC_Name__c).Id;
                            }
                        }
                    }
                }
            }
        }
    }   
    
    /*****************************************************************************
      // Populate Direcoty Edition on OPPLI
    /****************************************************************************/

    private static void populateDirectoryOrDirectoryEdition(List<OpportunityLineItem> opportunityLineItems, set<Id> setDirectoryID, set<Id> setOptyId) {
        map<Id,Opportunity> mapOpty = new map<Id,Opportunity>();
        list<Opportunity> optyLst;
        optyLst = [SELECT Id,Late_Print_Order_Approved__c  FROM Opportunity where Id IN : setOptyId];
                
        if(optyLst != null && optyLst.size() > 0){
            for(Opportunity opty : optyLst){
                mapOpty.put(opty.Id,opty);
            }
        }  
        list<Directory_Edition__c> lstDE;
        list<Directory_Edition__c> lstBOTSDE;
        if(setDirectoryID != null && setDirectoryID.size() > 0) {
            lstDE = [SELECT Id, Directory__c, Directory__r.Directory_Code__c from Directory_Edition__c where Book_Status__c = 'NI' and Directory__c IN:setDirectoryID];
            lstBOTSDE = [SELECT Id, Directory__c,Book_Status__c from Directory_Edition__c where Book_Status__c = 'BOTS' and Directory__c IN:setDirectoryID];
        }        
        map<Id, Directory_Edition__c> mapDE = new map<Id, Directory_Edition__c>();
        
        if(lstDE != null && lstDE.size() > 0) {
            for(Directory_Edition__c iterator : lstDE) {
                mapDE.put(iterator.Directory__c, iterator);
            }
        }
        
        map<Id, Directory_Edition__c> mapBOTSDE = new map<Id, Directory_Edition__c>();
        system.debug('*********DE Lst ************'+lstBOTSDE);
        if(lstBOTSDE != null && lstBOTSDE.size() > 0) {
            for(Directory_Edition__c iterator : lstBOTSDE) {
                mapBOTSDE.put(iterator.Directory__c, iterator);
            }
        }
        
        if(mapDE.size() > 0 || mapBOTSDE.size() > 0) {
            for(OpportunityLineItem iteratorOP : opportunityLineItems) {
                if(iteratorOP.OpportunityId != null){
                    if(mapOpty.get(iteratorOP.OpportunityId).Late_Print_Order_Approved__c == false && iteratorOP.Directory__c != Null && string.isBlank(iteratorOP.National_Staging_Line_Item__c)) {
                        if(mapDE.get(iteratorOP.Directory__c) != null) {
                        iteratorOP.Directory_Edition__c = mapDE.get(iteratorOP.Directory__c).Id;
                        }
                    }
                }
            }
        }
    }   
    
    private static void splitBeforeOpportunityProduct(List<OpportunityLineItem> opportunityLineItems, set<String> setHeadingCode,  set<String> setBCCName) {        
        if(setHeadingCode.size() > 0) {
            populateDirectoryHeading(setHeadingCode, opportunityLineItems);
        }
        if(setBCCName.size() > 0) {
            populateBCC(setBCCName, opportunityLineItems);
        }
    }
    
    public static void calculatePackageQuantity(List<OpportunityLineItem> OLIList) {
        Map<String, List<OpportunityLineItem>> packageIdOLIListMap = new Map<String, List<OpportunityLineItem>>();
        for(OpportunityLineItem OLI : OLIList) {
            if(OLI.Package_ID_External__c != null){
                if(!packageIdOLIListMap.containsKey(OLI.Package_ID_External__c)) {
                        packageIdOLIListMap.put(OLI.Package_ID_External__c, new List<OpportunityLineItem>());
                }
                packageIdOLIListMap.get(OLI.Package_ID_External__c).add(OLI);
            }
        }
        
        for(OpportunityLineItem OLI : OLIList) {
            if(OLI.Package_ID_External__c != null){
                OLI.Package_Item_Quantity__c = packageIdOLIListMap.get(OLI.Package_ID_External__c).size();
            }
        }
    }
    
    public static void populateDirectorySectionForMenu(Set<Id> OLIListId, Set<ID> directoryIds, Set<ID> prodIds, List<OpportunityLineItem> OppLIList) {
        Map<Id, Directory_Section__c> mapDirIdDirSec = new Map<Id, Directory_Section__c>();
        Map<Id, Product2> mapProduct = new Map<Id, Product2>();        
        List<Directory_Section__c> listDirSec = new List<Directory_Section__c>();
        
        listDirSec = DirectorySectionSOQLMethods.getYPDirectorySectionsByDirIds(directoryIds);
        mapProduct = Product2SOQLMethods.getProductMapByID(prodIds);
        system.debug('Dir Sec list size is ' + listDirSec.size());
        system.debug('Dir Sec list size is ' + mapProduct.size());
        if(listDirSec.size() > 0) {
            system.debug('Dir Sec list size is 1');
            for(Directory_Section__c DS : listDirSec) {
                mapDirIdDirSec.put(DS.Directory__c, DS);
            }
            for(OpportunityLineItem OLI : OppLIList) {
                system.debug('Dir Sec list size is 2');
                if(OLIListId.contains(OLI.id) && String.isNotBlank(OLI.Directory__c)) {
                    system.debug('Dir Sec list size is 3');
                    if(mapProduct.containsKey(OLI.Product2Id)) {
                        system.debug('Dir Sec list size is 4');
                        if(mapProduct.get(OLI.Product2Id).Print_Specialty_Product_Type__c == CommonMessages.menuPrintSpecialty) {
                            system.debug('Dir Sec list size is 4');
                            OLI.Directory_Section__c = mapDirIdDirSec.get(OLI.Directory__c).Id;
                        }
                    }
                }
            }
        }
    } 
    
    /* Updating Digital Product Active field on Account */
    public static void updateDigitalProductActive(Set<ID> OLIIds) {
        Set<ID> AcctIds = new Set<ID>();
        List<OpportunityLineItem> OpptyLineItemList = new List<OpportunityLineItem>();
        List<Account> AcctList = new List<Account>();
        Map<Id, List<OpportunityLineItem>> AcctOLIList = new Map<Id, List<OpportunityLineItem>>();
        
        OpptyLineItemList = [SELECT Opportunity.Account.Id FROM OpportunityLineItem WHERE Id IN : OLIIds];  
        
        for(OpportunityLineItem OLI : OpptyLineItemList) {
            if(!AcctOLIList.containsKey(OLI.Opportunity.Account.Id)) {
                AcctOLIList.put(OLI.Opportunity.Account.Id, new List<OpportunityLineItem>());
            }
            AcctOLIList.get(OLI.Opportunity.Account.Id).add(OLI);
        }
        
        if(!AcctOLIList.isEmpty()) {
            AcctIds = AcctOLIList.keySet();
            AcctList = [SELECT Id, Digital_Product_Active__c FROM Account WHERE Id IN : AcctIds];
            System.debug('Account List is ' + AcctList);
            
            if(!AcctList.isEmpty()) {
                for(Account Acc : AcctList) {
                    Acc.Digital_Product_Active__c = true;
                }
                update AcctList;
            }
        }
    }       
}