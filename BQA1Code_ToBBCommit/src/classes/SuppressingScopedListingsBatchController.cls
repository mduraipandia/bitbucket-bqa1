global class SuppressingScopedListingsBatchController implements Database.Batchable<sObject>,Database.Stateful,Schedulable {
    global String SOQL = '';
    global SuppressingScopedListingsBatchController(){}
    
    global SuppressingScopedListingsBatchController(String strSOQL){
        SOQL = strSOQL;
    }
    
    global void execute(SchedulableContext ctx) {
        Database.executeBatch(new SuppressingScopedListingsBatchController(), 2000);   
    }
       
    global Database.QueryLocator start(Database.BatchableContext bc) {
      if(SOQL == '' || SOQL == null) {
        SOQL = 'select id,CreatedDate, Listing__c,strSuppressionCheck__c  from Order_Line_Items__c where Requires_Scoped_Suppression_Processing__c = TRUE AND Scoped_Suppression_Processing_Complete__c = FALSE AND Media_Type__c=\'Print\' order by createdDate ASC' ;
      }
      return database.getQuerylocator(SOQL);
    }
    
    global void execute(Database.BatchableContext bc, List<Order_Line_Items__c > oliList) {
        Map<string,List<order_line_Items__c>> mapstrOli = new  Map<string,List<order_line_Items__c>>();
        map<String,List<Directory_Listing__c>> mapstrDL = new map<String,List<Directory_Listing__c>>();
        set<Id> setLstId =new Set<Id>();
        set<Id> setscopedListingId = new set<Id>();
        set<Id> failedScopedListing=new Set<Id>();
        set<Id> removeOLIfromupdate=new Set<Id>();
        for(Order_Line_Items__c OLI:oliList) { 
            if(OLI.Listing__c != null) {
                    setLstId.add(OLI.Listing__c);
            }
            if(OLI.strSuppressionCheck__c != null) {
                if(!mapstrOli.ContainsKey(OLI.strSuppressionCheck__c)) {
                    mapstrOli.put(OLI.strSuppressionCheck__c,new List<Order_Line_Items__c>());
                }
                mapstrOli.get(OLI.strSuppressionCheck__c).add(OLI);
            }
                
        }
        if(setLstId.size()> 0) {
            for(Directory_Listing__c iteratorSL:[select id ,strSuppressionCheck__c,Listing__c from Directory_Listing__c where Caption_Header__c=FALSE AND Do_Not_Suppress__c = FALSE AND Listing__c IN :setLstId AND strSuppressionCheck__c IN:mapstrOli.keySet()]){
                if(iteratorSL.strSuppressionCheck__c != null) {
                    if(!mapstrDL.containsKey(iteratorSL.strSuppressionCheck__c)) {
                        mapstrDL.put(iteratorSL.strSuppressionCheck__c,new list<Directory_Listing__c>());
                    }
                    mapstrDL.get(iteratorSL.strSuppressionCheck__c).add(iteratorSL);
                }
            }
        }
        list<Directory_Listing__c> lstDLUpdate=new list<Directory_Listing__c>();
        //list<Order_Line_Items__c> oliForUpdate=new list<Order_Line_Items__c >();
        map<Id,Order_Line_Items__c> mapOliforUpdate = new map<Id,Order_Line_Items__c>();
        set<Id> scopedListingId=new set<Id>();
        if(mapstrOli.size()> 0) {
            for(String str: mapstrOli.keySet()){
                for(Order_Line_Items__c iterator:mapstrOli.get(str)) {
                    System.debug('#############iterator.strSuppressionCheck__c '+iterator.strSuppressionCheck__c);
                    if(mapstrDL.get(iterator.strSuppressionCheck__c) != null){
                        for(Directory_Listing__c tempDL: mapstrDL.get(iterator.strSuppressionCheck__c)) {
                            if(tempDL.Listing__c==iterator.Listing__c && !setscopedListingId.contains(tempDL.Id)) {
                                tempDL.Suppressed__c = true;
                                tempDL.Suppressed_By_OLI__c = iterator.id;
                                lstDLUpdate.add(tempDL);
                                setscopedListingId.add(tempDL.Id);
                                iterator.Scoped_Suppression_Processing_Complete__c = true;
                                if(!mapOliforUpdate.containskey(iterator.Id)){
                                	mapOliforUpdate.put(iterator.Id,iterator);
                                }
                            }
                        }
                    }           
                }   
            }
        }
        if(lstDLUpdate.size()> 0) {
            update lstDLUpdate;
        }
        if(mapOliforUpdate.size()> 0) {
            update mapOliforUpdate.values();
        }
        /*Database.SaveResult[] srList = Database.update(scopeListingForUpdate, false);
        
        // Iterate through each returned result
        for (Database.SaveResult sr : srList) {
            if (!sr.isSuccess()) failedScopedListing.add(sr.getId());
        }    

        /*if(scopeListingForUpdate.size()>0)
            update scopeListingForUpdate;*  
            if(failedScopedListing.size()>0){
                for(Directory_Listing__c tempDl:scopeListingForUpdate)
                {
                    if(failedScopedListing.contains(tempDl.id))
                    {
                        removeOLIfromupdate.add(tempDl.Suppressed_By_OLI__c);
                    }
                }
                for(order_line_items__c tempOLi:oliForUpdate)
                {
                    if(removeOLIfromupdate.contains(tempOLi.id))
                    {
                        tempOLi.Scoped_Suppression_Processing_Complete__c=false;
                    }
                }
                if(oliForUpdate.size()>0)
                    update oliForUpdate; 
                
            }
            else
            {
                if(oliForUpdate.size()>0)
                    update oliForUpdate; 
            }*/
             
    }
    global void finish(Database.BatchableContext bc){}
}