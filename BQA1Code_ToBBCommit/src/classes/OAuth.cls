public with sharing class OAuth 
{
  public static final string VERSION_PARAM = 'oauth_version';
  public static final string DEFAULT_VERSION = '1.0';
  public static final string SIGNATURE_METHOD_PARAM = 'oauth_signature_method';
  public static final string DEFUALT_SIGNATURE_METHOD = 'HMAC-SHA1';
  public static final string TIMESTAMP_PARAM = 'oauth_timestamp';
  public static final string NONCE_PARAM = 'oauth_nonce';
  public static final string CONSUMER_KEY_PARAM = 'oauth_consumer_key';
  public static final string TOKEN_PARAM = 'oauth_token';
  
  public static final string OAUTH_HEADER_KEY = 'Authorization';
  public static final string OAUTH_HEADER_PARAM = 'OAuth';
  public static final string OAUTH_SIGNATURE_PARAM = 'oauth_signature';
  
  private OAuth_Service__c settings;
  
  public string URL
  {
    get
    {
      return settings.URL__c;
    }
    private set;
  }
  
  public OAuth(OAuth_Service__c serviceSettings)
  {
    settings = serviceSettings;
  }
  
  public String generateSignatureString(String baseString)
  {
    blob hashedSig = Crypto.generateMac('HmacSHA1', Blob.valueOf(baseString), Blob.valueOf(settings.Consumer_Secret__c+'&'+ settings.Token_Secret__c));
    return EncodingUtil.urlEncode(EncodingUtil.base64encode(hashedSig), 'UTF-8');
  }
  
  public Map<String,String> generateOauthParams(string timestamp,string nonce)
  {
    Map<String,String> oauthParams = new Map<String,String>();
    
    oauthParams.put(SIGNATURE_METHOD_PARAM,DEFUALT_SIGNATURE_METHOD);
    oauthParams.put(TIMESTAMP_PARAM,timestamp);
    oauthParams.put(NONCE_PARAM,nonce);
    oauthParams.put(CONSUMER_KEY_PARAM,settings.Consumer_Key__c);
    oauthParams.put(TOKEN_PARAM,settings.Token__c);
    oauthParams.put(VERSION_PARAM,DEFAULT_VERSION);
    
    return oauthParams;
  }
  
  public string createHeader(Map<String,String> parameters, String signature)
  {
    String header = OAUTH_HEADER_PARAM + ' ';
    
    for (String key : parameters.keySet()) 
    {
      header = header + key + '="'+parameters.get(key)+'", ';
    }
    
    header = header + OAUTH_SIGNATURE_PARAM + '="'+signature+'"';
    
    return header;
  }
  
  //-------------------------------------------------------------------------
  // adapted from SFDC Oauth playground
  //-------------------------------------------------------------------------
  
  public void sign(HttpRequest req) 
  {
    string nonce = String.valueOf(Crypto.getRandomLong());
    string timestamp = String.valueOf(DateTime.now().getTime()/1000);
    
    Map<String,String> parameters = generateOauthParams(timestamp,nonce);
    
    String baseString = createBaseString(parameters, req);
    
    System.debug('Signature base string: '+baseString);
                             
    string signature = generateSignatureString(baseString);
    System.debug('Signature: '+signature);
    
    String header = createHeader(parameters,signature);
    
    System.debug('Authorization: '+header);
    req.setHeader(OAUTH_HEADER_KEY,header);
  }
  
  public String createBaseString(Map<String,String> oauthParams, HttpRequest req) 
  {
    Map<String,String> params = oauthParams.clone();
    
    //if its a post, add the params to the body
    if(req.getMethod().equalsIgnoreCase('post') && req.getBody() != null &&  req.getHeader('Content-Type')=='application/x-www-form-urlencoded') 
    {
       params.putAll(getUrlParams(req.getBody()));
    }
    
    String host = req.getEndpoint();
    Integer queryStringStart = host.indexOf('?');
    
    //if it has a query string
    if(queryStringStart > -1) 
    {
      params.putAll(getUrlParams(host.substring(queryStringStart+1)));
      host = host.substring(0,queryStringStart);
    }
    
    List<String> keys = new List<String>();
    
    keys.addAll(params.keySet());
    
    keys.sort();
    
    //start with the first param, TODO::is this necessary? 
    String newQueryString = keys.get(0)+'='+params.get(keys.get(0));
    
    //then add the rest;
    for(Integer i=1;i<keys.size();i++) 
    {
      newQueryString = newQueryString + '&' + keys.get(i)+'='+params.get(keys.get(i));
    }

    // According to OAuth spec, host string should be lowercased, but Google and LinkedIn
    // both expect that case is preserved.
    return req.getMethod().toUpperCase()+ '&' + EncodingUtil.urlEncode(host, 'UTF-8') + '&' + EncodingUtil.urlEncode(newQueryString, 'UTF-8');
  }
  
  private Map<String,String> getUrlParams(String value) 
  {
    Map<String,String> res = new Map<String,String>();
    
    if(value==null || value=='') 
    {
      return res;
    }
    
    for(String parameter : value.split('&')) 
    {
      System.debug('getUrlParams: '+ parameter);
      List<String> keyValuePair = parameter.split('=');
      
      if(keyValuePair.size() > 1) 
      {
        System.debug('getUrlParams:  -> '+keyValuePair[0]+','+keyValuePair[1]);
        res.put(keyValuePair[0],keyValuePair[1]);
      }
    }
    
    return res;
  }
}