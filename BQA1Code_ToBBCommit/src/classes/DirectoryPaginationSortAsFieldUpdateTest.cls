@isTest
private class DirectoryPaginationSortAsFieldUpdateTest {
	static testMethod void dirPagSeqTest() {     
		Canvass__c c = TestMethodsUtility.createCanvass();        
        Account a = TestMethodsUtility.generateCustomerAccount();
        insert a;             
        Contact cnt = TestMethodsUtility.createContact(a);               
        Opportunity oppty = TestMethodsUtility.createOpportunity('new', a);                      
        Order__c ord = TestMethodsUtility.createOrder(a.Id);                
        Order_Group__c og = TestMethodsUtility.createOrderSet(a, ord, oppty);     
        Product2 product2 = TestMethodsUtility.createproduct2();
        product2.Print_Specialty_Product_Type__c = 'Banner';
        insert product2;   
        Order_Line_Items__c oln = TestMethodsUtility.generateOrderLineItem(a, cnt, oppty, ord, og);
        oln.Product2__c = product2.Id;
        insert oln;
        Directory__c dir = TestMethodsUtility.createDirectory();                  
        Telco__c telco = TestMethodsUtility.createTelco(a.Id);     
        Directory_Mapping__c DM = TestMethodsUtility.createDirectoryMapping(telco);       
        Directory_Edition__c DE = TestMethodsUtility.generateDirectoryEdition(dir.Id, c.id, telco.Id, DM.Id);  
        DE.Pub_Date__c = System.Today();
        insert DE;
        DE = [SELECT Id, Year__c FROM Directory_Edition__c WHERE Id =: DE.Id];
        Directory_Section__c dirSection = TestMethodsUtility.createDirectorySection(dir);        
        Directory_Pagination__c dirPagination = TestMethodsUtility.generateDirectoryPagination(dirSection.Id, oln.Id, DE.Id); 
        dirPagination.Year__c = DE.Year__c;
        dirPagination.Display_Ad__c = true;
        dirPagination.Under_Caption__c = oln.Id;
        dirPagination.DP_OLI_Anchor_Caption_Header__c = oln.Id;
        insert dirPagination;
        oln.Anchor_Caption_Header_Record_ID__c = oln.Id;
        update oln;
        List<Pagination_Job__c> PJs = new List<Pagination_Job__c>();
        DE = [SELECT Id, Year__c FROM Directory_Edition__c WHERE Id = : DE.Id]; 
        Directory__c dir1 = TestMethodsUtility.createDirectory(); 
        Directory_Section__c dirSection1 = TestMethodsUtility.generateDirectorySection(dir1);      
        dirSection1.Section_Page_Type__c = 'YP';  
        insert dirSection1;
        Directory_Pagination__c dirPagination1 = TestMethodsUtility.generateDirectoryPagination(dirSection1.Id, oln.Id, DE.Id); 
        dirPagination1.Year__c = DE.Year__c;
        dirPagination1.Under_Caption__c = oln.Id;
        dirPagination1.Display_Ad__c = true;
        dirPagination1.DP_OLI_Anchor_Caption_Header__c = oln.Id;
        insert dirPagination1;
        
		Test.StartTest();          
        DirectoryPaginationSortAsFieldUpdate obj = new DirectoryPaginationSortAsFieldUpdate(null, null);
		Database.executeBatch(obj); 
        Test.StopTest(); 		
	}
}