@IsTest(SeeAllData=True)
public class SalesInvoiceLineItemSOQLMethodsTest {
    static testMethod void SalesInvoiceLineItemSOQLMethodsCoverage() {
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
        if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
        newAccount = iterator;
        }
        else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
        newPubAccount = iterator;
        }
        else {
        newTelcoAccount = iterator;
        }
        }
        system.assertNotEquals(newAccount.ID, null);
        system.assertNotEquals(newPubAccount.ID, null);
        system.assertNotEquals(newAccount.Primary_Canvass__c, null);
        system.assertNotEquals(newTelcoAccount.ID, null);
        Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        system.assertNotEquals(newTelcoAccount.ID, null);
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        Division__c objDiv = TestMethodsUtility.createDivision();
        /*Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;*/
        Directory__c objDir =TestMethodsUtility.createDirectory();
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;

        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        objDirEd.Pub_Date__c=System.today().addMOnths(1);
        insert objDirEd;
        
        Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir);
        objDirEd1.Pub_Date__c=System.today().addMOnths(2);
        insert objDirEd1;

        system.assertNotEquals(objDir.ID, null);
        list<Product2> lstProduct = new list<Product2>();      
        for(Integer x=0; x<3;x++){
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Product_Type__c = CommonMessages.oliPrintProductType;
        newProduct.Inventory_Tracking_Group__c = 'YP Leader Ad';
        lstProduct.add(newProduct);
        }
        insert lstProduct;
     
        list<PricebookEntry> lstPBE = [Select Id, Product2Id from PricebookEntry where Product2Id IN:lstProduct];
        for(Product2 iterator : lstProduct) {
        //Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(objDir);
        //objDPM.Product2__c = iterator.Id;
        }
        //insert lstDPM;

        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        newOpportunity.Billing_Contact__c = newContact.Id;
        newOpportunity.Signing_Contact__c = newContact.Id;
        newOpportunity.Billing_Partner__c = objTelco.Telco_Code__c;
        newOpportunity.Payment_Method__c = CommonMessages.telcoPaymentMethod;        
        insert newOpportunity;

        list<OpportunityLineItem> lstOLI = new list<OpportunityLineItem>();
        map<Id, PricebookEntry> mapPBE = new map<Id, PricebookEntry>();
        for(PricebookEntry iterator : lstPBE) {
        OpportunityLineItem objOLI = TestMethodsUtility.generateOpportunityLineItem();
        objOLI.PricebookEntryId = iterator.Id;
        objOLI.Billing_Duration__c = 12;
        objOLI.Directory__c = objDir.Id;
        objOLI.Directory_Edition__c = objDirEd.Id;
        objOLI.Full_Rate__c = 30.00;
        objOLI.UnitPrice = 30.00;
        objOLI.Package_ID__c = '123456';
        objOLI.Billing_Partner__c = objOLI.Id;
        objOLI.OpportunityId = newOpportunity.Id;
        objOLI.Directory_Heading__c = objDH.Id;
        objOLI.Directory_Section__c = objDS.Id;
        lstOLI.add(objOLI);
        mapPBE.put(iterator.Id, iterator);
        }
        insert lstOLI;

        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        list<Order_Line_Items__c> lstOrderLI = new list<Order_Line_Items__c>();
        for(OpportunityLineItem iterator : lstOLI) {
        Order_Line_Items__c objOLI = TestMethodsUtility.generateOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet, iterator);
        objOLI.Package_ID__c = iterator.Package_ID__c;
        objOLI.Package_Item_Quantity__c = 2;
        objOLI.Directory__c = iterator.Directory__c;
        objOLI.Directory_Edition__c = iterator.Directory_Edition__c;
        objOLI.UnitPrice__c = iterator.UnitPrice;
        objOLI.Opportunity_line_Item_id__c = iterator.Id;
        objOLI.Media_Type__c = CommonMessages.oliPrintProductType;
        objOLI.Directory_Heading__c = iterator.Directory_Heading__c;
        objOLI.Directory_Section__c = iterator.Directory_Section__c;
        objOLI.Product2__c = mapPBE.get(iterator.PricebookEntryId).Product2Id;
        objOLI.Product_Inventory_Tracking_Group__c = 'YP Leader Ad';
        lstOrderLI.add(objOLI);
        }

        Test.startTest();
        insert lstOrderLI;
        Order_Line_Items__c firstOLI = lstOrderLI[0];
        firstOLI.Talus_Go_Live_Date__c = system.today();
        firstOLI.Directory_Edition__c = objDirEd1.Id;
        update firstOLI;
        
        c2g__codaInvoice__c newSalesInvoice = new c2g__codaInvoice__c(c2g__InvoiceDate__c = System.Today(), c2g__DueDate__c = System.Today()+30, c2g__Account__c = newAccount.Id, c2g__Opportunity__c = newOpportunity.Id);
        //newSalesInvoice.c2g__Period__c = System.Label.TestFFPeriods;
        newSalesInvoice.c2g__InvoiceCurrency__c = System.Label.AccountingCurrencyId;
        newSalesInvoice.Customer_Name__c = newAccount.id;
        insert newSalesInvoice;
        
        SalesInvoiceLineItemSOQLMethods.getInvoiceLineItemByOLI(new set<id>{firstOLI.id});
        Test.stopTest();        
    }
}