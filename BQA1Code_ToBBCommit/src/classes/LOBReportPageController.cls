public with sharing class LOBReportPageController {
    public String searchText {get;set;} 
    public Id selectedDirId {get;set;}
    public Id selectedDirEdId {get;set;}
    public String selectedReportType {get;set;}
    public List<SelectOption> listDirSelOpt {get;set;}
    public List<SelectOption> listDirEdSelOpt {get;set;}
    public List<SelectOption> reportTypes {get;set;}
    List<Directory__c> listDirectory;
    List<Directory_Edition__c> listDirEdition = new List<Directory_Edition__c>();
    Public Boolean printBool {get;set;}
    Public Boolean digitalBool {get;set;}        
    Public List<SelectOption> listBillingPartners {get;set;}	
    Public List<String> listSelBillingPartners {get;set;}
    public Boolean polleraction {get;set;}
    public Id batchId {get;set;}
    public String batchStatus {get;set;}
    public Boolean isBatchProcessed {get;set;}
    Public List<SelectOption> billingEntities {get;set;}  
	public string selectedEntity {get;set;}   
    public Date billDate {get;set;}
    public String billPrepDate {get;set;}
    public String invFromDate {get;set;}
    public String invToDate {get;set;}       
    Map<Id, String> mapDirEdIdCode = new Map<Id, String>();
    
    public LOBReportPageController() { 
		printBool = false;
		digitalBool = false;  
		reportTypes = new List<SelectOption>();
		reportTypes.add(new SelectOption('none', 'None'));
		reportTypes.add(new SelectOption('digital', 'Digital'));
		reportTypes.add(new SelectOption('print', 'Print'));
		listSelBillingPartners = new List<String>();	
		pollerAction = false;
        isBatchProcessed = false;
    }
    
    public void hideShowDigOrPrnt() { 
    	system.debug('Report type ' + selectedReportType);
    	if(selectedReportType == 'digital') {
    		printBool = false;
    		digitalBool = true;
	        billingEntities = new List<SelectOption>();
	        billingEntities.add(new SelectOption('0','--None--'));
	        Schema.DescribeFieldResult BillingEntityResult = Canvass__c.Billing_Entity__c.getDescribe();
	        List<Schema.PicklistEntry> BillingEntityList = BillingEntityResult.getPicklistValues();
	        for(Schema.PicklistEntry tl: BillingEntityList) {
				billingEntities.add(new SelectOption(tl.getValue(),tl.getValue()));
	        }
	        getTelcoDate();    		
    	} else {
    		printBool = true;
    		digitalBool = false;
    	}
    }
    
    public void fetchDirs() {
        String SOQL = 'SELECT Id, Name, Directory_Code__c, Telco_Provider__c FROM Directory__c WHERE Name LIKE \'' + searchText + '%\'' + 
        				' OR Directory_Code__c LIKE \'' + searchText + '%\' ORDER BY Name LIMIT 1000';
        listDirectory = Database.query(SOQL); 
        if(listDirectory.size() > 0) {
            listDirSelOpt = new List<SelectOption>();
            for(Directory__c dir : listDirectory) {
                listDirSelOpt.add(new SelectOption(dir.Id, dir.Name + ' - ' + dir.Directory_Code__c));
            }            
	        selectedDirId = listDirectory.get(0).Id;
	        fetchDirEditions();
        }
    }
    
    public void fetchDirEditions() {
    	listDirEdition = DirectoryEditionSOQLMethods.getDirEditionByDirIds(new Set<Id> {selectedDirId});
    	if(listDirEdition.size() > 0) {
            listDirEdSelOpt = new List<SelectOption>();
            mapDirEdIdCode = new Map<Id, String>();
            for(Directory_Edition__c dirEd : listDirEdition) {
                listDirEdSelOpt.add(new SelectOption(dirEd.Id, dirEd.Name));
                mapDirEdIdCode.put(dirEd.Id, dirED.Edition_Code__c);
            }
            fetchBillingPartners();
        }
    }
    
    public void fetchBillingPartners() {
    	listBillingPartners = new List<SelectOption>();
        if(String.isNotBlank(selectedDirId) && String.isNotBlank(selectedDirEdId)) {
            for(AggregateResult agr : [SELECT Billing_Partner__c BillingPartner FROM Order_Line_Items__c WHERE Directory__c =: selectedDirId AND Directory_Edition__c =: selectedDirEdId GROUP BY Billing_Partner__c Order BY Billing_Partner__c ASC]) {                
                if(agr.get('BillingPartner')!=null) {
                	listBillingPartners.add(new SelectOption((String)agr.get('BillingPartner'),(String)agr.get('BillingPartner')));
                }
            }
        }
    }
     
    public void runLOBBatch() { 
    	Integer batchSize = 0;
    	batchStatus = '';
    	List<List_Of_Business__c> listLOB = new List<List_Of_Business__c>();
    	List<List_Of_Business_Job__c> listLOBJob = new List<List_Of_Business_Job__c>();
    	String editionCode = '';
    	
    	if(selectedReportType == 'digital') {
    		listLOB = [SELECT Id FROM List_Of_Business__c WHERE LOB_Billing_Entity__c =: selectedEntity];    		
    	} else {
    		editionCode = mapDirEdIdCode.get(selectedDirEdId);
    		listLOB = [SELECT Id FROM List_Of_Business__c WHERE LOB_Edition_Code__c =: editionCode];
    		
    		listLOBJob = LOBJobSOQLMethods.getLOBJobsByDirIdDirEdId(selectedDirEdId, selectedDirId);
    	}
    	batchSize = listLOB.size() + listLOBJob.size();
    	
    	if(batchSize <= 5000) {
    		if(listLOB.size() > 0) {
	    		delete listLOB;	
	    		Database.emptyRecycleBin(listLOB);
    		}
    		if(listLOBJob.size() > 0) {
	    		delete listLOBJob;	
	    		Database.emptyRecycleBin(listLOBJob);
    		}
    		if(selectedReportType == 'digital') {
		    	LOBReportGenerationBatch lob = new LOBReportGenerationBatch(selectedDirId, selectedDirEdId, listSelBillingPartners, selectedReportType, invFromDate, 
		    									invToDate, selectedEntity);
		    	batchId = Database.executeBatch(lob);
	    	} else {
	    		LOBJobCreationBatch LOBJob = new LOBJobCreationBatch(selectedDirId, selectedDirEdId, listSelBillingPartners);
	    		batchId = Database.executeBatch(LOBJob);
	    	}
    		pollerAction = true; 
        	isBatchProcessed = true;
    	} else {
    		if(selectedReportType == 'print') {
    			editionCode = mapDirEdIdCode.get(selectedDirEdId);
    		}
    		LOBRecordDeleteBatch LOBDel = new LOBRecordDeleteBatch(selectedDirId, selectedDirEdId, listSelBillingPartners, selectedReportType, invFromDate, 
		    									invToDate, selectedEntity, editionCode);
	    	Database.executeBatch(LOBDel);
        	batchStatus = 'Please Check your email for batch completion notification.';
    	}
    	/*if(listLOB.size() > 0) {
    		if(listLOB.size() <= 5000) {
	    		delete listLOB;	
	    		Database.emptyRecycleBin(listLOB);
    		} else {
    			List<List_Of_Business__c> listTempLOB = new List<List_Of_Business__c>();
    			for(List_Of_Business__c LOB : listLOB) {
    				if(listTempLOB.size() == 10000) {
    					delete listTempLOB;	
	    				Database.emptyRecycleBin(listTempLOB);
	    				listTempLOB = new List<List_Of_Business__c>();
    				} else {
    					listTempLOB.add(LOB);
    				}
    			}
    		}
    	} 
    	
    	if(listLOBJob.size() > 0) {
    		if(listLOBJob.size() <= 5000) {
	    		delete listLOBJob;	
	    		Database.emptyRecycleBin(listLOBJob);
    		} else {
    			List<List_Of_Business_Job__c> listTempLOBJob = new List<List_Of_Business_Job__c>();
    			for(List_Of_Business_Job__c LOBJ : listLOBJob) {
    				if(listTempLOBJob.size() == 10000) {
    					delete listTempLOBJob;	
	    				Database.emptyRecycleBin(listTempLOBJob);
	    				listTempLOBJob = new List<List_Of_Business_Job__c>();
    				} else {
    					listTempLOBJob.add(LOBJ);
    				}
    			}
    		}
    	}
    	 
    	if(selectedReportType == 'digital') {
	    	LOBReportGenerationBatch lob = new LOBReportGenerationBatch(selectedDirId, selectedDirEdId, listSelBillingPartners, selectedReportType, invFromDate, 
	    									invToDate, selectedEntity);
	    	batchId = Database.executeBatch(lob);
    	} else {
    		LOBJobCreationBatch LOBJob = new LOBJobCreationBatch(selectedDirId, selectedDirEdId, listSelBillingPartners);
    		batchId = Database.executeBatch(LOBJob);
    	}
        pollerAction = true; 
        isBatchProcessed = true;*/
    }
    
    public void pollingBatchProcess() {                 
        if(batchId!=null) {
            AsyncApexJob FFjob = [SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =: batchId];
            if(!Test.isRunningTest())
            batchStatus = FFjob.Status;
        }
        if(batchStatus == 'Completed') {
            pollerAction = false;
        }
    } 
    
	public void getTelcoDate() {
		if(selectedEntity !=null) {
			List<Digital_Telco_Scheduler__c> listTelco = new List<Digital_Telco_Scheduler__c>();
			listTelco = [SELECT Billing_Entity__c,Id,Invoice_From_Date__c,Bill_Prep_Date__c,Invoice_To_Date__c,Name,OwnerId,Sent_Telco_File__c,
					    Telco_Bill_Date__c,Telco_Receives_EFile__c,Telco__c,XML_Output_Total_Amount__c FROM Digital_Telco_Scheduler__c 
					    WHERE Billing_Entity__c =: selectedEntity AND Invoice_From_Date__c != null AND Invoice_To_Date__c != null Limit 1];
			if(listTelco.size()>0) {
				Digital_Telco_Scheduler__c tlcConfig = listTelco[0];
				billDate = tlcConfig.Telco_Bill_Date__c;
				invFromDate = String.valueof(tlcConfig.Invoice_From_Date__c.format());
				invToDate = String.valueof(tlcConfig.Invoice_To_Date__c.format());
				billPrepDate = String.valueof(tlcConfig.Bill_Prep_Date__c.format());
			}
			else {
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, 'No record found in Digital Telco Scheduler for selected telco');
				ApexPages.addMessage(myMsg);
				billDate = null;
				invFromDate = '';
				invToDate = '';
				billPrepDate = '';
			}
		}     
	}		
}