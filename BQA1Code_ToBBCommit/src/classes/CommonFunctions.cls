public class CommonFunctions {    
    public static Map<string, ID> getRecordTypeDeveloperNameIDByObjectName(string objectName) {
        list<RecordType> lstRecordtype = [Select Id,Name,DeveloperName From RecordType where SobjectType =:objectName];
        map<String, ID> mapRT = new map<String, ID>();
        for(RecordType iteratorRT : lstRecordtype) {
            mapRT.put(iteratorRT.DeveloperName, iteratorRT.ID);
        }
        return mapRT;
    }
    
    public static List<pymt__PaymentX__c> getDeclinedInvoicePayment(set<String> setPymtStatus) {
        List<pymt__PaymentX__c> lstPayment = [Select pymt__Account__c, pymt__Key__c, pymt__Status__c, pymt__Amount__c, p2f__FF_Sales_Invoice__r.c2g__NetTotal__c, p2f__FF_Sales_Invoice__r.Name, p2f__FF_Sales_Invoice__c, p2f__FF_Account_Reference__c, Name From pymt__PaymentX__c p where pymt__Status__c IN :setPymtStatus and P2F__FF_SALES_INVOICE__C  != null order by pymt__Date__c desc limit 10];
        return lstPayment;
    }
    
    public static List<pymt__PaymentX__c> getDeclinedInvoicePaymentDynSOQL(set<String> setPymtStatus, string dateSearch) {
        String strQuery = 'Select pymt__Account__c, pymt__Date__c, pymt__Key__c, pymt__Status__c, pymt__Amount__c, p2f__FF_Sales_Invoice__r.c2g__NetTotal__c, p2f__FF_Sales_Invoice__r.Name, p2f__FF_Sales_Invoice__c, p2f__FF_Account_Reference__c, Name From pymt__PaymentX__c p where pymt__Status__c IN :setPymtStatus and P2F__FF_SALES_INVOICE__C != null and pymt__Date__c = '+ dateSearch +' order by pymt__Date__c desc';
        List<pymt__PaymentX__c> lstPayment = database.query(strQuery);
        return lstPayment;
    }
    
    public static list<c2g__codaCreditNote__c> getSalesCreditNoteCount(set<Id> caseID) {
        return [Select Id, Case__c, c2g__CreditNoteTotal__c From c2g__codaCreditNote__c where Case__c IN :caseID];
    }
    
    public static list<c2g__codaCreditNote__c> getSalesCreditNoteById(set<Id> cnID) {
        return [Select Id, Case__c, c2g__CreditNoteTotal__c, c2g__Invoice__c, Name From c2g__codaCreditNote__c where Id IN :cnID];
    }
    
    public static list<AggregateResult> getSalesCreditNoteTotalSumByCase(set<Id> caseID) {
        return [Select Case__c, sum(c2g__CreditNoteTotal__c) SumCredit From c2g__codaCreditNote__c where Case__c IN :caseID GROUP BY Case__c ];
    }
    
    public static list<Case> getCaseByID(set<String> caseID) {
        return [Select Id, Reason, CaseNumber, Total_Credit_Note__c, AccountId, ContactId from Case where Id IN :caseID];
    }
    
    public static list<Case> getCaseByID(set<Id> caseID) {
        return [Select Id, Reason, CaseNumber, Total_Credit_Note__c, AccountId, ContactId from Case where Id IN :caseID];
    }
    
    public static list<pymt__PaymentX__c> getPaymentsByInvoiceID(set<Id> salesInvoiceID) {
        return [Select pymt__Contact__c, pymt__Contact__r.Name, p2f__FF_Sales_Invoice__c, Name, Id From pymt__PaymentX__c where p2f__FF_Sales_Invoice__c IN :salesInvoiceID];
    }
    
    public static list<c2g__codaInvoiceLineItem__c> getSlaesInvoiceLineItem(set<Id> invoiceID) {
        return [Select Order_Line_Item__c, c2g__Product__c, c2g__LineNumber__c, c2g__Invoice__r.c2g__Opportunity__c, c2g__Invoice__c, Name, Id, c2g__NetValue__c, c2g__Quantity__c, c2g__UnitPrice__c From c2g__codaInvoiceLineItem__c where c2g__Invoice__c IN :invoiceID order by c2g__Invoice__c];
    }
    
    public static list<pymt__PaymentX__c> getPaymentDetailsBYOpportunity(set<Id> opportunityId) {
        return [SELECT Id, pymt__Payment_Method__c,pymt__Opportunity__c,pymt__Contact__c FROM pymt__PaymentX__c WHERE pymt__Opportunity__c IN :opportunityId AND pymt__Payment_Method__c != Null];
    }
    
    public static list<Group> getQueueByName(String queueName) {
        return [SELECT Id, Name, DeveloperName, RelatedId, Type, Email, OwnerId, DoesSendEmailToMembers, DoesIncludeBosses, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp from Group  where DeveloperName = :queueName];
    }
    
    public static list<ProcessInstanceWorkitem> getProcessInstanceWorkitemByTargetObjectId(Id targetObjectId) {
        return [Select Id from ProcessInstanceWorkitem where ProcessInstance.TargetObjectId =:targetObjectId];
    }
    
    public static list<c2g__codaAccountingCurrency__c> getAccountingCurrency() {
        return [SELECT Id from c2g__codaAccountingCurrency__c WHERE name='USD' limit 1];
    }
}