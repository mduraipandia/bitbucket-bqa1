/*
    Author  : Magulan D
    Purpose : Controller for InventoryTracking Page          
*/
public with sharing class InventoryTracking {
    String canvassId; 
    String Aid;
    String Oid;
    Account CurrentAcct;
    public List<Directory_Mapping__c> DMList;
    public List<Directory__c> dirList;
    public List<Directory_Product_Mapping__c> DPMList {get;set;}
    public Account Acct {get;set;}
    public Integer rowNum{get;set;}
    
    public Canvass__c Canvass {get;set;}
    public List<SelectOption> DirNameList {get;set;}
    public String DirCode {get;set;}
    Map<Id, List<Directory_Product_Mapping__c>> DirDPM;
    
    /* Boolean variables to hide and show VF components */
    public Boolean ResultBool {get;set;}    
    public Boolean PrintExportButnBool {get;set;}
    public Boolean SearchBool {get;set;}
    public Boolean MsgBool {get;set;}
    public Boolean AcctButnBool {get;set;}
    public Boolean OpptyButnBool {get;set;}
    
    public InventoryTracking() {  
        dirList = new List<Directory__c>();      
        Acct = new Account();
        DMList = new List<Directory_Mapping__c>();
        canvassId = '';
        ResultBool = false;
        PrintExportButnBool = true;
        SearchBool = true;
        MsgBool = false;
        AcctButnBool = false;
        OpptyButnBool = false;
        DirNameList = new List<SelectOption>();
        DirNameList.add(new SelectOption('None', 'None'));
        DPMList = new List<Directory_Product_Mapping__c>(); 
        
        //Getting Canvass Id
        canvassId = ApexPages.currentPage().getParameters().get('cid');
        
        Aid = ApexPages.currentPage().getParameters().get('aid');
        
        Oid = ApexPages.currentPage().getParameters().get('oid');                
        
        if(String.isNotEmpty(Aid)) {
            AcctButnBool = true;          
        } 
        
        if(String.isNotEmpty(Oid)) {
            OpptyButnBool = true;
            Opportunity Oppty = [SELECT AccountId FROM Opportunity WHERE ID = : Oid];
            Aid = Oppty.AccountId;
            AcctButnBool = true;            
        }  
        
        CurrentAcct = [SELECT Name FROM Account WHERE Id = : Aid];  
               
        System.debug('Canvass Id is ' + canvassId);
        if(String.isNotBlank(canvassId)) {
            Acct.Primary_Canvass__c = canvassId;
            fetchDirList();
        }
    }
    
    // added by Chris roberts  provides a link to the report  via a query
    Public pageReference Report1(){
      Report RPT = [SELECT ID FROM Report WHERE DeveloperName ='Leader_Ad_Inventory_Tracking' LIMIT 1];
      String ReptID= RPT.Id;
      pageReference PR = new pageReference('/'+ReptID+'?pv0='+DirCode);
      return PR;
    }
  

    public void searchDirProd() {       
        ResultBool = true;
        MsgBool = false;        
        System.debug('Directory Code is ' + DirCode);     
        DPMList = DirectoryProductMappingSOQLMethods.getDirProdMapByDirCode(DirCode); 
        System.debug('Directory Product Mapping list is ' + DPMList);                               
    }   
    
    public void fetchDirList() {
        canvassId = Acct.Primary_Canvass__c;
        dirList = DirectorySOQLMethods.getDirectoriesByCanvassId(canvassId);
        System.debug('Directory list is ' + dirList);
        
        DirNameList.clear();
        for(Directory__c dir : dirList) {
             DirNameList.add(new SelectOption(dir.Directory_Code__c, dir.Name));
        }       
        
        if(dirList.isEmpty()) {
            ResultBool = false;
            MsgBool = true;     
        }
    }       
    
    public PageReference exportPage() {
        PageReference pg = new PageReference('/apex/InventoryTrackingExcel?Cid=' + canvassId + '&dirCode=' +DirCode);
        pg.setRedirect(false);
        return pg;  
    }
    
    public PageReference returnOppty() {
        PageReference pg = new PageReference('/' + Oid);
        pg.setRedirect(false);
        return pg;
    }
    
    public PageReference returnAcct() {
        PageReference pg = new PageReference('/' + Aid);
        pg.setRedirect(false);
        return pg;
    } 
}