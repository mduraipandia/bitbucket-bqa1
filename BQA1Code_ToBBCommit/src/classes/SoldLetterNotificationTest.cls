@IsTest(SeeAllData= true)
public class SoldLetterNotificationTest{
    static testmethod void lineitemSoldNewTest()
    {
        Test.startTest();
        List<Order_Line_Items__c> lstOrLI = new List<Order_Line_Items__c>();
        List<c2g__codaDimension1__c> lstDimension1 = new List<c2g__codaDimension1__c>();
        Canvass__c newCanvass = TestMethodsUtility.createCanvass();
        Account newAccount = TestMethodsUtility.generateAccount('customer', false);
        newAccount.Primary_Canvass__c = newCanvass.Id;
        insert newAccount;
        system.debug('AAAAAAAAAAAAAAAAA'+ newAccount);
        Contact newContact = TestMethodsUtility.generateContact(newAccount.Id);
        newContact.HasOptedOutOfEmail = false;
        insert newContact;
        system.debug('BBBBBBBBBBBB'+ newContact);
        //Telco__c newTelco = TestMethodsUtility.createTelco(newAccount.Id);
        //Pricebook2 newPriceBook = [Select Id from Pricebook2 where isStandard=true limit 1];
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Family = 'Print';
        insert newProduct;
        system.debug('CCCCCCCCCCCCCCC'+ newProduct);
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        //newOpportunity.Pricebook2Id = newPriceBook.Id;
        newOpportunity.StageName = 'Proposal Created';
        newOpportunity.Billing_Contact__c = newContact.Id;
        newOpportunity.Signing_Contact__c=newContact.Id;
        insert newOpportunity; 
         
        newOpportunity.StageName = 'Closed Won';
        update newOpportunity;
        
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        insert objDir;
        
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        objDirEd.New_Print_Bill_Date__c = system.today();
        insert objDirEd;
       
        
        list<Opportunity> lstOpportunity = OpportunitySOQLMethods.setOpportunityByIdForLockCloneCreateOrder(new set<ID>{newOpportunity.Id});
         system.debug('DDDDDDDDDDDDDDDD'+ lstOpportunity.size());
        Directory__c newDirectory = TestMethodsUtility.createDirectory();
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        system.debug('newOrder '+ newOrder);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        system.debug('newOrderSet '+ newOrderSet );
        //Order_Line_Items__c newOrLI = TestMethodsUtility.createOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
         //lstOrLI.add(newOrLI);
        Order_Line_Items__c newOrLI = TestMethodsUtility.generateOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
        newOrLI.Media_type__c = 'Print';
        newOrLI.Product2__c=newProduct.Id;
        newOrLI.UnitPrice__c = 10;
        newOrLI.Directory__c = objDir.Id;
        newOrLI.Directory_Edition__c = objDirEd.Id;
        lstOrLI.add(newOrLI);
        Order_Line_Items__c newOrLI1 = TestMethodsUtility.generateOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
        newOrLI1.Media_type__c = 'Digital';
        newOrLI1.Product2__c=newProduct.Id;
        newOrLI1.UnitPrice__c = 10;
        newOrLI1.Package_ID__c = 'test007';
        newOrLI1.Package_Name__c = 'PackageName';
        Order_Line_Items__c newOrLI2 = TestMethodsUtility.generateOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
        newOrLI2.Media_type__c = 'Digital';
        newOrLI2.Product2__c=newProduct.Id;
        newOrLI2.UnitPrice__c = 10;
        newOrLI2.Package_ID__c = null;
        lstOrLI.add(newOrLI2);
        insert lstOrLI;
        system.assertNotEquals(null, lstOrLI);
        system.debug('EEEEEEEEEEEEEEEE'+ lstOrLI.size());
        set<ID> setOLIId = new set<ID>();
        for(Order_Line_Items__c iterator : lstOrLI) {
            setOLIId.add(iterator.Id);
        }
        list<Order_Line_Items__c> lstOLI = [Select Id, Account__c,Account__r.phone,Account__r.BillingState,Account__r.BillingPostalCode,Account__r.BillingStreet,Account__r.BillingCity,
                Opportunity__r.StageName, Addon_Type__c, Billing_Contact__c, Billing_Contact__r.Name, Billing_Contact__r.Phone, Directory_Section__r.Section_Page_Type__c,
                Billing_End_Date__c, Billing_Frequency__c,Opportunity__r.Billing_Contact__r.Email,Opportunity__r.Billing_Contact__r.HasOptedOutOfEmail, Billing_Partner__c, 
                Billing_Start_Date__c, Canvass__c, Category__c, Continious_Billing__c, Opportunity__r.Billing_Contact__c,RecordTypeId, Core_Opportunity_Line_ID__c, 
                Description__c, Directory__c, Directory_Edition__c, Discount__c, Distribution_Area__c, Product2__r.Name, Product2__r.Inventory_Tracking_Group__c,  
                Effective_Date__c, FulfillmentDate__c, Geo_Code__c, Geo_Type__c, Is_Caption__c, Directory_Section__r.Section_Code__c,Media_Type__c,Cutomer_Cancel_Date__c, Product2__r.Is_Anchor__c, 
                Is_Child__c, Is_P4P__c, Line_Status__c, Linvio_Payment_Method_Id__c, Directory_Heading__c, Directory_Heading__r.Name, Directory_Heading__r.code__c,  
                Locality__c, UnitPrice__c, Name, Seniority_Date__c, Directory_Section__r.Columns__c,ListPrice__c,Last_Billing_Date__c,Account__r.OwnerId,Account__r.Owner.Email,Account__r.Owner.Manager.Email,
                Opportunity__c, Opportunity_line_item_id__c,Directory__r.Branch_Office__c, Order__c, Order_Anniversary_Start_Date__c, Order_Group__c, Order_Line_Total__c, P4P_Billing__c, 
                P4P_Price_Per_Click_Lead__c, Package_ID__c, Parent_ID__c, Parent_ID_of_Addon__c, Parent_Line_Item__c, Payment_Duration__c, Payment_Method__c,isCanceled__c,
                Payments_Remaining__c, Pricing_Program__c, Print_First_Bill_Date__c,Package_Name__c, Product_Type__c, Product2__c, Product2__r.Vendor__c, ProductCode__c, Quantity__c, 
                Quote_signing_method__c, Scope__c, Directory_Section__c, Statement_Suppression__c, Status__c, Product2__r.Family, Product2__r.RGU__c, Product2__r.Is_IBUN_Bundle_Product__c,
                Directory_Section__r.Name,Directory__r.Pub_Date__c, Section_Page_Type__c, strOLICombo__c, Product_Inventory_Tracking_Group__c, Opportunity_Close_Date__c,
                Account__r.Name,Account__r.Owner.Name,Directory_Edition__r.Name,Directory__r.Name,UDAC__c,Successful_Payments__c, Talus_Go_Live_Date__c, Telco_Invoice_Date__c, Directory_Code__c, 
                Edition_Code__c, Canvass__r.Canvass_Code__c,Product2__r.Product_Type__c, Listing__r.Phone__c, Listing__r.Area_Code__c, Opportunity__r.Name, Billing_Partner_Account__c, 
                Listing__r.Name, Listing__r.Listing_City__c, Listing__r.Listing_Country__c, Listing__r.Listing_PO_Box__c, Listing__r.Listing_Postal_Code__c, Listing__r.Listing_State__c, 
                Listing__r.Listing_Street__c, Product2__r.Description,Listing__r.Listing_Street_Number__c,Directory_Edition__r.Book_Status__c,Directory_Edition__r.Pub_Date__c,Directory_Edition__r.Directory__c
                from Order_Line_Items__c where Id IN:setOLIId];
        
         List<Modification_Order_Line_Item__c> MOLIList = new List<Modification_Order_Line_Item__c>();
    Modification_Order_Line_Item__c mol = new Modification_Order_Line_Item__c(Billing_Frequency__c='Monthly',Order_Line_Item__c=lstOrLI[0].id);
    mol.Order_Group__c = newOrderSet.Id;
    mol.Action_Code__c = 'Renew';
     mol.Product_Type__c='SEO';
    mol.Product2__c =newProduct.id;  
    mol.Opportunity__c=newOpportunity.Id;
    mol.Account__c=newAccount.Id;
    mol.Media_Type__c='Digital';
    mol.UnitPrice__c=30;
    mol.MOLI_Package_Name__c='A La Carte';
    MOLIList.add(mol);
    Modification_Order_Line_Item__c mol1 = new Modification_Order_Line_Item__c(Billing_Frequency__c='Monthly',Order_Line_Item__c=lstOrLI[1].id);
    mol1.Order_Group__c = newOrderSet.Id;
    mol1.Action_Code__c = 'Renew';
     mol1.Product_Type__c='SEO';
    mol1.Product2__c =newProduct.id;  
    mol1.Opportunity__c=newOpportunity.Id;
    mol1.Account__c=newAccount.Id;
    mol1.Media_Type__c='Digital';
    mol1.UnitPrice__c=30;
    mol1.MOLI_Package_Name__c='8798678565';
    MOLIList.add(mol1);
       insert MOLIList;
       
       List<Modification_Order_Line_Item__c> retMoliList=[select id,Media_Type__c,MOLI_Package_Name__c,Product2__r.Description,UnitPrice__c,Opportunity__r.Billing_Contact__r.HasOptedOutOfEmail,Account__c,Opportunity__r.Billing_Contact__c,Opportunity__r.Billing_Contact__r.Email,Opportunity__r.StageName,Billing_Frequency__c,Order_Line_Item__c,Product_Type__c,Product2__c,Action_Code__c from Modification_Order_Line_Item__c where id IN :MOLIList];
       
        SoldLetterNotification.lineitemSoldNew(lstOLI, lstOpportunity,retMoliList);
        Test.stopTest();
    }
}