/*************************************************************
Apex class to update DFF record's Subscription and Product Ids
Created By: Reddy(reddy.yellanki@theberrycompany.com)
Updated Date: 10/14/2014
$Id$
**************************************************************/
public with sharing Class updtDffWithTalusId {

    public static void updtDffTId(Map < String, String > dffMps, Map < String, String > dffMps1) {

        List < Digital_Product_Requirement__c > updtd = new List < Digital_Product_Requirement__c > ();

        for (Digital_Product_Requirement__c d: [SELECT id, Talus_DFF_Id__c, Talus_Subscription_Id__c from Digital_Product_Requirement__c where id in : dffMps.keyset()]) {
            
            d.Talus_DFF_Id__c = dffMps.get(d.id);
            d.Fulfillment_Submit_Status__c = 'Complete';
            d.Talus_Subscription_Id__c = dffMps1.get(d.id);
            updtd.add(d);

        }

        if(updtd.size()>0){
            update updtd;
        }
    }
}