global class TBCbDirectoryEditionDateBatch implements Database.Batchable<sObject>,Database.Stateful {
	global Database.QueryLocator start(Database.BatchableContext bc) {
		//string rId = 'a4xZ0000000Coyy';
    	return database.getQuerylocator('SELECT Id, DE_Suppress_UnSuppress_OLI__c,Pub_Date__c FROM Directory_Edition__c where DE_Suppress_UnSuppress_OLI__c = null');
    }
    global void execute(Database.BatchableContext bc, List<Directory_Edition__c> deList) {
    	list<Directory_Edition__c> lstDE = new list<Directory_Edition__c>();
    	if(deList.size() > 0) {
            for(Directory_Edition__c iterator : deList) {
                if(iterator.Pub_Date__c != null) {
	                integer month = iterator.Pub_Date__c.month();
	                integer year = iterator.Pub_Date__c.year();
	                integer supUnsupDay = 14;
	                integer supUnsupmonth = month+1;
	                iterator.DE_Suppress_UnSuppress_OLI__c = date.newInstance(year,supUnsupmonth,supUnsupDay);
	                lstDE.add(iterator);
                }
            }
            if(lstDE.size() > 0) {
                update lstDE;
            }
        }
    }
    global void finish(Database.BatchableContext bc){}
}