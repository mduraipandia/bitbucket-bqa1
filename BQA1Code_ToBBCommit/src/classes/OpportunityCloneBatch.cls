global class OpportunityCloneBatch implements Database.Batchable<sObject> {
    global String strOPPID; 
    global Integer iCount; 
    global OpportunityCloneBatch(String oppID, Integer startCount) {
        strOPPID = oppID;
        iCount = startCount;
    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String SOQL = 'SELECT AccountId, Account_Number__c, Account_Manager__c, Allow_Upfront_Payment__c, Amount, Approval_Status__c, Auto_Number__c, BMI_Quote_Exists__c, pymt__Balance__c, Billing_Contact__c, Billing_Frequency__c, Billing_Locked__c, Billing_Partner__c, CORE_Migration_ID__c, CORE_Migration_ID_temp__c, CampaignId, Cancellation_Letter_Sent__c, Client_Tier__c, Close_Date__c, IsClosed, Closed_Lost_Reason__c, Co_Op_Ads_and_Brands__c, Converted_From_Lead__c, CreatedById, CreatedDate, Credit_Check__c, pymt__Currency_ISO_Code__c, DB_Competitor__c, IsDeleted, Override__c, Description, Directory_Number__c, DocuSign_Received__c, Enterprise_Customer_ID__c, CloseDate, ExpectedRevenue, Fiscal, FiscalQuarter, FiscalYear, ForecastCategoryName, ForecastCategory, pymt__Frequency__c, Guaranteed_Calls__c, Guaranteed_End_Date__c, Guaranteed_Start_Date__c, HasOpportunityLineItem, pymt__Invoice_Number__c, LastActivityDate, LastModifiedById, LastModifiedDate, Late_Print_Order_Approved__c, Late_Print_Order_Description__c, Late_Print_Order_Rejected__c, LeadSource, BigMachines__Line_Items__c, Main_Listed_City__c, Main_Listed_Phone__c, Main_Listed_State__c, Main_Listed_Street__c, Main_Listed_Zip_Code__c, Name, National_Staging_Order_Set__c, NextStep, Number_of_Cancelled_Opportunity_Products__c, Number_of_Opportunity_Products__c, pymt__Number_of_Payments_Made__c, pymt__Occurrences__c, Account_Primary_Canvass__c, Id, Opportunity_Owner_Email__c, Opportunity_Owner_Phone__c, Type, OwnerId, PIA__c, pymt__PO_Number__c, pymt__Paid_Off__c, Payment_Method__c, pymt__Payments_Made__c, pymt__Period__c, Prepayment_Required__c, Pricebook2Id, P4P_Price_Quote__c, IsPrivate, Probability, TotalOpportunityQuantity, RAS_Date__c, RAS_ID__c, RecordTypeId, pymt__Recurring_Amount__c, Requires_Late_Print_Order_Approval__c, Send_Cancellation_Letter__c, pymt__Shipping__c, Signing_Contact__c, Sig_Contact_Email__c, Sig_Contact_First__c, Sig_Contact_Last__c, Sig_Contact_FullName__c, Sig_Contact_Title__c, Signing_Method__c, pymt__Site_Name__c, pymt__SiteQuote_Expiration__c, pymt__SiteQuote_Recurring_Setup__c, StageName, pymt__Subscription_Start_Date__c, SystemModstamp, pymt__Tax__c, Telco__c, TmpOppty_Total_Price__c, pymt__Total_Amount__c, TotalSinglePaymentAmount__c, Transaction_Unique_ID__c, c2g__CODAUnitOfWork__c, Website__c, IsWon, billing_defaults_set__c, cmr_Number__c, isLocked__c, modifyid__c, (SELECT Action__c, Advertising_Data__c, Anchor_Ad__c, Auto_Number__c, BAS__c, BCC_Applied__c, BCC_Duration__c, BCC_Name__c, BCC_Redeemed__c, Berry_Cares_Certificate_ID__c, Billing_End_Date__c, Billing_Method__c, Billing_Partner__c, Billing_Start_Date__c, CMR_Number__c, CMR_Name__c, CORE_Migration_ID__c, Cancel_Date__c, Canvass__c, Category__c, Client_Number__c, Contact__c, CreatedById, CreatedDate, Client_Name__c, DAT__c, ServiceDate, Date__c, IsDeleted, Digital_Product_Requirement__c, Directory__c, Directory_Number__c, Directory_Edition__c, Directory_Heading__c, Directory_Heading1__c, Directory_Heading2__c, Directory_Heading3__c, Directory_Heading4__c, Directory_Section__c, Discount, Display_Ad__c, Distribution_Area__c, Duration__c, Effective_Date__c, From__c, Full_Rate__c, Geo_Code__c, Geo_Name__c, Geo_Type__c, HasQuantitySchedule, HasRevenueSchedule, HasSchedule, Heading__c, Heading_Code__c, IsActive__c, Is_Anchor__c, IsCancel__c, Is_Caption__c, Is_Child__c, Is_P4P__c, LastModifiedById, LastModifiedDate, Line_Number__c, Description, Id, ListPrice, Listing__c, Listing_Id__c, Listing_Name__c, Locality__c, Margin__c, Migration_ID__c, NAT__c, NAT_Client_ID__c, OpportunityId, BigMachines__Origin_Quote__c, Original_Line_Item_ID__c, Original_OLI_Seniority_Date__c, P4P_Billing__c, P4P_Price_Per_Click_Lead__c, Package_ID__c, Package_ID_External__c, Package_Item_Quantity__c, Package_Name__c, Parent_ID__c, Parent_ID_of_Addon__c, Parent_Line_Item__c, Billing_Duration__c, Previous_End_Date__c, Previous_Start_Date__c, PricebookEntryId, Pricing_Program__c, ProductCode__c, Product_Type__c, Publication_Code__c, Publication_Company__c, Publication_Date__c, Quantity, Renewals_Action__c, Requires_Inventory_Tracking__c, SPINS__c, Scope__c, Section_Code__c, Section_Page_Type__c, UnitPrice, Seniority_Date_Reset__c, Single_Payment_Total__c, SortOrder, Start_Date__c, Status__c, Subtotal, BigMachines__Synchronization_Id__c, SystemModstamp, To__c, To_Pubco__c, Trade_Mark_Parent_Id__c, Trans__c, Trans_Version__c, TransIDLineNo__c, Transaction_ID__c, Type__c, UDAC__c, c2g__CODAUnitOfWork__c, isModify__c, strOppliCombo__c, strOpplineProductCombo__c FROM OpportunityLineItems) FROM Opportunity where id =:strOPPID';
        return Database.getQueryLocator(SOQL);
    }
    
    global void execute(Database.BatchableContext bc, List<Opportunity> lstExistingOpp) {
        Integer intAutoNumber = 10000;
        //map<String,String> mpOldNewCoreId = new map<String,String>();
        //map<String,Opportunity> mpCoreIdOpportunity = new map<String,Opportunity>();
        List<Opportunity> lstOpportunity = new List<Opportunity>();
        List<OpportunityLineItem> lstOppLI = new List<OpportunityLineItem>();
        for(Opportunity objOpp : lstExistingOpp) {
            intAutoNumber++;
            Opportunity objClonedOpp = objOpp.clone();
            //objClonedOpp.core_migration_id__c = objOpp.core_migration_id__c+string.valueof(intAutoNumber);
            objClonedOpp.islocked__c = false;
            objClonedOpp.Ownerid = '005G0000001srdD';
            objClonedOpp.recordtypeid = label.TestOpportunityReadyRT;
            //mpOldNewCoreId.put(string.valueof(intAutoNumber), objOpp.core_migration_id__c);
            //mpCoreIdOpportunity.put(objOpp.core_migration_id__c, objOpp);
            lstOpportunity.add(objClonedOpp);
        }
        if(lstOpportunity != null && lstOpportunity.size() > 0) {
            insert lstOpportunity;
        }
        Integer intOppLIAutoNumber;
        for(Opportunity objOpp : lstOpportunity) {
            intOppLIAutoNumber = 100;
            for(OpportunityLineItem objOppLI : objOpp.OpportunityLineItems) {
                iCount++;                             
                OpportunityLineItem objClonedOppLI = objOppLI.clone();                  
                if(objOppLI.Parent_ID__c != null) {
                    objClonedOppLI.Parent_ID__c = objOppLI.Parent_ID__c+iCount;
                }
                else if(objOppLI.Parent_ID_of_Addon__c != null) {
                    objClonedOppLI.Parent_ID_of_Addon__c = objOppLI.Parent_ID_of_Addon__c+iCount;
                }
                //if(objOppLI.BigMachines__Synchronization_Id__c != null) {
                objClonedOppLI.BigMachines__Synchronization_Id__c = null;
                //}
                objClonedOppLI.opportunityid = objOpp.id;                      
                //objClonedOppLI.core_migration_id__c = 'Test'+objOppLI.core_migration_id__c+'-OppLI'+intOppLIAutoNumber;
                lstOppLI.add(objClonedOppLI);
            }
        }
        if(lstOppLI != null && lstOppLI.size() > 0) {
            insert lstOppLI;
        }
    }
    
    global void finish(Database.BatchableContext bc) {
        
    }
}