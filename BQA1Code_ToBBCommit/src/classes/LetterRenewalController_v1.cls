public class LetterRenewalController_v1{
    public static void letterRenewal(List<Order_Line_Items__c> lstOLI) {
        map<Id, list<Order_Line_Items__c>> mapTempOLI = new map<Id, list<Order_Line_Items__c>>();
        map<Id, map<ID, list<Order_Line_Items__c>>> mapAccDEOLIForRenewal1 = new map<Id, map<ID, list<Order_Line_Items__c>>>();
        map<Id, map<ID, list<Order_Line_Items__c>>> mapAccDEOLIForRenewal2 = new map<Id, map<ID, list<Order_Line_Items__c>>>();
        map<Id, list<Id>> mapAccOLIDirectoryIds = new map<Id, list<Id>>();
        map<Id, Order_Line_Items__c> mapOliMoli = new map<Id, Order_Line_Items__c>();
        set<Id> setAccIds = new set<Id>();
        set<Id> setRemovedAccIds = new set<Id>();
        set<Id> setOliId = new set<Id>();
        BerryLogo__c berrytest = BerryLogo__c.getInstance('Berryurl');
    
        for(Order_Line_Items__c iterator : lstOLI){
            if(iterator.Media_Type__c == 'Print' && iterator.Account__r.Delinquency_Indicator__c == false && iterator.Product2__r.Print_Product_Type__c != 'Specialty' && iterator.Is_P4P__c == false && iterator.Account__r.Open_Claim__c == false){
                if(!setRemovedAccIds.contains(iterator.Account__c)) {
                    if(!mapTempOLI.containsKey(iterator.Account__c)) {
                        mapTempOLI.put(iterator.Account__c, new list<Order_Line_Items__c>());
                    }
                    mapTempOLI.get(iterator.Account__c).add(iterator);
                }
            }
            else{
                setRemovedAccIds.add(iterator.Account__c);
                if(mapTempOLI.containsKey(iterator.Account__c)) {
                    mapTempOLI.remove(iterator.Account__c);                    
                }
            }            
        } 
        List<Order_Line_Items__c> lstOLILetterRenewal = new List<Order_Line_Items__c>();
        if(mapTempOLI.size() > 0) {
            for(Id accId :  mapTempOLI.keySet()) {
                if(mapTempOLI.get(accId) != null) {
                    for(Order_Line_Items__c iterator :  mapTempOLI.get(accId)) {
                        lstOLILetterRenewal.add(iterator);
                    }
                }
            }
        }
        if(lstOLILetterRenewal.size()>0){
            for(Order_Line_Items__c iterator : lstOLILetterRenewal) {
                setAccIds.add(iterator.Account__c);
                setOliId.add(iterator.Id);
                //need to create a set of account Ids for which Billing contact email is null
                if(!String.isBlank(iterator.Account__r.Letter_Renewal_Sequence__c)) {
                    if(iterator.Account__r.Letter_Renewal_Sequence__c.equals('1')) {
                        if(!mapAccDEOLIForRenewal1.containsKey(iterator.Account__c)) {
                            mapAccDEOLIForRenewal1.put(iterator.Account__c, new map<Id, list<Order_Line_Items__c>>());
                        }
                        if(!mapAccDEOLIForRenewal1.get(iterator.Account__c).containsKey(iterator.Directory_Edition__c)) {
                            mapAccDEOLIForRenewal1.get(iterator.Account__c).put(iterator.Directory_Edition__c, new list<Order_Line_Items__c>());
                        }
                        mapAccDEOLIForRenewal1.get(iterator.Account__c).get(iterator.Directory_Edition__c).add(iterator);
                    }
                    else if(iterator.Account__r.Letter_Renewal_Sequence__c.equals('2')) {
                     
                        if(!mapAccDEOLIForRenewal2.containsKey(iterator.Account__c)) {
                            mapAccDEOLIForRenewal2.put(iterator.Account__c, new map<Id, list<Order_Line_Items__c>>());
                        }
                        if(!mapAccDEOLIForRenewal2.get(iterator.Account__c).containsKey(iterator.Directory_Edition__c)) {
                            mapAccDEOLIForRenewal2.get(iterator.Account__c).put(iterator.Directory_Edition__c, new list<Order_Line_Items__c>());
                        }
                        mapAccDEOLIForRenewal2.get(iterator.Account__c).get(iterator.Directory_Edition__c).add(iterator);
                        
                    }                    
                }
            }
        }
        
        map<Id, Account> mapAccountWithBillingContact = new map<Id, Account>([Select Id,Name,BillingStreet,BillingCity,BillingState,BillingPostalCode,Phone,Account_Number__c ,(Select Id, Email from Contacts where Email != null) from Account where Id IN:setAccIds]);
        map<Id, List<String>> mapRenewalPDFString = new map<Id, List<String>>();
        List<String> pdfStringLst = new List<String>();
        
        if(mapAccDEOLIForRenewal1.size()>0){
       
            for(Id accId : mapAccDEOLIForRenewal1.keySet()) {
            String ContactId;
            if(mapAccountWithBillingContact.get(accId).contacts.size()>0){
                ContactId = mapAccountWithBillingContact.get(accId).contacts[0].Id;
            }
                if(mapAccDEOLIForRenewal1.get(accId) != null) {
                 //mapRenewalPDFString = new map<Id, List<String>>();             
                    for(Id deId : mapAccDEOLIForRenewal1.get(accId).keySet()) {
                        String pdfString = '';
                        pdfString += '<html><body>';
                        pdfString += generateHeaderPDFString(mapAccountWithBillingContact.get(accId));
                        if(mapAccDEOLIForRenewal1.get(accId).get(deId) != null) {
                            pdfString += generateDirectoryEditionString(mapAccDEOLIForRenewal1.get(accId).get(deId));
                            pdfString += '</body><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/></html>';
                            pdfString += '<html><body>';
                            pdfString += '<div><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td><img src=\"'+berrytest.BerryURL__c+'\" width="90"></td></tr></table></div>';
                            pdfString += '<table width="100%"><tr><td border="0" bgColor="#848484" align="left" width="40%">Products</td><td border="0" bgColor="#848484" align="center" width="20%">Intial Term</td><td border="0" bgColor="#848484" align="center" width="20%">Renewal Terms</td><td border="0" bgColor="#848484" align="right" width="20%">Monthy Rate</td></tr></table>'; 
                            pdfString += generateDirectoryEditionAccountString(mapAccDEOLIForRenewal1.get(accId).get(deId));
                            for(Order_Line_Items__c iterator : mapAccDEOLIForRenewal1.get(accId).get(deId)) {
                                pdfString += generateDirOlitemsString(iterator);
                            }
                            pdfString += generateTotalPrintInvestmentString(mapAccDEOLIForRenewal1.get(accId).get(deId));
                            system.debug('*********PDF STRING********'+pdfString );
                            //pdfStringLst.add(pdfString);
                        }
                        if(ContactId != null){
                            if(!mapRenewalPDFString.containskey(ContactId)){
                                mapRenewalPDFString.put(ContactId, new list<string>());
                            }
                            mapRenewalPDFString.get(ContactId).add(pdfString);
                        }
                       // mapRenewalPDFString.get(ContactId).add(pdfString);
                        
                    }
                }
                
            }
        }
        if(mapAccDEOLIForRenewal2.size()>0){
            for(Id accId : mapAccDEOLIForRenewal2.keySet()) {
            String Contact2Id;
            if(mapAccountWithBillingContact.get(accId).contacts.size()>0){
                Contact2Id = mapAccountWithBillingContact.get(accId).contacts[0].Id;
            }
            system.debug('************Contact Id****************'+Contact2Id );
             //mapRenewalPDFString = new map<Id, List<String>>();
                if(mapAccDEOLIForRenewal2.get(accId) != null) {
                    for(Id deId : mapAccDEOLIForRenewal2.get(accId).keySet()) {
                        String pdfString = '';
                        pdfString += '<html><body>';
                        pdfString += generateHeaderPDFString(mapAccountWithBillingContact.get(accId));
                        if(mapAccDEOLIForRenewal2.get(accId).get(deId) != null) {
                            pdfString += generateDirectoryEditionString(mapAccDEOLIForRenewal2.get(accId).get(deId));
                            pdfString += '</body><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/></html>';
                            pdfString += '<html><body>';
                            pdfString += '<div><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td><img src=\"'+berrytest.BerryURL__c+'\" width="90"></td></tr></table></div>';
                            pdfString += '<table width="100%"><tr><td border="0" bgColor="#848484" align="left" width="40%">Products</td><td border="0" bgColor="#848484" align="center" width="20%">Intial Term</td><td border="0" bgColor="#848484" align="center" width="20%">Renewal Terms</td><td border="0" bgColor="#848484" align="right" width="20%">Monthy Rate</td></tr></table>'; 
                            pdfString += generateDirectoryEditionAccountString(mapAccDEOLIForRenewal2.get(accId).get(deId));
                            for(Order_Line_Items__c iterator : mapAccDEOLIForRenewal2.get(accId).get(deId)) {
                                pdfString += generateDirOlitemsString(iterator);
                            }
                            pdfString += generateTotalPrintInvestmentString(mapAccDEOLIForRenewal2.get(accId).get(deId));
                            system.debug('*********PDF STRING********'+pdfStringLst);
                            //pdfStringLst.add(pdfString);
                        }
                        if(Contact2Id != null){
                            if(!mapRenewalPDFString.containskey(Contact2Id)){
                                mapRenewalPDFString.put(Contact2Id, new list<string>());
                            }
                            mapRenewalPDFString.get(Contact2Id).add(pdfString);
                        }
                       // mapRenewalPDFString.get(Contact2Id).add(pdfString);
                    }
                }
                 
            }
        }
        
        if(mapRenewalPDFString.size() > 0) {
        system.debug('*********LETTER RENWAL SEND EMAIL METHOD INVOKE ***********');
            sendEmail(mapRenewalPDFString);
        }
        if(setRemovedAccIds.size()>0){
            updateAccountFallOutFlag(setRemovedAccIds);
        }
        
        //As per BFTHREE-1839 creation of new OLI
        if(setOliId.size()>0){
            mapOliMoli = OrderLineItemSOQLMethods.getOrderLineItemandMoliByID(setOliId);
        }
        if(!Test.isRunningTest()) {
            if(mapOliMoli.size()>0){
                createNewOLI(mapOliMoli.values());
            }
        }
    }
    
    public static string generateHeaderPDFString(Account objAccount) {
      BerryLogo__c berrytest1 = BerryLogo__c.getInstance('Berryurl');
      system.debug('&&&&&&&&&'+berrytest1.BerryURL__c);
        String strOLIHPDF = '';
        strOLIHPDF += '<div><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td><img src=\"'+berrytest1.BerryURL__c+'\" width="90"></td></tr></table></div>';
        strOLIHPDF += '<div style="border:5px solid black;padding-left:10%;padding-right:10%;height:700px"><div style="float:left"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size:10px" align="left"><tr><td nowrap="true" style="font-size:9pt;"><br/><span>'+objAccount.Name+'</span><br/><span>'+objAccount.BillingStreet+'</span><br/><span>'+objAccount.BillingCity+'</span>,&nbsp;<span>'+objAccount.BillingState+'</span>,&nbsp;<span>'+objAccount.BillingPostalCode+'</span><br/>&nbsp;<span>'+objAccount.Phone+'</span><br/><b>Account Number:</b>&nbsp;<span>'+objAccount.Account_Number__c+'</span><br/><br/></td></tr><tr><td border="1"><hr></td></tr></table></div></div><br/>';
        strOLIHPDF += '<table width="100%"><tr><td border="0" bgColor="#848484" align="center">Advertising Order - Investment Summary</td></tr></table>';
        strOLIHPDF += '<table border= "0" width="100%"><tr><td width="50%" style="font-size:10px;color:#2E2E2E">PRINT DIRECTORY ADVERTISING</td><td width="25%" style="font-size:10px;color:#2E2E2E"; align="center">Effective Billing Date</td><td style="font-size:10px;color:#2E2E2E";width="25%" align= "right">Monthly Investment</td></tr></table>';
        system.debug('*********LETTER RENWAL STRING GENERATION ***********'+strOLIHPDF );
        return strOLIHPDF;
    }
    
    public static string generateDirectoryEditionString(list<Order_Line_Items__c> lstOLI) {
        decimal monthlyPayment = 0.00;
        String strOLIPDF = '';
        string Dt;
        List<Order_line_Items__c> oliLbdLst = new List<Order_line_Items__c>();
        set<Id> oliLbdId = new set<Id>();        
        for(Order_Line_Items__c objOli : lstOLI){
            monthlyPayment += objOli.UnitPrice__c;
            oliLbdId.add(objOli.Id);
        }
        
       Order_Line_Items__c objOli = lstOLI[0];
       if(oliLbdId.size()>0){
           oliLbdLst = [Select Id,Name,Last_Billing_Date__c from Order_Line_Items__c where Id IN:oliLbdId ORDER BY Last_Billing_Date__c ASC];
       }
       for(Integer i=0;i<oliLbdLst.size();){
           if(oliLbdLst[i].Last_Billing_Date__c != null){
               Integer day=0;
               Integer month=0;
               Integer year=0;
               day = oliLbdLst[i].Last_Billing_Date__c.day();
               month = oliLbdLst[i].Last_Billing_Date__c.month();
               year = oliLbdLst[i].Last_Billing_Date__c.year();
               
               Dt = month+'-'+ day+'-'+ year;
               break;
           }
           i++;
       }
       
        strOLIPDF += '<table width="100%" cellpadding="0" cellspacing="0" style="margin:0px auto;" border="0"><tr>';
        strOLIPDF += '<td style="font-weight:normal;color:#848484" width="50%">'+objOli.Directory_Edition__r.Name+'</td><td style="font-weight:normal;color:#848484" width="25%" align="center">'+Dt+'</td><td style="font-weight:normal;color:#848484" width="25%" align="right">$'+monthlyPayment+'</td></tr></table>';
        strOLIPDF += '<br/><table width="100%" cellpadding="0" cellspacing="0"><tr><td width="75%" align="left">Total Monthly Investment</td><td align="right" width="25%">$'+monthlyPayment+'</td></tr></table>';
        system.debug('*********LETTER RENWAL STRING GENERATION ***********'+strOLIPDF);
        return strOLIPDF;
    }
    
    public static string generateDirectoryEditionAccountString(list<Order_Line_Items__c> lstOLIDE) {
        Order_Line_Items__c objOliDEAct = lstOLIDE[0];
        String strOLIDEACT = '';
        strOLIDEACT += '<table width="100%" cellpadding="0" cellspacing="0" style="margin:0px auto;" border="0"><tr><td style="font-weight:bold;" width="50%">'+objOliDEAct.Directory_Edition__r.Name+'</td></tr><tr><td nowrap="true" style="font-size:9pt;font-style:italic;color:#848484"><span>'+objOliDEAct.Account__r.Phone+'</span><br/><span>'+objOliDEAct.Account__r.Name+'</span><br/><span>'+objOliDEAct.Account__r.BillingStreet+'</span><br/><span>'+objOliDEAct.Account__r.BillingCity+'</span>,&nbsp;<span>'+objOliDEAct.Account__r.BillingState+'</span> &nbsp;<span>'+objOliDEAct.Account__r.BillingPostalCode+'</span><br/></td></tr></table>';
        return strOLIDEACT;
    }
    
    public static string generateDirOlitemsString(Order_Line_Items__c iterator){
        String oliDirString = '';
        system.debug('*********Monthly Payment*********'+iterator.UnitPrice__c);
        oliDirString += '<table width="100%"><tr><td border="0" align="left" width="40%">'+iterator.Product2__r.Name+'</td><td border="0"  align="center" width="20%">12 Months</td><td border="0"  align="center" width="20%">12 Months</td><td border="0" align="right" width="40%"></td></tr></table>'; 
        return oliDirString;
    }
    public static string generateTotalPrintInvestmentString(list<Order_Line_Items__c> lstTotalOli) {
        decimal totalmonthlyPayment = 0.00;
        Order_Line_Items__c objTotalOli = lstTotalOli[0];
        for(Order_Line_Items__c objOli : lstTotalOli){
            totalmonthlyPayment += objOli.UnitPrice__c;
        }
        String strTotalOli = '';
        strTotalOli += '<table width="100%" border="0" style="table-layout:fixed;"><tr><td border="0" style="font-weight:bold;" align="left" width="80%">Total Print Investment for '+objTotalOli.Account__r.Phone+'</td><td border="0" style="font-weight:bold;text-align:right;" width="20%">$'+totalmonthlyPayment+'</td></tr></table>';
        strTotalOli += '</body></html>';
        return strTotalOli;
    }
    public static void sendEmail(map<Id, List<String>> mapRenewalPDFString) {
        List<Messaging.SingleEmailMessage> lstEmail= new list<Messaging.SingleEmailMessage>();
        for(Id conId : mapRenewalPDFString.keySet()) {
            for(integer i=0;i<mapRenewalPDFString.get(conId).size();i++){
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            //Initializing the Email Attachment
            List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
            Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
            efa.setFileName('Letter Renewal.pdf');
            efa.setBody(Blob.toPDF(mapRenewalPDFString.get(conId)[i]));
            fileAttachments.add(efa);
            email.setFileAttachments(fileAttachments);
            email.setTargetObjectId(conId);
            email.setTemplateId(Label.Letter_Renewal_Report_Template);
            lstEmail.add(email);
            }
        }
        Messaging.sendEmail(lstEmail);
    }
    public static void updateAccountFallOutFlag(set<Id> actIds){
        List<Account> objActFalloutLst = [Select Id,Name,Letter_Renewal_Fallout_Flag__c from Account where Id IN : actIds];
        if(objActFalloutLst.size()>0){
            for(Account objActLRF : objActFalloutLst){
                if(objActLRF.Letter_Renewal_Fallout_Flag__c == false){
                    objActLRF.Letter_Renewal_Fallout_Flag__c = true;
                }
            }
            if(objActFalloutLst.size()>0){
                update objActFalloutLst;
            }
        }
    }
    public static void createNewOLI(List<Order_Line_Items__c> lstOLI){
        map<Id,list<Order_Line_Items__c>> mapOptyOliLst = new map<Id,list<Order_Line_Items__c>>();
        set<Id> setOrderSetId = new set<Id>();
        map<Id,Order_Group__c> mapnewobjOrderSet = new map<Id,Order_Group__c>();
        map<Id,opportunity> mapnewobjOpp = new map<Id,opportunity>();
        set<Id> setOpptyId = new set<Id>();
        map<string,Directory_Product_Mapping__c> mapDPM = new map<string,Directory_Product_Mapping__c>();
        set<Id> setProdId = new set<Id>();
        set<Id> setDirId = new set<Id>();
        for(Order_Line_Items__c Oli : lstOLI){
            if(Oli.Cutomer_Cancel_Date__c == null){
                if(!mapOptyOliLst.containskey(Oli.Opportunity__c)){
                mapOptyOliLst.put(Oli.Opportunity__c,new list<Order_Line_Items__c>());
                }
                mapOptyOliLst.get(Oli.Opportunity__c).add(Oli);
                setOpptyId.add(Oli.Opportunity__c);
                setOrderSetId.add(Oli.Order_Group__c);
                if(Oli.Product2__c != null) {
                    setProdId.add(Oli.Product2__c);
                }
                if(Oli.Directory__c != null) {
                    setDirId.add(Oli.Directory__c);
                }
            }
        }
       
        system.debug('*********Opty Id for New Opty********'+ setOpptyId);  
        if(setOpptyId.size()>0){
            mapnewobjOpp = createNewOpty(setOpptyId);
        }
        //system.debug('*********Opty Id for New Opty********'+ setOpptyId);  
        system.debug('*********Modify New Oppty********'+ mapnewobjOpp.values());
        if(mapnewobjOpp.size()>0 && CommonVariables.inFutureContext  == false){
            CommonVariables.inFutureContext  = true;
            insert mapnewobjOpp.values();
        }
        
        if(setOrderSetId.size()> 0 && mapnewobjOpp.size()> 0){
            mapnewobjOrderSet = createNewOrderSet(setOrderSetId,mapnewobjOpp);
        }
        if(mapnewobjOrderSet.size()>0){
            insert mapnewobjOrderSet.values();
        }
        List<Directory_Product_Mapping__c> objDirPM = DirectoryProductMappingSOQLMethods.getDirProdMapByProdIdAndDirId(setProdId,setDirId);
        
        if(objDirPM.size()> 0){
            for(Directory_Product_Mapping__c objDPM : objDirPM){
                string strDirProd = objDPM.Directory__c+''+objDPM.Product2__c ;
                if(!mapDPM.containskey(strDirProd)){
                    mapDPM.put(strDirProd,objDPM);
                }
            }
        }
        
        list<directory_edition__c> dirEditionLst = new list<directory_edition__c>();
        map<Id,directory_edition__c> mapOliDir = new map<Id,directory_edition__c>();
        
        if(setDirId.size()> 0){
            dirEditionLst = DirectoryEditionSOQLMethods.getCurrentDEByDirectory(setDirId);
        }
        if(dirEditionLst.size()>0){
            for(directory_edition__c objDirEdition : dirEditionLst){
                if(!mapOliDir.containskey(objDirEdition.Directory__c)){
                    mapOliDir.put(objDirEdition.Directory__c,objDirEdition);
                }
            }
        }
        map<Id,Order_Line_Items__c> mapnewOLI = new map<Id,Order_Line_Items__c>();
        map<Id,Id> mapOldNewOli = new map<Id,Id>();
        list<Order_Line_Items__c> lstOroliUpdate = new list<Order_Line_Items__c>(); 
        
        for(Order_Line_Items__c objOrLi : lstOLI){
            if(mapnewobjOpp.size()>0 && mapnewobjOrderSet.size()>0){
                //create a New Order Line Item
                //mapnewOLILst = NewOrderLineItem(OliIterator,mapnewobjOpp,mapnewobjOrderSet,mapnewOLI);
                decimal modifiedUnitPrice;
                string strOliDirProd;
                if(objOrLi.Directory__c != null && objOrLi.Product2__c != null) {
                    strOliDirProd = objOrLi.Directory__c+''+objOrLi.Product2__c;
                }
                if(mapDPM.get(strOliDirProd) != null){
                    if(mapDPM.get(strOliDirProd).FullRate__c != null && objOrLi.UnitPrice__c != mapDPM.get(strOliDirProd).FullRate__c && objOrLi.Discount__c != null){
                        modifiedUnitPrice = (mapDPM.get(strOliDirProd).FullRate__c-((mapDPM.get(strOliDirProd).FullRate__c * objOrLi.Discount__c)/100)).setScale(2, RoundingMode.HALF_UP);
                    }
                }
                
                Order_Line_Items__c objNewOrdL = new Order_Line_Items__c();
                objNewOrdL.Order__c = objOrLi.Order__c;
                objNewOrdL.Order_Group__c = mapnewobjOrderSet.get(objOrLi.Order_Group__c).Id;
                if(mapnewobjOpp.get(objOrLi.Opportunity__c) != null){
                    objNewOrdL.Opportunity__c = mapnewobjOpp.get(objOrLi.Opportunity__c).Id;
                }
                objNewOrdL.Product2__c = objOrLi.Product2__c;
                objNewOrdL.FulfillmentDate__c = system.today();
                objNewOrdL.Canvass__c = objOrLi.Canvass__c;
                //objNewOrdL.Account_Manager__c = objOrLi.Account_Manager__c;
                objNewOrdL.Quote_signing_method__c = objOrLi.Quote_signing_method__c;
                objNewOrdL.Account__c = objOrLi.Account__c;
                objNewOrdL.Product_Type__c = objOrLi.Product_Type__c;
                //objNewOrdL.Parent_ID__c = objOrLi.Parent_ID__c;
                objNewOrdL.Package_ID__c = objOrLi.Package_ID__c;        
                objNewOrdL.Directory_Heading__c= objOrLi.Directory_Heading__c;
                objNewOrdL.Parent_Line_Item__c = objOrLi.Parent_Line_Item__c;
                objNewOrdL.Geo_Type__c  = objOrLi.Geo_Type__c ;
                objNewOrdL.Geo_Code__c = objOrLi.Geo_Code__c;
                objNewOrdL.Scope__c = objOrLi.Scope__c ;
                objNewOrdL.Directory_Section__c = objOrLi.Directory_Section__c;
                objNewOrdL.Status__c = objOrLi.Status__c;
                objNewOrdL.Directory_Heading__c = objOrLi.Directory_Heading__c;
                objNewOrdL.Directory__c = objOrLi.Directory__c;
                //objNewOrdL.Directory_Edition__c = DirEditionId;
                objNewOrdL.Statement_Suppression__c = objOrLi.Statement_Suppression__c;
                objNewOrdL.Order_Anniversary_Start_Date__c = objOrLi.Order_Anniversary_Start_Date__c;
                objNewOrdL.Quantity__c = objOrLi.Quantity__c;
                objNewOrdL.ListPrice__c= objOrLi.ListPrice__c;
                objNewOrdL.Listing__c= objOrLi.Listing__c;
                objNewOrdL.Description__c = objOrLi.Description__c;            
                objNewOrdL.Discount__c = objOrLi.Discount__c;
                objNewOrdL.Billing_End_Date__c = objOrLi.Billing_End_Date__c;
                objNewOrdL.Billing_Start_Date__c = objOrLi.Billing_Start_Date__c;
                objNewOrdL.Billing_Frequency__c = objOrLi.Billing_Frequency__c;
                objNewOrdL.Payment_Method__c = objOrLi.Payment_Method__c;
                objNewOrdL.Payment_Duration__c = objOrLi.Payment_Duration__c ;
                objNewOrdL.Billing_Partner__c = objOrLi.Billing_Partner__c;
                objNewOrdL.Billing_Contact__c = objOrLi.Billing_Contact__c;
                objNewOrdL.Billing_Partner_Account__c = objOrLi.Billing_Partner_Account__c;
                objNewOrdL.Payments_Remaining__c = 12;
                objNewOrdL.Successful_Payments__c = 0;
                objNewOrdL.Line_Status__c = objOrLi.Line_Status__c;
                objNewOrdL.Media_Type__c = objOrLi.Media_Type__c;
                objNewOrdL.RecordTypeId = objOrLi.RecordTypeId;
                objNewOrdL.Original_Order_Line_Item__c = objOrLi.Id;
                objNewOrdL.Seniority_Date__c = objOrLi.Seniority_Date__c;
                //As per ATP-4375
                if(objOrLi.Anchor_Listing_Caption_Header__c != null){
                    objNewOrdL.Anchor_Listing_Caption_Header__c = objOrLi.Anchor_Listing_Caption_Header__c;
                }
                if(objOrLi.Trademark_Finding_Line_OLI__c != null){
                    objNewOrdL.Trademark_Finding_Line_OLI__c = objOrLi.Trademark_Finding_Line_OLI__c;
                }
                if(objOrLi.Scoped_Caption_Header__c != null){
                    objNewOrdL.Scoped_Caption_Header__c = objOrLi.Scoped_Caption_Header__c;
                }
                objNewOrdL.Scoped_Suppression_Processing_Complete__c=false;
                if(mapOliDir.get(objOrLi.Directory__c) != null) {
                    objNewOrdL.Telco_Invoice_Date__c = mapOliDir.get(objOrLi.Directory__c).Bill_Prep__c;
                    objNewOrdL.Print_First_Bill_Date__c = mapOliDir.get(objOrLi.Directory__c).New_Print_Bill_Date__c;
                    
                    if(objNewOrdL.Payment_Method__c == 'Telco Billing'){
                        objNewOrdL.Effective_Date__c = mapOliDir.get(objOrLi.Directory__c).Pub_Date__c.adddays(-65);
                    }
                    else{
                        objNewOrdL.Effective_Date__c = objOrLi.Last_Billing_Date__c;
                    }
                    objNewOrdL.Directory_Edition__c = mapOliDir.get(objOrLi.Directory__c).Id;
                }
                if(modifiedUnitPrice != null){
                    objNewOrdL.UnitPrice__c = modifiedUnitPrice;  
                }
                else{
                    system.debug('************Enter Here if no Rate*********');
                    objNewOrdL.UnitPrice__c = objOrLi.UnitPrice__c;
                }
                if(!mapnewOLI.containskey(objOrLi.Id)){
                    mapnewOLI.put(objOrLi.Id, objNewOrdL);
                }
                objOrLi.Is_Handled__c = true;
                lstOroliUpdate.add(objOrLi);
            }
        }
        if(mapnewOLI.size()>0){
            insert mapnewOLI.values();
            /*if(lstOroliUpdate.size()> 0){
                update lstOroliUpdate;
            }*/
            for(Order_Line_Items__c iteratorNewOLI :mapnewOLI.values()){
                mapOldNewOli.put(iteratorNewOLI.Original_Order_Line_Item__c,iteratorNewOLI.Id);
            }
            if(mapOldNewOli.size()> 0){
                for(Order_Line_Items__c iteratorNewOLI :mapnewOLI.values()){
                    if(iteratorNewOLI.Anchor_Listing_Caption_Header__c != null){
                        iteratorNewOLI.Anchor_Listing_Caption_Header__c = mapOldNewOli.get(iteratorNewOLI.Anchor_Listing_Caption_Header__c);
                        lstOroliUpdate.add(iteratorNewOLI);
                    }
                    else if(iteratorNewOLI.Trademark_Finding_Line_OLI__c != null){
                        iteratorNewOLI.Trademark_Finding_Line_OLI__c = mapOldNewOli.get(iteratorNewOLI.Trademark_Finding_Line_OLI__c);
                        lstOroliUpdate.add(iteratorNewOLI);
                    }
                }
            }
            if(lstOroliUpdate.size()> 0){
                update lstOroliUpdate;
            }
            createDFFClone(mapnewOLI,mapOliDir);
        }
    }
    
    public static map<Id,Opportunity> createNewOpty(set<Id> setOpptyId){
        List<Opportunity> OptyrecordLst = new List<Opportunity>();
        map<Id,Opportunity> mapnewOpty = new map<Id,Opportunity>();    
        OptyrecordLst = [Select Id,Name,AccountId,Billing_Contact__c,Signing_Contact__c,Signing_Method__c,Billing_Partner__c,Payment_Method__c,Billing_Frequency__c  from Opportunity where Id = :setOpptyId ];
       
        if(OptyrecordLst.size()>0){
            for(Opportunity OldOpty :OptyrecordLst){
                Opportunity newOpty = new Opportunity();
                newOpty =  OldOpty.clone(false);
                newOpty.StageName = 'Auto Renewal';
                system.debug('******old opty billing******'+OldOpty.Billing_Partner__c);
                newOpty.Billing_Partner__c = OldOpty.Billing_Partner__c;
                newOpty.billing_defaults_set__c = true;
                //newOpty.isLocked__c = true;
                newOpty.RecordTypeId = CommonMethods.getRedordTypeIdByName(CommonMessages.opportunityLockRTName, CommonMessages.opportunityObjectName);       
                newOpty.CloseDate = system.today();
                //newOpty.OwnerId=Label.Label_LetterRenewelUser;
                if(!mapnewOpty.containsKey(OldOpty.Id)) {
                    mapnewOpty.put(OldOpty.Id, newOpty);
                }
            }
        }
        return mapnewOpty;
    }
    
    public static map<Id,Order_Group__c> createNewOrderSet(set<Id> setOrdsetId, map<Id,Opportunity> mapnewobjOpp){
        List<Order_Group__c> OrdersetLst = new List<Order_Group__c>();
        map<Id,Order_Group__c> mapnewOrderSet = new map<Id,Order_Group__c>();    
        OrdersetLst = [Select Id,Name,Order__c,IsActive__c,Order_Account__c,Opportunity__c,selected__c,Transaction_ID__c,Type__c from Order_Group__c where Id = :setOrdsetId ];
        system.debug('OrdersetLst --------'+OrdersetLst);
        
        if(OrdersetLst.size()>0){
            for(Order_Group__c OldOrderSet :OrdersetLst){
            system.debug('OldOrderSet -------'+OldOrderSet);
                Order_Group__c newOrderSet = new Order_Group__c();
                newOrderSet =  OldOrderSet.clone(false);
                system.debug('newOrderSet ---'+newOrderSet);
                system.debug('mapnewobjOpp---'+mapnewobjOpp);
                system.debug('get id of opp---'+mapnewobjOpp.get(OldOrderSet.Opportunity__c));
                newOrderSet.Opportunity__c = mapnewobjOpp.get(OldOrderSet.Opportunity__c).Id;
                if(!mapnewOrderSet.containsKey(OldOrderSet.Id)) {
                    mapnewOrderSet.put(OldOrderSet.Id, newOrderSet);
                }
            }
        }
        return mapnewOrderSet;
       
    }
    
    public static void createDFFClone(map<Id,Order_Line_Items__c> mapnewobjOLI,map<Id,directory_edition__c> mapOliDirEdition){
        set<Id> oldOLIId = new set<Id>();
        list<Digital_Product_Requirement__c> DFFList = new list<Digital_Product_Requirement__c> ();
        map<Id, Digital_Product_Requirement__c> OLIDFFMap = new map<Id, Digital_Product_Requirement__c>();
        list<Digital_Product_Requirement__c> DFFInsertList = new list<Digital_Product_Requirement__c> ();
        list<order_line_items__c> lstNewoliDFF = new list<order_line_items__c>();
        for(Id OldId : mapnewobjOLI.keyset()){
            oldOLIId.add(OldId);
        }
        String strQuery = CommonMethods.getCreatableFieldsSOQL('Digital_Product_Requirement__c');
        strQuery+='where OrderLineItemID__c in :oldOLIId';
        DFFList = Database.query(strQuery);
        system.debug('*******OLI DFF records********'+DFFList.size());
        if(DFFList.size()>0){
            for(Digital_Product_Requirement__c DFF : DFFList){
                OLIDFFMap.put(DFF.OrderLineItemID__c, DFF);
            }
        }
        //if(OLIDFFMap.size()>0){
            for(Id OLIId: mapnewobjOLI.keyset()){
                if(OLIDFFMap.get(OLIId) != null){
                system.debug('*******OLI DFF Enter here records********'+OLIDFFMap.get(OLIId));
                    Digital_Product_Requirement__c DFF = new Digital_Product_Requirement__c();
                    DFF = OLIDFFMap.get(OLIId).clone(false, false, false, false);
                    DFF.Core_Opportunity_Line_ID__c = '';
                    DFF.OrderLineItemID__c = mapnewobjOLI.get(OLIId).Id;
                    DFF.Directory_Edition__c = mapnewobjOLI.get(OLIId).Directory_edition__c;
                    DFF.Issue_Number__c = mapOliDirEdition.get(DFF.DFF_Directory__c).Edition_Code__c;
                    DFFInsertList.add(DFF);
                }
            }
        //}
        if(DFFInsertList.size() > 0 ){
            insert DFFInsertList;
        }
        for(Digital_product_requirement__c objDFFNew : DFFInsertList){
            order_line_items__c objOLINewDFF = new Order_line_items__c(Id = objDFFNew.OrderLineItemID__c);
            objOLINewDFF.Digital_Product_Requirement__c = objDFFNew.Id;
            lstNewoliDFF.add(objOLINewDFF);
        }
        if(lstNewoliDFF.size()>0){
            update lstNewoliDFF;
        }
    }
}