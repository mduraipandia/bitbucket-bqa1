@IsTest (SeeAllData=true)
public class MassUpdateLeadListingsTestClass2 {
    public static testmethod void MassUpdateLeadListings2Test (){
        Test.startTest();
        Account telcoAcc = TestMethodsUtility.createAccount('telco');
        Telco__c telco = TestMethodsUtility.createTelco(telcoAcc.Id);
		Canvass__c objCanvass = TestMethodsUtility.createCanvass(telco);
        
        Lead objLead = new Lead(FirstName = 'Test', LastName = 'Test Unit', Company = 'Test Berry', Primary_Canvass__c = objCanvass.Id, Phone = '9999999999', Status = 'Open');
        insert objLead; 
        
        Listing__c objListing = new Listing__c();
            objListing.Name = 'test listing name2';
            objListing.Bus_Res_Gov_Indicator__c = 'Business';
            objListing.Phone__c = '(414) 068-1588';
            objListing.Listing_Street__c = 'Test st123';
            objListing.Listing_City__c = 'Texas';
            objListing.Listing_State__c = 'TX';
            objListing.Listing_Country__c = 'USA';
            objListing.Lead__c = objLead.Id;
            objListing.Listing_Street_Number__c = '12345';
            objListing.Listing_PO_Box__c = '123456';
            objListing.Changes_Authorized_By__c = 'John Doe';
            objListing.Customer_Advised_Charges_May_Apply__c = false;
            insert objListing;
            
            objListing.Listing_Street__c = 'Test st1234';
            objListing.Listing_City__c = 'Texas';
            objListing.Listing_State__c = 'CA';
            objListing.Listing_Country__c = 'USA';
            objListing.Changes_Authorized_By__c = 'Jane Doe';
            objListing.Customer_Advised_Charges_May_Apply__c = true;
            
            update objListing;
            
            
        ApexPages.StandardController sc = new ApexPages.standardController(new Lead());    
        ApexPages.currentPage().getParameters().put('Id',objLead.Id);
        
        MassUpdateLeadListings2 mupLdLst = new MassUpdateLeadListings2(sc);
        MassUpdateLeadListings2.WrapData wrap = new MassUpdateLeadListings2.WrapData();
        wrap.ischecked = true;
        wrap.objL = objListing;
        List<MassUpdateLeadListings2.WrapData> lstW = new List<MassUpdateLeadListings2.WrapData>();
        lstW.add(wrap);
        
        mupLdLst.listingActLst = new List<MassUpdateLeadListings2.WrapData>();
        mupLdLst.listingActLst.addall(lstW);
        mupLdLst.listing = new Listing__c();
        mupLdLst.listing = objListing;
        mupLdLst.listingsData();
        try{
            mupLdLst.SaveSelected();
        }
        catch(Exception e){}
        mupLdLst.redirect();
        
        Test.stopTest();
    }
}