public class NationalBillingReportMaint {

    public Map<String,String> mpReportDevNameId{get;set;}
    public Id ISSToCMRReportId {get;set;}
    public Id NSLIsReportId {get;set;}
    public Id TaxRateReportId {get;set;}
    public Id DirStatusReportId {get;set;}
    public Id NatBillAuditReportId {get;set;}
    public Id NatInvReprintReportId {get;set;}
    public Id InvNotPagedReportId {get;set;}
    public Id PageNumValReportId {get;set;}
    public Id DirsToBillReportId {get;set;}
    public Id CommAndTaxReportId {get;set;}
    public Id PrintLOBReportId {get;set;}
    public Id TaxClientReportId {get;set;}
    public Id ISSRePrintReportId {get;set;}

    public NationalBillingReportMaint() {        
        String strSfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
        mpReportDevNameId = new Map<String,String>();
        Schema.DescribeSObjectResult r = Directory__c.sObjectType.getDescribe();
        mpReportDevNameId.put('DirectoryObjURL', strSfdcBaseURL+'/'+r.getKeyPrefix()+'/o');
        r = National_Tax__c.sObjectType.getDescribe();
        mpReportDevNameId.put('NationalTax', strSfdcBaseURL+'/'+r.getKeyPrefix()+'/o');
        
        ISSToCMRReportId = National_Billing_Reports__c.getInstance('ISSCMRReport').Report_Id__c; 
        NSLIsReportId = National_Billing_Reports__c.getInstance('NSLIsReport').Report_Id__c; 
        TaxRateReportId = National_Billing_Reports__c.getInstance('TaxRateReport').Report_Id__c; 
        DirStatusReportId = National_Billing_Reports__c.getInstance('DirStatusReport').Report_Id__c; 
        NatBillAuditReportId = National_Billing_Reports__c.getInstance('NatBillAuditReport').Report_Id__c; 
        ISSRePrintReportId = National_Billing_Reports__c.getInstance('ISSRePrintReport').Report_Id__c; 
        NatInvReprintReportId = National_Billing_Reports__c.getInstance('NatInvReprintReport').Report_Id__c; 
        InvNotPagedReportId = National_Billing_Reports__c.getInstance('InvNotPagedReport').Report_Id__c; 
        PageNumValReportId = National_Billing_Reports__c.getInstance('PageNumValReport').Report_Id__c; 
        DirsToBillReportId = National_Billing_Reports__c.getInstance('DirsToBillReport').Report_Id__c; 
        CommAndTaxReportId = National_Billing_Reports__c.getInstance('CommAndTaxReport').Report_Id__c; 
        PrintLOBReportId = National_Billing_Reports__c.getInstance('PrintLOBReport').Report_Id__c; 
        TaxClientReportId = National_Billing_Reports__c.getInstance('TaxClientReport').Report_Id__c;
    }
}