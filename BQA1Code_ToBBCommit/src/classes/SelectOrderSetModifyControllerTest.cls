@isTest
private class SelectOrderSetModifyControllerTest{
    
    static testMethod void test_displayOrderLineItems(){
       list<Account> lstAccount = new list<Account>();
lstAccount.add(TestMethodsUtility.generateAccount('telco'));
lstAccount.add(TestMethodsUtility.generateAccount('customer'));
lstAccount.add(TestMethodsUtility.generateAccount('publication'));
insert lstAccount;  
Account newAccount = new Account();
Account newPubAccount = new Account();
Account newTelcoAccount = new Account();
for(Account iterator : lstAccount) {
if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
newAccount = iterator;
}
else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
newPubAccount = iterator;
}
else {
newTelcoAccount = iterator;
}
}
system.assertNotEquals(newAccount.ID, null);
system.assertNotEquals(newPubAccount.ID, null);
system.assertNotEquals(newAccount.Primary_Canvass__c, null);
system.assertNotEquals(newTelcoAccount.ID, null);
Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.id);
objTelco.Telco_Code__c = 'Test';
update objTelco;
system.assertNotEquals(newTelcoAccount.ID, null);
Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
Division__c objDiv = TestMethodsUtility.createDivision();
/*Directory__c objDir = TestMethodsUtility.generateDirectory();
objDir.Telco_Provider__c = objTelco.Id;
objDir.Canvass__c = newAccount.Primary_Canvass__c;        
objDir.Publication_Company__c = newPubAccount.Id;
objDir.Division__c = objDiv.Id;
insert objDir;*/
Directory__c objDir =TestMethodsUtility.createDirectory();
Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
insert objSHM;

Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
objDirEd.Book_Status__c='BOTS';
objDirEd.Ship_Date__c=System.Today();
objDirEd.Pub_Date__c=system.today();
insert objDirEd;
system.debug('*****Directory Edition******'+objDirEd);
Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir);
objDirEd1.Book_Status__c='BOTS-1';
objDirEd1.Pub_Date__c =Date.parse('05/05/2015');
insert objDirEd1;
//Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir);
//insert objDirEd1;

Directory_Mapping__c objDM = TestMethodsUtility.generateDirectoryMapping(null);
objDM.Telco__c = objDir.Telco_Provider__c;
objDM.Canvass__c = objDir.Canvass__c;
objDM.Directory__c = objDir.Id;
insert objDM;

system.assertNotEquals(objDir.ID, null);
list<Product2> lstProduct = new list<Product2>();      
for(Integer x=0; x<3;x++){
Product2 newProduct = TestMethodsUtility.generateproduct();
newProduct.Product_Type__c = CommonMessages.oliPrintProductType;
newProduct.Inventory_Tracking_Group__c = 'YP Leader Ad';
lstProduct.add(newProduct);
}
insert lstProduct;

list<Directory_Product_Mapping__c> lstDPM = new list<Directory_Product_Mapping__c>(); 
list<PricebookEntry> lstPBE = new list<PricebookEntry>();
        for(Product2 iterator : lstProduct) {
        
            PricebookEntry pbe = new PricebookEntry(UnitPrice = 0, Product2Id = iterator.ID, Pricebook2Id = newPriceBook.id, IsActive = iterator.IsActive);
            lstPBE.add(pbe);
        }
        insert lstPBE;       
//list<PricebookEntry> lstPBE = [Select Id, Product2Id from PricebookEntry where Product2Id IN:lstProduct];
for(Product2 iterator : lstProduct) {
Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(objDir);
objDPM.Product2__c = iterator.Id;
}
insert lstDPM;

Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
newOpportunity.AccountId = newAccount.Id;
newOpportunity.Pricebook2Id = newPriceBook.Id;
newOpportunity.Billing_Contact__c = newContact.Id;
newOpportunity.Signing_Contact__c = newContact.Id;
newOpportunity.Billing_Partner__c = objTelco.Telco_Code__c;
newOpportunity.Payment_Method__c = CommonMessages.telcoPaymentMethod;
newOpportunity.Account_Primary_Canvass__c=newAccount.Primary_Canvass__c;     
insert newOpportunity;

list<OpportunityLineItem> lstOLI = new list<OpportunityLineItem>();
map<Id, PricebookEntry> mapPBE = new map<Id, PricebookEntry>();
for(PricebookEntry iterator : lstPBE) {
OpportunityLineItem objOLI = TestMethodsUtility.generateOpportunityLineItem();
objOLI.PricebookEntryId = iterator.Id;
objOLI.Billing_Duration__c = 12;
objOLI.Directory__c = objDir.Id;
objOLI.Directory_Edition__c = objDirEd.Id;
objOLI.Full_Rate__c = 30.00;
objOLI.UnitPrice = 30.00;
objOLI.Package_ID__c = '123456';
objOLI.Billing_Partner__c = objOLI.Id;
objOLI.OpportunityId = newOpportunity.Id;
objOLI.Directory_Heading__c = objDH.Id;
objOLI.Directory_Section__c = objDS.Id;
lstOLI.add(objOLI);
mapPBE.put(iterator.Id, iterator);
}
insert lstOLI;
system.debug('****OpportunityProducts****'+lstOLI);

Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
system.debug('*****Order set****'+newOrderSet);
list<Order_Line_Items__c> lstOrderLI = new list<Order_Line_Items__c>();
Integer i=0;
for(OpportunityLineItem iterator : lstOLI) {
    i++;
Order_Line_Items__c objOLI = TestMethodsUtility.generateOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet, iterator);
objOLI.Package_ID__c = iterator.Package_ID__c;
objOLI.Package_Item_Quantity__c = 2;
objOLI.Directory__c = iterator.Directory__c;
objOLI.Directory_Edition__c = iterator.Directory_Edition__c;
objOLI.UnitPrice__c = iterator.UnitPrice;
objOLI.Opportunity_line_Item_id__c = iterator.Id;
objOLI.Media_Type__c = CommonMessages.oliPrintProductType;   
objOLI.Payment_Method__c = CommonMessages.telcoPaymentMethod;
if(i==1){
    objOLI.Media_Type__c = 'Digital';
    objOLI.isCanceled__c = false;
    objOLI.Parent_ID__c='1233-3434';
    objOLI.Parent_ID_of_Addon__c='8785-3454';
    objOLI.Package_ID_External__c='34346';  
}else if(i==2){
    objOLI.Parent_ID_of_Addon__c='4567-3454';
    objOLI.Directory_Edition__c = objDirEd1.Id;
}

objOLI.Directory_Heading__c = iterator.Directory_Heading__c;
objOLI.Directory_Section__c = iterator.Directory_Section__c;
objOLI.Product2__c = mapPBE.get(iterator.PricebookEntryId).Product2Id;
objOLI.Product_Inventory_Tracking_Group__c = 'YP Leader Ad';
lstOrderLI.add(objOLI);
}
system.debug('***OLI List****'+lstOrderLI);

Test.startTest();
insert lstOrderLI;
Order_Line_Items__c firstOLI = lstOrderLI[0];
firstOLI.Talus_Go_Live_Date__c = system.today();
firstOLI.Media_Type__c='Digital';
//firstOLI.Directory_Edition__c = objDirEd1.Id;
//update firstOLI; //did now
Order_Line_Items__c secondOLI = lstOrderLI[1];
secondOLI.Talus_Go_Live_Date__c = system.today();
update secondOLI;

firstOLI.Talus_Cancel_Date__c = system.today();
firstOLI.Status__c = 'Cancelation Requested';
//update firstOLI; //did now
 Apexpages.Standardcontroller ctrl = new Apexpages.Standardcontroller(newAccount );
        SelectOrderSetModifyController_V3 selCtrl = new SelectOrderSetModifyController_V3(ctrl);
        selCtrl.check=true;
        selCtrl.CheckBool=true;
        selCtrl.canvassId=newAccount.Primary_Canvass__c;
        selCtrl.OLIIdforcheck=lstOrderLI[0].Id;  
       selCtrl.displayOrderLineItems();
         selCtrl.digitalOLIList=new List<SelectOrderSetModifyController_V3.DigitalDisplayWrapper>();
        SelectOrderSetModifyController_V3.DigitalDisplayWrapper digital=new SelectOrderSetModifyController_V3.DigitalDisplayWrapper();
        digital.isSelected=true;
        digital.digitalOLI=firstOLI;
        selCtrl.digitalOLIList.add(digital);
        selCtrl.saveOppandOLIs();
        selCtrl.checkSimilarOlis();
        selCtrl.CheckBundle();
      SelectOrderSetModifyController_V3 selCtrl1 = new SelectOrderSetModifyController_V3(ctrl);
        selCtrl1.check=false;
        selCtrl1.CheckBool=false;
        selCtrl1.canvassId=newAccount.Primary_Canvass__c;
        selCtrl1.OLIIdforcheck=lstOrderLI[1].Id;     
        
         selCtrl1.digitalOLIList=new List<SelectOrderSetModifyController_V3.DigitalDisplayWrapper>();
        SelectOrderSetModifyController_V3.DigitalDisplayWrapper digital1=new SelectOrderSetModifyController_V3.DigitalDisplayWrapper();
        digital1.isSelected=true;
        digital1.digitalOLI=secondOLI;
        selCtrl1.digitalOLIList.add(digital1);
        selCtrl1.digitalOLIList.add(digital);
        selCtrl1.printDisplayList=new List<SelectOrderSetModifyController_V3.OrderSetWrapper>();
        SelectOrderSetModifyController_V3.OrderSetWrapper os=new SelectOrderSetModifyController_V3.OrderSetWrapper();
        os.orderSet=newOrderSet;
        os.oliList=new List<SelectOrderSetModifyController_V3.DigitalDisplayWrapper>();
        os.oliList.add(digital1);
        os.oliList.add(digital);
        selCtrl1.printDisplayList.add(os);
        selCtrl1.checkSimilarOlis();
        selCtrl1.CheckBundle();
        selCtrl1.PendingMOLIPage();
        selCtrl1.populateDigitalDisplayList(firstOLI);
        selCtrl1.saveOppandOLIs();
Test.stopTest();
    }
}