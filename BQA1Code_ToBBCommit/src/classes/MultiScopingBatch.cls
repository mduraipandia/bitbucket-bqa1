global class MultiScopingBatch Implements Database.Batchable <sObject>, Database.Stateful {
    public integer batchcount = 0 ;
    public String dirId = ''; 
    
    global MultiScopingBatch(String dirId) {
    	this.dirId = dirId;
    }
    
    global Database.queryLocator start(Database.BatchableContext bc) {
    	String SOQL;
    	if(String.isBlank(dirId)) {
        	SOQL = 'SELECT Id, MSH_Area_Code__c, MSH_Directory__c, MSH_Directory_Section__c, MSH_Exchange_Code__c, MSH_Bus_Res_Gov_Indicator__c, '
        				+ '(SELECT ID, MSC_Multi_Scoping_Header__c, MSC_Area_Code__c, MSC_Directory__c, MSC_Directory_Section__c, MSC_Exchange_Code__c FROM ' 
        				+ 'Multi_Scoping_Children__r) FROM Multi_Scoping_Header__c';
    	} else {
    		SOQL = 'SELECT Id, MSH_Area_Code__c, MSH_Directory__c, MSH_Directory_Section__c, MSH_Exchange_Code__c, MSH_Bus_Res_Gov_Indicator__c, '
        				+ '(SELECT ID, MSC_Multi_Scoping_Header__c, MSC_Area_Code__c, MSC_Directory__c, MSC_Directory_Section__c, MSC_Exchange_Code__c FROM ' 
        				+ 'Multi_Scoping_Children__r) FROM Multi_Scoping_Header__c WHERE MSH_Directory__c = \'' + dirId + '\'';
    	}
        return Database.getQueryLocator(SOQL);
    }

    global void execute(Database.BatchableContext bc, List<Multi_Scoping_Header__c> listMSH) {
    	Set<String> setAreaCodes = new Set<String>();
    	Set<String> setBusGovIndcs = new Set<String>();
    	Set<String> setExchangeCodes = new Set<String>();
    	Set<Id> setDirIds = new Set<Id>();
    	Set<Id> setDirSecIds = new Set<Id>();
    	Map<String, List<Multi_Scoping_Child__c>> mapComboListChild = new Map<String, List<Multi_Scoping_Child__c>>(); 
    	List<Directory_Listing__c> listScopedListing = new List<Directory_Listing__c>();
    	List<Directory_Listing__c> listNonMultiParentSL = new List<Directory_Listing__c>();
    	List<Directory_Listing__c> listNonMultiChildSL = new List<Directory_Listing__c>();
    	List<Directory_Listing__c> listInsertParentSL = new List<Directory_Listing__c>();
    	List<Directory_Listing__c> listInsertChildSL = new List<Directory_Listing__c>();
    	Map<String, Directory_Listing__c> mapAlreadyMultiScoped = new Map<String, Directory_Listing__c>();
    	Map<Id, Id> mapOldUnderCaptionNewSLId = new Map<Id, Id>();
    	    	
    	for(Multi_Scoping_Header__c MSH : listMSH) {
    		if(String.isNotBlank(MSH.MSH_Area_Code__c)) {
    			setAreaCodes.add(MSH.MSH_Area_Code__c);
    		}
    		if(String.isNotBlank(MSH.MSH_Bus_Res_Gov_Indicator__c)) {
    			setBusGovIndcs.addAll(MSH.MSH_Bus_Res_Gov_Indicator__c.split(';'));
    		}
    		if(String.isNotBlank(MSH.MSH_Exchange_Code__c)) {
    			setExchangeCodes.add(MSH.MSH_Exchange_Code__c);
    		}
    		if(String.isNotBlank(MSH.MSH_Directory__c)) {
    			setDirIds.add(MSH.MSH_Directory__c);
    		}
    		if(String.isNotBlank(MSH.MSH_Directory_Section__c)) {
    			setDirSecIds.add(MSH.MSH_Directory_Section__c);
    		}
    		if(MSH.Multi_Scoping_Children__r != null) {
	    		if(String.isNotBlank(MSH.MSH_Area_Code__c) && String.isNotBlank(MSH.MSH_Exchange_Code__c) && String.isNotBlank(MSH.MSH_Directory__c) &&
	    			String.isNotBlank(MSH.MSH_Directory_Section__c)) {
		    		mapComboListChild.put(MSH.MSH_Area_Code__c + MSH.MSH_Exchange_Code__c + MSH.MSH_Directory__c + MSH.MSH_Directory_Section__c, 
		    							MSH.Multi_Scoping_Children__r);
	    		}
	    		for(Multi_Scoping_Child__c MSC : MSH.Multi_Scoping_Children__r) {
		    		if(String.isNotBlank(MSC.MSC_Area_Code__c)) {
		    			setAreaCodes.add(MSC.MSC_Area_Code__c);
		    		}
		    		if(String.isNotBlank(MSC.MSC_Exchange_Code__c)) {
		    			setExchangeCodes.add(MSC.MSC_Exchange_Code__c);
		    		}
		    		if(String.isNotBlank(MSC.MSC_Directory__c)) {
		    			setDirIds.add(MSC.MSC_Directory__c);
		    		}
		    		if(String.isNotBlank(MSC.MSC_Directory_Section__c)) {
		    			setDirSecIds.add(MSC.MSC_Directory_Section__c);
		    		}	    			
	    		}
    		}
    	}
    	
    	listScopedListing = DirectoryListingSOQLMethods.getScopedListingsByAreaExchDirDirSec(setAreaCodes, setExchangeCodes, setDirIds, setDirSecIds, 
    																						setBusGovIndcs);
    	
    	if(listScopedListing.size() > 0) {
    		for(Directory_Listing__c SL : listScopedListing) {
	    		if(SL.SL_Is_Multi_Scoped__c) {
	    			mapAlreadyMultiScoped.put(SL.Area_Code__c + SL.Exchange__c + SL.Directory__c + SL.Directory_Section__c + SL.Phone_Number__c + SL.Listing__c, SL);
	    			if(SL.Under_Caption__c == null) {
		    			mapOldUnderCaptionNewSLId.put(SL.SL_Original_Scoped_Listing_Id__c, SL.Id);
		    		}
	    		} else if(SL.Under_Caption__c == null) {
    				listNonMultiParentSL.add(SL);
	    		} else {
	    			listNonMultiChildSL.add(SL);
	    		}
    		}
    		
    		if(listNonMultiParentSL.size() > 0) {
		    	for(Directory_Listing__c SL : listNonMultiParentSL) {
		    		if(mapComboListChild.containsKey(SL.Area_Code__c + SL.Exchange__c + SL.Directory__c + SL.Directory_Section__c)) {
		    			for(Multi_Scoping_Child__c MSC : mapComboListChild.get(SL.Area_Code__c + SL.Exchange__c + SL.Directory__c + SL.Directory_Section__c)) {
		    				if(!mapAlreadyMultiScoped.containsKey(MSC.MSC_Area_Code__c + MSC.MSC_Exchange_Code__c + MSC.MSC_Directory__c + MSC.MSC_Directory_Section__c + SL.Phone_Number__c + SL.Listing__c)) {
			    				Directory_Listing__c tempSL = SL.clone();
			    				tempSL.Name = SL.Name;
			    				tempSL.Directory__c = MSC.MSC_Directory__c;
			    				tempSL.Directory_Section__c = MSC.MSC_Directory_Section__c;
			    				tempSL.SL_Is_Multi_Scoped__c = true;
			    				tempSL.Suppressed__c = false;
			    				tempSL.Suppressed_By_OLI__c = null;
			    				tempSL.SL_Original_Scoped_Listing_Id__c = SL.Id;
			    				listInsertParentSL.add(tempSL);
		    				}
		    			}
		    		}
		    	}
    		}
    		 
    		if(listInsertParentSL.size() > 0) {
    			insert listInsertParentSL;
    			for(Directory_Listing__c SL : listInsertParentSL) {
    				mapOldUnderCaptionNewSLId.put(SL.SL_Original_Scoped_Listing_Id__c, SL.Id);
    			}
    		}
    		
    		if(listNonMultiChildSL.size() > 0) {
		    	for(Directory_Listing__c SL : listNonMultiChildSL) {
		    		if(mapComboListChild.containsKey(SL.Area_Code__c + SL.Exchange__c + SL.Directory__c + SL.Directory_Section__c)) {
		    			for(Multi_Scoping_Child__c MSC : mapComboListChild.get(SL.Area_Code__c + SL.Exchange__c + SL.Directory__c + SL.Directory_Section__c)) {
		    				if(!mapAlreadyMultiScoped.containsKey(MSC.MSC_Area_Code__c + MSC.MSC_Exchange_Code__c + MSC.MSC_Directory__c + MSC.MSC_Directory_Section__c + SL.Phone_Number__c + SL.Listing__c)) {
			    				Directory_Listing__c tempSL = SL.clone();
			    				tempSL.Name = SL.Name;
			    				tempSL.Directory__c = MSC.MSC_Directory__c;
			    				tempSL.Directory_Section__c = MSC.MSC_Directory_Section__c;
			    				tempSL.SL_Is_Multi_Scoped__c = true;
			    				tempSL.Suppressed__c = false;
			    				tempSL.Suppressed_By_OLI__c = null;
			    				tempSL.Under_Caption__c = mapOldUnderCaptionNewSLId.get(SL.Under_Caption__c);
			    				listInsertChildSL.add(tempSL);
		    				}
		    			}
		    		}
		    	}
    		}
	    	
	    	if(listInsertChildSL.size() > 0) {
	    		if(!Test.isRunningTest()) { 
    				insert listInsertChildSL;
	    		}
    		}
    	}
    	batchcount += listMSH.size();
    }

    global void finish(Database.BatchableContext bc) {
    	String strErrorMessage = '';
        AsyncApexJob a = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
        String[] toAddresses = new String[] {a.CreatedBy.Email};
        
        if(a.NumberOfErrors > 0){
            strErrorMessage = a.ExtendedStatus;
        }
        
        String body = 'The batch job processed ' + a.TotalJobItems + ' batch(es).<br/>Total number of records processed is ' + batchcount + 
                      ' with '+ a.NumberOfErrors + ' failures.<br/> ' + strErrorMessage;  
        
        CommonEmailUtils.sendHTMLEmailForTargetObject(a.CreatedById, 'Multi Scoping Batch Processing Status: ' + a.Status, body);
    }
}