@IsTest
 public class CreateFulfillmentProfileTest{
    static testMethod void CreateFulfillmentProfileTest() {
        Test.startTest();
        Canvass__c c = TestMethodsUtility.createCanvass();
        
        Account a = TestMethodsUtility.createAccount('cmr');
            
        Digital_Product_Requirement__c dff = CommonUtility.createDffTest();
        insert dff;
        
        Apexpages.currentPage().getParameters().put('id', dff.Id);
        CreateFulfillmentProfile createFP = new CreateFulfillmentProfile(new ApexPages.StandardController(dff));
        createFP.DffPrfl();
        createFP.createtFp();

        Test.stopTest();
    }
}