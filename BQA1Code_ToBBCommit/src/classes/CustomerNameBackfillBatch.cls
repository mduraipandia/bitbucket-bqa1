public class CustomerNameBackfillBatch implements Database.Batchable<SObject>, Database.Stateful
{
    public CustomerNameBackfillBatch()
    {
        
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC) 
    {
        String recordType = 'Telco Partner';
        Set<String> transTypes = new Set<String>{'Invoice','Credit Note'};
        String query = 'Select '+
                            'Id, '+
                            'Customer_Name__c, '+
                            'c2g__DocumentNumber__c, '+
                            'c2g__TransactionType__c, '+
                            'Name '+
                        'From '+
                            'c2g__codaTransaction__c '+
                        'Where '+
                            'Customer_Name__c = null '+
                        'And '+
                            'c2g__Account__r.RecordType.Name = :recordType '+
                        'And '+
                            'c2g__TransactionType__c IN :transTypes';
        return Database.getQueryLocator(query);
    }

    public void execute( Database.BatchableContext BC, List<SObject> unTypedScope)
    {
        Map<String, c2g__codaTransaction__c> transactionToInvoice = new Map<String, c2g__codaTransaction__c>();
        Map<String, c2g__codaTransaction__c> transactionToCreditNote = new Map<String, c2g__codaTransaction__c>();
        
        for(c2g__codaTransaction__c trans : (List<c2g__codaTransaction__c>)unTypedScope)
        {
            if(trans.c2g__TransactionType__c == 'Invoice')
            {
                transactionToInvoice.put(trans.c2g__DocumentNumber__c, trans);
            }
            else if(trans.c2g__TransactionType__c == 'Credit Note')
            {
                transactionToCreditNote.put(trans.c2g__DocumentNumber__c, trans);
            }
        }
        List<c2g__codaTransaction__c> transactionsToUpdate = new List<c2g__codaTransaction__c>();
        
        for(c2g__codaInvoice__c inv : [Select Id, Name, Customer_Name__c From c2g__codaInvoice__c Where Name IN :transactionToInvoice.KeySet()])
        {
            if(transactionToInvoice.containsKey(inv.Name))
            {
                transactionToInvoice.get(inv.Name).Customer_Name__c = inv.Customer_Name__c;
                transactionsToUpdate.add(transactionToInvoice.get(inv.Name));
            }
        }
        
        for(c2g__codaCreditNote__c creditNote : [Select Id, Name, Customer_Name__c From c2g__codaCreditNote__c Where Name IN :transactionToCreditNote.KeySet()])
        {
            if(transactionToCreditNote.containsKey(creditNote.Name))
            {
                transactionToCreditNote.get(creditNote.Name).Customer_Name__c = creditNote.Customer_Name__c;
                transactionsToUpdate.add(transactionToCreditNote.get(creditNote.Name));
            }
        }
        
        update transactionsToUpdate;
    }
    
    public void finish( Database.BatchableContext BC )
    {
        
    }
}