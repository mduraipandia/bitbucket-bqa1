public with sharing class CommunitySectionAbbreviationSOQLMethods { 
    public static List<Community_Section_Abbreviation__c> fetchCommSecAbb(set<Id> setSectionId) {
        return [SELECT Id, Name, Community__c, Community_Abbreviation__c, Community_Name__c, Directory_Section__c,
        State__c, Suppress_Abbreviation_Rule__c FROM Community_Section_Abbreviation__c where Directory_Section__c IN:setSectionId];
    }
    
    public static List<Community_Section_Abbreviation__c> fetchCommSecAbb() {
        return [SELECT Id, Name, Community__c, Community_Abbreviation__c, Community_Name__c, Directory_Section__c,
        State__c, Suppress_Abbreviation_Rule__c FROM Community_Section_Abbreviation__c];
    }
}