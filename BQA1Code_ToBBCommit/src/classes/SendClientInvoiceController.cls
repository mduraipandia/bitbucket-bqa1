public class SendClientInvoiceController {
    public Decimal natInvTotForInvSend {get;set;}
    public Decimal OLITotalForInvSend {get;set;}
    public Decimal totalTaxForInvSend {get;set;}
    public Decimal totalNetForInvSend {get;set;}
    public String pubCode{get;set;}
    public String editionCode{get;set;}
    public Id dirId {get;set;}
    public List<SelectOption> dirList {get;set;}
    public Boolean displayBool {get;set;}
    public Boolean balancedBool {get;set;}
    public Boolean natInvCompleteBool {get;set;}
    String selectedDirCode;
    Map<Id, String> mapDirIdDirCode = new Map<Id, String>();
    public National_Billing_Status__c natBilling {get;set;}
    List<National_Billing_Status__c> listNatBill = new List<National_Billing_Status__c>();
    public list<NationalInvoice__c> listNatInvoices {get;set;}
    public boolean displayPopup {get; set;}
    public String strDirName {get;set;}
    map<Id, String> mapDirIdName = new map<id,string>();
    map<String, Id> mapDirNameId = new map<String, Id>();
    public ID SACRACreportId {get;set;} 
    public Decimal totalInvTaxAmount {get;set;}
    public Decimal totalInvAmount {get;set;}
    public Decimal totalInvNetAmount {get;set;} 
    public Decimal totalInvGrossAmount {get;set;} 
    public Boolean sendToElitBool {get;set;}
    
    public SendClientInvoiceController() {
        natBilling = new National_Billing_Status__c();
        dirList = new List<SelectOption>();
        displayBool = false;
        showNatInv();
        displayPopup = false;
        listNatInvoices = new list<NationalInvoice__c>();
        SACRACreportId = National_Billing_Reports__c.getInstance('SACRACTransLog').Report_Id__c;
        sendToElitBool = false;
    }
    
    public void fetchDirs() {
        if(String.isNotBlank(pubCode) && String.isNotBlank(editionCode)) {
            dirList = new List<SelectOption>();
            list<Directory_Edition__c> dirEditionList = new list<Directory_Edition__c>(); 
            dirEditionList = DirectoryEditionSOQLMethods.getDirEdByDirPubCodeAndEdCode(pubCode, editionCode);     
            system.debug('Directory Edition list is ' + dirEditionList);             
            if(dirEditionList.size() > 0) {             
                mapDirNameId = new map<String, Id>();
                mapDirIdName = new map<Id, String>();
                for(Directory_Edition__c DE : dirEditionList){
                    mapDirNameId.put(DE.Directory__r.Name, DE.Directory__c);
                    mapDirIdDirCode.put(DE.Directory__c, DE.Directory__r.Directory_Code__c);
                    mapDirIdName.put(DE.Directory__c, DE.Directory__r.Name);
                }
                List<String> dirNames = new List<String>(mapDirNameId.keySet());
                dirNames.sort();
                for(String str : dirNames) {
                    dirList.add(new SelectOption(mapDirNameId.get(str), str));
                }
            }
        }
        displayBool = false;
    }
    
    public void calculateTotalForSendInv() {
        natBilling = new National_Billing_Status__c();
        displayBool = false;
        showNatInv();
        displayPopup = false;
        listNatInvoices = new list<NationalInvoice__c>();
        selectedDirCode = mapDirIdDirCode.get(dirId);  
        strDirName = mapDirIdName.get(dirId);                  
        fetchNationalBilling();
        if(listNatBill.size() == 0) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'No record found in Status Table'));
            displayBool = false;
        } else { 
            displayBool = true;
            listNatInvoices = NationalInvoiceSOQLMethods.getNatInvByPubCodeEdtncodeDirId(pubCode, editionCode, dirId);
            natInvTotForInvSend = 0.00;
            OLITotalForInvSend = 0.00;
            totalTaxForInvSend = 0.00;
            totalNetForInvSend = 0.00;
            if(listNatInvoices.size() > 0){
                //totalNatCommForInvSend = 0.00;
                for(NationalInvoice__c NI : listNatInvoices){
                    totalTaxForInvSend += NI.Tax_Amount_New__c;
                    natInvTotForInvSend += NI.Total_Amount__c;
                    totalNetForInvSend += NI.Net_Amount__c;
                    if(NI.OrderSet__r.OS_Total_OLI_Value__c != null) {
                        OLITotalForInvSend += NI.OrderSet__r.OS_Total_OLI_Value__c;
                    }
                }
            }
            assignValuesFromNatBill();
        }
    }    
    
    public void updateStatusTable() {
        natBilling.SCI_CLIENT_INVOICES_BALANCED__c = balancedBool;
        natBilling.SCI_National_Invoice_Complete__c = natInvCompleteBool; 
        
        update natBilling;
    }
    
    public void fetchNationalBilling() {
        listNatBill = NationalBillingStatusSOQLMethods.getNatBillByPubEditionDirCode(pubCode, editionCode, selectedDirCode);
        system.debug('National Billing status size is ' + listNatBill.size());
        if(listNatBill.size() > 0) {
            natBilling = listNatBill.get(0);
        }        
    }
    
    public void assignValuesFromNatBill() {
        balancedBool = natBilling.SCI_CLIENT_INVOICES_BALANCED__c;
        natInvCompleteBool = natBilling.SCI_National_Invoice_Complete__c;
        system.debug('Transmission Date is ' + natBilling.Client_Invoice_Transmission_Date__c);
        if(natBilling.Client_Invoice_Transmission_Date__c != null || natBilling.SCI_INITIATE_INFORMATICA__c) {
        	sendToElitBool = true;
        } else {
        	sendToElitBool = false;
        }
    }
    
    public void closePopup() {
        displayPopup = false;     
    }
    
    public void showNatInv() {
        displayPopup = true;
        listNatInvoices = NationalInvoiceSOQLMethods.getNatInvByPubCodeEdtncodeDirId(pubCode, editionCode, dirId); 
        calcualteTotalForInv();
    }
    
    public void sendToElite() {
        natBilling.SCI_INITIATE_INFORMATICA__c = true;
        update natBilling;
        sendToElitBool = true;
    }                    
    
    public void calcualteTotalForInv() {
        totalInvTaxAmount = 0;
        totalInvAmount = 0;
        totalInvNetAmount = 0;
        totalInvGrossAmount = 0;
        
        if(listNatInvoices.size() > 0) {
            for(NationalInvoice__c NI : listNatInvoices) {
                if(NI.Tax_Amount_New__c != null) {
                    totalInvTaxAmount += NI.Tax_Amount_New__c;
                }
                
                if(NI.Total_Amount__c != null) {
                    totalInvAmount += NI.Total_Amount__c;
                }
                
                if(NI.Net_Amount__c != null) {
                    totalInvNetAmount += NI.Net_Amount__c;
                }
                
                if(NI.TotalGrossAmount__c != null) {
                    totalInvGrossAmount += NI.TotalGrossAmount__c;
                }
            }
        }
    }
}