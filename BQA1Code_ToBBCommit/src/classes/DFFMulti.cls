public class DFFMulti {
    private string Id;
    private decimal URN;
    public String DFFParentProof;
    public String DFFPaperEmail;
    public string DFFProofContact;
    public string DFFProofContactEmailAddress;
    public string DFFProofContactMailingAddress;
    public string DFFProofContactTitle;
    
    public list<Digital_Product_Requirement__c> dffcompleteLst {get;set;}
    public list<Digital_Product_Requirement__c> dffLst {get;set;}    
    public map<Decimal, list<Digital_Product_Requirement__c>> mapURNDFF = new map<Decimal, list<Digital_Product_Requirement__c>>();
    public DFFMulti(ApexPages.StandardController controller) {
        Id = Apexpages.currentPage().getParameters().get('Id');
        dffcompleteLst = new list<Digital_Product_Requirement__c>();
        dffLst = new list<Digital_Product_Requirement__c>();
        DFFMultiOli();
    }
    
    private void DFFMultiOli() {
        system.debug('***********DFF ID*******'+Id);
        dffLst = [Select Id,Name,Account__c,OrderLineItemID__c,OrderLineItemID__r.Name,OrderLineItemID__r.Canvass__r.Name,ProductID__c,UDAC__c,URN_number__c,DFF_Proof_Contact_Email_Address__c,Previous_URN_Number__c,Paper_or_Email__c,Proof_Contact_Title__c,Heading__c,Directory_Edition__c,Directory_Section_Columns__c,Directory_Edition__r.Name,Directory_Name__c,OrderLineItemID__r.Directory_Section__c,OrderLineItemID__r.Directory_Section__r.Name,CreatedDate,Canvass__c,Proof__c,Proof_Contact__c,DFF_Proof_Contact_Mailing_Address__c from Digital_Product_Requirement__c where Id = : Id limit 1];
        set<Id> actId = new set<Id>();
        set<String> setUdac = new set<String>();
        set<string> setcanvassId = new set<string>();
        String dirSecCols;
        //String CanvassId;
       
        if(dffLst.size()>0) {
            for(Digital_Product_Requirement__c objDFF : dffLst) {
                if(objDFF.Account__c!= null) {
                    actId.add(objDFF.Account__c);
                }
                if(objDFF.UDAC__c != null) {
                    setUdac.add(objDFF.UDAC__c);
                }
                if(objDFF.URN_number__c != null) {
                    URN = objDFF.URN_number__c ;
                }
                if(objDFF.Directory_Section_Columns__c != null){
                    dirSecCols = objDFF.Directory_Section_Columns__c;
                }
                if(objDFF.Canvass__c != null){
                    setcanvassId.add(objDFF.Canvass__c);
                }
                if(objDFF.Proof__c != null){
                    DFFParentProof = objDFF.Proof__c;
                }
                if(objDFF.Paper_or_Email__c != null){
                    DFFPaperEmail = objDFF.Paper_or_Email__c;
                }
                if(objDFF.Proof_Contact__c != null){
                    DFFProofContact = objDFF.Proof_Contact__c;
                }
                if(objDFF.Proof_Contact_Title__c != null){
                    DFFProofContactTitle = objDFF.Proof_Contact_Title__c;
                }
                if(!mapURNDFF.containskey(objDFF.URN_number__c)){
                    mapURNDFF.put(objDFF.URN_number__c, new list<Digital_Product_Requirement__c>());
                }
                mapURNDFF.get(objDFF.URN_number__c).add(objDFF);
            }
        }
        if(actId.size()>0) {
            dffcompleteLst = [Select Id,Name,Account__c,URN_number__c,Previous_URN_Number__c,Is_Multied__c,OrderLineItemID__c,OrderLineItemID__r.Name,OrderLineItemID__r.Product_Type__c,Directory_Section_Columns__c,OrderLineItemID__r.Canvass__r.Name,ProductID__c,UDAC__c,Heading__c,Directory_Edition__c,Directory_Edition__r.Name,Directory_Name__c,OrderLineItemID__r.Directory_Section__c,OrderLineItemID__r.Directory_Section__r.Name,CreatedDate,Canvass__c,sequence__c from Digital_Product_Requirement__c where (Account__c IN :actId and UDAC__c IN:setUdac and Id != :Id and Directory_Section_Columns__c =:dirSecCols and Canvass__c IN:setcanvassId and Is_Multied__c = false) OR (Is_Multied__c = True and URN_number__c =: URN ) ];
        }
    }
    
    public pagereference SaveSelectedDFF() {
        system.debug('************Enter into SAVE***********');
        ID lockRT = CommonMethods.getRedordTypeIdByName(CommonMessages.dffLockRT, CommonMessages.dffObjectName);
        for(Digital_Product_Requirement__c iterator : dffcompleteLst) {
            Integer Count = 1;
            if(iterator.Is_Multied__c) {
                iterator.Previous_URN_Number__c = iterator.URN_number__c;
                iterator.URN_number__c = URN;
                iterator.Production_Notes__c = 'This DFF is a multi of ' + URN ;
                iterator.RecordtypeId = lockRT;
                iterator.Proof__c = DFFParentProof;
                iterator.Paper_or_Email__c = DFFPaperEmail;
                iterator.Proof_Contact__c = DFFProofContact;
                iterator.Proof_Contact_Title__c = DFFProofContactTitle;
                if(!mapURNDFF.containskey(iterator.URN_number__c)){
                    mapURNDFF.put(iterator.URN_number__c, new list<Digital_Product_Requirement__c>());
                }
                mapURNDFF.get(iterator.URN_number__c).add(iterator);
            }
            else {
                //TODO
                //Should we revert back to old RT & Old URn Number
                iterator.URN_number__c = iterator.Previous_URN_Number__c;
                iterator.Previous_URN_Number__c = null;
                iterator.sequence__c = 1;
                if(String.isNotEmpty(iterator.OrderLineItemID__r.Product_Type__c)) {
                    system.debug('*********record type product type************'+iterator.OrderLineItemID__r.Product_Type__c);
                    iterator.RecordtypeId = CommonMethods.getRedordTypeIdByName(iterator.OrderLineItemID__r.Product_Type__c, CommonMessages.dffObjectName);
                }
            }
        }
        
        if(dffcompleteLst.size() > 0) {
            update dffcompleteLst;
        }
        list<Digital_Product_Requirement__c> updateDFFSeqLst = new list<Digital_Product_Requirement__c>();
        if(mapURNDFF.size()> 0){
            for(Decimal urnStr : mapURNDFF.keyset()){
                decimal Count=1;
                if(mapURNDFF.get(urnStr) != null){
                    for(Digital_Product_Requirement__c iteratorDFFSeq : mapURNDFF.get(urnStr)){
                        iteratorDFFSeq.Sequence__c = Count;
                        updateDFFSeqLst.add(iteratorDFFSeq);
                        count++;
                    }
                }
                
            }
        }
        if(updateDFFSeqLst.size()> 0){
            update updateDFFSeqLst;
        }
        return null;
    }    
}