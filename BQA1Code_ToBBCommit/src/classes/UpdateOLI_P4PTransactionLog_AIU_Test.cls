@isTest(SeeAllData = true)
private class UpdateOLI_P4PTransactionLog_AIU_Test {
    static testMethod void myUnitTest() {
        Canvass__c newCanvass = TestMethodsUtility.createCanvass();
        Account newAccount = TestMethodsUtility.generateAccount('customer', false);
        newAccount.Primary_Canvass__c = newCanvass.Id;
        insert newAccount;
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Telco__c newTelco = TestMethodsUtility.createTelco(newAccount.Id);
        Pricebook2 newPriceBook = [Select Id from Pricebook2 where isStandard=true limit 1];
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Family = 'Print';
        insert newProduct;
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        insert newOpportunity;
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        Order_Line_Items__c newOrLI = TestMethodsUtility.generateOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
        newOrLI.Service_Start_Date__c = system.today().addDays(-5);
        newOrLI.Service_End_Date__c = system.today().addDays(5);
        newOrLI.P4P_Billing__c = true;
        newOrLI.P4P_Current_Billing_Clicks_Leads__c = 10;
        newOrLI.P4P_Price_Per_Click_Lead__c = 10.00;
        insert newOrLI;
        P4P_Transaction__c newP4PTransaction = TestMethodsUtility.generateP4PTransaction(newOrLI.Id);
        newP4PTransaction.Date_of_Click_Call_Lead__c = system.today();
        newP4PTransaction.Total_Billable_Calls__c = 10;
        insert newP4PTransaction;    
    }
}