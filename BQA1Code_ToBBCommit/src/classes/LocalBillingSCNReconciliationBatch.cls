global class LocalBillingSCNReconciliationBatch implements Database.Batchable<sObject> {  
    global set<Id> SCNsetIds {get;set;}
    
    global LocalBillingSCNReconciliationBatch(set<Id> SCNsetIds ) {
        this.SCNsetIds = SCNsetIds;
    }
        
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String strQuery = 'SELECT Id, Reconcile__c FROM c2g__codaCreditNote__c WHERE Id In: SCNsetIds';
        return Database.getQueryLocator(strQuery);
    } 
    
     global void execute(Database.BatchableContext bc, List<c2g__codaCreditNote__c > listSI) {
        list<c2g__codaCreditNote__c> lstSCNUpdate = new list<c2g__codaCreditNote__c>();
        for(c2g__codaCreditNote__c iterator: listSI){
        lstSCNUpdate.add(new c2g__codaCreditNote__c (Id = iterator.Id, Reconcile__c = true));
        }
        update lstSCNUpdate;
     }
     
      global void finish(Database.BatchableContext bc) {
      	AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email, ExtendedStatus from AsyncApexJob where Id =:BC.getJobId()];
        /*Map<String, LocalSISCNPostBatchEmails__c> allemails = LocalSISCNPostBatchEmails__c.getAll();
        Set<String> emailaddrSet=new Set<String>();
        emailaddrSet.add(a.CreatedBy.Email);
        String emailaddr='';*/
         Set<String> recipientIds = new Set<String>();
         recipientIds.addAll(User_Ids_For_Email__c.getInstance('SISCNPost').User_Ids__c.split(';'));
         recipientIds.add(UserInfo.getUserId());
        /*for(LocalSISCNPostBatchEmails__c iter : allemails.values()) {
            emailaddrSet.add(iter.email__c);
        }
        List<String> emailaddrLst=new List<String>();
        emailaddrLst.addall(emailaddrSet);*/
        String strErrorMessage = '';
        if(a.NumberOfErrors > 0){
          strErrorMessage = a.ExtendedStatus;
        }
        for(String str : recipientIds) {
            CommonEmailUtils.sendHTMLEmailForTargetObject(str, 'Local Billing Sales Credit Note Reconciliation Batch  is ' + a.Status, 'The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.'+strErrorMessage+'.');
        }
    }
}