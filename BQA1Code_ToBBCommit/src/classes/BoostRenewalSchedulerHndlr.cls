global class BoostRenewalSchedulerHndlr implements BoostRenewalScheduler.BoostRenewalSchedulerInterface{
    global void execute(SchedulableContext sc) {     
    Date TodayDate = Date.today();
    string BookStatus = 'BOTS';
    set<Id> setOpportunityID = new set<Id>();
    String soql = 'Select Id,Book_Status__c,Final_Auto_Renew_Job__c,Directory__c from Directory_Edition__c where Book_Status__c = : BookStatus '+' AND Final_Auto_Renew_Job__c =: '+'TodayDate';
    List<Directory_edition__c> DirEditionLst = database.query(soql);
    set<Id> objDirId = new set<Id>();
    for(Directory_Edition__c objDE : DirEditionLst){
        objDirId.add(objDE.Directory__c);
    }
    map<Id, directory_edition__c> mapDEBOTS = new map<Id, directory_edition__c>([Select Id,Book_Status__c,Directory__c from directory_edition__c where Directory__c IN :objDirId AND Book_Status__c = 'BOTS-1']);
    if(mapDEBOTS.size() > 0) {
        list<Order_Line_Items__c> oliLst = [Select Id,Name,Account__r.Delinquency_Indicator__c,Account__r.OwnerId,Product_Type__c,Product2__r.Print_Product_Type__c,RecordtypeId,UnitPrice__c,Is_P4P__c,Directory__c,Directory_Edition__c,Directory_Edition__r.Name,Directory_Edition__r.Book_Status__c,Directory_Edition__r.Final_Auto_Renew_Job__c,Account__c,Account__r.Name,Account__r.Open_or_Closed_Claim_Past_18_Months__c,Account__r.Boost_Fallout_Flag__c,Product2__r.Name,Is_Handled__c from Order_Line_Items__c where Directory_Edition__c IN :mapDEBOTS.keySet() AND Is_Handled__c = false];
        if(oliLst.size()>0) {
            for(Order_Line_Items__c iterator : oliLst) {
                setOpportunityID.add(iterator.Opportunity__c);
            }
        }
    } 
    BoostRenewalAccountsBatchController BABatch = new BoostRenewalAccountsBatchController(setOpportunityID);
    database.executebatch(BABatch);
   }  
}