global class LOBReportGenerationFromJobBatch Implements Database.Batchable <sObject> {
	Id selectedDirId;
    Id selectedDirEdId; 
    Map<Id, String> mapAcctIdPrimConPhone = new Map<Id, String>();
    Boolean IsDMIGR = false;
    
    global LOBReportGenerationFromJobBatch(Boolean IsDMIGR) {
    	this.IsDMIGR = IsDMIGR;
    }
    
	global LOBReportGenerationFromJobBatch(Id selectedDirId, Id selectedDirEdId) {
        this.selectedDirId = selectedDirId;
        this.selectedDirEdId = selectedDirEdId;
    }
    
    global Database.queryLocator start(Database.BatchableContext bc) {
        String SOQL = 'SELECT Id, LBJ_Order_Ids__c FROM List_Of_Business_Job__c ';
        if(!IsDMIGR) {
        	SOQL += 'WHERE LBJ_Directory_Id__c =: selectedDirId AND LBJ_Directory_Edition_Id__c =: selectedDirEdId';
        }
        return Database.getQueryLocator(SOQL);
    }

    global void execute(Database.BatchableContext bc, List<List_Of_Business_Job__c> listLOBJob) {
        List<List_of_Business__c> listLOB = new List<List_of_Business__c>();
    	List<Order_Line_Items__c> listOLI;
        Map<Id, Map<String, Map<String, Map<String, Map<String, List<Order_Line_Items__c>>>>>> mapAccBPBFPhoneLisNamOLI = new Map<Id, Map<String, Map<String, Map<String, Map<String, List<Order_Line_Items__c>>>>>>();     
        RecordType  objRT = CommonMethods.getRecordTypeDetailsByName('Order_Line_Items__c','National Order Line Item');         
        Set<String> setOrderIds = new Set<String>();
        Set<Id> acctIds = new Set<Id>();
        
    	for(List_Of_Business_Job__c LOBJob : listLOBJob) {
    		setOrderIds.addAll(LOBJob.LBJ_Order_Ids__c.split(';'));
    	}       
    	
    	if(IsDMIGR) {
    		listOLI = [SELECT Listing__r.Phone__c, Account__r.X3l_External_Id__c, Account__r.Parent_3L_External_Id__c,Id,
	                Digital_Product_Requirement__r.business_phone_number_office__c,UnitPrice__c, Account__r.Account_Number__c,
	                Account__r.Parent.Account_Number__c, Telco__r.Name,Listing_Name__c,Listing__r.Name, Billing_Partner__c,Billing_Frequency__c,  
	                Directory_Code__c, Edition_Code__c, Media_Type__c, Telco_Invoice_Date__c, Service_End_Date__c, Service_Start_Date__c, 
	                Canvass__c, Directory_Edition_Name__c, Directory_Name__c FROM Order_Line_Items__c 
	                WHERE RecordtypeId !=: objRT.Id AND Product2__r.Media_Type__c='Print' AND Billing_Partner__c != null AND 
	                Order__c IN: setOrderIds ORDER BY Billing_Partner__c, Account__r.Name];
    	} else {
	        listOLI = [SELECT Listing__r.Phone__c, Account__r.X3l_External_Id__c, Account__r.Parent_3L_External_Id__c,Id,
	                Digital_Product_Requirement__r.business_phone_number_office__c,UnitPrice__c, Account__r.Account_Number__c,
	                Account__r.Parent.Account_Number__c, Telco__r.Name,Listing_Name__c,Listing__r.Name, Billing_Partner__c, Billing_Frequency__c,  
	                Directory_Code__c, Edition_Code__c, Media_Type__c, Telco_Invoice_Date__c, Service_End_Date__c, 
	                Canvass__c, Directory_Edition_Name__c, Directory_Name__c, Service_Start_Date__c FROM Order_Line_Items__c 
	                WHERE RecordtypeId !=: objRT.Id AND isCanceled__c = false AND Product2__r.Media_Type__c='Print' AND Directory__c=: selectedDirId AND 
	                Directory_Edition__c =: selectedDirEdId AND Billing_Partner__c != null AND Order__c IN: setOrderIds ORDER BY Billing_Partner__c, Account__r.Name];
    	}
    	        
        if(listOLI.size() > 0) {
            for(Order_Line_Items__c OLI : listOLI) {                
                String AdvPhone = OLI.Digital_Product_Requirement__r.business_phone_number_office__c;
                String ListingName = String.valueof(OLI.Listing__r.Name + OLI.Listing__r.Phone__c).replaceAll('\\s+','');
                acctIds.add(OLI.Account__c);
                if(!mapAccBPBFPhoneLisNamOLI.containsKey(OLI.Account__c)) {
                    mapAccBPBFPhoneLisNamOLI.put(OLI.Account__c, new Map<String, Map<String, Map<String, Map<String, List<Order_Line_Items__c>>>>>());
                } 
                if(!mapAccBPBFPhoneLisNamOLI.get(OLI.Account__c).containsKey(OLI.Billing_Partner__c)) {
                    mapAccBPBFPhoneLisNamOLI.get(OLI.Account__c).put(OLI.Billing_Partner__c, new Map<String, Map<String, Map<String, List<Order_Line_Items__c>>>>());
                }
                if(!mapAccBPBFPhoneLisNamOLI.get(OLI.Account__c).get(OLI.Billing_Partner__c).containsKey(OLI.Billing_Frequency__c)) {
                    mapAccBPBFPhoneLisNamOLI.get(OLI.Account__c).get(OLI.Billing_Partner__c).put(OLI.Billing_Frequency__c, new Map<String, Map<String, List<Order_Line_Items__c>>>());
                }
                if(!mapAccBPBFPhoneLisNamOLI.get(OLI.Account__c).get(OLI.Billing_Partner__c).get(OLI.Billing_Frequency__c).containsKey(AdvPhone)) {
                    mapAccBPBFPhoneLisNamOLI.get(OLI.Account__c).get(OLI.Billing_Partner__c).get(OLI.Billing_Frequency__c).put(AdvPhone, new Map<String, List<Order_Line_Items__c>>());
                }
                if(!mapAccBPBFPhoneLisNamOLI.get(OLI.Account__c).get(OLI.Billing_Partner__c).get(OLI.Billing_Frequency__c).get(AdvPhone).containsKey(ListingName)) {
                    mapAccBPBFPhoneLisNamOLI.get(OLI.Account__c).get(OLI.Billing_Partner__c).get(OLI.Billing_Frequency__c).get(AdvPhone).put(ListingName, new List<Order_Line_Items__c>());
                }
                mapAccBPBFPhoneLisNamOLI.get(OLI.Account__c).get(OLI.Billing_Partner__c).get(OLI.Billing_Frequency__c).get(AdvPhone).get(ListingName).add(OLI);
            }
            
            fetchBillingContact(acctIds);               
            
            for(Id acctId : mapAccBPBFPhoneLisNamOLI.keySet()) {
                for(String BP : mapAccBPBFPhoneLisNamOLI.get(acctId).keySet()) {
                    for(String BF : mapAccBPBFPhoneLisNamOLI.get(acctId).get(BP).keySet()) {
                        for(String advPhone : mapAccBPBFPhoneLisNamOLI.get(acctId).get(BP).get(BF).keySet()) {
                            for(String listName : mapAccBPBFPhoneLisNamOLI.get(acctId).get(BP).get(BF).get(advPhone).keySet()) {
                                Double billAmount = 0;
                                List<Order_Line_Items__c> listTempOLI = mapAccBPBFPhoneLisNamOLI.get(acctId).get(BP).get(BF).get(advPhone).get(listName);
                                if(listTempOLI != null) {
                                    Order_Line_Items__c tempOLI = listTempOLI.get(0);
                                    String OLIIds = '';
                                    set<Id> setOLIIds = new set<Id>();
                                    for(Order_Line_Items__c OLI : listTempOLI) {
                                        billAmount += OLI.UnitPrice__c;
                                        OLIIds += OLI.Id + ';';
                                        setOLIIds.add(OLI.Id);
                                    }           
                					List_of_Business__c lob = new List_of_Business__c();   
                					lob = LOBReportGenerationBatch.newLOB(tempOLI, billAmount, OLIIds.removeEnd(';'), setOLIIds.size());   
                					lob.LOB_Billing_Telephone__c = mapAcctIdPrimConPhone.get(tempOLI.Account__c);   
                        			listLOB.add(lob);          
                                }
                            }
                        }
                    }
                }
            }    
        } 
        
        if(listLOB.size() > 0) {
            insert listLOB;
        }
    }

    global void finish(Database.BatchableContext bc) {
    	AsyncApexJob a = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
    	BillingAmountPopulationOnOLIFromLOBBatch obj = new BillingAmountPopulationOnOLIFromLOBBatch();
        Database.executeBatch(obj, Integer.valueOf(system.label.OLI_Bill_Amount_Batch_Size));
        if(a.NumberOfErrors > 0) {
			CommonEmailUtils.sendHTMLEmailForTargetObject(UserInfo.getUserId(), 'LOB Report batch process status : ' + a.Status, 'The Apex batch job picked ' + a.TotalJobItems + ' batches and processed with '
			+ a.NumberOfErrors + ' failures. Please check Exception/Errors record.'); 
			futureCreateErrorLog.createErrorRecordBatch(a.ExtendedStatus, 'LOB Report batch process status :'  + a.Status+'. The Apex batch job picked '+a.TotalJobItems+' batches andProcessed with '
            + a.NumberOfErrors + ' failures.', 'LOB Report batch process');
		} else if(a.JobItemsProcessed != a.TotalJobItems) {
			CommonEmailUtils.sendHTMLEmailForTargetObject(UserInfo.getUserId(), 'LOB Report batch process status : ' + a.Status, 'The Apex batch job picked '+ a.TotalJobItems +' batches and processed with '
			+ a.NumberOfErrors + ' failures. Please check Exception/Errors record.');
			futureCreateErrorLog.createErrorRecordBatch('Either the batch job was aborted or Job failed to process due to internal error', 'LOB Report batch process status : ' + a.Status+'. The Apex batch job picked '+ a.TotalJobItems+' batches and Processed with '
            + a.NumberOfErrors + ' failures.', 'LOB Report batch process');
		} else {
			CommonEmailUtils.sendHTMLEmailForTargetObject(UserInfo.getUserId(), 'LOB Report batch process status : ' + a.Status, 'The Apex batch job picked '+ a.TotalJobItems+' batches  and processed with ' 
			+ a.NumberOfErrors + ' failures.');
		}
    }
    
    void fetchBillingContact(Set<Id> acctIds) {
        List<Contact> listContact = new List<Contact>();
        listContact = ContactSOQLMethods.getPrimaryBillContactByAccountID(acctIds); 
        if(listContact.size() > 0) {
            for(Contact con : listContact) {
                mapAcctIdPrimConPhone.put(con.AccountId, con.Phone);
            }
        }
    }     
}