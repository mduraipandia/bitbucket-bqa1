/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class TestBSure_InsertPIARequestReview {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Opportunity opp = new Opportunity(Name='test opp', StageName='stage', Probability = 95, CloseDate=system.today());
        insert opp;
        Vertex_Berry__PIA_Credit_Review__c piaobj = new Vertex_Berry__PIA_Credit_Review__c();
        //piaobj.Name = 'Test Data';
        piaobj.Vertex_Berry__Analyst__c = UserInfo.getUserId();
        piaobj.Vertex_Berry__Account_Name__c = 'test Account';
        piaobj.Vertex_Berry__Opportunity_ID__c = opp.Id;
        piaobj.Vertex_Berry__Opportunity_Name__c = 'test Oppt name';
        piaobj.Vertex_Berry__Current_PIA__c = 75;
        piaobj.Vertex_Berry__Former_PIA__c = 100;
        piaobj.Vertex_Berry__Status__c = 'Pending for Approval';
        insert piaobj;
        piaobj.Vertex_Berry__Former_PIA__c = 75;
        piaobj.Vertex_Berry__Current_PIA__c = 100;
        piaobj.Vertex_Berry__Analyst__c = UserInfo.getUserId();
        piaobj.Vertex_Berry__Status__c = 'Completed';
        update piaobj;
        
        PIA_Credit_Review_Opp__c pobj = new PIA_Credit_Review_Opp__c();
        pobj.Former_PIA__c = 75;
        pobj.Current_PIA__c = 75;
        pobj.Analyst__c = UserInfo.getUserId();
        pobj.Opportunity__c = opp.Id;
        //pobj.Name__c = 'test name';
        pobj.Status__c ='pending for approval';
        insert pobj;
        pobj.ExternalId__c = pobj.id;
        pobj.Former_PIA__c =100;
        pobj.Current_PIA__c =100;
        pobj.Status__c ='Completed';
    }
    static testMethod void myUnitTest1() {
        // TO DO: implement unit test
        //BSure_Configuration_Settings__c objCSettings1 = new BSure_Configuration_Settings__c();
        //objCSettings1.Name = 'CACreateAccess';
        //objCSettings1.Parameter_Key__c = 'CACreateAccess';
        //objCSettings1.Parameter_Value__c = 'true';
        //insert objCSettings1;
        //system.assertEquals('true', objCSettings1.Parameter_Value__c);
         Opportunity opp = new Opportunity(Name='test opp', StageName='stage', Probability = 95, CloseDate=system.today());
        insert opp;
        Vertex_Berry__PIA_Credit_Review__c piaobj = new Vertex_Berry__PIA_Credit_Review__c();
        //piaobj.Name = 'Test Data';
        piaobj.Vertex_Berry__Analyst__c = UserInfo.getUserId();
        piaobj.Vertex_Berry__Account_Name__c = 'test Account';
        piaobj.Vertex_Berry__Opportunity_ID__c = opp.Id;
        piaobj.Vertex_Berry__Opportunity_Name__c = 'test Oppt name';
        piaobj.Vertex_Berry__Current_PIA__c = 75;
        piaobj.Vertex_Berry__Former_PIA__c = 100;
        piaobj.Vertex_Berry__Status__c = 'Pending for Approval';
        insert piaobj;
        BsureCreditApplication__c bobj = new BsureCreditApplication__c();
        bobj.BCS__c = '527';
        bobj.PCS__c = '200';
        bobj.Bsure_CA_Id__c = piaobj.Id;
        bobj.Opportunity__c = opp.id;
        insert bobj;
        
       ApexPages.StandardController suplcontroller = new ApexPages.StandardController(bobj);  
       BSure_ViewCreditApplicationOpp cobj = new BSure_ViewCreditApplicationOpp(suplcontroller);
       cobj.caId = bobj.Id;
       cobj.getCreditApplicationOppDetails();
       BSure_ViewCreditApplicationOpp.validatePermissionCA(UserInfo.getUserId());
       
       //BSure_ViewCreditApplicationOpp.validatePermissionPIA(UserInfo.getUserId());
    }
    
    static testMethod void myUnitTest2() {
         Opportunity opp = new Opportunity(Name='test opp', StageName='stage', Probability = 95, CloseDate=system.today());
        insert opp;
         Vertex_Berry__PIA_Credit_Review__c piaobj = new Vertex_Berry__PIA_Credit_Review__c();
        //piaobj.Name = 'Test Data';
        piaobj.Vertex_Berry__Analyst__c = UserInfo.getUserId();
        piaobj.Vertex_Berry__Account_Name__c = 'test Account';
        piaobj.Vertex_Berry__Opportunity_ID__c = opp.Id;
        piaobj.Vertex_Berry__Opportunity_Name__c = 'test Oppt name';
        piaobj.Vertex_Berry__Current_PIA__c = 75;
        piaobj.Vertex_Berry__Former_PIA__c = 100;
        piaobj.Vertex_Berry__Status__c = 'Pending for Approval';
        insert piaobj;
        PIA_Credit_Review_Opp__c p = new PIA_Credit_Review_Opp__c();
        p.ExternalId__c = piaobj.Id;
        p.Opportunity__c = opp.id;
        insert p;
        ApexPages.StandardController piacontroller = new ApexPages.StandardController(p);  
        BSure_ManualReviewBerry  bm = new BSure_ManualReviewBerry(piacontroller);
        bm.getPIAReviewDetails();
        
    }
    static testMethod void myUnitTest3()
    {
        Opportunity opp = new Opportunity(Name='test opp', StageName='stage', Probability = 95, CloseDate=system.today());
        insert opp;
        Vertex_Berry__BsureCreditApplication__c bc = new Vertex_Berry__BsureCreditApplication__c();
        bc.Vertex_Berry__Opportunity_Name__c = opp.Name;
        bc.Vertex_Berry__AlertCodeDescription__c = 'test desc';
        bc.Vertex_Berry__Year_Business_Established__c = System.today();
        bc.Vertex_Berry__Year_Business_Established1__c = '2014';
        bc.Vertex_Berry__Title2__c = 'test title';
        bc.Vertex_Berry__Title1__c = 'test title';
        bc.Vertex_Berry__Temporary_Zip__c = '12345';
        bc.Vertex_Berry__Temporary_Street__c = 'test street';
        bc.Vertex_Berry__Temporary_State__c = 'IL';
        bc.Vertex_Berry__Temporary_State__c = 'Maxico';
        bc.Vertex_Berry__Telephone_Numbers_New__c = '4353453345';
        bc.Vertex_Berry__Telephone_Numbers__c = '2345353345';
        bc.Vertex_Berry__Status__c = 'Completed';
        bc.Vertex_Berry__ExternalId__c = opp.id;
        bc.Vertex_Berry__Buisness_Name__c = 'test Business';
        //bc.Vertex_Berry__Salesperson_Email__c = 'test@test.com';
        insert bc;
        bc.Vertex_Berry__Opportunity_Name__c = 'Test opp';
        bc.Vertex_Berry__AlertCodeDescription__c = 'test desc1';
        bc.Vertex_Berry__Year_Business_Established__c = System.today();
        bc.Vertex_Berry__Year_Business_Established1__c = '2015';
        bc.Vertex_Berry__Title2__c = 'test title1';
        bc.Vertex_Berry__Title1__c = 'test title2';
        bc.Vertex_Berry__Temporary_Zip__c = '123456';
        bc.Vertex_Berry__Temporary_Street__c = 'test street1';
        bc.Vertex_Berry__Temporary_State__c = 'IL';
        bc.Vertex_Berry__Temporary_State__c = 'Maxico';
        bc.Vertex_Berry__Telephone_Numbers_New__c = '4353453346';
        bc.Vertex_Berry__Telephone_Numbers__c = '2345353345';
        bc.Vertex_Berry__Buisness_Name__c = 'test Business';
        bc.Vertex_Berry__Status__c = 'Completed';
        //bc.Vertex_Berry__Salesperson_Email__c = 'test@test.com';
        update bc;
        
        BsureCreditApplication__c c= new BsureCreditApplication__c();
        c.BCS__c = '500';
        c.PCS__c = '800';
        c.Buisness_Name__c = 'test business';
        c.Number_of_Employees__c = 230;
        c.Billing_City__c ='test city';
        c.Billing_State__c ='test state';
        c.Billing_Street__c ='test street';
        c.Billing_Zip__c ='57790';
        c.ExternalId__c = bc.id;
        c.Year_Business_Established1__c ='2013';
        c.Legal_Business_Name__c ='test legal';
        c.Status__c ='Pending for Approval';
        c.Number_Of_Liens__c ='56';
        c.Opportunity__c = opp.Id;
        c.Status__c = 'Completed';
        insert c;
        c.PCS__c = '600';
        update c;
        
    }
}