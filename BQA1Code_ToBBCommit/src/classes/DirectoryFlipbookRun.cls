public class DirectoryFlipbookRun {
    public list<wrapperDir> wrapperDirLst{get;set;}
    public list<wrapperDir> wrapperDirSLLst{get;set;}
    public list<Directory__c> objDirList;
    public string userinputName{get;set;}
    public List<Directory__c> lstSearchDir;
    public boolean selectedCheckbox{get;set;}
    public DirectoryFlipbookRun(ApexPages.StandardController controller) {
        wrapperDirLst = new list<wrapperDir>();
        objDirList = new list<Directory__c>();
        lstSearchDir = new list<Directory__c>();
        selectedCheckbox = false;
    }
    public pagereference fetchSLDirectories(){
        if(selectedCheckbox){
            wrapperDirLst = new list<wrapperDir>();
            objDirList=[Select Id,Name,Sales_Lockout__c,CANCELLED_DIRECTORIES_INDICATOR__c from Directory__c where Name != Null AND CANCELLED_DIRECTORIES_INDICATOR__c = false AND Sales_Lockout__c < Today];
            if(objDirList.size()>0){
                objDirList.sort();
                for(Directory__c obDir : objDirList){
                    wrapperDir objWDir = new wrapperDir();
                    objWDir.objDir = obDir;
                    wrapperDirLst.add(objWDir);
                }
            }
        }
        else {
        wrapperDirLst = new list<wrapperDir>();
            objDirList=[Select Id,Name,Sales_Lockout__c,CANCELLED_DIRECTORIES_INDICATOR__c from Directory__c where Name != Null AND CANCELLED_DIRECTORIES_INDICATOR__c = false AND Sales_Lockout__c != null Limit 1000];
            if(objDirList.size()>0){
                objDirList.sort();
                for(Directory__c obDir : objDirList){
                    wrapperDir objWDir = new wrapperDir();
                    objWDir.objDir = obDir;
                    wrapperDirLst.add(objWDir);
                }
            }
        }
        return null;
    }
    public pagereference doRun() {
        set<Id> setSelDir = new set<Id>();
        List<directory__c> lstDir = new List<Directory__c>();
        for(wrapperDir objWrapDir : wrapperDirLst){
            if(objWrapDir.isChecked == true){
                setSelDir.add(objWrapDir.objDir.Id);
            }
        }
        if(setSelDir.size()>0){    
            lstDir = [select id,Name,Sales_Lockout__c,EBD__c,Ship_Date__c,DCR_Close__c,BOC__c,Final_Service_Order_Due_to_Berry__c,
                        Book_Extract_YP__c,Pub_Date__c,Sales_Start__c,Delivery_Start_Date__c,Coup_Prelim_Filler__c, 
                        Distribution_Method__c, Printer__c, Printer_Location__c, WP_PROD_MGR__c, YP_PROD_MGR__c,CANCELLED_DIRECTORIES_INDICATOR__c, 
                        NCR_INDICATOR__c, Branch_Manager__c, ALPHA_COLOR_LEVEL__c, CLASS_COLOR_LEVEL__c, Directory_Coordinator__c  from Directory__c where Id IN :setSelDir AND CANCELLED_DIRECTORIES_INDICATOR__c = false];
        }      
        if(lstDir.size() > 0) {
            DirectoryFlipBookCommonMethods.doDirectoryFlipBookProcess(lstDir);
        }
        else if(lstDir.size() == 0){
          ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Please Select any one Record');
          ApexPages.addMessage(myMsg);
        }
        return null;
    }
    
    public pagereference doSearch() {
        lstSearchDir = new list<Directory__c>();
        String soqlquery='Select Id,Name,Sales_Lockout__c,CANCELLED_DIRECTORIES_INDICATOR__c from Directory__c where CANCELLED_DIRECTORIES_INDICATOR__c = false';
        string tempInput = '\'%' + userinputName + '%\'';
        if(userinputName.length()>0 && userinputName != null && userinputName != '') {
            soqlquery=soqlquery+' AND Name like '+tempInput+'Limit 900';
            lstSearchDir = database.query(soqlquery);
        }
        else if(userinputName == null || userinputName == '') {
          ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Please Provide Input for Search');
          ApexPages.addMessage(myMsg);
        }
        wrapperDirLst = new list<wrapperDir>();
        if(lstSearchDir.size()>0){
            for(Directory__c dirObj : lstSearchDir){
                wrapperDir objWDir = new wrapperDir();
                objWDir.objDir = dirObj;
                wrapperDirLst.add(objWDir);
           }
        }
       return null;
    }
    public class wrapperDir{
        public boolean isChecked{get;set;}
        public Directory__c objDir{get;set;}
    }
}