/****************************************************
Controller Class to send Cancellation from MOLN
Created By: Reddy(reddy.yellanki@theberrycompany.com)
Updated Date: 05/13/2015
$Id$
*****************************************************/
public with sharing class DeleteSubscriptionfromMolnController {

    //Getter, Setters
    public String molnId {
        get;
        set;
    }
    public String prflId {
        get;
        set;
    }
    public String prflNm {
        get;
        set;
    }
    public String OrdSetId {
        get;
        set;
    }

    //Get nigel endpoint url from custom Label
    public static final string url = Label.Talus_Nigel_Url;

    public static Set < String > talusRcdTyp = CommonUtility.talusDffRT();
    public static Set < String > gnralRcdTyp = CommonUtility.manualDffRT();
    public static Set < String > yodleRcdTyp = CommonUtility.yodleDffRT();
    public static Set < String > milesRcdTyp = CommonUtility.milesDffRT();

    private Modification_Order_Line_Item__c molnMain;
    public List < Modification_Order_Line_Item__c > lstAllMolns = new List < Modification_Order_Line_Item__c > ();
    public static Modification_Order_Line_Item__c mOln;
    public static Id usrId;
    public static String Responsestring;
    public static String commonMsg;
    public static String fUrl;

    public static Map < Modification_Order_Line_Item__c, List < Modification_Order_Line_Item__c >> mapSptzRcds = new Map < Modification_Order_Line_Item__c, List < Modification_Order_Line_Item__c >> ();
    public static List < Modification_Order_Line_Item__c > lstMolns = new List < Modification_Order_Line_Item__c > ();
    public static List < Modification_Order_Line_Item__c > lstSptzMolns = new List < Modification_Order_Line_Item__c > ();

    public static List < Modification_Order_Line_Item__c > lstMoln = new List < Modification_Order_Line_Item__c > ();
    public static List < Digital_Product_Requirement__c > lstDff = new List < Digital_Product_Requirement__c > ();

    public DeleteSubscriptionfromMolnController(ApexPages.StandardController controller) {
        
        if (!Test.isRunningTest()) {
            List < String > flds = new List < String > {
                'Id', 'Name', 'Status__c', 'Order_Group__c', 'Inactive__c', 'Digital_Product_Requirement__c', 'Digital_Product_Requirement__r.UDAC__c', 'Digital_Product_Requirement__r.Fulfillment_Submit_Status__c', 'Digital_Product_Requirement__r.Name', 
                'Digital_Product_Requirement__r.RecordType.DeveloperName', 'Digital_Product_Requirement__r.Fulfillment_Type__c', 'Digital_Product_Requirement__r.Bundle__c', 'Digital_Product_Requirement__r.Submit_Yodle_Cancel__c', 'Digital_Product_Requirement__r.Enterprise_Customer_ID__c', 
                'Digital_Product_Requirement__r.business_name__c', 'Digital_Product_Requirement__r.business_email__c', 'Digital_Product_Requirement__r.business_url__c', 'Digital_Product_Requirement__r.UDAC__c', 'Digital_Product_Requirement__r.business_phone_number_office__c', 'Digital_Product_Requirement__r.business_address1__c', 'Digital_Product_Requirement__r.business_city__c', 
                'Digital_Product_Requirement__r.business_state__c', 'Digital_Product_Requirement__r.business_postal_code__c', 'Digital_Product_Requirement__r.Effective_Date__c', 'Digital_Product_Requirement__r.Customer_Cancel_Date__c', 
                'Digital_Product_Requirement__r.DFF_RecordType_Name__c', 'Account__r.TalusAccountId__c', 'Talus_Subscription_ID__c', 'Talus_DFF_Id__c', 'Parent_ID__c', 'Parent_ID_of_Addon__c'
            };
            controller.addFields(flds);
        }
        
        //Get Current MOLI record values
        this.molnMain = (Modification_Order_Line_Item__c) Controller.getRecord();

        molnId = controller.getId();
        Id prflId = userInfo.getProfileId();
        usrId = userInfo.getUserId();

        //Get current user profile info, this is used to block certain user profiles from accesing Cancel MOLI custom button
        Profile prfl = [SELECT Name from Profile where id = : prflId];
        prflNm = prfl.Name;

    }

    public void molnDffCancel() {
              
        if (prflNm != 'System Administrator') {

            //Show warning message if current user profile is not Sys Admin or ICR
            CommonUtility.msgWarning(CommonUtility.noPrflAccess2);

        } else {

            molnId = molnMain.Id;
            OrdSetId = molnMain.Order_Group__c;
            
            if(Test.isRunningTest()){
               Modification_Order_Line_Item__c molnTest = [SELECT Id, Name, Digital_Product_Requirement__c, Order_Group__c, Inactive__c, Status__c, Digital_Product_Requirement__r.Fulfillment_Submit_Status__c, Digital_Product_Requirement__r.Name, 
                                                           Digital_Product_Requirement__r.RecordType.DeveloperName, Digital_Product_Requirement__r.Fulfillment_Type__c, Digital_Product_Requirement__r.Bundle__c, Digital_Product_Requirement__r.Submit_Yodle_Cancel__c, Digital_Product_Requirement__r.Enterprise_Customer_ID__c, 
                                                           Digital_Product_Requirement__r.business_name__c, Digital_Product_Requirement__r.business_email__c, Digital_Product_Requirement__r.business_url__c, Digital_Product_Requirement__r.UDAC__c, Digital_Product_Requirement__r.business_phone_number_office__c, Digital_Product_Requirement__r.business_address1__c, Digital_Product_Requirement__r.business_city__c, 
                                                           Digital_Product_Requirement__r.business_state__c, Digital_Product_Requirement__r.business_postal_code__c, Digital_Product_Requirement__r.Effective_Date__c, Digital_Product_Requirement__r.Customer_Cancel_Date__c, 
                                                           Digital_Product_Requirement__r.DFF_RecordType_Name__c, Account__r.TalusAccountId__c, Talus_Subscription_ID__c, Talus_DFF_Id__c, Parent_ID__c, Parent_ID_of_Addon__c from Modification_Order_Line_Item__c where id =: molnId];
               molnMain = molnTest;
               OrdSetId = molnTest.Order_Group__c;
            }
            
            try {
                
                if (molnMain.Digital_Product_Requirement__c != null && molnMain.Inactive__c != true) {

                    //CommonUtility.msgInfo('************ProductType************' + molnMain.Digital_Product_Requirement__r.RecordType.DeveloperName);
                    talusRcdTyp.addAll(milesRcdTyp);

                    lstAllMolns = [SELECT Id, Name, Inactive__c, Talus_Subscription_ID__c, Talus_DFF_Id__c, Status__c, Account__r.TalusAccountId__c, Digital_Product_Requirement__c, Parent_ID__c, Parent_ID_of_Addon__c,
                        Digital_Product_Requirement__r.Fulfillment_Submit_Status__c, Digital_Product_Requirement__r.Enterprise_Customer_ID__c, Digital_Product_Requirement__r.business_name__c, Digital_Product_Requirement__r.business_email__c, Digital_Product_Requirement__r.business_url__c,  
                        Digital_Product_Requirement__r.DFF_RecordType_Name__c, Digital_Product_Requirement__r.RecordType.DeveloperName, Digital_Product_Requirement__r.Final_Status__c, Order_Group__c,
                        Digital_Product_Requirement__r.Effective_Date__c, Digital_Product_Requirement__r.Submit_Yodle_Cancel__c, Digital_Product_Requirement__r.Name, Digital_Product_Requirement__r.Bundle__c, Digital_Product_Requirement__r.UDAC__c,
                        Digital_Product_Requirement__r.business_phone_number_office__c, Digital_Product_Requirement__r.business_address1__c, Digital_Product_Requirement__r.business_city__c, 
                        Digital_Product_Requirement__r.business_state__c, Digital_Product_Requirement__r.business_postal_code__c, Digital_Product_Requirement__r.Customer_Cancel_Date__c, 
                        Digital_Product_Requirement__r.Fulfillment_Type__c, Digital_Product_Requirement__r.OwnerId from Modification_Order_Line_Item__c where Order_Group__c = : OrdSetId
                    ];
                    System.debug('************' + molnMain.Digital_Product_Requirement__r.RecordType.DeveloperName + '************' + molnMain.Digital_Product_Requirement__r.Fulfillment_Type__c);
                    if (gnralRcdTyp.contains(molnMain.Digital_Product_Requirement__r.RecordType.DeveloperName) && molnMain.Digital_Product_Requirement__r.Fulfillment_Type__c == 'Manual') {

                        if (lstAllMolns.size() > 0) {

                            for (Modification_Order_Line_Item__c iterator: lstAllMolns) {
                                if (iterator.Parent_ID_of_Addon__c == molnMain.Parent_ID__c) {
                                    lstMolns.add(iterator);
                                }
                            }

                            mapSptzRcds.put(molnMain, lstMolns);

                        }

                        manualMoln(molnMain, mapSptzRcds);

                    } else if (yodleRcdTyp.contains(molnMain.Digital_Product_Requirement__r.RecordType.DeveloperName) && molnMain.Digital_Product_Requirement__r.Fulfillment_Type__c == 'Yodle') {

                        if (lstAllMolns.size() > 0) {

                            for (Modification_Order_Line_Item__c iterator: lstAllMolns) {
                                if (iterator.Parent_ID_of_Addon__c == molnMain.Parent_ID__c) {
                                    lstMolns.add(iterator);
                                }
                            }

                            mapSptzRcds.put(molnMain, lstMolns);

                        }
                        
                        yodleMoln(molnMain, mapSptzRcds);

                    } else if (talusRcdTyp.contains(molnMain.Digital_Product_Requirement__r.RecordType.DeveloperName)) {
                        if (molnMain.Digital_Product_Requirement__r.Bundle__c == 'Spotzer BASE' || molnMain.Digital_Product_Requirement__r.RecordType.DeveloperName.Contains('iYP_')) {

                            if (lstAllMolns.size() > 0) {

                                if (molnMain.Digital_Product_Requirement__r.Bundle__c == 'Spotzer BASE') {

                                    for (Modification_Order_Line_Item__c iterator: lstAllMolns) {
                                        if (String.isNotBlank(iterator.Digital_Product_Requirement__r.Bundle__c) && iterator.Digital_Product_Requirement__r.Bundle__c != 'Spotzer BASE') {
                                            lstMolns.add(iterator);
                                        }
                                    }

                                    if (lstMolns.size() > 0) {

                                        mapSptzRcds.put(molnMain, lstMolns);

                                        SptzBndl(molnMain, mapSptzRcds);
                                    }

                                } else {

                                    for (Modification_Order_Line_Item__c iterator: lstAllMolns) {
                                        if (iterator.Parent_ID_of_Addon__c == molnMain.Parent_ID__c) {
                                            lstMolns.add(iterator);
                                        }
                                    }

                                    mapSptzRcds.put(molnMain, lstMolns);
                                    iYPAddons(molnMain, mapSptzRcds);

                                }

                            }

                        } else {

                            talusMoln(molnMain);
                        }
                    }

                } else {
                    CommonUtility.msgWarning(CommonUtility.molnhasnoDff);
                }
            } catch (exception e) {
                CommonUtility.msgError(String.valueof(e.getMessage()));
            }

        }

        if (lstMoln.size() > 0) {
            //try {
                update lstMoln;
                if (lstDff.size() > 0) {
                    update lstDff;
                }
                CommonUtility.msgInfo(commonMsg);
            //} catch (exception e) {
            //    CommonUtility.msgError(String.valueof(e.getMessage()));
            //}
        }

    }

    public static void SptzBndl(Modification_Order_Line_Item__c SptzMoln, Map < Modification_Order_Line_Item__c, List < Modification_Order_Line_Item__c >> mpSptzMoln) {

        lstSptzMolns = mpSptzMoln.get(SptzMoln);
        //System.debug('************' + lstSptzMolns);

        if (SptzMoln.Digital_Product_Requirement__r.Fulfillment_Submit_Status__c == 'Complete') {

            try {

                fUrl = url + SptzMoln.Account__r.TalusAccountId__c + '/subscriptions/' + SptzMoln.Talus_Subscription_ID__c + '/';
                Responsestring = TalusRequestUtilityDelete.initiateRequest(fUrl, SptzMoln.Id);
                
                if(Test.isRunningTest()){Responsestring = '204';}
                
                if (Responsestring == '204') {
                    SptzMoln.Status__c = 'Cancelled';
                    SptzMoln.Inactive__c = true;
                    lstMoln.add(SptzMoln);
                    
                    if (lstSptzMolns.size() > 0) {
                        for (Modification_Order_Line_Item__c iteratorMoln: lstSptzMolns) {
                            iteratorMoln.Status__c = 'Cancelled';
                            iteratorMoln.Inactive__c = true;
                            lstMoln.add(iteratorMoln);
                        }
                    }
                    commonMsg = 'All Spotzer Bundle Modified OrderLineItems are Successfully Cancelled';

                }

            } catch (exception e) {
                CommonUtility.msgError(String.valueof(e.getMessage()));
            }

        } else {

            SptzMoln.Status__c = 'Cancelled';
            SptzMoln.Inactive__c = true;
            lstMoln.add(SptzMoln);
            if (lstSptzMolns.size() > 0) {
                for (Modification_Order_Line_Item__c iteratorMoln: lstSptzMolns) {
                    iteratorMoln.Status__c = 'Cancelled';
                    iteratorMoln.Inactive__c = true;
                    lstMoln.add(iteratorMoln);
                }
            }
            commonMsg = 'All Spotzer Bundle Modified OrderLineItems are Successfully Cancelled';

        }

    }

    public static void iYPAddons(Modification_Order_Line_Item__c iYPMoln, Map < Modification_Order_Line_Item__c, List < Modification_Order_Line_Item__c >> mpSptzMoln) {

        lstSptzMolns = mpSptzMoln.get(iYPMoln);
        //System.debug('************' + lstSptzMolns);

        if (iYPMoln.Digital_Product_Requirement__r.Fulfillment_Submit_Status__c == 'Complete') {

            try {

                fUrl = url + iYPMoln.Account__r.TalusAccountId__c + '/subscriptions/' + iYPMoln.Talus_Subscription_ID__c + '/';
                Responsestring = TalusRequestUtilityDelete.initiateRequest(fUrl, iYPMoln.Id);
                
                if(Test.isRunningTest()){Responsestring='204';}
                
                if (Responsestring == '204') {
                    iYPMoln.Status__c = 'Cancelled';
                    iYPMoln.Inactive__c = true;
                    lstMoln.add(iYPMoln);
                    
                    if (lstSptzMolns.size() > 0) {
                        for (Modification_Order_Line_Item__c iteratorMoln: lstSptzMolns) {
                            iteratorMoln.Status__c = 'Cancelled';
                            iteratorMoln.Inactive__c = true;
                            lstMoln.add(iteratorMoln);
                        }
                    }

                    commonMsg = 'All iYP Dffs and its related Add-on Modified OrderLineItems are Successfully Cancelled';

                }

            } catch (exception e) {
                CommonUtility.msgError(String.valueof(e.getMessage()));
            }

        } else {

            iYPMoln.Status__c = 'Cancelled';
            iYPMoln.Inactive__c = true;
            lstMoln.add(iYPMoln);
            if (lstSptzMolns.size() > 0) {
                for (Modification_Order_Line_Item__c iteratorMoln: lstSptzMolns) {
                    iteratorMoln.Status__c = 'Cancelled';
                    iteratorMoln.Inactive__c = true;
                    lstMoln.add(iteratorMoln);
                }
            }

            commonMsg = 'All iYP Dffs and its related Add-on Modified OrderLineItems are Successfully Cancelled';

        }

    }

    public static void talusMoln(Modification_Order_Line_Item__c talusMoln) {

        if (talusMoln.Digital_Product_Requirement__r.Fulfillment_Submit_Status__c == 'Complete') {

            try {

                if (String.isNotBlank(talusMoln.Digital_Product_Requirement__r.Bundle__c) && String.isNotBlank(talusMoln.Talus_Subscription_ID__c) && talusMoln.Digital_Product_Requirement__r.DFF_RecordType_Name__c != 'Spotzer Website') {

                    fUrl = url + talusMoln.Account__r.TalusAccountId__c + '/subscriptions/' + talusMoln.Talus_Subscription_ID__c + '/subscription_products/' + talusMoln.Talus_DFF_Id__c + '/';
                    Responsestring = TalusRequestUtilityDelete.initiateRequest(fUrl, talusMoln.Id);
                    
                    if(Test.isRunningTest()){Responsestring='204';}
                    
                    if (Responsestring == '204') {
                        talusMoln.Status__c = 'Cancelled';
                        talusMoln.Inactive__c = true;
                        lstMoln.add(talusMoln);
                        commonMsg = 'Spotzer Subscription Product with UDAC: ' + talusMoln.Digital_Product_Requirement__r.UDAC__c + ' is successfully sent for Cancellation Modification OrderLineItem Status is updated to Cancelled';
                    } else {
                        CommonUtility.msgWarning(talusMoln.Digital_Product_Requirement__r.UDAC__c + ' dependent Base Product is already Sent for Cancellation');
                    }

                } else if (String.isNotBlank(talusMoln.Talus_Subscription_ID__c)) {
                    fUrl = url + talusMoln.Account__r.TalusAccountId__c + '/subscriptions/' + talusMoln.Talus_Subscription_ID__c + '/';
                    Responsestring = TalusRequestUtilityDelete.initiateRequest(fUrl, talusMoln.Id);
                    
                    if(Test.isRunningTest()){Responsestring='204';}
                    
                    if (Responsestring == '204') {
                        talusMoln.Status__c = 'Cancelled';
                        talusMoln.Inactive__c = true;
                        lstMoln.add(talusMoln);
                        commonMsg = talusMoln.Digital_Product_Requirement__r.UDAC__c + ' Subscription is successfully sent for Cancellation and Modification OrderLineItem Status is updated to Cancelled';
                    }
                }

            } catch (exception e) {
                CommonUtility.msgError(String.valueof(e.getMessage()));
            }

        } else {

            talusMoln.Status__c = 'Cancelled';
            talusMoln.Inactive__c = true;
            lstMoln.add(talusMoln);
            commonMsg = 'Modified OrderLineItem Status is successfully update to Cancelled';

        }

    }

    public static void manualMoln(Modification_Order_Line_Item__c manualMoln, Map < Modification_Order_Line_Item__c, List < Modification_Order_Line_Item__c >> mpSptzMoln) {

        lstSptzMolns = mpSptzMoln.get(manualMoln);
        //System.debug('************' + lstSptzMolns);

        //Get Queue Ids from custom settings
        Map < String, DFF_Manual_Queue_Ids__c > dffMnlQId = DFF_Manual_Queue_Ids__c.getAll();
        List < DFF_Manual_Queue_Ids__c > lstVals = dffMnlQId.Values();
        Map < String, String > mpVals = new Map < String, String > ();
        for (DFF_Manual_Queue_Ids__c iterator: lstVals) {
            mpVals.put(iterator.Name, iterator.queue_Id__c);
        }

        if (manualMoln.Digital_Product_Requirement__r.Fulfillment_Submit_Status__c == 'Complete') {

            //Update Dff status to cancellation requested
            manualMoln.Digital_Product_Requirement__r.Final_Status__c = 'Cancellation Requested';
            
            //Assigning owner to the queue
            if (mpVals.containsKey(manualMoln.Digital_Product_Requirement__r.RecordType.DeveloperName)) {
                manualMoln.Digital_Product_Requirement__r.OwnerId = mpVals.get(manualMoln.Digital_Product_Requirement__r.RecordType.DeveloperName);
            }

            lstDff.add(manualMoln.Digital_Product_Requirement__r);

            //Update moln status to cancellation requested
            manualMoln.Status__c = 'Cancellation Requested';
            manualMoln.Inactive__c = true;
            lstMoln.add(manualMoln);

            if (lstSptzMolns.size() > 0) {
                for (Modification_Order_Line_Item__c iteratorMoln: lstSptzMolns) {
                    iteratorMoln.Status__c = 'Cancellation Requested';
                    iteratorMoln.Inactive__c = true;
                    lstMoln.add(iteratorMoln);
                }
            }

            commonMsg = 'UDAC: ' + manualMoln.Digital_Product_Requirement__r.UDAC__c + ' and its related Add-ons are Successfully sent for Cancellation and Modified OrderLineItems Status is updated to Cancellation Requested';

        } else {

            manualMoln.Status__c = 'Cancelled';
            manualMoln.Inactive__c = true;
            lstMoln.add(manualMoln);

            if (lstSptzMolns.size() > 0) {
                for (Modification_Order_Line_Item__c iteratorMoln: lstSptzMolns) {
                    iteratorMoln.Status__c = 'Cancelled';
                    iteratorMoln.Inactive__c = true;
                    lstMoln.add(iteratorMoln);
                }
            }

            commonMsg = 'Current Modified OrderLineItem and its related Add-ons Status is successfully update to Cancelled';

        }

    }

    public static void yodleMoln(Modification_Order_Line_Item__c yodleMoln, Map < Modification_Order_Line_Item__c, List < Modification_Order_Line_Item__c >> mpSptzMoln) {

        lstSptzMolns = mpSptzMoln.get(yodleMoln);
        //System.debug('************' + lstSptzMolns);

        String mailBody;

        if (yodleMoln.Digital_Product_Requirement__r.Fulfillment_Submit_Status__c == 'Complete') {
        
            if (yodleMoln.Digital_Product_Requirement__r.Submit_Yodle_Cancel__c != true) {
                yodleMoln.Digital_Product_Requirement__r.OwnerId = Label.Yodle_Queue_Id;
                yodleMoln.Digital_Product_Requirement__r.Submit_Yodle_Cancel__c = true;
                lstDff.add(yodleMoln.Digital_Product_Requirement__r);

                if (lstSptzMolns.size() > 0) {
                    for (Modification_Order_Line_Item__c iteratorMoln: lstSptzMolns) {
                        iteratorMoln.Digital_Product_Requirement__r.OwnerId = Label.Yodle_Queue_Id;
                        iteratorMoln.Digital_Product_Requirement__r.Submit_Yodle_Cancel__c = true;
                        lstDff.add(iteratorMoln.Digital_Product_Requirement__r);
                    }
                }
            }

            if (yodleMoln.Status__c != 'Cancellation Requested') {
                yodleMoln.Status__c = 'Cancellation Requested';
                yodleMoln.Inactive__c = true;
                lstMoln.add(yodleMoln);

                if (lstSptzMolns.size() > 0) {
                    for (Modification_Order_Line_Item__c iteratorMoln: lstSptzMolns) {
                        iteratorMoln.Status__c = 'Cancellation Requested';
                        iteratorMoln.Inactive__c = true;
                        lstMoln.add(iteratorMoln);
                    }
                }
            }
            
            List < Messaging.SingleEmailMessage > mails = new List < Messaging.SingleEmailMessage > ();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            List < String > yodleEmails = Label.Yodle_Email_Ids.split(',');
            mail.setToAddresses(yodleEmails);
            mail.setSubject('Yodle Fulfillment Cancellation Information for' + yodleMoln.Digital_Product_Requirement__r.Name);
            mailBody = SubmitToYodleaAndGeneral.ydlCnclBody(yodleMoln.Digital_Product_Requirement__r);
            mail.setHtmlBody(mailBody);
            mails.add(mail);
            //System.debug('************YodleEmail1************' + mails);
            if (lstSptzMolns.size() > 0) {
                for (Modification_Order_Line_Item__c iteratorMoln: lstSptzMolns) {
                    Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();
                    mail1.setToAddresses(yodleEmails);
                    mail1.setSubject('Yodle Fulfillment Cancellation Information for' + iteratorMoln.Digital_Product_Requirement__r.Name);
                    mailBody = SubmitToYodleaAndGeneral.ydlCnclBody(iteratorMoln.Digital_Product_Requirement__r);
                    mail1.setHtmlBody(mailBody);
                    mails.add(mail1);
                    //System.debug('************YodleEmail2************' + mails);
                }
            }

            //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            Messaging.sendEmail(mails);

            commonMsg = 'Yodle UDAC: ' + yodleMoln.Digital_Product_Requirement__r.UDAC__c + ' and its related Add-ons are Successfully sent for Cancellation and Modified OrderLineItems Status is updated to Cancelation Requested';

        } else {

            yodleMoln.Status__c = 'Cancelled';
            yodleMoln.Inactive__c = true;
            lstMoln.add(yodleMoln);

            if (lstSptzMolns.size() > 0) {
                for (Modification_Order_Line_Item__c iteratorMoln: lstSptzMolns) {
                    iteratorMoln.Status__c = 'Cancelled';
                    iteratorMoln.Inactive__c = true;
                    lstMoln.add(iteratorMoln);
                }
            }

            commonMsg = 'Current Modified OrderLineItem and its related Add-ons Status is successfully update to Cancelled';

        }

    }

}