@isTest
public class InventoryTrackingGroupUpdateTest {
    static testMethod void testInventory() {
        Test.StartTest();
        Product2 prod = new Product2(Name = 'Test', Inventory_Tracking_Group__c = 'Spine');
        insert prod;
        
        /*Directory__c dir = new Directory__c(Name = 'test');
        insert dir;*/
        
        Directory__c dir= TestMethodsUtility.createDirectory();
                                  
        Directory_Product_Mapping__c dirProdMapping = new Directory_Product_Mapping__c(is_Active__c = true, Directory__c = dir.Id, Product2__c = prod.Id);
        insert dirProdMapping;
        
        prod.Inventory_Tracking_Group__c = 'WP Banners';
        update prod;
        Test.stopTest();
    }
}