/***************************************************************
Apex class is to create all the package product property records 
Created By: Reddy(reddy.yellanki@theberrycompany.com)
Updated Date: 10/07/2013
$Id$
****************************************************************/
public with sharing class CreateUDACPackageProperties_2 {

    public static List < Product_Properties__c > createProperties(Id pkgId) {

        String RequestUrl;
        List < Package_ID__c > pkgidrecd = [Select Name, Description__c, Resource_URI__c, Product_Information__c from Package_ID__c where id = : pkgId limit 1];
        //System.debug('************Debug1************' + pkgidrecd);

        List < Package_Product__c > pkgprdt1 = [Select Name, id, Package_ID__c, Package_Product_ID__c from Package_Product__c where Package_ID__c = : pkgId];

        List < Product_Properties__c > finalpkgprpts = new List < Product_Properties__c > ();

        for (Package_ID__c pkgrcd: pkgidrecd) {

            RequestUrl = Label.Talus_Basic_Url + pkgrcd.Resource_URI__c;
            String responseString;

            //Make GET call to retrieve package products information
            if (!Test.isRunningTest()) {
                responseString = TalusRequestUtilityGET.initiateRequest(Requesturl);
            } else {
                responseString = '{"description":"Trumpia Package","end_date":"2099-01-01","name":"Trumpia Package","products":[{"code":"TRMP_BASE","cycle":"monthly","default_quantity":0,"max_quantity":1,"min_quantity":0,"product":{"billing_cycle":"monthly","code":"TRUMPIA_BASE","description":"Trumpia MCM","name":"Trumpia MCM","properties":[{"name":"username","optional":false,"type":"string","value":null},{"name":"plan_id","optional":false,"type":"int","value":null},{"name":"promo_code","optional":true,"type":"string","value":""},{"name":"signup_source","optional":false,"type":"int","value":null},{"name":"industry","optional":false,"type":"int","value":null},{"name":"time_zone","optional":false,"type":"string","value":null},{"name":"sms_short_code","optional":true,"type":"int","value":null},{"name":"password","optional":false,"type":"string","value":null},{"name":"signup_url","optional":false,"type":"string","value":null}],"sku":"MCM0001_BASE","unit_of_measure":"flat"}},{"code":"TRMP_ADD","cycle":"monthly","default_quantity":0,"max_quantity":1,"min_quantity":0,"product":{"billing_cycle":"monthly","code":"TRUMPIA_ADD","description":"MCM add-on pack","name":"MCM add-on pack","properties":[{"name":"username","optional":false,"type":"string","value":null},{"name":"plan_id","optional":false,"type":"int","value":null},{"name":"promo_code","optional":true,"type":"string","value":""},{"name":"signup_source","optional":false,"type":"int","value":null},{"name":"industry","optional":false,"type":"int","value":null},{"name":"time_zone","optional":false,"type":"string","value":null},{"name":"sms_short_code","optional":true,"type":"int","value":null},{"name":"password","optional":false,"type":"string","value":null},{"name":"signup_url","optional":false,"type":"string","value":null}],"sku":"MCM0001_ADD","unit_of_measure":"flat"}}],"resource_uri":"/catalog/api/v2/packages/PKG_MCM0001/","sku":"PKG_MCM0001","start_date":"2012-01-01","status":"active"}';
            }

            //System.debug('************Debug2************' + responseString);

            if (responseString != null && (!responseString.contains('error_message'))) {
                //System.debug('************ResponseString************' + responseString);
                AllPackageProductsWrapper pkgrecrds = (AllPackageProductsWrapper) System.JSON.deserialize(responseString, AllPackageProductsWrapper.class);

                system.debug('************PkgRrecords************' + pkgrecrds);

                List < AllPackageProductsWrapper.products > prdlst = pkgrecrds.products;

                //System.debug('************Final List of Products************' + prdlst);

                List < Package_Product__c > lstpkgids = new List < Package_Product__c > ();


                for (AllPackageProductsWrapper.products prdt: prdlst) {
                    List < AllPackageProductsWrapper.Properties > prdpkglst = prdt.product.properties;

                    //System.debug('************packageproductprpts************' + prdpkglst);

                    for (Package_Product__c pkprdt: pkgprdt1) {
                        //system.debug('************ProductSKU************' + prdt.product.sku);
                        if (String.valueof(prdt.product.sku) == String.valueof(pkprdt.Package_Product_ID__c)) {

                            //System.debug('************Product Properties************' + prdpkglst.size() + prdpkglst);

                            if (prdpkglst.size() > 0) {
                                for (AllPackageProductsWrapper.Properties p1: prdpkglst) {

                                    Product_Properties__c pkgprpt = new Product_Properties__c();
                                    pkgprpt.Package_Product__c = pkprdt.id;
                                    pkgprpt.Name__c = string.valueof(p1.name);
                                    pkgprpt.optional__c = boolean.valueof(p1.optional);
                                    pkgprpt.Type__c = p1.type;

                                    finalpkgprpts.add(pkgprpt);
                                }
                            }
                        }
                    }
                }

            }

        }

        return finalpkgprpts;
    }
}