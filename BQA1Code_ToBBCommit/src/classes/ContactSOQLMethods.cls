public class ContactSOQLMethods {
    public static map<ID, Contact> getContactByContactID(set<ID> contactID) {        
        return new map<ID, Contact>([Select MailingStreet,MailingCity,MailingState,MailingPostalCode,MailingCountry,Phone, Name, Id, Fax, Email, AccountId, PrimaryBilling__c, Statement_Suppression__c From Contact where ID IN :contactID]);       
    }
    
    public static list<Contact> getContactByAccountID(set<ID> accountID) {
        return [Select Phone, Name, Id, Fax, Email, AccountId, PrimaryBilling__c, Statement_Suppression__c From Contact where AccountId IN :accountID];       
    }
  public static list<Contact> getContactByAccountIDForBT(set<ID> accountID) {
        return [Select MailingStreet,MailingCity,MailingState,MailingPostalCode,MailingCountry,Phone, Name, Id, Fax, Email, AccountId, PrimaryBilling__c, Statement_Suppression__c From Contact where AccountId IN :accountID AND (Contact_Role__c includes ('Billing') OR  PrimaryBilling__c=true
    )];       
    }       
    
    public static List<pymt__Payment_Method__c> getContact_PaymentMethodByContactID(set<ID> contactID) {
        return [Select id ,Phone, Name, Fax, Email, AccountId, PrimaryBilling__c,(SELECT Id,Expiration_Status__c,Name,pymt__Billing_City__c,pymt__Billing_Country__c,
        pymt__Billing_Email__c,pymt__Billing_First_Name__c,pymt__Billing_Last_Name__c,pymt__Billing_Phone__c,
        pymt__Billing_Postal_Code__c,pymt__Billing_Salutation__c,pymt__Billing_State__c,pymt__Billing_Street__c,
        pymt__Card_Type__c,pymt__Contact__c,pymt__Customer_Profile_Id__c,pymt__Default__c,
        pymt__Expiration_Month__c,pymt__Expiration_Status__c,pymt__Expiration_Year__c,pymt__Last_4_Digits__c,
        pymt__Payment_Type__c,pymt__Processor_Connection__c,pymt__Profile_Id__c,pymt__Type__c 
        FROM pymt__Payment_Methods__r) from Contact where ID IN :contactID].pymt__Payment_Methods__r;       
    }
    
    public static list<Contact> getContactStmtSuppressionByAccountID(set<ID> accountID) {
        return [Select Phone, Name, Id, Fax, Email, AccountId, PrimaryBilling__c, Statement_Suppression__c From Contact where AccountId IN :accountID and Statement_Suppression__c = false];       
    }
    
    public static list<Contact> getPrimaryBillContactByAccountID(set<ID> accountID) {
        return [Select Phone, Name, Id, Fax, Email, AccountId, PrimaryBilling__c From Contact where PrimaryBilling__c =: True and AccountId IN :accountID LIMIT 50000];       
    }
}