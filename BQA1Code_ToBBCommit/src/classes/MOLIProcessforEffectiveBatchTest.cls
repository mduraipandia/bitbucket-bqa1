@isTest
public class MOLIProcessforEffectiveBatchTest {
    static testMethod void MOLIProcessforEffectiveTest() {
        Test.startTest();        
         Account acct = TestMethodsUtility.createAccount('cmr');
        Account childAcct = TestMethodsUtility.generateAccount();
        childAcct.ParentId = acct.Id;
        insert childAcct;
        Contact cnt = TestMethodsUtility.createContact(acct.Id);
        Order__c ord = TestMethodsUtility.createOrder(acct.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(acct, cnt);
        insert oppty;
        Order__c newOrder = TestMethodsUtility.createOrder(acct.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(acct, newOrder, oppty);
        Order_Line_Items__c newOrLI = TestMethodsUtility.createOrderLineItem(acct, cnt, oppty, newOrder, newOrderSet);
        Directory__c directory = TestMethodsUtility.createDirectory();
       // Directory_Section__c dirSection = TestMethodsUtility.createDirectorySection(directory);

      Modification_Order_Line_Item__c mol = new Modification_Order_Line_Item__c(Billing_Frequency__c='Monthly',Order_Line_Item__c=newOrLI.id);
     // mol.Directory_Section__c = dirSection.Id;
     date MOLIDate=date.today();
     mol.Billing_End_Date__c = MOLIDate.addDays(2);
     mol.Action_Code__c='New';
     mol.IsUDACChange__c=false;
     mol.Completed__c=false;
     mol.Inactive__c=false;
     mol.Patch_Complete_Date__c=MOLIDate;
     mol.Order_Group__c =newOrderSet.id;
      insert mol;
      
        MOLIProcessforEffective obj = new MOLIProcessforEffective(mol.Billing_End_Date__c);
        ID batchprocessid = Database.executeBatch(obj);
        Test.stopTest();
    }
}