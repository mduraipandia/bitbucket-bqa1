public class AdvertisingSummaryReport_v1{
public List<Order_Line_Items__c> OLIs {get;set;}
public Id AccountId{get;set;}
public List<SummaryReport> ListSR{get;set;}
public List<Order_Line_Items__c> FetchOLIs {get;set;}
public List<ArrangeSummaryReport> ArrSummaryReport {get;set;}
    public AdvertisingSummaryReport_v1(ApexPages.StandardController controller) 
    {
     AccountId=ApexPages.currentPage().getParameters().get('Id');
     FetchOLIs =new List<Order_Line_Items__c>();
     fetchOLI(AccountId);
     
    }
public void fetchOLI(ID AccountId)
{
     OLIs=new List<Order_Line_Items__c>();
     OLIs=[SELECT Id,Name,Product2__r.Media_Type__c,Directory_Edition__r.Edition_Code__c,Action_Code__c,Directory_Edition__r.Book_Status__c,UnitPrice__c,Directory_Edition__r.Sales_Lockout__c,Directory__r.Name,Directory__c, Product2__r.Product_type__c,Telco__r.Name,Canvass__c,Canvass__r.Name,Order_Line_Total__c FROM Order_Line_Items__c where Account__c=:AccountId Order By Directory__r.Name];
     Set<ID> PrintOLIs=new Set<ID>();
     Set<ID> DigitalOLIs=new Set<ID>();
     Set<ID> DirectoriesId =new Set<ID>();
     Set<String> DirecotryEditionIds=new Set<String>();
     Set<ID> CanvassIds=new Set<ID>();
     Map<String,List<Order_Line_Items__c>> MapEditionPrintOLI= new Map<String,List<Order_Line_Items__c>>();
     Map<ID,List<Order_Line_Items__c>> MapCanvassDigitalOLI= new Map<ID,List<Order_Line_Items__c>>();
     Map<ID,List<Directory_Edition__c>> mapDirE=new Map<ID,List<Directory_Edition__c>>();
     ListSR=new List<SummaryReport>();
     if(OLIs.size()>0)
     {
     for(Order_Line_Items__c oliIterator : OLIs)
     {
        DirectoriesId.add(oliIterator.Directory__c);
     }
     
     for(Directory_Edition__c dirEIterator : [Select id,Directory__c,Edition_Code__c,Book_Status__c,Sales_Lockout__c,Directory__r.Name,Canvass__c,Canvass__r.Name,Telco__r.Name,(Select id from Order_Line_Items__r where Account__c=:AccountId) from Directory_Edition__c where Directory__c in : DirectoriesId])
     {
       if(dirEIterator.Order_Line_Items__r.size()==0 && (dirEIterator.Book_Status__c=='NI' || dirEIterator.Book_Status__c=='BOTS' || dirEIterator.Book_Status__c=='BOTS-1'))
       {
          Double SpendAmount=0;
          //DirecotryEditionIds.add(dirEIterator.ID);
          ListSR.add(new SummaryReport(dirEIterator.Order_Line_Items__r.size(),String.valueof(dirEIterator.Directory__r.Name),String.valueof(dirEIterator.Canvass__c),String.valueof(dirEIterator.Canvass__r.Name),String.valueof(dirEIterator.Telco__r.Name),dirEIterator.Edition_Code__c,String.valueof(dirEIterator.Book_Status__c),Double.valueof(SpendAmount), String.valueof(dirEIterator.Sales_Lockout__c),'Print','Unworked'));
        
       }
     }
     
     
     for(Order_Line_Items__c oliIterator : OLIs)
     {
       DirecotryEditionIds.add(oliIterator.Directory_Edition__c);
       String BookStatus=oliIterator.Directory_Edition__r.Book_Status__c;
       if(oliIterator.Product2__r.Media_Type__c=='Print' && (BookStatus=='NI' || BookStatus=='BOTS' || BookStatus=='BOTS-1'))
       {
         PrintOLIs.add(oliIterator.id);
         String EditionID=String.valueof(oliIterator.Directory_Edition__r.Edition_Code__c);
         if(MapEditionPrintOLI.containskey(EditionID))
         {
           MapEditionPrintOLI.get(EditionID).add(oliIterator);
         }
         else
         {
           MapEditionPrintOLI.put(EditionID,new List<Order_Line_Items__c>{oliIterator});
         }
         
       }
       else if(oliIterator.Product2__r.Media_Type__c!='Print')
       {
         if(oliIterator.Canvass__c!=null)
         {
             DigitalOLIs.add(oliIterator.id);
             CanvassIds.add(oliIterator.Canvass__c);
             if(MapCanvassDigitalOLI.containskey(oliIterator.Canvass__c))
             {
               MapCanvassDigitalOLI.get(oliIterator.Canvass__c).add(oliIterator);
             }
             else
             {
               MapCanvassDigitalOLI.put(oliIterator.Canvass__c,new List<Order_Line_Items__c>{oliIterator});
             }
         }
         else
         {
                  ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Canvass are not populated in Order Lien Items.'+oliIterator.Name);
                  ApexPages.addMessage(myMsg);
                  break;
         }
       }
       else{}
     }
     System.debug('############'+MapCanvassDigitalOLI);
     Map<String,Directory_Edition__c> mapDE=new Map<String,Directory_Edition__c>();
     
     Map<ID,Canvass__c> MapCanvassDOLI=new Map<ID,Canvass__c>();
     for(Directory_Edition__c dirEIterator : [Select Directory__r.Name,Book_Status__c,Canvass__r.Name,Telco__r.Name,Edition_Code__c,Sales_Lockout__c from Directory_Edition__c where ID in:DirecotryEditionIds])
     {
       mapDE.put(dirEIterator.Edition_Code__c ,dirEIterator);
     } 
     
     for(Canvass__c cIterator:[Select id,Name,Primary_Telco__r.Name from Canvass__c where id in:CanvassIds])
     {
        MapCanvassDOLI.put(cIterator.id,cIterator);
     } 
     
     //For Print Product
     
     for(String strIterator: MapEditionPrintOLI.keyset())
     {
        Double SpendAmount=0;
        String SpendStatus ='Worked';
        for(Order_Line_Items__c oliIterator :  MapEditionPrintOLI.get(strIterator))
        {
             System.debug(oliIterator.Action_Code__c+'@@@@@@@@@@@'+oliIterator.Order_Line_Total__c);
             if(oliIterator.Order_Line_Total__c ==0 || oliIterator.Action_Code__c==null)
             {
               SpendStatus='Unworked';
             }
             SpendAmount=SpendAmount+oliIterator.Order_Line_Total__c;
             
        }
        if(mapDE.containskey(strIterator))
        {
          ListSR.add(new SummaryReport(MapEditionPrintOLI.get(strIterator).size(),String.valueof(mapDE.get(strIterator).Directory__r.Name),String.valueof(mapDE.get(strIterator).Canvass__c),String.valueof(mapDE.get(strIterator).Canvass__r.Name),String.valueof(mapDE.get(strIterator).Telco__r.Name),strIterator,String.valueof(mapDE.get(strIterator).Book_Status__c),Double.valueof(SpendAmount), String.valueof(mapDE.get(strIterator).Sales_Lockout__c),'Print',SpendStatus));
        }
     }
     
      System.debug('$$$$$PRINT=======$$$$$$$$$'+ListSR);
     //For Digital Products
     
     for(ID strDigitalIterator: MapCanvassDigitalOLI.keyset())
     {
        Double SpendAmount=0;
        String SpendStatus ='Worked';
        for(Order_Line_Items__c oliIterator :  MapCanvassDigitalOLI.get(strDigitalIterator))
        {
            if(oliIterator.Order_Line_Total__c ==0 && oliIterator.Action_Code__c==null)
             {
               SpendStatus='Unworked';
               
             }
             
            SpendAmount=SpendAmount+oliIterator.Order_Line_Total__c;
           
        }
        if(MapCanvassDigitalOLI.containskey(strDigitalIterator))
        {
          System.debug(MapCanvassDOLI.get(strDigitalIterator)+'//////////######'+MapCanvassDigitalOLI.get(strDigitalIterator));
          if(MapCanvassDOLI.get(strDigitalIterator) !=null)
          {
          ListSR.add(new SummaryReport(MapCanvassDigitalOLI.get(strDigitalIterator).size(),'Digital',String.valueof(MapCanvassDOLI.get(strDigitalIterator).ID),String.valueof(MapCanvassDOLI.get(strDigitalIterator).Name),String.valueof(MapCanvassDOLI.get(strDigitalIterator).Primary_Telco__r.Name),String.valueof(''),String.valueof(''),Double.valueof(SpendAmount),String.valueof(''),'Digital',SpendStatus));
          //ListSReport.add(new SortSummaryReport(MapCanvassDigitalOLI.get(strDigitalIterator).size(),'Digital',String.valueof(MapCanvassDOLI.get(strDigitalIterator).ID),String.valueof(MapCanvassDOLI.get(strDigitalIterator).Name),String.valueof(MapCanvassDOLI.get(strDigitalIterator).Primary_Telco__r.Name),'','',Double.valueof(SpendAmount), '','Digital',SpendStatus));
          }
        }
     }
     System.debug('$$$$$$$$$$$$$$'+ListSR);
             if(ListSR.size()>0)
             {
                ArrSummaryReport=new List<ArrangeSummaryReport>();
                Map<String,ArrangeSummaryReport> MapDirSR=new Map<String,ArrangeSummaryReport>();
                for(SummaryReport srIterator : ListSR)
                {
                   if(srIterator.BookStatus =='NI' && srIterator.ProductType=='Print')
                   {
                     if(MapDirSR.containskey(String.valueof(srIterator.DirectoryName)))
                     {
                       MapDirSR.get(String.valueof(srIterator.DirectoryName)).NISpend= Double.valueof(srIterator.TotalAmountSpend);
                       MapDirSR.get(String.valueof(srIterator.DirectoryName)).NIEdition= srIterator.Edition;
                       MapDirSR.get(String.valueof(srIterator.DirectoryName)).NIOLICount= srIterator.OLICount;
                       if(srIterator.TotalAmountSpend>0)
                       {
                          MapDirSR.get(String.valueof(srIterator.DirectoryName)).SpentStatus= String.valueof('Worked');
                       }
                       else
                       {
                           MapDirSR.get(String.valueof(srIterator.DirectoryName)).SpentStatus= String.valueof('Unworked');
                       } 
                       MapDirSR.get(String.valueof(srIterator.DirectoryName)).SalesLockOut=srIterator.SalesLockOut;
                     }
                     else
                     {            
                     System.debug('$$$$$$$$$$$$$$'+srIterator.SpentStatus);
                      System.debug('$$$$$$$srIterator====$$$$$$$'+srIterator);
                     ArrangeSummaryReport arr= new ArrangeSummaryReport(0,srIterator.OLICount,0,0,String.valueof(srIterator.DirectoryName), String.valueof(srIterator.CanvassID),String.valueof(srIterator.CanvassName), String.valueof(srIterator.TelcoName), String.valueof(srIterator.Edition),String.valueof(''),String.valueof(''), String.valueof(srIterator.BookStatus),Double.valueof(srIterator.AmountSpend), String.valueof(srIterator.SalesLockOut),String.valueof(srIterator.ProductType),String.valueof(srIterator.SpentStatus),Double.valueof('0.00'),Double.valueof('0.00'));
                     MapDirSR.put(String.valueof(srIterator.DirectoryName),arr);
                     }
                   }
                   if(srIterator.BookStatus =='BOTS' && srIterator.ProductType=='Print')
                   {
                     System.debug(srIterator.DirectoryName+'#########################'+MapDirSR.containskey(String.valueof(srIterator.DirectoryName)));
                     if(MapDirSR.containskey(String.valueof(srIterator.DirectoryName)))
                     {
                       MapDirSR.get(String.valueof(srIterator.DirectoryName)).BotSpend= Double.valueof(srIterator.TotalAmountSpend);
                       MapDirSR.get(String.valueof(srIterator.DirectoryName)).B_Edition= srIterator.Edition;
                       MapDirSR.get(String.valueof(srIterator.DirectoryName)).B_OLICount= srIterator.OLICount;
                     }
                     else
                     {
                       ArrangeSummaryReport arr= new ArrangeSummaryReport(0,0,srIterator.OLICount,0,String.valueof(srIterator.DirectoryName), String.valueof(srIterator.CanvassID),String.valueof(srIterator.CanvassName), String.valueof(srIterator.TelcoName),String.valueof(''),String.valueof(srIterator.Edition),String.valueof(''),String.valueof(srIterator.BookStatus),Double.valueof('0.00'), String.valueof(''),String.valueof(srIterator.ProductType),String.valueof(srIterator.SpentStatus),Double.valueof(srIterator.TotalAmountSpend),Double.valueof('0.00'));
                       MapDirSR.put(String.valueof(srIterator.DirectoryName),arr);
                     }
                   }
                   if(srIterator.BookStatus =='BOTS-1' && srIterator.ProductType=='Print')
                   {
                     System.debug(srIterator.DirectoryName+'#########################'+MapDirSR.containskey(String.valueof(srIterator.DirectoryName)));
                     if(MapDirSR.containskey(String.valueof(srIterator.DirectoryName)))
                     {
                       MapDirSR.get(String.valueof(srIterator.DirectoryName)).Bot1Spend= Double.valueof(srIterator.TotalAmountSpend);
                       MapDirSR.get(String.valueof(srIterator.DirectoryName)).B1_Edition= srIterator.Edition;
                       MapDirSR.get(String.valueof(srIterator.DirectoryName)).B1_OLICount= srIterator.OLICount;
                     }
                      else
                     {
                       ArrangeSummaryReport arr= new ArrangeSummaryReport(0,0,0,srIterator.OLICount,String.valueof(srIterator.DirectoryName), String.valueof(srIterator.CanvassID),String.valueof(srIterator.CanvassName), String.valueof(srIterator.TelcoName),String.valueof(''),String.valueof(''),String.valueof(srIterator.Edition), String.valueof(srIterator.BookStatus),Double.valueof('0.00'), String.valueof(''),String.valueof(srIterator.ProductType),String.valueof(srIterator.SpentStatus),Double.valueof('0.00'),Double.valueof(srIterator.TotalAmountSpend));
                       MapDirSR.put(String.valueof(srIterator.DirectoryName),arr);
                     }             
                   }
                }
               for(String str: MapDirSR.keyset())
                {
                  System.debug('**********'+str);
                  ArrSummaryReport.add(MapDirSR.get(str));
                }
                
                for(SummaryReport srIterator : ListSR)
                {
                   if(srIterator.ProductType=='Digital')
                   {
                     ArrangeSummaryReport arr= new ArrangeSummaryReport(srIterator.OLICount,0,0,0,'Digital', String.valueof(srIterator.CanvassID),String.valueof(srIterator.CanvassName), String.valueof(srIterator.TelcoName),String.valueof(''),String.valueof(''),String.valueof(''), String.valueof(''),Double.valueof(srIterator.AmountSpend), String.valueof(''),String.valueof(srIterator.ProductType),String.valueof(srIterator.SpentStatus),Double.valueof('0.0'),Double.valueof('0.0'));
                     ArrSummaryReport.add(arr);
                     
                   }
                }
                if(ArrSummaryReport.size()>0)
                {
                   ArrSummaryReport.sort();
                }
                else
                {
                  ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Errors are not Found in Order Lien Items.');
                  ApexPages.addMessage(myMsg);
            
                }
             }
             else
            {
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: OrderLineItems are not Found.');
              ApexPages.addMessage(myMsg);
        
            }
    }
    else
    {
      ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: OrderLineItems are not Found.');
      ApexPages.addMessage(myMsg);

    }        
}

public void FetchDetailedInfo()
{
  AccountId=ApexPages.currentPage().getParameters().get('Id');
  String Producttype=ApexPages.currentPage().getParameters().get('ptype');
  FetchOLIs =new List<Order_Line_Items__c>();
  
  if(Producttype=='Print')
  {
    //https://c.cs11.visual.force.com/apex/AdvertisingSummaryDetail?id=001Z000000dsM1Z&ptype=Print&de=1412
    String DEdition=ApexPages.currentPage().getParameters().get('de');
    FetchOLIs =[SELECT Id,Name,UnitPrice__c,Product2__r.Product_type__c,UDAC__c,Directory__r.Pub_Date__c,Directory_Heading__c,Directory_Heading__r.Name, Directory_Heading__r.Directory_Heading_Name__c, Order_Line_Total__c,Payment_Duration__c,Publication_Date__c FROM Order_Line_Items__c where Account__c=:AccountId AND Directory_Edition__r.Edition_Code__c=:DEdition AND Product2__r.Media_Type__c='Print'];
    
  }
  else
  {
    
    //https://c.cs11.visual.force.com/apex/AdvertisingSummaryDetail?id=001Z000000dsM1Z&ptype=UA&can=a21Z0000000UEnU
    String Canvass=ApexPages.currentPage().getParameters().get('can');
    FetchOLIs =[SELECT Id,Name,UnitPrice__c,Product2__r.Product_type__c,UDAC__c,Directory__r.Pub_Date__c,Directory_Heading__c,Directory_Heading__r.Name, Directory_Heading__r.Directory_Heading_Name__c, Order_Line_Total__c,Payment_Duration__c,Publication_Date__c FROM Order_Line_Items__c where Account__c=:AccountId AND Product2__r.Media_Type__c!='Print' AND Canvass__c=:Canvass];
  }
}  
public class SummaryReport
{
 public String DirectoryName{get;set;}
 public Integer OLICount{get;set;}
 public String CanvassName {get;set;}
 public String TelcoName {get;set;}
 public String BookStatus{get;set;}
 public String Edition{get;set;}
 public Double AmountSpend{get;set;}
 public Decimal TotalAmountSpend{get;set;}
 public String SalesLockOut{get;set;}
 public String ProductType{get;set;}
 public String CanvassID{get;set;}
 public String SpentStatus{get;set;}
 public SummaryReport(Integer OLICount,String DirectoryName, String CanvassID,String CanvassName, String TelcoName, String Edition, String BookStatus,Double AmountSpend, String SalesLockOut,String ProductType,String SpentStatus)
 {
     this.OLICount=OLICount;
     this.DirectoryName =DirectoryName;
     this.CanvassID=CanvassID;
     this.CanvassName =CanvassName ;
     this.TelcoName =TelcoName ;
     this.Edition=Edition;
     this.BookStatus=BookStatus;
     this.AmountSpend=AmountSpend;
     this.SpentStatus=SpentStatus;
     if(AmountSpend==null)
     {
      AmountSpend=0.00;
     }
     this.TotalAmountSpend=Decimal.valueof(AmountSpend).setscale(2);
     this.SalesLockOut=SalesLockOut;
     this.ProductType=ProductType;
    // this.mapOLI=mapOLI;
     }
}

public class ArrangeSummaryReport implements comparable
{
 public String DirectoryName{get;set;}
 public Integer DIGCount{get;set;}
 public Integer NIOLICount{get;set;}
 public Integer B_OLICount{get;set;}
 public Integer B1_OLICount{get;set;}
 public String CanvassName {get;set;}
 public String TelcoName {get;set;}
 public String BookStatus{get;set;}
 public String NIEdition{get;set;}
 public String B_Edition{get;set;}
 public String B1_Edition{get;set;}
 public String SalesLockOut{get;set;}
 public String ProductType{get;set;}
 public String CanvassID{get;set;}
 public String SpentStatus{get;set;}
 public Double NISpend{get;set;}
 public Double BotSpend {get;set;}
 public Double Bot1Spend {get;set;}
 
 public ArrangeSummaryReport(Integer DIGCount,Integer NIOLICount,Integer B_OLICount,Integer B1_OLICount,String DirectoryName, String CanvassID,String CanvassName, String TelcoName, String NIEdition,String B_Edition,String B1_Edition , String BookStatus,Double NISpend, String SalesLockOut,String ProductType,String SpentStatus,Double BotSpend,Double Bot1Spend)
 {
     this.DIGCount=DIGCount;
     this.NIOLICount=NIOLICount;
     this.B_OLICount=B_OLICount;
     this.B1_OLICount=B1_OLICount;
     this.DirectoryName =DirectoryName;
     this.CanvassID=CanvassID;
     this.CanvassName =CanvassName ;
     this.TelcoName =TelcoName ;
     this.BookStatus=BookStatus;
     this.NISpend=NISpend;
     this.SpentStatus=SpentStatus;
     this.Bot1Spend=Bot1Spend;
     this.BotSpend=BotSpend;
     this.SalesLockOut=SalesLockOut;
     this.ProductType=ProductType;
     this.NIEdition=NIEdition;
     this.B_Edition=B_Edition;
     this.B1_Edition=B1_Edition;
     // this.mapOLI=mapOLI;
     }
     public Integer compareTo(Object ObjToCompare) 
     {
            if(((ArrangeSummaryReport)ObjToCompare).CanvassName!=null && CanvassName!=null)
            {
            return CanvassName.CompareTo(((ArrangeSummaryReport)ObjToCompare).CanvassName );
            }
            else
            {
            return -1;
            }
        }
}
}