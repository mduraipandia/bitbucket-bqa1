public with sharing class LocalBillingMaintenanceController {
	
	public String selectedType{get;set;}
	public Integer set1StartNum{get;set;}
	public Integer set1EndNum{get;set;}
	public Integer set1BatchSize{get;set;}  
	public Integer set1MinLineCount{get;set;}
	public Integer set1MaxLineCount{get;set;}	
	public Integer set2StartNum{get;set;}
	public Integer set2EndNum{get;set;}
	public Integer set2BatchSize{get;set;}
	public Integer set2MinLineCount{get;set;}
	public Integer set2MaxLineCount{get;set;}
	public Integer set3StartNum{get;set;}
	public Integer set3EndNum{get;set;}
	public Integer set3BatchSize{get;set;}
	public Integer set3MinLineCount{get;set;}
	public Integer set3MaxLineCount{get;set;}
	private static Set<String> priorRandoms;
	public Boolean show{get;set;}
	public StatementSplit stmntSplit{get;set;}
	public Boolean showStmnt{get;set;}
	
	public LocalBillingMaintenanceController(){
		show=false;
		showStmnt=false;
		stmntSplit=new StatementSplit();
		selectedType = 'First Month Print';
		loadValues();
		loadStatementValues();
	}
	
	public List<SelectOption> getTypes() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('First Month Print','First Month Print'));
		options.add(new SelectOption('Subsequent Print','Subsequent Print'));
		options.add(new SelectOption('Digital Local','Digital Local'));
		options.add(new SelectOption('Reconcile Statement','Reconcile Statement'));
		return options;
	}
	
	public void saveInvoiceRange(){
		List<SplitBatchJobs__c> splitBatchList=new List<SplitBatchJobs__c>();
		SplitBatchJobs__c spliBatchObj=null;
		String strName = '';
		if(set1StartNum!=null && set1EndNum!=null && set1StartNum!=0 && set1EndNum!=0){
			spliBatchObj=CommonMethods.createSplitBatchJob(set1StartNum,set1EndNum, CommonMethods.returnSettingsName(selectedType, '1'),set1BatchSize,set1MinLineCount,set1MaxLineCount,selectedType);
			splitBatchList.add(spliBatchObj);	  
		}
		if(set2StartNum!=null &&  set2EndNum!=null && set2StartNum!=0 && set2EndNum!=0){
			spliBatchObj=CommonMethods.createSplitBatchJob(set2StartNum,set2EndNum, CommonMethods.returnSettingsName(selectedType, '2'),set2BatchSize,set2MinLineCount,set2MaxLineCount,selectedType);
			splitBatchList.add(spliBatchObj);	
		}  
		if(set3StartNum!=null && set3EndNum!=null && set3StartNum!=0 && set3EndNum!=0){
			spliBatchObj=CommonMethods.createSplitBatchJob(set3StartNum,set3EndNum, CommonMethods.returnSettingsName(selectedType, '3'),set3BatchSize,set3MinLineCount,set3MaxLineCount,selectedType);
			splitBatchList.add(spliBatchObj);	
		}
		//Save SplitBatchJobs__c list in custom setting
		if(splitBatchList!=null && splitBatchList.size()>0){  
			upsert splitBatchList Name;
			show=true;
		}
	}
	
	
	public void setStmntSplitBatch(){
		List<StatementSplitBatch__c> stmntSplitList=new List<StatementSplitBatch__c>();
		StatementSplitBatch__c stmntSplitObj=null;
		if(stmntSplit.accStart1!=null && stmntSplit.accStart1!='' && stmntSplit.accEnd1!=null && stmntSplit.accEnd1!=''){
			stmntSplitObj=createStatementSplitRecs(stmntSplit.accStart1,stmntSplit.accEnd1,'1');
			stmntSplitList.add(stmntSplitObj);
		}
		if(stmntSplit.accStart2!=null && stmntSplit.accStart2!='' && stmntSplit.accEnd2!=null && stmntSplit.accEnd2!=''){
			stmntSplitObj=createStatementSplitRecs(stmntSplit.accStart2,stmntSplit.accEnd2,'2');
			stmntSplitList.add(stmntSplitObj);
		}
		if(stmntSplit.accStart3!=null && stmntSplit.accStart3!='' && stmntSplit.accEnd3!=null && stmntSplit.accEnd3!=''){
			stmntSplitObj=createStatementSplitRecs(stmntSplit.accStart3,stmntSplit.accEnd3,'3');
			stmntSplitList.add(stmntSplitObj);
		}
		if(stmntSplit.accStart4!=null && stmntSplit.accStart4!='' && stmntSplit.accEnd4!=null && stmntSplit.accEnd4!=''){
			stmntSplitObj=createStatementSplitRecs(stmntSplit.accStart4,stmntSplit.accEnd4,'4');
			stmntSplitList.add(stmntSplitObj);
		}
		
		if(stmntSplitList!=null && stmntSplitList.size()>0){
			upsert stmntSplitList Name;	
			showStmnt=true;
		}
	}
	
	public StatementSplitBatch__c createStatementSplitRecs(String accStart,String accEnd,String name){
		StatementSplitBatch__c stmntSplit=new StatementSplitBatch__c();
		stmntSplit.Account_Start_Number__c=accStart;
		stmntSplit.Account_End_Number__c=accEnd;
		stmntSplit.Name=name;
		return stmntSplit;
	}
	
	public void clearValue() {
		set1StartNum = null;
		set1EndNum = null;
		set2StartNum = null;
		set2EndNum = null;
		set3StartNum = null;
		set3EndNum = null;
		set1BatchSize=null;
		set1MinLineCount=null;
	 	set1MaxLineCount=null;
	 	set2BatchSize=null;
		set2MinLineCount=null;
	 	set2MaxLineCount=null;
	 	set3BatchSize=null;
		set3MinLineCount=null;
	 	set3MaxLineCount=null;
		show = false;
		showStmnt=false;
		stmntSplit=null;
		loadValues();
		loadStatementValues();
	}

	private void loadValues() {
		list<SplitBatchJobs__c> lstSplit = [SELECT Id, Name, Start_Number__c, End_Number__c, Type__c, Max_Line_Item_Count__c, Batch_Size__c, Min_Line_Item_Count__c FROM SplitBatchJobs__c where Type__c =:selectedType order by Name];
		for(SplitBatchJobs__c iterator : lstSplit) {
			if(iterator.Name.endsWith('1')) {
				set1StartNum = Integer.valueOf(iterator.Start_Number__c);
				set1EndNum = Integer.valueOf(iterator.End_Number__c);
				set1BatchSize = Integer.valueOf(iterator.Batch_Size__c);
				set1MinLineCount = Integer.valueOf(iterator.Min_Line_Item_Count__c);
	 			set1MaxLineCount = Integer.valueOf(iterator.Max_Line_Item_Count__c);
			}
			else if(iterator.Name.endsWith('2')) {
				set2StartNum = Integer.valueOf(iterator.Start_Number__c);
				set2EndNum = Integer.valueOf(iterator.End_Number__c);
				set2BatchSize = Integer.valueOf(iterator.Batch_Size__c);
				set2MinLineCount = Integer.valueOf(iterator.Min_Line_Item_Count__c);
	 			set2MaxLineCount = Integer.valueOf(iterator.Max_Line_Item_Count__c);
			}
			else if(iterator.Name.endsWith('3')) {
				set3StartNum = Integer.valueOf(iterator.Start_Number__c);
				set3EndNum = Integer.valueOf(iterator.End_Number__c);
				set3BatchSize = Integer.valueOf(iterator.Batch_Size__c);
				set3MinLineCount = Integer.valueOf(iterator.Min_Line_Item_Count__c);
	 			set3MaxLineCount = Integer.valueOf(iterator.Max_Line_Item_Count__c);
			}
		}
	}

	private void loadStatementValues() {
		stmntSplit = new StatementSplit();
		list<StatementSplitBatch__c> lstSplitStatement = [SELECT Id, Name, Account_End_Number__c, Account_Start_Number__c FROM StatementSplitBatch__c order by Name];
		for(StatementSplitBatch__c iterator : lstSplitStatement) {
			if(String.isNotEmpty(iterator.Name)) {
				if(iterator.Name.equals('1')) {
					stmntSplit.accStart1 = iterator.Account_Start_Number__c;
					stmntSplit.accEnd1 = iterator.Account_End_Number__c;
				}
				else if(iterator.Name.equals('2')) {
					stmntSplit.accStart2 = iterator.Account_Start_Number__c;
					stmntSplit.accEnd2 = iterator.Account_End_Number__c;
				}
				else if(iterator.Name.equals('3')) {
					stmntSplit.accStart3 = iterator.Account_Start_Number__c;
					stmntSplit.accEnd3 = iterator.Account_End_Number__c;
				}
			}
		}
	}
	
	 public static String generateRandomString(Integer length){
        if(priorRandoms == null)
            priorRandoms = new Set<String>(); 

        if(length == null) length = 1+Math.round( Math.random() * 8 );
        String characters = 'abcdefghijklmnopqrstuvwxyz1234567890';
        String returnString = '';
        while(returnString.length() < length){
            Integer charpos = Math.round( Math.random() * (characters.length()-1) );
            returnString += characters.substring( charpos , charpos+1 );
        }
        if(priorRandoms.contains(returnString)) {
            return 'a'+generateRandomString(length);
        } else {
            priorRandoms.add(returnString);
            return 'a'+returnString;
        }
    }
    
    //Wrapper class that holds Statement split batch configure variables
    public class StatementSplit{
    	public String accStart1{get;set;}
    	public String accEnd1{get;set;}
    	public String accStart2{get;set;}
    	public String accEnd2{get;set;}
    	public String accStart3{get;set;}
    	public String accEnd3{get;set;}
    	public String accStart4{get;set;}
    	public String accEnd4{get;set;}
    }

}