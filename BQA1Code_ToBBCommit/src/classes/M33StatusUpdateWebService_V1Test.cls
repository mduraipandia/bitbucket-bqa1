@isTest(seeAllData = True)
public class M33StatusUpdateWebService_V1Test {
    public static testMethod void M33StatusUpdateWebService_V1Test01() {

        Test.startTest();
        Account objAcc = TestMethodsUtility.generateAccount('customer');
        objAcc.name = 'Test123';
        insert objAcc;
        Spec_Art_Request__c sff = new Spec_Art_Request__c(Account__c = objAcc.Id, Miles_Status__c = 'Complete', Spec_Art_URL__c = 'http://10.48.10.35/webprod4/getpdf.aspx?path=(0)');
        insert sff;

        String sffId = sff.id;

        M33StatusUpdateWebService_V1.updateStatus(null, 'S000240', 'Complete', 'http://67.211.229.244/LoResCopy/8031776.gif');

        M33StatusUpdateWebService_V1 MSU = new M33StatusUpdateWebService_V1();
        new M33StatusUpdateWebService_V1.M33StatusUpdateResponse();


        Test.stopTest();
    }

    public static testMethod void M33StatusUpdateWebService_V1NegTest01() {

        Test.startTest();
        Account objAcc = TestMethodsUtility.generateAccount('customer');
        objAcc.name = 'Test123';
        insert objAcc;
        List < Spec_Art_Request__c > Lstartreqs22 = new List < Spec_Art_Request__c > ();

        Spec_Art_Request__c sff2 = new Spec_Art_Request__c(Account__c = objAcc.Id, Miles_Status__c = 'Complete', Spec_Art_URL__c = 'http://10.48.10.35/webprod4/getpdf.aspx?path=(0)');
        Lstartreqs22.add(sff2);
        insert Lstartreqs22;

        M33StatusUpdateWebService_V1.updateStatus(Lstartreqs22[0].id, 'S000240', 'Complete', 'http://10.48.10.35/webprod4/getpdf.aspx?path=(0)');
        M33StatusUpdateWebService_V1 MSU = new M33StatusUpdateWebService_V1();
        new M33StatusUpdateWebService_V1.M33StatusUpdateResponse();


        Test.stopTest();
    }

    public static testMethod void M33StatusUpdateWebService_V1Test02() {

        Test.startTest();

        Account objAcc = TestMethodsUtility.generateAccount('customer');
        objAcc.name = 'Test123';
        insert objAcc;
        Spec_Art_Request__c sff = new Spec_Art_Request__c(Account__c = objAcc.Id);
        insert sff;

        Digital_Product_Requirement__c objDFF = TestMethodsUtility.generatePrintGraphic();
        Insert objDFF;
        String sffId = sff.id;
        List < YPC_Graphics__c > ListYff = new list < YPC_Graphics__c > ();
        YPC_Graphics__c YFF = new YPC_Graphics__c(DFF__c = objDFF.Id, Miles_Status__c = 'Complete', YPC_Graphics_URL__c = 'http://67.211.229.244/LoResCopy/8031776.gif');
        insert YFF;
        ListYff.add(YFF);
        String yffId = YFF.id;


        M33StatusUpdateWebService_V1.updateStatus(ListYff[0].Id, 'Test', 'Complete', 'http://67.211.229.244/LoResCopy/8031776.gif');
        Test.stopTest();
    }



    public static testMethod void M33StatusUpdateWebService_V1NegTest02() {

        Test.startTest();

        Account objAcc = TestMethodsUtility.generateAccount('customer');
        objAcc.name = 'Test123';
        insert objAcc;
        Spec_Art_Request__c sff = new Spec_Art_Request__c(Account__c = objAcc.Id);
        insert sff;

        Digital_Product_Requirement__c objDFF = TestMethodsUtility.generatePrintGraphic();
        Insert objDFF;
        String sffId = sff.id;
        List < YPC_Graphics__c > ListYff = new list < YPC_Graphics__c > ();
        YPC_Graphics__c YFF = new YPC_Graphics__c(DFF__c = objDFF.Id, Miles_Status__c = 'Complete', YPC_Graphics_URL__c = 'http://67.211.229.244/LoResCopy/8031776.gif');
        insert YFF;
        ListYff.add(YFF);
        String yffId = YFF.id;


        M33StatusUpdateWebService_V1.updateStatus(null, 'Test', 'Complete', 'http://67.211.229.244/LoResCopy/8031776.gif');
        Test.stopTest();
    }
    public static testMethod void M33StatusUpdateWebService_V1Test03() {
        Test.startTest();

        String sffId = null;
        List < Digital_Product_Requirement__c > ListOTPID = [Select Test_PackageId__c, URN_Number__c, Miles_Status__c, Print_Ad_URL__c from Digital_Product_Requirement__c where Id = : sffId];
        insert ListOTPID;
        M33StatusUpdateWebService_V1.updateStatus(null, '200000', 'Complete', 'http://67.211.229.244/LoResCopy/8031776.gif');

        Test.stopTest();
    }
    public static testMethod void M33StatusUpdateWebService_V1NegTest03() {

        Test.startTest();
        Account objAcc = TestMethodsUtility.generateAccount('customer');
        objAcc.name = 'Test123';
        insert objAcc;
        Spec_Art_Request__c sff = new Spec_Art_Request__c(Account__c = objAcc.Id);
        insert sff;

        Digital_Product_Requirement__c objDFF = TestMethodsUtility.generatePrintGraphic();
        Insert objDFF;
        String sffId = sff.id;
        List < Digital_Product_Requirement__c > lstDFF2 = new List < Digital_Product_Requirement__c > ();
        Digital_Product_Requirement__c dff1 = new Digital_Product_Requirement__c(

            URN_Number__c = 2.0,
            URN_text__c = '11000000025',
            Miles_Status__c = 'Complete',
            Print_Ad_URL__c = 'http://10.48.10.35/webprod4/getpdf.aspx?path=(0)');
        lstDFF2.add(dff1);

        Digital_Product_Requirement__c dff2 = new Digital_Product_Requirement__c(
            URN_Number__c = 2.0,
            URN_text__c = '11000000025',
            Miles_Status__c = 'Complete',
            Print_Ad_URL__c = 'http://10.48.10.35/webprod4/getpdf.aspx?path=(0)');
        lstDFF2.add(dff2);

        insert lstDFF2;
        List < YPC_Graphics__c > ListYff1 = new list < YPC_Graphics__c > ();
        YPC_Graphics__c YFF1 = new YPC_Graphics__c(DFF__c = lstDFF2[0].Id, Miles_Status__c = 'Complete', YPC_Graphics_URL__c = 'http://67.211.229.244/LoResCopy/8031776.gif');
        insert ListYff1;
        ListYff1.add(YFF1);
       M33StatusUpdateWebService_V1.updateStatus(dff1.Id, '200000', 'Complete', 'http://67.211.229.244/LoResCopy/8031776.gif');
       Test.stopTest();
    }




}