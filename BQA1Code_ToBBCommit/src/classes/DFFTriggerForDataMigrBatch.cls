global class DFFTriggerForDataMigrBatch implements Database.Batchable<sObject>{
    
    global Boolean bolStopExecution;
    
    global DFFTriggerForDataMigrBatch(){
        
    }
    
    global DFFTriggerForDataMigrBatch(Boolean bolStopExec){
        bolStopExecution = bolStopExec;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        //List<string> setMigrationUserID = System.Label.MigrationUserID.split(',');
        String media='Digital';
        String SOQL = CommonMethods.getCreatableFieldsSOQL('Digital_Product_Requirement__c');
        SOQL+='where Core_Opportunity_Line_ID__c!=null and DM_isTriggerExecuted__c=false  and Media_Type__c =:media order by Fulfillment_Profile__c';        
        System.debug('Testingg SOQL '+SOQL);
        return Database.getQueryLocator(SOQL);
    }
    
    global void execute(Database.BatchableContext bc, List<Digital_Product_Requirement__c> lstDFF) {
        Savepoint sp = Database.setSavepoint();
        try{
            Set<Id> lstFPId = new Set<Id>();
            List<Fulfillment_Profile__c> lstFP = new List<Fulfillment_Profile__c>();
            Map<id, List<Digital_Product_Requirement__c>> mapFPIdDFF = new Map<id, List<Digital_Product_Requirement__c>>();
            Map<String,String> mapDFFFields = CommonMethods.fetchDFFFields();
            for(Digital_Product_Requirement__c objDFF : lstDFF) {    
                if(!mapFPIdDFF.containsKey(objDFF.Fulfillment_Profile__c)) {
                    mapFPIdDFF.put(objDFF.Fulfillment_Profile__c, new List<Digital_Product_Requirement__c>());
                    lstFPId.add(objDFF.Fulfillment_Profile__c);
                }
                mapFPIdDFF.get(objDFF.Fulfillment_Profile__c).add(objDFF);
            }
            Fulfillment_Profile__c newFP;
            List<Digital_Product_Requirement__c> lstUpdtDFF = new List<Digital_Product_Requirement__c>();
            String type='Fulfillment_Profile__c';
            Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            Schema.SObjectType objFPSchema = schemaMap.get(type);
            Map<String, Schema.SObjectField> fieldMap = objFPSchema.getDescribe().fields.getMap();
            Schema.DisplayType fielddataType;
            for(Id idFP : lstFPId) {
                newFP = new Fulfillment_Profile__c(id = idFP);
                for(Digital_Product_Requirement__c objDFF : mapFPIdDFF.get(idFP)) {
                    newFP = CommonMethods.copyValuesFromDFFToFProfile(objDFF,newFP,mapDFFFields);
                    
                    //public static Fulfillment_Profile__c copyValuesFromDFFToFProfile(Digital_Product_Requirement__c objDFF, Fulfillment_Profile__c objProfile, Map < String, String > mapDFFFields) {
                    if (newFP != null && objDFF != null) {
                        String strDFFFields = mapDFFFields.get(objDFF.DFF_RecordType_Name__c);
                        string[] strArrDFFFields;
                        if (String.isNotBlank(strDFFFields)) {
                            strArrDFFFields = strDFFFields.split(',');
                            for (String strField: strArrDFFFields) {
                                fielddataType = fieldMap.get(strField).getDescribe().getType();
                                if(newFP.get(strField)!=objDFF.get(strField) && (fielddataType == Schema.DisplayType.Boolean || objDFF.get(strField) != null)){
                                    newFP.put(strField, objDFF.get(strField));
                                    System.debug('************Field From DFF To FP************' + strField + '************Value************' + objDFF.get(strField));
                                }
                            }
                        }
                    }
                    //return objProfile;
                //}
    
    
                    lstUpdtDFF.add(new Digital_Product_Requirement__c(id = objDFF.id, DM_isTriggerExecuted__c = true));
                }
                lstFP.add(newFP);            
            }
            update lstFP;
            update lstUpdtDFF;
        }catch(Exception e){
            System.debug('The exception is'+e);
            Database.rollback(sp);
            futureCreateErrorLog.createErrorRecordBatch('Error Type : '+e.getTypename()+'. Error Message : '+e.getMessage(), e.getStackTraceString(), 'Batch update of migrated DFF');
        }
    }
    
    global void finish(Database.BatchableContext bc) {
        if(bolStopExecution != true) {
            DFFTriggerForDataMigrBatch objBatch = new DFFTriggerForDataMigrBatch(true);
            Database.executeBatch(objBatch, 500);
        }
    }
}