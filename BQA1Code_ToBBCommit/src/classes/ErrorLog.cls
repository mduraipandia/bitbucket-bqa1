/****************************************************************
Apex class to create an error log records for tracking exceptions
Created By: Reddy(pyellanki2@csc.com)
Updated Date: 10/13/2014
$Id$
*****************************************************************/
public class ErrorLog{
    
    public static void CreateErrorLog(String errormessage,Id failedId,integer statuscode,string status){
            
        ErrorLog__c err=new ErrorLog__c();
        err.Error_Message__c=errormessage;
        err.FailedSFDCID__c=failedId;
        err.ResponseHeader__c=String.ValueOf(statuscode)+'----####----####----'+status;
        insert err; 
        }
        
        @IsTest(SeeAllData=true)
        public static void ErrorLogTest(){
        string status = 'Test Status';
        Integer statuscode = 200;
        string errormessage = 'Test Errormsg';
        Id failedId = '01pZ0000000DhFj';
        String ObjectName;
        
        ErrorLog.CreateErrorLog(errormessage, failedId, statuscode, status);
        
        }        

}