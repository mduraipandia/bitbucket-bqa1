public class CustomException extends Exception{

    public Integer intStatusCode;
    public String strStackTrace;
    public String strType;
    public String strRecordIds;
    public Exception objExp;
    public void CustomException(){}
    
    public CustomException (final string message, final Integer intStatusCode) {
        // Pass 'message' to the base class
        this.setMessage(message);
        this.intStatusCode = intStatusCode;
    }
    public CustomException (final string message, final String strStackTrace) {
        // Pass 'message' to the base class
        this.setMessage(message);
        this.strStackTrace = strStackTrace;
    }
    public CustomException (final string message, final String strStackTrace, Exception objExp) {
        // Pass 'message' to the base class
        this.setMessage(message);
        this.strStackTrace = strStackTrace;
        this.objExp = objExp;
    }
    public CustomException (final string message, final String strType, final string strStackTrace) {
        // Pass 'message' to the base class
        this.setMessage(message);
        this.strStackTrace = strStackTrace;
        this.strType = strType;
    }
    
    public CustomException (final string message, final String strType, final string strStackTrace, final string strRecordIds) {
        // Pass 'message' to the base class
        this.setMessage(message);
        this.strStackTrace = strStackTrace;
        this.strType = strType;
        this.strRecordIds = strRecordIds;
    }
    // Test Methods
  @IsTest(SeeAllData=true)
  static void CustomExceptionTest() {
    Test.startTest();
    CustomException CE = New CustomException();
        CE.CustomException();
        new CustomException('unit test', 1024);
        new CustomException ('unit test', 'unit test');
        new CustomException ('unit test', 'unit test', 'unit test'); 
        new CustomException ('unit test', 'unit test', 'unit test', 'unit test');
       // new CustomException ('unit test', 'unit test' , new Exception());
        
        
        
    Test.stopTest();
  }  
    
    
}