global class BillingFirstMonthPrintSchedulerHndlr implements BillingFirstMonthPrintScheduler.BillingFirstMonthPrintSchedulerInterface {
    global void execute(SchedulableContext SC) {
        Database.executeBatch(new PrintFirstMonthBilling(system.today(), 'FMP'), 20);
    }   
}