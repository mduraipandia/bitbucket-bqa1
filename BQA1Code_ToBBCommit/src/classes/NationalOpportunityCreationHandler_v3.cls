public class NationalOpportunityCreationHandler_v3 {

    public static void NationalOrderProcessByManually(set<Id> setNSOSId) {
        list<National_Staging_Order_Set__c> lstNSOS = NationalStagingOrderSetSOQLMethods.getNationalStagingOrderSetWithOpportunity(setNSOSId);
        //System.debug('Testingg NationalOrderProcessByManually');
        createNationalOpportunity(lstNSOS);
    }
    
    public static void NationalOrderProcessByCron() {
        list<National_Staging_Order_Set__c> lstNSOS = NationalStagingOrderSetSOQLMethods.getNationalStagingOrderSetWithOpportunity();
        createNationalOpportunity(lstNSOS);
    }
    
    private static void createNationalOpportunity(list<National_Staging_Order_Set__c> lstNSOS) {
        //System.debug('Testingg lstNSOS '+lstNSOS);
        if(lstNSOS.size() > 0) {
            set<Id> setNationalOSId = new set<Id>();
            set<Id> setAccount = new set<Id>();
            Integer iCount = 0;
            Id nationalOPPRTID = CommonMethods.getRedordTypeIdByName(CommonMessages.opportunityNationalRT, CommonMessages.opportunityObjectName);
            for(National_Staging_Order_Set__c iterator : lstNSOS) {
                setNationalOSId.add(iterator.Id);
                setAccount.add(iterator.CMR_Name__c);
                //System.debug('Testingg 1 setNationalOSId '+setNationalOSId);
            }
             
            if(setNationalOSId.size() > 0) {
                //System.debug('Testingg 1.1');
                list<National_Staging_Line_Item__c> lstNSLI = NationalStagingLineItemSOQLMethods.getNationalStagingLineItemWithOPPLineItem(setNationalOSId);
                list<National_Staging_Line_Item__c> lstNSLIRef = NationalStagingLineItemSOQLMethods.getNationalStagingLineItemReference(setNationalOSId);
                //System.debug('Testingg 1lstNSLI '+lstNSLI);
                if(lstNSLI.size() > 0) {
                    //System.debug('Testingg 1.2');
                    map<Id, list<National_Staging_Line_Item__c>> mapNSLIByParent = new map<Id, list<National_Staging_Line_Item__c>>();
                    set<Id> setProductId = new set<Id>();
                    set<Id> setProductIdforDeletion = new set<Id>();
                    set<Id> setNSOSXTrans=new set<Id>();
                    for(National_Staging_Line_Item__c iterator : lstNSLI) {
                        //System.debug('Testingg 1.3');
                        if(iterator.Product2__c != null) {
                            setProductId.add(iterator.Product2__c);
                        }
                        if((iterator.National_Staging_Header__r.IsMigratedXtrans__c && iterator.National_Staging_Header__r.TRANS_Code__c=='X') || iterator.National_Staging_Header__r.RecordType.Name=='Manual NS RT'){
                            setNSOSXTrans.add(iterator.Id);
                        }
                        if(!mapNSLIByParent.containsKey(iterator.National_Staging_Header__c)) {
                            //System.debug('Testingg 1.5');
                            mapNSLIByParent.put(iterator.National_Staging_Header__c, new list<National_Staging_Line_Item__c>());
                        }
                        mapNSLIByParent.get(iterator.National_Staging_Header__c).add(iterator);
                    }
                    System.debug('Testingg 2========>'+mapNSLIByParent);
                    if(mapNSLIByParent.size() > 0) {
                    //System.debug('Testingg 3');
                        map<Id, Id> mapPBEID = new map<Id, Id>();
                        if(setProductId.size() > 0) {
                            mapPBEID = getPriceBookEntryById(setProductId);
                        }
                        if(mapPBEID.size() > 0) {
                            list<Opportunity> lstUpsertOpp = new list<Opportunity>();
                            map<Integer, list<OpportunityLineItem>> mapUpsertOppLineItem = new map<Integer, list<OpportunityLineItem>>();
                            map<Id,OpportunityLineItem> deletionMapNSLIOPPLI=new map<Id,OpportunityLineItem>();
                            //System.debug('Testingg 4');
                            for(National_Staging_Order_Set__c iterator : lstNSOS) {
                                iCount++;
                                lstUpsertOpp.add(generateOpportunity(iterator, iCount, nationalOPPRTID));
                                //System.debug('Testingg 5');
                                if(mapNSLIByParent.get(iterator.Id) != null) {
                                    for(National_Staging_Line_Item__c iteratorChild : mapNSLIByParent.get(iterator.Id)) {
                                        
                                        if(iteratorChild.Is_UDAC_Changed__c && iteratorChild.Opportunity_Product__r.size()>0){
                                           deletionMapNSLIOPPLI.put(iteratorChild.Id,iteratorChild.Opportunity_Product__r[0]);
                                        }
                                        if(!mapUpsertOppLineItem.containsKey(iCount)) {
                                                mapUpsertOppLineItem.put(iCount, new list<OpportunityLineItem>());
                                            }
                                        mapUpsertOppLineItem.get(iCount).add(generateOpportunityLineItem(iterator, iteratorChild, mapPBEID.get(iteratorChild.Product2__c)));
                                        //System.debug('Testingg 6');
                                    }
                                }
                            }
                            
                            if(lstUpsertOpp.size() > 0) {
                                System.debug('Testingg 7'+mapUpsertOppLineItem);
                                upsert lstUpsertOpp;
                                set<Id> setOpportunity = new set<id>();
                                list<OpportunityLineItem> lstUpsertOPPLI = new list<OpportunityLineItem>();
                                list<National_Staging_Order_Set__c> lstUpdateNSOS = new list<National_Staging_Order_Set__c>();
                                list<National_Staging_Line_Item__c> lstUpdateNSLI = new list<National_Staging_Line_Item__c>();
                                for(Opportunity iterator : lstUpsertOpp) {
                                    setOpportunity.add(iterator.Id);
                                    lstUpdateNSOS.add(new National_Staging_Order_Set__c(Id = iterator.National_Staging_Order_Set__c, Is_Converted__c = true,Header_Error_Description__c=''));
                                    if(mapUpsertOppLineItem.get(Integer.valueOf(iterator.Auto_Number__c)) != null) {
                                        boolean xTRANSFlag=true;
                                        System.debug('####mapUpsertOppLineItem#######'+mapUpsertOppLineItem.get(Integer.valueOf(iterator.Auto_Number__c)));
                                        for(OpportunityLineItem iteratorOppLI : mapUpsertOppLineItem.get(Integer.valueOf(iterator.Auto_Number__c))) {
                                            if(iteratorOppLI.Id == null) {
                                                iteratorOppLI.OpportunityId = iterator.Id;
                                            }
                                            lstUpsertOPPLI.add(iteratorOppLI);
                                           if(setNSOSXTrans.size()>0 && setNSOSXTrans.contains(iteratorOppLI.National_Staging_Line_Item__c)){
                                               xTRANSFlag=false;
                                           }
                                           // lstUpdateNSLI.add(new National_Staging_Line_Item__c(Id = iteratorOppLI.National_Staging_Line_Item__c, Is_Changed__c = false, Is_Processed__c = true, New_Transaction__c = false,Line_Error_Description__c= ''));
                                           lstUpdateNSLI.add(new National_Staging_Line_Item__c(Id = iteratorOppLI.National_Staging_Line_Item__c, Is_Changed__c = false, Is_Processed__c = true,New_Transaction__c = xTRANSFlag, Line_Error_Description__c= '',Prior_UDAC_Group__c='',Prior_Directory_Heading_SFDC_ID__c=''));
                                        }
                                    }
                                }
                                System.debug(setOpportunity+'===OPPLI@@@@=====>'+[Select count() from OpportunityLineItem where OpportunityId in:setOpportunity]);     
        
                                if(lstNSLIRef.size() > 0) {
                                    boolean xTRANSFlag=true;
                                    for(National_Staging_Line_Item__c iterator : lstNSLIRef) {
                                      if((iterator.National_Staging_Header__r.IsMigratedXtrans__c && iterator.National_Staging_Header__r.TRANS_Code__c=='X') || iterator.National_Staging_Header__r.RecordType.Name=='Manual NS RT'){
                                               xTRANSFlag=false;
                                      }
                                      iterator.New_Transaction__c = xTRANSFlag;
                                      iterator.Line_Error_Description__c='';
                                      lstUpdateNSLI.add(iterator);
                                    }
                                }
                                System.debug(setOpportunity+'===OPPLI@@@@=====>'+[Select count() from OpportunityLineItem where OpportunityId in:setOpportunity]);     
        
                                //System.debug('####deletionMapNSLIOPPLI####'+deletionMapNSLIOPPLI);
                                if(deletionMapNSLIOPPLI.size()>0){
                                    delete deletionMapNSLIOPPLI.values();
                                }
                                if(lstUpsertOPPLI.size() > 0) {
                                    System.debug('Testingg OPPLI=='+lstUpsertOPPLI);
                                    upsert lstUpsertOPPLI;
                                    System.debug(setOpportunity+'===OPPLI@@@@=====>'+[Select count() from OpportunityLineItem where OpportunityId in:setOpportunity]);     
        
                                }
                                System.debug(setOpportunity+'===OPPLI@@@@=====>'+[Select count() from OpportunityLineItem where OpportunityId in:setOpportunity]);     
        
                                //Updating OppLI with appropriate TrademarkParentId
                                map<Id,Id> MapNSLIOPLI =new map<id,id>();
                                list<OpportunityLineItem> lstUpdateOPPLI = new list<OpportunityLineItem>();
                                for(OpportunityLineItem  oppLi : lstUpsertOPPLI){
                                    MapNSLIOPLI.put(oppLi.National_Staging_Line_Item__c,oppLi.Id); 
                                }
                                for(OpportunityLineItem  oppLi : lstUpsertOPPLI){
                                    //System.debug('Price Book Entry ID : '+ oppLi.PricebookEntryId);
                                    //System.debug('NSLI ID : '+ oppLi.National_Staging_Line_Item__c);
                                   if(oppLi.Trade_Mark_Parent_Id__c!=null){                                   
                                       if(MapNSLIOPLI.containskey(oppLi.Trade_Mark_Parent_Id__c)){
                                           oppLi.Trade_Mark_Parent_Id__c= MapNSLIOPLI.get(oppLi.Trade_Mark_Parent_Id__c);
                                           lstUpdateOPPLI.add(oppLi);
                                       }
                                   }
                                }
                                System.debug(setOpportunity+'===OPPLI@@@@=====>'+[Select count() from OpportunityLineItem where OpportunityId in:setOpportunity]);     
        
                                System.debug('lstUpdateOPPLI=====>'+lstUpdateOPPLI);     
        
                                if(lstUpdateOPPLI.size()>0) update lstUpdateOPPLI;
                                //Creating Order/Order Set/Order Line Item
                                if(setAccount.size() > 0 && setOpportunity.size() > 0) {
                                    System.debug(setOpportunity+'===OPPLI@@@@=====>'+[Select count() from OpportunityLineItem where OpportunityId in:setOpportunity]);     
        
                                    NationalOrderCreationHandler_V2.createNationalOrder(setOpportunity, setAccount);
                                }
                                
                                if(lstUpdateNSOS.size() > 0) {
                                    update lstUpdateNSOS;
                                }
                                
                                if(lstUpdateNSLI.size() > 0) {
                                    update lstUpdateNSLI;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    private static map<Id, Id> getPriceBookEntryById(set<Id> setProductID) {
        list<PricebookEntry> lstPriceBookEntry = PriceBookEntrySOQLMethods.getPriceBookEntryByProductIDandIsActive(setProductID);
        map<Id, Id> mapUDAC = new map<Id, Id>();
        for(PricebookEntry iterator : lstPriceBookEntry) {
            mapUDAC.put(iterator.Product2Id, iterator.Id);
        }
        return mapUDAC;
    }
    
    private static Opportunity generateOpportunity(National_Staging_Order_Set__c objNSOS, Integer iCount, ID nationalOPPRTID) {
        Opportunity objOpportunity = new Opportunity(AccountId = objNSOS.CMR_Name__c, Auto_Number__c = iCount, StageName = CommonMessages.closedOwnStageOpp, 
            Name = objNSOS.CMR_Name__r.Name +' ' + System.today(), CloseDate = System.today(), Pricebook2Id = system.label.PricebookId,
            RecordTypeId = nationalOPPRTID, National_Staging_Order_Set__c = objNSOS.Id, Transaction_Unique_ID__c = objNSOS.Transaction_Unique_ID__c,Account_Primary_Canvass__c=objNSOS.Directory__r.Canvass__c);
        
        if(objNSOS.Opportunities__r.size() > 0) {
            for(Opportunity iterator : objNSOS.Opportunities__r) {
                objOpportunity.Id = iterator.Id;
            }
        }
        return objOpportunity;
    }
    
    private static OpportunityLineItem generateOpportunityLineItem(National_Staging_Order_Set__c objNSOS, National_Staging_Line_Item__c objNSLI, Id pbeID) {
        OpportunityLineItem objOPPLI = new OpportunityLineItem(Transaction_ID__c = objNSOS.Transaction_ID__c, Trans__c = objNSOS.TRANS_Code__c, 
        Trans_Version__c = objNSOS.Transaction_Version__c, 
        Publication_Date__c = objNSOS.Publication_Date__c,Listing__c= objNSLI.Listing__c, 
        Directory__c = objNSOS.Directory__c, Directory_Edition__c = objNSOS.Directory_Edition__c, Directory_Section__c = objNSLI.Directory_Section__c, 
        Directory_Heading__c = objNSLI.Directory_Heading__c, 
        Seniority_Date__c= objNSLI.Seniority_Date__c,
        Client_Number__c = objNSOS.Client_Number__c, Client_Name__c = objNSOS.Client_Name__c, CMR_Number__c = objNSOS.CMR_Number__c, 
        CMR_Name__c = objNSOS.CMR_Name__c, 
        NAT__c = objNSOS.NAT__c, NAT_Client_ID__c = objNSOS.NAT_Client_Id__c,Discount= objNSLI.National_Discount__c,
        Line_Number__c = objNSLI.Line_Number__c, PricebookEntryId = pbeID, Action__c = objNSLI.Action__c, DAT__c = objNSLI.DAT__c,
        BAS__c = objNSLI.BAS__c, SPINS__c = objNSLI.SPINS__c, Advertising_Data__c = objNSLI.Advertising_Data__c, 
        Quantity = 1.0,  Type__c = objNSLI.Type__c, UnitPrice = objNSLI.Sales_Rate__c, Full_Rate__c = objNSLI.Full_Rate_f__c,
        National_Staging_Line_Item__c = objNSLI.Id, Line_Unique_Id__c = objNSLI.Transaction_Line_Id__c,Trade_Mark_Parent_Id__c=objNSLI.TradeMark_Parent_Id__c,
        OP_National_Graphics_ID__c = objNSLI.National_Graphics_ID__c);
        if(!objNSLI.Is_UDAC_Changed__c){
            if(objNSLI.Opportunity_Product__r.size() > 0) {
                for(OpportunityLineItem iterator : objNSLI.Opportunity_Product__r) {
                    objOPPLI.Id = iterator.Id;
                }
            }
        }
        return objOPPLI;
    }
}