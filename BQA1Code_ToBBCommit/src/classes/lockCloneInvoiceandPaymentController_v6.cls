public with sharing class lockCloneInvoiceandPaymentController_v6 {
    String opportunityID;
    String terminalURL;
    String strPaymentId;
    
    public lockCloneInvoiceandPaymentController_v6(ApexPages.StandardController controller) {
        opportunityID = ApexPages.currentPage().getParameters().get('Id');
    }
    
    public lockCloneInvoiceandPaymentController_v6(Id opptyId) {
    	opportunityID = opptyId;
    }
    
    public Pagereference showTerminal() {
        Opportunity objOpportunity = OpportunitySOQLMethods.setOpportunityByIdForLockCloneInvoicePayment(new set<Id>{opportunityID})[0];
        list<OpportunityLineItem> lstOPPLIP4P = new list<OpportunityLineItem>();
        
        //Account & Billing contact address validation
        boolean bFlag = false;
        if(!Test.isRunningTest()) {
            if(CommonMethods.checkOpportunityandBMIQuoteBeforeCreateOrder(objOpportunity, null)) {
                bFlag = true;
            }
        }
        else {
             bFlag = true;
        }
        
        if(bFlag == true) {
            splitOpportunityProduct(objOpportunity, lstOPPLIP4P);
            system.debug('P4P Line : ' + lstOPPLIP4P);
            //Check for P4P Item or not
            if(objOpportunity.modifyid__c == null) {
                if(objOpportunity.OpportunityLineItems.size() == lstOPPLIP4P.size()) {
                    validateP4PBillingContactPaymentMethod(objOpportunity.Billing_Contact__c, objOpportunity);
                }
                else {
                    doPaymentProcess(objOpportunity);
                }
            }
            system.debug('Payment ID : ' + strPaymentId);
            redirectToCreateOrder(objOpportunity);
            system.debug('Terminal URL : ' + terminalURL);
            PageReference pr = new PageReference(terminalURL);
            pr.setRedirect(true);
            return pr;
        }
        return null;
    }
    
    public Pagereference showTerminalForOpptyVal() {
        Opportunity objOpportunity = OpportunitySOQLMethods.setOpportunityByIdForLockCloneInvoicePayment(new set<Id>{opportunityID})[0];
        list<OpportunityLineItem> lstOPPLIP4P = new list<OpportunityLineItem>();
       
        splitOpportunityProduct(objOpportunity, lstOPPLIP4P);
        system.debug('P4P Line : ' + lstOPPLIP4P);
        //Check for P4P Item or not
        if(objOpportunity.modifyid__c == null) {
            if(objOpportunity.OpportunityLineItems.size() == lstOPPLIP4P.size()) {
                validateP4PBillingContactPaymentMethod(objOpportunity.Billing_Contact__c, objOpportunity);
            }
            else {
                doPaymentProcess(objOpportunity);
            }
        }
        system.debug('Payment ID : ' + strPaymentId);
        redirectToCreateOrder(objOpportunity);
        system.debug('Terminal URL : ' + terminalURL);
        PageReference pr = new PageReference(terminalURL);
        pr.setRedirect(true);
        return pr;
    }
    
    public Pagereference showTerminalNew() {
        Opportunity objOpportunity = OpportunitySOQLMethods.setOpportunityByIdForLockCloneInvoicePayment(new set<Id>{opportunityID})[0];
        list<OpportunityLineItem> lstOPPLIP4P = new list<OpportunityLineItem>();
        
        //Account & Billing contact address validation
        boolean bFlag = false;
        if(!Test.isRunningTest()) {
            if(CommonMethods.checkOpportunityandBMIQuoteBeforeCreateOrder(objOpportunity, null)) {
                bFlag = true;
            }
        }
        else {
             bFlag = true;
        }
        
        if(bFlag == true) {
            doPaymentProcessNew(objOpportunity);            
            system.debug('Payment ID : ' + strPaymentId);
            redirectToCreateOrder(objOpportunity);
            system.debug('Terminal URL : ' + terminalURL);
            PageReference pr = new PageReference(terminalURL);
            pr.setRedirect(true);
            return pr;
        }
        return null;
    }
    
    private void splitOpportunityProduct(Opportunity objOpportunity, list<OpportunityLineItem> lstOPPLIP4P) {
        for(OpportunityLineItem iterator : objOpportunity.OpportunityLineItems) {
            if(iterator.P4P_Billing__c == true || iterator.Is_P4P__c == true) {
                lstOPPLIP4P.add(iterator);
            }
        }
    }
    
    private void validateP4PBillingContactPaymentMethod(String contactId, Opportunity objOpportunity) { 
        String strOppOrder = checkNewOrModifyOpp(objOpportunity);
        if(objOpportunity.Billing_Partner__c.equalsIgnoreCase(CommonMessages.berryBillingPartner) && objOpportunity.Payment_Method__c.equals(CommonMessages.StatementPaymentMethod)) {
            terminalURL = '/apex/'+strOppOrder+'?Id='+opportunityID;
        }
        else {
            if(contactId != null) {
                list<pymt__Payment_Method__c> paymentMethodList = ContactSOQLMethods.getContact_PaymentMethodByContactID(new set<Id>{ContactId});
                if(PaymentMethodList.size() > 0) {
                    terminalURL = '/apex/'+strOppOrder+'?Id='+opportunityID;
                }
                else {
                    terminalURL = '/apex/CreatePaymentMethod?conid='+ContactId+'&retURL=/apex/'+strOppOrder+'?Id='+opportunityID;
                }
            }
        }   
    }
    
    private string checkNewOrModifyOpp(Opportunity objOpportunity) {
        if(objOpportunity.modifyid__c == null) {
            return 'createOrderPage_V6';
        }
        else {
            return 'createModifyOrderPage_V6';
        }
    }
    
    private void redirectToCreateOrder(Opportunity objOpportunity) {
        String strOppOrder = checkNewOrModifyOpp(objOpportunity);
        system.debug('Term : '+ terminalURL);
        system.debug(strPaymentId);
        if(String.isBlank(terminalURL)) {
            if(String.isBlank(strPaymentId)) {
                terminalURL = '/apex/'+strOppOrder+'?Id='+opportunityID;
            }
            else {
                terminalURL = '/apex/pymt__paymentterminal?';
                terminalURL = terminalURL+'id='+strPaymentId;
                terminalURL = terminalURL+'&cancelURL='+EncodingUtil.urlEncode(URL.getSalesforceBaseUrl().toExternalForm()+'/apex/cancelPaymentPage?Id='+opportunityID,'UTF-8');
                terminalURL = terminalURL+'&finishURL='+EncodingUtil.urlEncode(URL.getSalesforceBaseUrl().toExternalForm()+'/apex/'+strOppOrder+'?Id='+opportunityID+'&paymentid='+strPaymentId,'UTF-8');
            }
        }
        system.debug('After Term : '+ terminalURL);
    }
    
    private void doPaymentProcess(Opportunity objOpportunity) {
        Double totalAmount = 0;
        Boolean bFlag = true;
        if(objOpportunity.Payment_Method__c.equals(CommonMessages.ccPaymentMethod) || objOpportunity.Payment_Method__c.equals(CommonMessages.ACHPaymentMethod)) {
            totalAmount = Double.valueOf(objOpportunity.Net_Total__c);
        }
        
        system.debug('totalAmount : '+ totalAmount);
        if(totalAmount > 0) {
            createPayment(objOpportunity, bFlag, totalAmount);
        }
    }
    
    private void doPaymentProcessNew(Opportunity objOpportunity) {
        list<OpportunityLineItem> lstOPPLIP4P = new list<OpportunityLineItem>();
        Double totalAmount = 0;
        Boolean bFlag = true;
        
        for(OpportunityLineItem iterator : objOpportunity.OpportunityLineItems) {
            if(String.isBlank(iterator.Renewals_Action__c) || iterator.Renewals_Action__c.equals('New')) {
                totalAmount += Double.valueOf(iterator.UnitPrice);
            }
            if(iterator.P4P_Billing__c == true || iterator.Is_P4P__c == true) {
                lstOPPLIP4P.add(iterator);
            }
        }
        if(!objOpportunity.Payment_Method__c.equals(CommonMessages.ccPaymentMethod) && !objOpportunity.Payment_Method__c.equals(CommonMessages.ACHPaymentMethod)) {
            totalAmount = 0;
        }
        
        if(objOpportunity.OpportunityLineItems.size() == lstOPPLIP4P.size()) {
            validateP4PBillingContactPaymentMethod(objOpportunity.Billing_Contact__c, objOpportunity);
        }
        
        system.debug('totalAmount : '+ totalAmount);
        if(String.isBlank(terminalURL)) {
            if(totalAmount > 0) {
                createPayment(objOpportunity, bFlag, totalAmount);
            }
        }
    }
    
    private void createPayment(Opportunity objOpportunity, Boolean bFlag, Double totalAmount) {
        list<pymt__PaymentX__c> lstPayment = PaymentXSOQLMethods.getPaymentxByOpportunityID(new set<Id> {objOpportunity.Id});
        list<pymt__PaymentX__c> lstInsertPayment = new list<pymt__PaymentX__c>();
        pymt__PaymentX__c newPayment = new pymt__PaymentX__c();
        if(lstPayment.size() > 0) {
            newPayment = lstPayment[0];
        }
                
        lstInsertPayment.add(newPaymentX(newPayment, objOpportunity, totalAmount, ''));
        
        if(lstInsertPayment.size() > 0) {
            upsert lstInsertPayment;
        }
        system.debug('lstInsertPayment[0].Id : '+ lstInsertPayment[0].Id);
        
        strPaymentId = lstInsertPayment[0].Id;
    }
    
    public Pagereference backToOpportunity() {
        return new Pagereference('/'+opportunityID);
    }
    
    private pymt__PaymentX__c newPaymentX(pymt__PaymentX__c newPayment, Opportunity objOpportunity,Double totalAmount, String salesInvoice) {
        newPayment.Name='Initial Payment';
        newPayment.pymt__Contact__c = objOpportunity.Signing_Contact__c;
        newPayment.pymt__Amount__c = totalAmount;
        newPayment.pymt__Opportunity__c = objOpportunity.Id;
        newPayment.pymt__Transaction_Type__c = 'Payment';
        newPayment.pymt__currency_iso_code__c = 'USD';
        newPayment.pymt__Date__c = system.today();
        newPayment.p2f__FF_Account_Reference__c = objOpportunity.AccountId;
        if(String.isNotEmpty(salesInvoice)) {
            newPayment.p2f__FF_Sales_Invoice__c = salesInvoice;
        }
        return newPayment;
    }
}