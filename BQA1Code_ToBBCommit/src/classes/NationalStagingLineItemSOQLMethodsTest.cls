@isTest(SeeAllData=True)
public class NationalStagingLineItemSOQLMethodsTest{
@IsTest static void NationalStagingLineItemSOQLMethodsTest() {
        Set<String> stringTransUniqueId = new Set<String>();
        Set<Id> setNSLI = new Set<Id>();
        Set<Id> setNSOS = new Set<Id>();
        stringTransUniqueId.add('1234567890123456789012345678901234567890');
       /* Directory__c dir = TestMethodsUtility.generateDirectory();
        dir.Months_for_National__c = 12;
        insert dir;*/
        Directory__c dir=TestMethodsUtility.createDirectory();  

        National_Staging_Order_Set__c newNSOS = TestMethodsUtility.generateNationalStagingOrderSet('Elite RT');
        newNSOS.Directory__c = dir.Id;
        insert newNSOS;
        setNSOS.add(newNSOS.Id);
        National_Staging_Line_Item__c newNSLI = TestMethodsUtility.createNationalStagingLineItemCreation(newNSOS);
        setNSLI.add(newNSLI.Id); 
        System.assertNotEquals(null, NationalStagingLineItemSOQLMethods.getNationalStagingLIListForTransactionByNSOSId(newNSOS.Id));
        System.assertNotEquals(null, NationalStagingLineItemSOQLMethods.getNationalStagingLIListForStadingOrderByNSOSId(newNSOS.Id));
        System.assertNotEquals(null, NationalStagingLineItemSOQLMethods.getNationalStagingLIListByNSLIId(setNSLI));
        System.assertNotEquals(null, NationalStagingLineItemSOQLMethods.getNationalStagingLineItemByIds(setNSLI));
        System.assertNotEquals(null, NationalStagingLineItemSOQLMethods.getNationalStagingLineItemListByNSOSIds(setNSOS));  
        System.assertNotEquals(null, NationalStagingLineItemSOQLMethods.getNationalStagingLineItemReference(setNSLI));
        System.assertNotEquals(null, NationalStagingLineItemSOQLMethods.getNationalStagingLineItemWithOPPLineItem(setNSLI)); 
        System.assertNotEquals(null, NationalStagingLineItemSOQLMethods.getNSLIForTNXHTransactionByNSOSId(newNSOS.Id,'N'));
        System.assertNotEquals(null, NationalStagingLineItemSOQLMethods.getNSLIForTNXHTransactionByNSOSId(newNSOS.Id,'I'));
        System.assertNotEquals(null, NationalStagingLineItemSOQLMethods.getNSLIForStadingOrderTNXHByNSOSId(newNSOS.Id,'N')); 
        System.assertNotEquals(null, NationalStagingLineItemSOQLMethods.getNSLIForStadingOrderTNXHByNSOSId(newNSOS.Id,'I')); 
        
    }
    }