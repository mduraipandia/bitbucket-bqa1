/*
    * FinancialForce.com, inc. claims copyright in this software, its screen display designs and
    * supporting documentation. FinancialForce and FinancialForce.com are trademarks of FinancialForce.com, inc.
    * Any unauthorized use, copying or sale of the above may constitute an infringement of copyright and may
    * result in criminal or other legal proceedings.
    *
    * Copyright FinancialForce.com, inc. All rights reserved.
*/

public class FFA_TelcoTransactionLineWrapper 
{
    //Used in Batch Class not for UI
    public boolean selected { get; set; }
    //Internal Writeoff or External WriteOff only used for Write Off
    public String writeOffType { get; set; }
    //Account line if Written off from Header or Cash Entry or Analysis line if written off by line for Sales Invoice/Sales Credit Note
    public c2g__codaTransactionLineItem__c transLine { get; set; }
    //Account line if Written off from Header or Cash Entry or Analysis line if written off by line for Sales Invoice/Sales Credit Note
    public Id transLineId { get; set; }
    //Account Line Id used for Matching API
    public id accountLineId { get; set; }
    //Amount that needs to be matched or written off
    public Decimal amountToBeMatched { get; set; } 
    //Outstanding line Balance
    public Decimal balance { get; set; }   
}