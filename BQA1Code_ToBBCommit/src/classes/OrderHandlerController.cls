public with sharing class OrderHandlerController {
    public static void onAfterUpdate(list<Order__c> listOrder, Map<Id, Order__c> oldMap) {
        Boolean skipTrigger=CommonMethods.skipTriggerMethodLogic(CommonMessages.orderObjectName);
        if(!skipTrigger){
            callUpdateOrderLineItemAndAccountAnniversaryDate(listOrder, oldMap);
        }
    }
    
    public static void callUpdateOrderLineItemAndAccountAnniversaryDate(list<Order__c> listOrder, Map<Id, Order__c> oldMap) {
        list<String> OrdIds = new list <String>();
        map<Id, Order__c> AccountOrderMap = new map<Id, Order__c>();        
        Set<Id> AccountIds = new Set<Id>();
        for (Order__c newOrder : listOrder) {
            Order__c oldOrder = oldMap.get(newOrder.Id);
            //system.debug('*********Old Order : ' + oldOrder);
            if(oldOrder != null) {
                if(newOrder.Billing_Anniversary_Date__c != null && oldOrder.Billing_Anniversary_Date__c == null) {
                    OrdIds.add(newOrder.Id);                                   
                    //system.debug('*********List Order : ' + OrdIds);
                    AccountIds.add(newOrder.Account__c);
                    AccountOrderMap.put(newOrder.Account__c,newOrder);
                }
            }
        }
        
        if(AccountIds.size() > 0) {
            updateAccountAnniversaryDate(AccountIds, AccountOrderMap);
        }
        
        if(!system.isBatch()) {
	        if(OrdIds.size() > 0) {
	            //system.debug('*********Order updateOrderLineItemAnniversaryDate Start************');
	            //Calling @future method to update the order line item anniversary start date.
	            updateOrderLineItemAnniversaryDate(OrdIds);
	            //system.debug('*********Order updateOrderLineItemAnniversaryDate End************');
	        }
        }
    }
    
    @future
    public static void updateOrderLineItemAnniversaryDate(list<String> OrdIds) {
        //system.debug('*********OrderAUtrigger Future Method Start************');
        //get all line items that have not set the ann date for this order
        list<order_line_items__c> lstOLI = [SELECT Id,  Order_Anniversary_Start_Date__c, Order__r.Billing_Anniversary_Date__c 
                                                FROM Order_Line_Items__c 
                                                WHERE Order__c in :OrdIds AND Order_Anniversary_Start_Date__c = Null];
        //system.debug('*********Order Line Item : '+ OLIlist.Size());
        if(lstOLI.size() > 0) {
            for(order_line_items__c iterator : lstOLI) {
                iterator.Order_Anniversary_Start_Date__c = iterator.Order__r.Billing_Anniversary_Date__c;
                //system.debug('*********Order Line Item Anniversary_Start_Date : '+ OLI.Order_Anniversary_Start_Date__c);
            }
            //system.debug('*********Order Line Item Update List : '+ OLIlist);
            //Update the order line item with order anniversary start date.
            update lstOLI;
            //system.debug('*********OrderAUtrigger Future Method End************');
        }
    }
    
    private static void updateAccountAnniversaryDate(Set<Id> AccountIds, map<Id, Order__c> AccountOrderMap) {
        list<Account> lstAccount = new list<Account>();
        if(AccountIds.size() > 0) {
            for(Id accountId : AccountIds) {
                lstAccount.add(new Account(Id = accountId, Billing_Anniversary_Date__c = AccountOrderMap.get(accountId).Billing_Anniversary_Date__c));
            }
            if(lstAccount.size()>0) {
                //system.debug('**********AccList:' + AccList.size());
                update lstAccount;
            }
        }
    }
}