/************************************************************************
Utility class to generate Request and Response body for Talus DELETE call
Created By: Reddy(reddy.yellanki@theberrycompany.com)
Updated Date: 09/18/2015
$Id$
*************************************************************************/

public with sharing class TalusRequestUtilityDelete {
    
    //This is a common Utility Class for Delete request
    public static HttpResponse initiateRequest(String requestUrl,Id FailedId){
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        //Default timeout in milliseconds - this is now set to two minutes
        req.setTimeout(120000); 
        req.setMethod('DELETE');
        if(Test.isRunningTest()){
        //requestUrl+'param=1 & param=2'
        req.setEndpoint(requestUrl+'q?param=1 & param=2');
        }else{
        req.setEndpoint(requestUrl);
        }
        req.setHeader('Content-Type','application/json');
        req.setHeader('Accept','application/json');
        //req.setBody(Body);
        Oauth1 reuest=new Oauth1();
        reuest.sign(req);
        HttpResponse res=new HttpResponse();
        try{
         res=h.send(req);
         if(res.getStatusCode()!=204){
            //ErrorLog.CreateErrorLog(res.getbody(),FailedId,res.getStatusCode(),'Failed in DeleteUtility');
         }       
        }catch (Exception e){
            ErrorLog.CreateErrorLog(e.getMessage(),FailedId,500,'Exception found in TalusRequestUtilityDelete Class');
        }
        //System.debug('************RESPONSE BODY************'+ res.getBody());
        //return String.valueof(res.getStatusCode());
        return res;
    }
    
}