public with sharing class GalleyReportPageController_V1 {
    List<Directory__c> listDirectory;
    public List<SelectOption> listDirSelOpt {get;set;}
    public Id selectedDirId {get;set;}
    set<Id> setTelcoIds = new set<Id>();
    map<Id, Id> mapDirIdTelcoId = new map<Id, Id>();
    Map<Id, String> mapDirIdName = new Map<Id, String>();
    public String searchText {get;set;}
    set<String> setBookStatus = new set<String>();
    public Id dirEdForInformatica {get;set;}
    public Boolean isBatchProcessed {get;set;}
    public String batchStatus {get;set;}
    Directory_Edition__c dirEdForGRS = new Directory_Edition__c();
    public String localOrNational {get;set;}
    public Galley_Staging_Job__c objGSJ {get;set;}
    
    public List<SelectOption> listDirEdSelOpt {get;set;}
    public List<SelectOption> listDirEdAvaOpt {get;set;}
    public List<SelectOption> listDirSecSelOpt {get;set;}
    public List<SelectOption> listDirSecAvaOpt {get;set;}
    public List<SelectOption> listBusTypSelOpt {get;set;}
    public List<SelectOption> listBusTypAvaOpt {get;set;}
    public List<SelectOption> listTelcoSelOpt {get;set;}
    public List<SelectOption> listTelcoAvaOpt {get;set;}
    
    public List<String> listDirEdYearSelVal {get;set;}
    public List<String> listDirEdYearAvaVal {get;set;}
    public List<String> listDirEdSelVal {get;set;}
    public List<String> listDirEdAvaVal {get;set;}
    public List<String> listDirSecSelVal {get;set;}
    public List<String> listDirSecAvaVal {get;set;}
    public List<String> listBusTypSelVal {get;set;}
    public List<String> listBusTypAvaVal {get;set;}
    public List<String> listTelcoSelVal {get;set;}
    public List<String> listTelcoAvaVal {get;set;}
    
    public GalleyReportPageController_V1() {        
        listDirSelOpt = new List<SelectOption>();
        listDirEdYearSelVal = new List<String>();
        listDirEdYearAvaVal = new List<String>();
        listDirEdSelVal = new List<String>();
        listDirEdAvaVal = new List<String>();
        listDirSecSelVal = new List<String>();
        listDirSecAvaVal = new List<String>();
        listBusTypSelVal = new List<String>();
        listBusTypAvaVal = new List<String>();
        listTelcoSelVal = new List<String>();
        listTelcoAvaVal = new List<String>();
        listBusTypAvaOpt = new List<SelectOption>();
        listBusTypSelOpt = new List<SelectOption>();      
        listDirEdSelOpt = new List<SelectOption>();
        listDirSecSelOpt = new List<SelectOption>();
        listTelcoSelOpt = new List<SelectOption>();  
        objGSJ = new Galley_Staging_Job__c();
        objGSJ.GSJ_Galley_Header_Limit__c = '40000';
        setBookStatus.add('NI');
        setBookStatus.add('BOTS');
        isBatchProcessed = false;
        listBusTypAvaOpt.add(new SelectOption('Business', 'Business'));
        listBusTypAvaOpt.add(new SelectOption('Residential', 'Residential'));
        listBusTypAvaOpt.add(new SelectOption('Government', 'Government'));
        listBusTypAvaOpt.add(new SelectOption('Combination', 'Combination'));
        listBusTypAvaOpt.add(new SelectOption('Non-Pub', 'Non-Pub'));
        localOrNational = 'local';
    }
    
    public void fetchDirs() {
        String SOQL = 'SELECT Id, Name, Directory_Code__c, Telco_Provider__c FROM Directory__c WHERE Name LIKE \'' + searchText + '%\'' + 
                        ' OR Directory_Code__c LIKE \'' + searchText + '%\' ORDER BY Name LIMIT 1000';
        listDirectory = Database.query(SOQL); 
        if(listDirectory.size() > 0) {
            listDirSelOpt = new List<SelectOption>();
            for(Directory__c dir : listDirectory) {
                listDirSelOpt.add(new SelectOption(dir.Id, dir.Name + ' - ' + dir.Directory_Code__c));
                mapDirIdName.put(dir.Id, dir.Name);
                if(dir.Telco_Provider__c != null) {
                    mapDirIdTelcoId.put(dir.Id, dir.Telco_Provider__c);
                }
            }
        }
    }
    
    public void addDirEd() {
        if(listDirEdAvaVal.size() > 0) {
            for(String str : listDirEdAvaVal) {
                for(Integer i = 0; i < listDirEdAvaOpt.size(); i++) {
                    if(listDirEdAvaOpt.get(i).getValue() == str) {
                        listDirEdSelOpt.add(listDirEdAvaOpt.get(i));
                        listDirEdAvaOpt.remove(i);
                        break;
                    }
                }
            }
            listDirEdSelOpt.sort();
        }
    }
    
    public void addAllDirEd() {
        if(listDirEdAvaOpt.size() > 0) {
            listDirEdSelOpt.addAll(listDirEdAvaOpt);
            listDirEdAvaOpt.clear();
            listDirEdSelOpt.sort();
        }
    }
    
    public void removeDirEd() {
        if(listDirEdSelVal.size() > 0) {
            for(String str : listDirEdSelVal) {
                for(Integer i = 0; i < listDirEdSelOpt.size(); i++) {
                    if(listDirEdSelOpt.get(i).getValue() == str) {
                        listDirEdAvaOpt.add(listDirEdSelOpt.get(i));
                        listDirEdSelOpt.remove(i);
                        break;
                    }
                }
            }
            listDirEdAvaOpt.sort();
        }
    }
    
    public void removeAllDirEd() {
        listDirEdAvaOpt.addAll(listDirEdSelOpt);
        listDirEdSelOpt.clear();
        listDirEdAvaOpt.sort();
    }
    
    public void addDirSec() {
        if(listDirSecAvaVal.size() > 0) {
            for(String str : listDirSecAvaVal) {
                for(Integer i = 0; i < listDirSecAvaOpt.size(); i++) {
                    if(listDirSecAvaOpt.get(i).getValue() == str) {
                        listDirSecSelOpt.add(listDirSecAvaOpt.get(i));
                        listDirSecAvaOpt.remove(i);
                        break;
                    }
                }
            }
            listDirSecSelOpt.sort();
        }
    }
    
    public void addAllDirSec() {
        if(listDirSecAvaOpt.size() > 0) {
            listDirSecSelOpt.addAll(listDirSecAvaOpt);
            listDirSecAvaOpt.clear();
            listDirSecSelOpt.sort();
        }
    }
    
    public void removeDirSec() {
        if(listDirSecSelVal.size() > 0) {
            for(String str : listDirSecSelVal) {
                for(Integer i = 0; i < listDirSecSelOpt.size(); i++) {
                    if(listDirSecSelOpt.get(i).getValue() == str) {
                        listDirSecAvaOpt.add(listDirSecSelOpt.get(i));
                        listDirSecSelOpt.remove(i);
                        break;
                    }
                }
            }
            listDirSecAvaOpt.sort();
        }
    }
    
    public void removeAllDirSec() {
        listDirSecAvaOpt.addAll(listDirSecSelOpt);
        listDirSecSelOpt.clear();
        listDirSecAvaOpt.sort();
    }
    
    public void addBusTyp() {
        if(listBusTypAvaVal.size() > 0) {
            for(String str : listBusTypAvaVal) {
                for(Integer i = 0; i < listBusTypAvaOpt.size(); i++) {
                    if(listBusTypAvaOpt.get(i).getValue() == str) {
                        listBusTypSelOpt.add(listBusTypAvaOpt.get(i));
                        listBusTypAvaOpt.remove(i);
                        break;
                    }
                }
            }
            listBusTypSelOpt.sort();
        }
    }
    
    public void addAllBusTyp() {
        if(listBusTypAvaOpt.size() > 0) {
            listBusTypSelOpt.addAll(listBusTypAvaOpt);
            listBusTypAvaOpt.clear();
            listBusTypSelOpt.sort();
        }
    }
    
    public void removeBusTyp() {
        if(listBusTypSelVal.size() > 0) {
            for(String str : listBusTypSelVal) {
                for(Integer i = 0; i < listBusTypSelOpt.size(); i++) {
                    if(listBusTypSelOpt.get(i).getValue() == str) {
                        listBusTypAvaOpt.add(listBusTypSelOpt.get(i));
                        listBusTypSelOpt.remove(i);
                        break;
                    }
                }
            }
            listBusTypAvaOpt.sort();
        }
    }
    
    public void removeAllBusTyp() {
        listBusTypAvaOpt.addAll(listBusTypSelOpt);
        listBusTypSelOpt.clear();
        listBusTypAvaOpt.sort();
    }
    
    public void addTelco() {
        if(listTelcoAvaVal.size() > 0) {
            for(String str : listTelcoAvaVal) {
                for(Integer i = 0; i < listTelcoAvaOpt.size(); i++) {
                    if(listTelcoAvaOpt.get(i).getValue() == str) {
                        listTelcoSelOpt.add(listTelcoAvaOpt.get(i));
                        listTelcoAvaOpt.remove(i);
                        break;
                    }
                }
            }
            listTelcoSelOpt.sort();
        }
    }
    
    public void addAllTelco() {
        if(listTelcoAvaOpt.size() > 0) {
            listTelcoSelOpt.addAll(listTelcoAvaOpt);
            listTelcoAvaOpt.clear();
            listTelcoSelOpt.sort();
        }
    }
    
    public void removeTelco() {
        if(listTelcoSelVal.size() > 0) {
            for(String str : listTelcoSelVal) {
                for(Integer i = 0; i < listTelcoSelOpt.size(); i++) {
                    if(listTelcoSelOpt.get(i).getValue() == str) {
                        listTelcoAvaOpt.add(listTelcoSelOpt.get(i));
                        listTelcoSelOpt.remove(i);
                        break;
                    }
                }
            }
            listTelcoAvaOpt.sort();
        }
    }
    
    public void removeAllTelco() {
        listTelcoAvaOpt.addAll(listTelcoSelOpt);
        listTelcoSelOpt.clear();
        listTelcoAvaOpt.sort();
    }       
    
    public void fetchDirEdDirSec() {
        listDirEdSelOpt = new List<SelectOption>();
        listDirEdAvaOpt = new List<SelectOption>();
        listDirSecSelOpt = new List<SelectOption>();
        listDirSecAvaOpt = new List<SelectOption>();
        listBusTypSelOpt = new List<SelectOption>();
        listTelcoSelOpt = new List<SelectOption>();
        listTelcoAvaOpt = new List<SelectOption>();
        List<Directory_Section__c> listDirSec = [SELECT Id, Name FROM Directory_Section__c WHERE Directory__c =: selectedDirId ORDER BY Name];      
        List<Directory_Edition__c> listDirEd = [SELECT Id, Year__c, Name, Telco__c, Sales_Lockout__c, Pub_Date__c FROM Directory_Edition__c WHERE 
                                                Directory__c =: selectedDirId AND Book_Status__c IN: setBookStatus ORDER BY Name];
        if(listDirEd.size() > 0) {
            listDirEdAvaOpt.clear();
            listDirEdSelOpt.clear();
            setTelcoIds.clear();
            for(Directory_Edition__c DE : listDirEd) {
                listDirEdAvaOpt.add(new SelectOption(DE.Name + '--' + DE.Id, DE.Name + ' / Sales Lock Out ' + DE.Sales_Lockout__c.format() + ' / Pub Date ' + DE.Pub_Date__c.format()));
                if(DE.Telco__c != null) {
                    setTelcoIds.add(DE.Telco__c);
                }
            }
        } 
        if(listDirSec.size() > 0) {
            for(Directory_Section__c DS : listDirSec) {
                listDirSecAvaOpt.add(new SelectOption(DS.Name + '--' + DS.Id, DS.Name));
            }
        }
        fetchTelco();
    }
    
    public void fetchTelco() {
        setTelcoIds.add(mapDirIdTelcoId.get(selectedDirId));
        
        List<Directory_Mapping__c> listDirMap = new List<Directory_Mapping__c>();
        listDirMap = [SELECT Id, Telco__c FROM Directory_Mapping__c WHERE Directory__c =: selectedDirId];
        
        if(listDirMap.size() > 0) {
            for(Directory_Mapping__c DM : listDirMap) {
                if(DM.Telco__c != null) {
                    setTelcoIds.add(DM.Telco__c);
                }
            }
        }
        
        if(setTelcoIds.size() > 0) {
            listTelcoAvaOpt.clear();
            List<Telco__c> listTelco = new List<Telco__c>();
            listTelco = [SELECT Id, Name FROM Telco__c WHERE Id IN: setTelcoIds];
            listTelcoAvaOpt.add(new SelectOption('-NullValues-','--Include Null Values--'));
            for(Telco__c telc : listTelco) {
                listTelcoAvaOpt.add(new SelectOption(telc.Name, telc.Name));
            }
        } else {
            listTelcoAvaOpt.clear();
        }
    }
    
    public void callGalleyBatch() {
        batchStatus = '';        
        if(validateEntries()) {
			set<String> setDirCodes = new set<String>();
			set<Id> dirEdIds = new set<Id>();
			set<Id> dirSecIds = new set<Id>();
			set<Id> telcoIds = new set<Id>();
			set<String> setBusTyp = new set<String>();
			map<String, list<String>> mapSelectedValues = new map<String, list<String>>();
			set<String> telcoNames = new set<String>();
			set<String> setYears = new set<String>();
			String businessTyp = '';
			String dirCode = '';
			String dirEdition = '';
			String dirSection = '';
			String editionYear = '';
			String telco = '';
			String dirEditionIds = '';
			String whereCondition = '';
            
            if(listDirEdSelOpt.size() > 0) { 
                Boolean DESelBool = false;
                mapSelectedValues.put('DirEd', new list<String>());
                mapSelectedValues.put('DirCode', new list<String>());
                mapSelectedValues.put('Year', new list<String>());
                for(SelectOption SO : listDirEdSelOpt) {
                    dirEdIds.add(SO.getValue().substringAfter('--'));             
                    mapSelectedValues.get('DirEd').add(SO.getLabel());
                    dirEdition += SO.getLabel() + ';';
                }
                
                List<Directory_Edition__c> listDirEd = DirectoryEditionSOQLMethods.getDEByIds(dirEdIds);
                
                for(Directory_Edition__c dirEd : listDirEd) {              
                    mapSelectedValues.get('DirCode').add(dirEd.Directory_Code__c);
                    setDirCodes.add(dirEd.Directory_Code__c);
                    setYears.add(dirEd.Year__c);
                    editionYear += dirEd.Year__c + ';';
                    if(!DESelBool) {
                        if(dirEd.Book_Status__c == CommonMessages.bots) {
                            dirEdForGRS = dirEd;
                            dirEdForInformatica = dirEd.Id;
                            DESelBool = true;
                        } else {
                            dirEdForGRS = dirEd;
                            dirEdForInformatica = dirEd.Id;
                        }
                    }
                }
                mapSelectedValues.get('Year').addAll(setYears);
            }
            
            if(listDirSecSelOpt.size() > 0) {
                mapSelectedValues.put('DirSec', new list<String>());
                for(SelectOption SO : listDirSecSelOpt) {
                    dirSecIds.add(SO.getValue().substringAfter('--'));              
                    mapSelectedValues.get('DirSec').add(SO.getLabel());
                    dirSection += SO.getLabel() + ';';
                }
            }
            
            if(listTelcoSelOpt.size() > 0) {
                mapSelectedValues.put('Telcos', new list<String>());
                for(SelectOption SO : listTelcoSelOpt) {
                    telcoNames.add(SO.getValue());          
                    mapSelectedValues.get('Telcos').add(SO.getLabel());
                    telco += '\'' + SO.getLabel() + '\',';
                }
            }
            
            if(listBusTypSelOpt.size() > 0) {
                mapSelectedValues.put('BusType', new list<String>());
                for(SelectOption SO : listBusTypSelOpt) {
                    setBusTyp.add(SO.getValue());            
                    mapSelectedValues.get('BusType').add(SO.getLabel());
                    businessTyp += '\'' + SO.getLabel() + '\',';
                }
            }
            
            mapSelectedValues.put('DirName', new list<String> {mapDirIdName.get(selectedDirId)});
            
            if(localOrNational == 'local') {
                mapSelectedValues.put('localOrNational', new list<String> {'local'});
            } else {
                mapSelectedValues.put('localOrNational', new list<String> {'national'});
            }
            
            for(String str : dirEdIds) {
                dirEditionIds += '\'' + str + '\',';
            }
            
            if(setDirCodes.size() > 0) {
                whereCondition += 'Directory_Code__c IN (';
                for(String str : setDirCodes) {
                    whereCondition += '\'' + str + '\',';                    
                    dirCode += str + ';';
                }
                whereCondition = whereCondition.removeEnd(',') + ') AND ';
            }
          
            if(dirSecIds.size() > 0) {
                whereCondition += 'Directory_Section__c IN (';
                for(String str : dirSecIds) {
                    whereCondition += '\'' + str + '\',';
                }
                whereCondition = whereCondition.removeEnd(',') + ') AND';
            }
          
			if(localOrNational == 'national') {
				whereCondition += ' Order_Line_Item__r.RecordTypeId = \'' + System.Label.TestOLIRTNational + '\' AND';
			} 
			
			objGSJ.GSJ_Business_Type__c = businessTyp.removeEnd(',');
			objGSJ.GSJ_Directory_Code__c = dirCode.removeEnd(';');
			objGSJ.GSJ_Directory_Name__c = mapDirIdName.get(selectedDirId);
			objGSJ.GSJ_Directory_Edition__c = dirEdition.removeEnd(';');
			objGSJ.GSJ_Directory_Section__c = dirSection.removeEnd(';');
			objGSJ.GSJ_Edition_Year__c = editionYear.removeEnd(';');
			objGSJ.GSJ_Requested_User_Name__c = UserInfo.getUserId();
			objGSJ.GSJ_Telco__c = telco.removeEnd(',');
			objGSJ.GSJ_Where_Condition__c = whereCondition.removeEnd('AND') + ' ORDER BY Directory_Heading__c, DP_Sort_As__c';
			objGSJ.GSJ_Directory_Edition_Ids__c = dirEditionIds.removeEnd(',');   
			objGSJ.GSJ_Directory_Edition_For_Informatica__c = dirEdForInformatica;             
			SortAsPopulationBatch obj = new SortAsPopulationBatch(objGSJ, whereCondition.removeEnd('AND'), selectedDirId, dirSecIds);
			Database.executeBatch(obj);  
			objGSJ = new Galley_Staging_Job__c();
			objGSJ.GSJ_Galley_Header_Limit__c = '40000';
			isBatchProcessed = true;
        }
    }
    
    private Boolean validateEntries() {
        Boolean bFlag = true;
        if(listDirEdSelOpt.size() == 0) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select Directory Edition'));
            bFlag = false;
        }
        if(listDirSecSelOpt.size() == 0) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select Directory Section'));
            bFlag = false;
        }
        if(listTelcoSelOpt.size() == 0) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select Telco'));
            bFlag = false;
        }
        if(listBusTypSelOpt.size() == 0) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select Business Type'));
            bFlag = false;
        }
        return bFlag;
    }
}