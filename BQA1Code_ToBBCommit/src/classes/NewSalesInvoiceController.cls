public class NewSalesInvoiceController {
        public c2g__codaInvoice__c newSI {get;set;}
        public NewSalesInvoiceController(ApexPages.StandardController controller) {
                newSI = new c2g__codaInvoice__c();
    }
    
    public PageReference savSI() {
        try {
                newSI.Is_Manually_Created__c = true;
                insert newSI;
                PageReference pg = new PageReference('/' + newSI.Id);
                pg.setRedirect(true);
                return pg;
        }catch(Exception e) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
                return null;
        }
    }
}