public with sharing class MultiScopingBatchCallController {
    public Boolean polleraction {get;set;}
    public String batchStatus {get;set;}
    public Id batchId {get;set;}
    public Boolean isBatchProcessed {get;set;}
    public Id multiScopeReportId {get;set;}
    public Id multiScopeHeadReportId {get;set;}
    public String dirCode {get;set;}
    public String areaCode {get;set;}
    public String exchCode {get;set;}
    public Multi_Scoping_Header__c objMSH {get;set;}
    public Id MSHReportBasedOnDirId {get;set;}
    
    public MultiScopingBatchCallController() {
    	if(!Test.isRunningTest()) {
	    	multiScopeReportId = Multi_Scoping_Reports__c.getInstance('MultiScopedSL').Report_Id__c; 
	    	multiScopeHeadReportId = Multi_Scoping_Reports__c.getInstance('MultiScopeMSH').Report_Id__c; 
	    	MSHReportBasedOnDirId = Multi_Scoping_Reports__c.getInstance('MultiScopeMSHBasedOnDir').Report_Id__c;
    	}
    	objMSH = new Multi_Scoping_Header__c();
    }
    
    public void callMultiScopeBatch() {
        MultiScopingBatch obj = new MultiScopingBatch('');
        batchId = Database.executeBatch(obj, 1);
        pollerAction = true;
        isBatchProcessed = true;
        batchStatus = 'Started Processing';
    }
    
    public void pollingBatchProcess() {                 
        if(batchId!=null) {
            AsyncApexJob FFjob = [SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =: batchId];
            if(!Test.isRunningTest())
            batchStatus = FFjob.Status;
        }
        if(batchStatus == 'Completed' || batchStatus == 'Aborted') {
            pollerAction = false;
        }
    }
    
    public void fetchDirectoryCode() {
    	if(String.isNotBlank(objMSH.MSH_Directory__c)) {
    		Directory__c objDir = [SELECT Id, Directory_Code__c FROM Directory__c WHERE Id =: objMSH.MSH_Directory__c];
    		dirCode = objDir.Directory_Code__c;
    	}
    }
}