global class ScheduleLOBRecordDeleteBatch implements Schedulable {
   public Interface ScheduleLOBRecordDeleteBatchInterface{
     void execute(SchedulableContext sc);
   }
   global void execute(SchedulableContext sc) {
    Type targetType = Type.forName('ScheduleLOBRecordDeleteBatchHndlr');
        if(targetType != null) {
            ScheduleLOBRecordDeleteBatchInterface obj = (ScheduleLOBRecordDeleteBatchInterface)targetType.newInstance();
            obj.execute(sc);
        }
   
   }
}