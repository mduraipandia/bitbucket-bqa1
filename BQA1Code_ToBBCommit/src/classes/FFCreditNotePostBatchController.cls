global class FFCreditNotePostBatchController  implements Database.Batchable<sObject> {
	
	global set<Id> setIds {get;set;}
	
	global FFCreditNotePostBatchController(set<Id> setIds) {
		this.setIds = setIds;
	}
	
	global Database.QueryLocator start(Database.BatchableContext bc){
    	String strQuery = 'Select Id from c2g__codaCreditNote__c where ID IN:setIds';
        return Database.getQueryLocator(strQuery);
    }
    
    global void execute(Database.BatchableContext bc, List<c2g__codaCreditNote__c> lstSCN) {
    	set<Id> setSCNID = new set<Id>();
    	for(c2g__codaCreditNote__c iterator : lstSCN) {
    		setSCNID.add(iterator.Id);
    	}
    	
    	if(setSCNID.size() > 0) {
    		BillingWizardCommonController.postSalesCreditNote(BillingWizardCommonController.generateCODAAPICommonReference(setSCNID));
    	}
    }
    
    global void finish(Database.BatchableContext bc) {
    	AsyncApexJob a = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
    	String strErrorMessage = '';
    	if(a.NumberOfErrors > 0){
        	strErrorMessage = a.ExtendedStatus;
        }
        CommonEmailUtils.sendHTMLEmailForTargetObject(a.CreatedById, 'Sales Credit Note Post ' + a.Status, 'The batch Apex job processed ' + a.TotalJobItems +
          ' batches with '+ a.NumberOfErrors + ' failures.'+strErrorMessage+'.');
    }
}