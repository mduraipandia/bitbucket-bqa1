@IsTest
public class BillBackInvoicePdfTestClass {
    public static testmethod void BillBackPdfTest(){
        Test.startTest();
        //User objUser = [Select UserRole.Name, UserRole.Id, UserRoleId, Id From User where UserRole.Name = 'Account Manager' limit 1];
        Canvass__c objCanvass = TestMethodsUtility.createCanvass();
        //insert objCanvass;
        Account objAccount = new Account(Primary_Canvass__c = objCanvass.Id, Name = 'Test Account', Phone = '(422) 552-4444', Billing_Phone__c='(422) 552-4444', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');        
        insert objAccount;
        
        Contact objContact = new Contact(LastName='TestContact',FirstName='Test',AccountId=objAccount.Id,Email='test@gm.com',PrimaryBilling__c=true,Phone='(422) 552-4444',MailingCity ='Texas',MailingState='TX',MailingStreet='St1245',MailingPostalCode='34567',MailingCountry='US');
        insert objContact;
        Telco__c objTelco = new Telco__c(Name = 'Test Telco',Account__c = objAccount.Id);
        insert objTelco;
        
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Name = 'Test Dir2';
        objDir.EBD__c = System.Today().addDays(1);
        objDir.DCR_Close__c = System.Today().addDays(5);
        objDir.BOC__c = System.Today().addDays(2);
        objDir.Final_Service_Order_Due_to_Berry__c = System.Today().addDays(1);
        objDir.Sales_Lockout__c = System.Today();
        objDir.Book_Extract_YP__c = System.Today().addDays(3);
        objDir.Ship_Date__c = System.Today().addDays(-2);
        objDir.Pub_Date__c = System.Today().addDays(4);
        objDir.Directory_Code__c =  'A12345';
        objDir.Telco_Provider__c =objTelco.id;
        
        insert objDir;
        
       Directory_Edition__c objDirE = new Directory_Edition__c();
        objDirE.Name = 'Test Directory Edition 1';
        objDirE.Directory__c = objDir.id;
        objDirE.Book_Status__c = 'NI';
        objDirE.EBD__c = System.Today().addDays(1);
        objDirE.DCR_Close__c = System.Today().addDays(5);
        objDirE.BOC__c = System.Today().addDays(2);
        objDirE.Final_Service_Order_Due_to_Berry__c = System.Today().addDays(1);
        objDirE.Sales_Lockout__c = System.Today();
        objDirE.Book_Extract_YP__c = System.Today().addDays(3);
        objDirE.Ship_Date__c = Date.valueof('2013-07-28');
        objDirE.Pub_Date__c = System.Today().addDays(4);
        objDirE.Final_Auto_Renew_Job__c = System.Today().addDays(2);
        objDirE.EACD__c = System.Today().addDays(3);
        objDirE.Write_Away_Date__c = System.Today().addDays(1);
        objDirE.Free_Listings_Copied_To_New_Edition__c = System.Today().addDays(3);
        objDirE.National_Rates_Due__c = System.Today().addDays(6);  
        objDirE.National_Close_Date__c = System.Today().addDays(1);
        objDirE.Proof_Close__c = System.Today().addDays(4);
        objDirE.Galleys_Due_Telco__c = System.Today().addDays(3);
        objDirE.Galleys_Due_Berry__c = System.Today().addDays(1);
        objDirE.Renewal_Open_Date__c = System.Today().addDays(4);
        objDirE.Annual_Load_File_Due_Berry__c = System.Today().addDays(2);
        objDirE.Book_Extract_WP__c = System.Today().addDays(2); 
        objDirE.Setup_Cost__c = 1000;
        objDirE.Printing_Cost__c = 2000;
        objDirE.Other_Cost__c = 3000;
        objDirE.Telco__c = objTelco.Id;
        try{
            insert objDirE;
        }
        catch(exception e){}
        
        //insert objDirE;
        
        Directory_Edition__c objDirE1 = new Directory_Edition__c();
        objDirE1.Name = 'Test Directory Edition 2';
        objDirE1.Directory__c = objDirE.id;
        objDirE1.Book_Status__c = 'BOTS';
        objDirE1.Pub_Date__c = System.Today().addDays(14);
        objDirE1.Setup_Cost__c = 1000; 
        objDirE1.Printing_Cost__c = 2000;
        objDirE1.Other_Cost__c = 3000;
        objDirE1.Telco__c = objTelco.Id;
        
        try{
            insert objDirE1;
        }
        catch(Exception e){}
        
        
        Directory_Edition__c objDirE2 = new Directory_Edition__c();
        objDirE2.Name = 'Test Directory Edition 2';
        objDirE2.Directory__c = objDirE.id;
        objDirE2.Book_Status__c = 'BOTS';
        objDirE2.Pub_Date__c = System.Today().addDays(14);
        objDirE2.Setup_Cost__c = 1000; 
        objDirE2.Printing_Cost__c = 2000;
        objDirE2.Other_Cost__c = 3000;
        objDirE2.Telco__c = objTelco.Id;
        objDirE2.Directory_Edition_Code__c = objDirE1.Id;
        try{
        Insert objDirE2;
        }
        catch(Exception e){}
        
        
       /* Edition_Pagination_Setting__c objEps = new  Edition_Pagination_Setting__c();
        objEps.Name = 'Test EPS';
        objEps.Directory_Edition__c = objDirE1.Id;
        try{
        insert objEps;
        }
        catch(Exception e){}*/
        
        
        //PageReference testPage1 = new PageReference('/apex/BillBackInvoice_v1' + '?id=' + objDirE.Id);
        PageReference pageRef = Page.BillBackInvoice_v1;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',objDirE.Id);
        
        Billing_Settlement__c objBS = new Billing_Settlement__c();
        objBS.Name = 'Test Billing Settlement';
        objBS.Directory__c = objDir.Id;
        objBS.Directory_Edition__c = objDirE.Id;
        objBS.Telco_Cost_Share__c = 10;
        Insert objBS;
        
        Edition_Cost__c objEc = new Edition_Cost__c();
        objEc.Cost_type__c = 'Tipons';
        objEc.Amount__c = 4000;
        objEc.Directory_Edition__c = objDirE.Id;
        try{
        insert objEc;
        }
        catch(Exception e){}
        
        
        BillbackInvoice_v1 BillInvoice = new  BillbackInvoice_v1();
        ApexPages.currentPage().getParameters().put('id',objDirE.Id);
        BillInvoice.editionCostLst();
        Test.stopTest();
    }
}