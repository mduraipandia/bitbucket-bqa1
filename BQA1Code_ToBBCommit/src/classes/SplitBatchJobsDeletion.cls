global class SplitBatchJobsDeletion implements Database.Batchable <sObject> {
	global Database.QueryLocator start(Database.BatchableContext bc) {
        String SOQL = 'SELECT Id,Name,End_Number__c,Start_Number__c,Type__c FROM SplitBatchJobs__c';
        return Database.getQueryLocator(SOQL);
    }
    global void execute(Database.BatchableContext bc, List<SplitBatchJobs__c> listSplitJobs) {
        if(listSplitJobs.size()> 0) {
        	delete listSplitJobs;
        }
        list<StatementSplitBatch__c> lstStatementJob = [Select Id from StatementSplitBatch__c];
        if(lstStatementJob.size() > 0) {
        	delete lstStatementJob;
        }
    }
    
    global void finish(Database.BatchableContext bc) {
    }
    
}