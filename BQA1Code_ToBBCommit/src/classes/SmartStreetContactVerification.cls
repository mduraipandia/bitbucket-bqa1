/***********************************************************************************************
* Controller Name   : SmartStreetContactVerification     
* Date              :   
* Author            : Partha.C   
* Purpose           : Class for Verification of Address from Smart Street      
* Change History    : 
* Date                  Programmer              Reason  
* -------------------- ------------------- -------------------------    
*                       Praveenkumar.B          Initial Version  
    
**************************************************************************************************/ 



public with sharing class SmartStreetContactVerification {
    
     
      public static Contact con{get;set;}
      public Integer count=0;                       // Vairable for count
      public Date d1{get;set;}                      // to take date from LastModified date of a record
      public Date d2{get;set;}                      // to capture system date
      public Time t1{get;set;}                      // to capture Last Modified time 
      public Time t2{get;set;}                      // to Capture System time
      public Time t3{get;set;}                      // to Capture elapsed time for Polling
      public boolean stop_ap {get;set;}             //for stopping the action poller
    
    
    // smart street response fields
      public static string street_name;             //for capture stree_name       
      public static string primary_number;          //for capture primarynumber
      public static string treet_suffix;            //for capture treetsuffix
      public static string city_name;               //for capture cityname
      public static string zipcode1;                //for capture zipcode
      public static string input_index;             //for capture inputindex
      public static string candidate_index;         //for capture candidateindex
      public static string delivery_line_1;         //for capture deliveryline1
      public static string last_line;               //for capture lastline
      public static string delivery_point_barcode;  //for capture delivery point barcode
      public static string county_name;             //for capture county name
      public static string county_fips;             //for capture county fips
      public static string plus4_code;              //for capture plus4code
      public static string active;                  //for capture active
      public static string validaddress;            //for capture validaddress
      public static string state_abbreviation;      //for capture state abbreviation
      public static string record_type;             //for capture record type
      public static string zip_type;                //for capture zip type
      public static string carrier_route;           //for capture carrier route
      public static string rdi;                     //for capture rdi
      public static double latitude;                //for capture latitude
      public static double longitude;               //for capture longitude
      public static string precision;               //for capture precision
      public static string time_zone;               //for capture timezone
      public static string utc_offset;              //for capture utc offset
      public static string dst;                     //for capture dst
      public static string dpv_match_code;          //for capture dpvmatchcode
      public static string dpv_footnotes;           //for capture dpvfootnotes
      public static string dpv_cmra;                //for capture dpvcmra
      public static string dpv_vacant;              //for capture dpvvacant
      public static string footnotes;               //for capture footnotes
      public static string URLForPage;              //for capture urlforpage
      public static string delivery_point_check_digit;//for capture delivery point check digit
      public static string delivery_point;          //for capture delivery point
      public static string congressional_district;  //for capture congressiondistrict
      public static string elot_sequence;           //for capture elotsequence
      public static string elot_sort;               //for capture elotsort
      public static JSONParser parser;              //Parsing the response from smart street
      public static string ews_match;                // for capture ews_match  
      public static string addressee;               // for capture addressess
      public static string LACSLink_Code;           //for capture LACSLink_Code
      public static string LACSLink_Match_Indicator;//for capture LACSLink_Match_Indicator
      public static string SuiteLink_Match;         //for capture SuiteLink_Match
      public static string building_default_indicator;//for capture building_default_indicator
      public static string pmb_number;              //for capture pmb_number
      public static string pmb_designator;          //for capture pmb_designator
      public static string delivery_line_2;         //for captue delivery_line_2
      public boolean process{get;set;}              //for showing Process icon if response is in Process
      public boolean green{get;set;}                //for showing green icon if response is valid
      public boolean red{get;set;}                  //for showing red icon if response is invalid
    //smart street response fields
    //smart street fields
     
    /// <summary>
    /// Constructor 
    /// </summary>
   public SmartStreetContactVerification(ApexPages.StandardController controller){      
        con=(Contact)controller.getRecord();
        stop_ap=true;
        process=true;
        green=false;
        red=false;
        con=[select Do_Not_Validate_Address__c,Time_Zone__c
,Private_Mailbox_Number__c,Addressee__c,Carrier_Route__c,Latitude__longitude__s,Latitude__latitude__s,UTC_Offset__c,Street_Status__c,County_FIPS_Code__c,County_Name__c ,Geolocation_Precision__c,Daylight_Savings_Observed__c,Delivery_Point_Validation_Code__c,Type_of_Zip_Code__c ,Delivery_Point_Vacant__c,Address_Changes_Made_Codes__c,Residential_Delivery_Indicator__c,LACSLink_Code__c,LACSLink_Match_Indicator__c,SuiteLink_Match__c,Building_Default_Indicator__c,POSTNET_Barcode__c,Private_Mailbox_Unit_Designator__c,Uses_Commercial_Mail_Receiving_Agency__c,DPV_Reason_Code_s__c,Type_of_Address__c,Address_is_Active__c,Address_Not_Ready_for_Mail__c  from Contact where id=:con.id limit 1];
        if(con.Do_Not_Validate_Address__c==false){
         if(con.Street_Status__c=='valid'){
              process=false;
             green=true;
             red=false;
            
             if(con.Do_Not_Validate_Address__c==true){
             stop_ap=false;
             }
             }
          else if (con.Street_Status__c=='invalid') {
             process=false; 
            green=false;
            red=true;
           
            if(con.Do_Not_Validate_Address__c==true){
            stop_ap=false;
             }
            }
            else{
                process=true;
                green=false;
                red=false;
            }
            }
            else{
             if(con.Do_Not_Validate_Address__c==true){
             green=false;
             red=false;
             process=false;
             stop_ap=false;
            }
            }
         
    }
    
     /// <summary>
    ///  To make Increment of the Page  
    /// </summary>
    
  
  public PageReference incrementCounter(){
    
        String id = ApexPages.CurrentPage().getParameters().get('id');
        System.debug('Vertex-SmartyStreets id ***************'+id);
       
        con = [Select id,Last_Modified_Date_For_Reload__c,Time_Zone__c,
Addressee__c,Carrier_Route__c,Latitude__longitude__s,Latitude__latitude__s,UTC_Offset__c,Private_Mailbox_Number__c,Street_Status__c,County_FIPS_Code__c,County_Name__c ,Geolocation_Precision__c,Daylight_Savings_Observed__c,Delivery_Point_Validation_Code__c,Type_of_Zip_Code__c ,Delivery_Point_Vacant__c,Address_Changes_Made_Codes__c,Residential_Delivery_Indicator__c,LACSLink_Code__c,LACSLink_Match_Indicator__c,SuiteLink_Match__c,Building_Default_Indicator__c,POSTNET_Barcode__c,Private_Mailbox_Unit_Designator__c,Uses_Commercial_Mail_Receiving_Agency__c,DPV_Reason_Code_s__c,Type_of_Address__c,Address_is_Active__c,Address_Not_Ready_for_Mail__c,lastmodifieddate from Contact where id=:id];
        d1=con.Last_Modified_Date_For_Reload__c.Date();
        d2=system.now().Date();
        t1=con.Last_Modified_Date_For_Reload__c.Time();
        t2=system.now().Time();
        System.debug('Vertex-SmartyStreets t2**********************'+t2);
        t3=t1+(1000*30);
        System.debug('Vertex-SmartyStreets t4**************'+t3);
       
          if(t3<t2){
           stop_ap=false; 
           
           System.debug('Vertex-SmartyStreets t4******2********'+t3);
          }
        // con=[select Street_Status__c,Do_Not_Validate_Address__c from Contact  where id=:id limit 1];
         
         if(con.Street_Status__c!=null){
          if(con.Street_Status__c=='valid'){
          
                        stop_ap=false; 
                        process=false;
                        green=true;
                        red=false;
                        }
           else{
           
                        stop_ap=false; 
                        process=false;
                        green=false;
                        red=true;
           }             
                        
         }
         
         
        return null;
    }
    
    
  
    
    
     /// <summary>
    ///  To make Future Callout to SmartStreet   
    /// </summary>
    
    
    
    @Future(callout=true)
    public static void updateContact(string id) {
       
        con =[select id,name,mailingstate,mailingcity,mailingStreet,mailingpostalcode,mailingCountry,Street_Status__c ,description from contact where id =:id];
        System.debug('Testingg con '+con);
        Boolean bolValid = false;
        //construct an HTTP request
        HttpRequest req = new HttpRequest();
        HttpResponse res;
        String Url = 'https://api.smartystreets.com/street-address?street='+con.mailingStreet+'&city='+con.mailingcity+'&state='+con.mailingState+'&zipcode='+con.mailingpostalcode+'&candidates=5&auth-id=ad36b28c-2b30-d1e0-4d25-e28af101b7a7&auth-token=jLI2rTAr4NFNJeKK8QG3p7J2mNGLqDCFQ8PxOZZ7sPPI3s9FqILlQ%2FQvdZFMtNZJollBnhDHvfLWmotF2x8fNA%3D%3D';
        Url = Url.replace(' ','+');
        String urll = Url;
        req.setEndpoint(Url);
        req.setMethod('GET');
        
        //send the request
        Http http = new Http();
        System.debug('Testingg req '+req);
        if(!Test.isRunningTest()){
        res = http.send(req);
        }else
        {
        res= respond();
        }
        System.debug('Testingg res '+res);
         updateHelper.inFutureContext = true;
         if(res.getStatusCode()==200)
          {
            System.debug('Testingg res.getBody() '+res.getBody());
            /*if(res.getBody()== null) {
            	System.debug('Testingg Inside Iff');
            }
            else if(res.getBody()== '') {
            	System.debug('Testingg Inside elsee 1');
            }
            else if(String.isBlank(res.getBody())){
            	System.debug('Testingg Inside elsee 2');
            }
            else if(res.getBody().equals('[]')){
            	System.debug('Testingg Inside elsee 4'+res.getBody()+'Inside');
            }
            else {
            	System.debug('Testingg Inside elsee');
            }*/
            
            //System.debug('Testingg res.getBodyDocument() '+res.getBodyDocument());
            parser = JSON.createParser(res.getBody());
            System.debug('Testingg parser.nextToken() '+parser.nextToken());
            /*if(res.getBody()!='[]')
            {
             */
             System.debug('Testingg parser '+parser);
             System.debug('Testingg parser hasCurrentToken '+parser.hasCurrentToken());
              while (parser.nextToken() != null) {
                
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                	bolValid = true;
                    String text = parser.getText();
                    parser.nextToken();
                    System.debug('Testingg text '+text);
                    System.debug('text*************1'+text);
                        if (text == 'input_index') {
                            input_index = parser.getText();
                        }
                        if (text == 'candidate_index') {
                            candidate_index = parser.getText();
                        } 
                        if (text == 'delivery_line_1') {
                            delivery_line_1 = parser.getText();
                        } 
                         if(text=='last_line'){
                            last_line=parser.getText();
                         }
                         if(text=='delivery_point_barcode'){
                            delivery_point_barcode=parser.getText();
                         }
                         if (text == 'primary_number') {
                            primary_number = parser.getText();
                         }
                        if(text=='street_name'){
                            street_name=parser.getText();
                         }
                         if(text=='street_suffix'){
                            treet_suffix=parser.getText();
                        }
                          if(text=='city_name'){
                              city_name=parser.getText();
                         }
                         if(text=='state_abbreviation'){
                            state_abbreviation=parser.getText();
                            
                        }
                          if(text=='zipcode'){
                                    zipcode1=parser.getText();
                        }
                        if(text=='plus4_code'){
                                plus4_code=parser.getText();               
                        }
                        if(text=='delivery_point'){
                            delivery_point=parser.getText();
                        }
                        if(text=='delivery_point_check_digit'){
                            delivery_point_check_digit=parser.getText();
                        } 
                         if(text=='record_type'){
                            record_type=parser.getText();
                        }
                        if(text=='zip_type'){
                            zip_type=parser.getText();
                        }
                         if(text=='county_fips'){
                            county_fips=parser.getText();
                        }
                        if(text=='county_name'){
                               county_name=parser.getText();
                        }
                        if(text=='carrier_route'){
                            carrier_route=parser.getText();
                        }
                        if(text=='congressional_district'){
                            congressional_district=parser.getText();
                        }
                         if(text=='rdi'){
                            rdi=parser.getText();
                        }         
                        if(text=='elot_sequence'){
                            elot_sequence=parser.getText(); 
                        }
                        if(text=='elot_sort'){
                            
                            elot_sort=parser.getText();
                        }
                         if(text=='latitude'){
                            latitude=parser.getDoubleValue();
                        }
                        if(text=='longitude'){
                            longitude=parser.getDoubleValue();
                        }
                        if(text=='precision'){
                            precision=parser.getText();
                        }
                         if(text=='time_zone'){
                            time_zone=parser.getText();
                        }
                         if(text=='utc_offset'){
                                utc_offset=parser.getText();
                        }
                          if(text=='dst')
                        {
                            dst=parser.getText();
                        } 
                        if(text=='dpv_match_code'){
                            dpv_match_code=parser.getText();
                        }
                        if(text=='dpv_footnotes'){
                            dpv_footnotes=parser.getText();
                        }
                        if(text=='dpv_cmra'){
                            dpv_cmra=parser.getText();
                        }
                        if(text=='dpv_vacant'){
                            dpv_vacant=parser.getText();
                        }
                         if(text=='active'){
                            active=parser.getText();
                        }
                        if(text=='footnotes'){    
                            footnotes=parser.getText();
                        }
                        if(text=='ews_match'){
                             ews_match =parser.getText();
                        }
                        if(text=='addressee')
                        {
                             addressee=parser.getText();
                        }
                        if(text=='LACSLink_Code'){    
                           LACSLink_Code=parser.getText();
                        }
                 
                        if(text=='LACSLink_Match_Indicator'){    
                            LACSLink_Match_Indicator=parser.getText();
                                        }
                        if(text=='SuiteLink_Match'){    
                            SuiteLink_Match=parser.getText();
                                        }
                        if(text=='building_default_indicator'){    
                           building_default_indicator=parser.getText();
                                        }
                        if(text=='pmb_number'){    
                           pmb_number=parser.getText();
                                        }
                        if(text=='pmb_designator'){    
                            pmb_designator=parser.getText();
                                        }
                        if(text=='delivery_point_barcode'){    
                            delivery_point_barcode=parser.getText();
                                        }
                        if(text=='delivery_line_2'){
                        
                        delivery_line_2 =parser.getText();
                        }                
                          
                }
           }
                
                if(bolValid) {
	                System.debug('Vertex-SmartyStreets**county_name**********'+county_name+'county_fips********'+county_fips+'active**********'+active+'state_abbreviation************'+state_abbreviation+'record_type**************'+record_type+'zip_type********'+zip_type+'carrier_route***********'+carrier_route+'rdi***********'+rdi+'latitude*********'+latitude+'time_zome*********'+time_zone+'delivery_point_check_digit***********'+delivery_point_check_digit);
	                System.debug('Vertex-SmartyStreets**utc_offset*********'+utc_offset+'dst***********'+dst+'dpv_match_code**********'+dpv_match_code+'dpv_footnotes**********'+dpv_footnotes+'dpv_cmra************'+dpv_cmra+'dpv_vacant************'+dpv_vacant+'footnotes************'+footnotes);
	                zipcode1=zipcode1+'-'+plus4_code;
                
	                con.Latitude__latitude__s=Double.valueOf(latitude); 
	                con.Latitude__longitude__s=Double.valueOf(longitude);
	                con.County_FIPS_Code__c= county_fips;
	                con.County_Name__c =county_name;
	                con.Geolocation_Precision__c =precision;
	                con.Daylight_Savings_Observed__c = dst ;
	                con.Delivery_Point_Validation_Code__c = dpv_match_code;
	                con.Type_of_Zip_Code__c=zip_type;
	                con.Delivery_Point_Vacant__c=dpv_vacant;
	                con.Address_Changes_Made_Codes__c =footnotes;
	                con.Residential_Delivery_Indicator__c=rdi; 
	                con.LACSLink_Code__c=LACSLink_Code;
	                con.LACSLink_Match_Indicator__c=LACSLink_Match_Indicator;
	                con.SuiteLink_Match__c=SuiteLink_Match;
	                con.Building_Default_Indicator__c=building_default_indicator;
	                con.POSTNET_Barcode__c=delivery_point_barcode;
	                con.Private_Mailbox_Number__c=pmb_number;
	                con.Private_Mailbox_Unit_Designator__c=pmb_designator;
	                con.Uses_Commercial_Mail_Receiving_Agency__c=dpv_cmra;
	                con.DPV_Reason_Code_s__c=dpv_footnotes;
	                con.Type_of_Address__c=record_type;
	                con.Address_is_Active__c =active;
	                con.Address_Not_Ready_for_Mail__c=ews_match;
	                con.UTC_Offset__c =utc_offset;
	                con.Carrier_Route__c = carrier_route; 
	                con.Time_Zone__c=time_zone;
	                con.Addressee__c=addressee;
	                con.mailingStreet=delivery_line_1;
	                con.mailingCity=city_name;
	                con.mailingstate=state_abbreviation;
	                con.mailingPostalCode=zipcode1; 
	                 
	                //these filds for validation purpose
	                con.Street_Status__c ='valid';
                
                
                
                /*  
                  Missing filed from response
               
                con.Address_Changes_Made_Codes__c = footnotes;
                con.Address_Not_Ready_for_Mail__c = ews_match; 
                con.Addressee__c=addressee
                */
                if(delivery_line_2!=null){
                    con.MailingStreet=delivery_line_1+delivery_line_2;
                 }
                
            
             try{
                    updateHelper.inFutureContext = true;   
                    update con;
                }
                catch(DmlException e)
                {
                    System.debug('Vertex-SmartyStreets**e*******************'+e);
                }
             }
            //}
          
           else{
               
                con.Latitude__latitude__s=Double.valueOf(00.000); 
                con.Latitude__longitude__s=Double.valueOf(00.000);
                con.County_FIPS_Code__c= '';
                con.County_Name__c ='';
                con.Geolocation_Precision__c ='';
                con.Daylight_Savings_Observed__c ='' ;
                con.Delivery_Point_Validation_Code__c = '';
                con.Type_of_Zip_Code__c='';
                con.Delivery_Point_Vacant__c='';
                con.Address_Changes_Made_Codes__c ='';
                con.Residential_Delivery_Indicator__c=''; 
                con.LACSLink_Code__c='';
                con.LACSLink_Match_Indicator__c='';
                con.SuiteLink_Match__c='';
                con.Building_Default_Indicator__c='';
                con.POSTNET_Barcode__c='';
                con.Private_Mailbox_Number__c='';
                con.Private_Mailbox_Unit_Designator__c='';
                con.Uses_Commercial_Mail_Receiving_Agency__c='';
                con.DPV_Reason_Code_s__c='';
                con.Type_of_Address__c='';
                con.Address_is_Active__c ='';
                con.Address_Not_Ready_for_Mail__c='';
                con.UTC_Offset__c ='';
                con.Carrier_Route__c =''; 
                con.Time_Zone__c='';
                /*con.mailingStreet='';
                con.mailingCity='';
                con.mailingstate='';
                con.mailingPostalCode='';
                con.mailingCountry=''; */
                
                 //these filds for validation purpose
               
                con.Street_Status__c ='invalid';
                
             try{
                    updateHelper.inFutureContext = true;   
                    update con;
                }
                catch(DmlException e)
                {
                    System.debug('Vertex-SmartyStreets**e*******************'+e);
                }
          }
    }
                    else
                    {
                        con.Street_Status__c = 'BadRequest';    
                        try{
                                updateHelper.inFutureContext = true;   
                                update con;
                            }
                       catch(DmlException e)
                        {
                                System.debug('Vertex-SmartyStreets**e*******************'+e);
                        }
    
                   }        
        
               }
                        
       public static HTTPResponse respond() {
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('[{"input_index":0,"candidate_index":0,"delivery_line_1":"3785 Las Vegas Blvd S","last_line":"Las Vegas NV 89109-4333","delivery_point_barcode":"891094333992","components":{"primary_number":"3785","street_name":"Las Vegas","street_postdirection":"S","street_suffix":"Blvd","city_name":"Las Vegas","state_abbreviation":"NV","zipcode":"89109","plus4_code":"4333","delivery_point":"99","delivery_point_check_digit":"2"},"metadata":{"record_type":"H","zip_type":"Standard","county_fips":"32003","county_name":"Clark","carrier_route":"C024","congressional_district":"01","building_default_indicator":"Y","rdi":"Commercial","elot_sequence":"0119","elot_sort":"A","latitude":36.10363,"longitude":-115.17237,"precision":"Zip9","time_zone":"Pacific","utc_offset":-8.0,"dst":true},"analysis":{"dpv_match_code":"D","dpv_footnotes":"AAN1","dpv_cmra":"N","dpv_vacant":"N","active":"Y","footnotes":"A#B#H#L#M#","dpv_vacant":"N","ews_match":"esw","addressee":"address","LACSLink_Code":"LACS","LACSLink_Match_Indicator":"LACS","SuiteLink_Match":"Suite","building_default_indicator":"Build","pmb_number":"pmbn","pmb_designator":"pmbd","delivery_point_barcode":"Dpb","delivery_line_2":"Dl2"}}]');
        res.setStatusCode(200);
        return res;
    }      
                        
                        
        }