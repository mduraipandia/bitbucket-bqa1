@isTest(seeAllData=true)
public class SubmitChangesManualYodleControllerTest {

    public static testmethod void submitChngs1() {
        
        Test.StartTest();

        String rTypId = Label.Video_RT_Id;
        Id accMngrId = System.Label.TestUserPerformanceAdvisorId;
        List < ChangedDFFField__c > allCDffs = new List < ChangedDFFField__c > ();
                 
        Canvass__c c = CommonUtility.createCanvasTest();
        insert c;

        Account a = CommonUtility.createAccountTest(c);
        a.Account_Manager__c = accMngrId;
        a.TalusAccountId__c = 'afdfd132323';
        insert a;

        Contact cnt = CommonUtility.createContactTest(a);
        insert cnt;
        
        Opportunity oppty = CommonUtility.createOpptyTest(a);
        insert oppty;

        Order__c ord = CommonUtility.createOrderTest(a);
        insert ord;

        Order_Group__c og = CommonUtility.createOGTest(a, ord, oppty);
        insert og;
        
        Order_Line_Items__c oln = CommonUtility.createOLITest(c, a, cnt, oppty, ord, og);
        oln.Status__c = 'In Progress';
        oln.Talus_Go_Live_Date__c = system.Today();
        insert oln;

        Modification_Order_Line_Item__c mOLI = CommonUtility.createmOLITest(c, a, cnt, oppty, ord, og);
        mOLI.Status__c = 'In Progress';
        mOLI.Order_Line_Item__c = oln.Id;
        insert mOLI;

        Digital_Product_Requirement__c dff = CommonUtility.createDffTest();
        dff.OrderLineItemID__c = oln.Id;
        dff.recordTypeId = Label.Extra_URL_RT_Id;
        dff.Fulfillment_Submit_Status__c = 'Complete';
        dff.Contact__c = cnt.Id;
        insert dff;

        ChangedDFFField__c cDff1 = CommonUtility.createCDffFldTest(dff.Id, 'UDAC', 'String');
        allCDffs.add(cDFf1);

        ChangedDFFField__c cDff2 = CommonUtility.createCDffFldTest(dff.Id, 'setup_notes', 'String');
        allCDffs.add(cDFf2);

        Digital_Product_Requirement__c dff1 = CommonUtility.createDffTest();
        dff1.ModificationOrderLineItem__c = mOLI.Id;
        dff1.recordTypeId = Label.Extra_URL_RT_Id;
        dff1.Fulfillment_Submit_Status__c = 'Complete';
        dff1.Contact__c = cnt.Id;
        insert dff1;

        ChangedDFFField__c cDff3 = CommonUtility.createCDffFldTest(dff1.Id, 'UDAC', 'String');
        allCDffs.add(cDff3);

        ChangedDFFField__c cDff4 = CommonUtility.createCDffFldTest(dff1.Id, 'setup_notes', 'String');
        allCDffs.add(cDff4);

        insert allCDffs; 
                       
        dffChngs(dff, dff1);

        Digital_Product_Requirement__c dffChngs1 = CommonUtility.createDffTest();
        dffChngs1.OrderLineItemID__c = oln.Id;
        dffChngs1.recordTypeId = Label.Extra_URL_RT_Id;
        dffChngs1.Fulfillment_Submit_Status__c = 'Complete';
        dffChngs1.Contact__c = cnt.Id;
        insert dffChngs1;

        Digital_Product_Requirement__c dffChngs2 = CommonUtility.createDffTest();
        dffChngs2.ModificationOrderLineItem__c = MOLI.Id;
        dffChngs2.recordTypeId = Label.Extra_URL_RT_Id;
        dffChngs2.Fulfillment_Submit_Status__c = 'Complete';
        dffChngs2.Contact__c = cnt.Id;
        insert dffChngs2;
        
        dffChngs(dffChngs1, dffChngs2);
        
        oln.Status__c = 'Cancelled';
        update oln;
        
        mOLI.Status__c = 'Cancelled';
        update mOLI;
        
        dffChngs(dff, dff1);                
        
        Test.StopTest();
        
    }
    
    public static testmethod void submitChngs2() {
        
        Test.StartTest();
        String rTypId = Label.Video_RT_Id;
        Id accMngrId = System.Label.TestUserPerformanceAdvisorId;
        List < ChangedDFFField__c > allCDffs = new List < ChangedDFFField__c > ();
                 
        Canvass__c c = CommonUtility.createCanvasTest();
        insert c;

        Account a = CommonUtility.createAccountTest(c);
        a.Account_Manager__c = accMngrId;
        a.TalusAccountId__c = 'afdfd132323';
        insert a;

        Contact cnt = CommonUtility.createContactTest(a);
        insert cnt;
        
        Opportunity oppty = CommonUtility.createOpptyTest(a);
        insert oppty;

        Order__c ord = CommonUtility.createOrderTest(a);
        insert ord;

        Order_Group__c og = CommonUtility.createOGTest(a, ord, oppty);
        insert og;
        
        Order_Line_Items__c oln = CommonUtility.createOLITest(c, a, cnt, oppty, ord, og);
        oln.Status__c = 'New';
        oln.Talus_Go_Live_Date__c = system.Today();
        insert oln;

        Modification_Order_Line_Item__c mOLI = CommonUtility.createmOLITest(c, a, cnt, oppty, ord, og);
        mOLI.Status__c = 'New';
        mOLI.Order_Line_Item__c = oln.Id;
        insert mOLI;

        Digital_Product_Requirement__c dff = CommonUtility.createDffTest();
        dff.OrderLineItemID__c = oln.Id;
        dff.recordTypeId = Label.Yodle_SEM_RT_Id;
        dff.Fulfillment_Submit_Status__c = 'Complete';
        dff.Contact__c = cnt.Id;
        insert dff;

        ChangedDFFField__c cDff1 = CommonUtility.createCDffFldTest(dff.Id, 'UDAC', 'String');
        allCDffs.add(cDFf1);

        ChangedDFFField__c cDff2 = CommonUtility.createCDffFldTest(dff.Id, 'setup_notes', 'String');
        allCDffs.add(cDFf2);

        Digital_Product_Requirement__c dff1 = CommonUtility.createDffTest();
        dff1.ModificationOrderLineItem__c = mOLI.Id;
        dff1.recordTypeId = Label.Yodle_SEM_RT_Id;
        dff1.Fulfillment_Submit_Status__c = 'Complete';
        dff1.Contact__c = cnt.Id;
        insert dff1;

        ChangedDFFField__c cDff3 = CommonUtility.createCDffFldTest(dff1.Id, 'UDAC', 'String');
        allCDffs.add(cDff3);

        ChangedDFFField__c cDff4 = CommonUtility.createCDffFldTest(dff1.Id, 'setup_notes', 'String');
        allCDffs.add(cDff4);

        insert allCDffs; 
                       
        dffChngs(dff, dff1);

        Digital_Product_Requirement__c dffChngs1 = CommonUtility.createDffTest();
        dffChngs1.OrderLineItemID__c = oln.Id;
        dffChngs1.recordTypeId = Label.Yodle_SEM_RT_Id;
        dffChngs1.Fulfillment_Submit_Status__c = 'Complete';
        dffChngs1.Contact__c = cnt.Id;
        insert dffChngs1;

        Digital_Product_Requirement__c dffChngs2 = CommonUtility.createDffTest();
        dffChngs2.ModificationOrderLineItem__c = MOLI.Id;
        dffChngs2.recordTypeId = Label.Yodle_SEM_RT_Id;
        dffChngs2.Fulfillment_Submit_Status__c = 'Complete';
        dffChngs2.Contact__c = cnt.Id;
        insert dffChngs2;
        
        dffChngs(dffChngs1, dffChngs2);
        
        oln.Status__c = 'Cancelled';
        update oln;
        
        mOLI.Status__c = 'Cancelled';
        update mOLI;
        
        dffChngs(dff, dff1);                
        
        Test.StopTest();
        
    }
    
    public static void dffChngs(Digital_Product_Requirement__c dff, Digital_Product_Requirement__c dff1){
        
        ApexPages.StandardController sc1 = new ApexPages.StandardController(dff);
        SubmitChangesManualYodleController CTS1 = new SubmitChangesManualYodleController(sc1);
        CTS1.sbmtChngs();

        ApexPages.StandardController sc2 = new ApexPages.StandardController(dff1);
        SubmitChangesManualYodleController CTS2 = new SubmitChangesManualYodleController(sc2);
        CTS2.sbmtChngs();            
    
    }
               
}