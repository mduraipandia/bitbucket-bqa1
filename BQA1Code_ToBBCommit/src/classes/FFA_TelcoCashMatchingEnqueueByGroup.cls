/*
    * FinancialForce.com, inc. claims copyright in this software, its screen display designs and
    * supporting documentation. FinancialForce and FinancialForce.com are trademarks of FinancialForce.com, inc.
    * Any unauthorized use, copying or sale of the above may constitute an infringement of copyright and may
    * result in criminal or other legal proceedings.
    *
    * Copyright FinancialForce.com, inc. All rights reserved.
*/

public class FFA_TelcoCashMatchingEnqueueByGroup implements Database.Batchable<SObject>, Database.Stateful
{
    private Set<String> errorMessages;
    private Set<String> successMessages;
    
    public Integer enqueuedCount = 0;
    
    public FFA_TelcoCashMatchingEnqueueByGroup() 
    {
        errorMessages = new Set<String>();
        successMessages = new Set<String>();
    }

    public Database.QueryLocator start(Database.BatchableContext BC) 
    {
        return Database.getQueryLocator('Select Id From ffps_bmatching__Custom_Cash_Matching_History_Group__c Order By ffps_bmatching__Group_Number__c, ffps_bmatching__Group_Size__c');
    }

    public void execute(Database.BatchableContext BC, List<SObject> unTypedScope) 
    {
        for(SObject scopeItem :  (List<SObject>)unTypedScope)
        {
            System.enqueueJob(new FFA_TelcoCashMatchingByGroupQueueable(scopeItem.Id));
            enqueuedCount += 1;
        }
    }
    
    public void finish(Database.BatchableContext BC) 
    {
        Datetime sysTime = System.now().addminutes( 10 );
        String chron_exp = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
        FFA_UpdateTelcoAccountRollupBatch batch = new FFA_UpdateTelcoAccountRollupBatch();
        System.schedule('FFA_UpdateTelcoAccountRollupBatch' + sysTime.getTime(), chron_exp, batch);

        successMessages.add('Threads started = '+enqueuedCount +' At - '+System.now());
        sendEmail('uravat@financialforce.com',successMessages,errorMessages);
    }
    
    private void sendEmail( String address, Set<String> successMessages, Set<String> errorMessages  )
    {   
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses( new String[] { address } );

        String body = 'Results of Matching - Matching API (FFA_CashMatchingEnqueueByAccountGroup):\n\n';
        
        if( errorMessages.size() > 0 )
        {
            body += 'Errors occurred.\n';
            for( String error : errorMessages )
            {
                body += error +'\n';
            }
            body += '\n';
        }
        else
        {
            body += 'No errors occurred.\n';
            body += '\n';
        }
        for( String success : successMessages )
        {
            body += success +'\n';
        }
        
        mail.setPlainTextBody( body );
        mail.setSubject( 'Results of Matching - Matching API (FFA_CashMatchingEnqueueByAccountGroup)' );
        
        Messaging.sendEmail( new Messaging.SingleEmailMessage[] { mail } );
    }
}