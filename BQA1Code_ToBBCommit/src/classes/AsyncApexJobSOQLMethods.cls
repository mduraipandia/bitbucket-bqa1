global class AsyncApexJobSOQLMethods {

    public static AsyncApexJob getBatchDetails(ID batchId) {
        return [Select Id, Status, NumberOfErrors, JobItemsProcessed,
        TotalJobItems, CreatedBy.Email, ExtendedStatus from AsyncApexJob where Id =:batchId];
    }
}