public class OpportunityHandlerController {
    public static void onBeforeInsert(list<Opportunity> lstOpportunity) {
		Set<Id> setAcctIdsForAcctCon = new Set<Id>();
		//Commented out by sathish - ATP-4481
		//Set<Id> setAcctIdsForTelcoPart = new Set<Id>();
		//Set<Id> setCanvassIdsForTelcoPart = new Set<Id>();
		
		set<Id> setOwnerIDs = new set<Id>();
		list<Opportunity> listOpptyForAcctCon = new list<Opportunity>();
		//Commented out by sathish - ATP-4481
		//list<Opportunity> listOpptyForTelcoPart = new list<Opportunity>();
		list<Opportunity> listOpptyForOwner = new list<Opportunity>();		
		
        ID nRecordTypeID = CommonMethods.getRedordTypeIdByName(CommonMessages.opportunityNationalRT, CommonMessages.opportunityObjectName);     
		
		for(Opportunity iteratorOpp : lstOpportunity) {
			if(iteratorOpp.Converted_From_Lead__c == true) {
                iteratorOpp.CloseDate = system.today();
            }
			
			if(String.isNotBlank(iteratorOpp.AccountId)) {
				setAcctIdsForAcctCon.add(iteratorOpp.AccountId);
				listOpptyForAcctCon.add(iteratorOpp);
			}
			
			if(nRecordTypeID != iteratorOpp.RecordTypeId) {
				//Commented out by sathish - ATP-4481
				/*if(iteratorOpp.billing_defaults_set__c != True) {
					setAcctIdsForTelcoPart.add(iteratorOpp.AccountId);
					listOpptyForTelcoPart.add(iteratorOpp);
				}*/
				if(iteratorOpp.OwnerId != null) {
					setOwnerIDs.add(iteratorOpp.OwnerId);
					listOpptyForOwner.add(iteratorOpp);
				}
				else {
					setOwnerIDs.add(UserInfo.getUserId());
					listOpptyForOwner.add(iteratorOpp);
				}
			}
			
			/* Commented as per CC-3136
			if(iteratorOpp.StageName == CommonMessages.closedOwnStageOpp && iteratorOpp.CORE_Migration_ID__c == null && iteratorOpp.CreatedById != System.Label.MigrationUserID) {
				iteratorOpp.Close_Date__c = Datetime.now();
			}*/
		}	

		if(setAcctIdsForAcctCon.size() > 0) {
			populateAccountAndBillingContactInfo(listOpptyForAcctCon, setAcctIdsForAcctCon);
		}
		//Commented out by sathish - ATP-4481
		/*if(setCanvassIdsForTelcoPart.size() > 0) {
			defaultBillingTelcoPartner(listOpptyForTelcoPart, setAcctIdsForTelcoPart, setCanvassIdsForTelcoPart);
		}*/
		
		if(setOwnerIDs.size() > 0) {
			populateOpportunityOwnerFields(listOpptyForOwner, setOwnerIDs);
		}
    }
    
    public static void onBeforeUpdate(list<Opportunity> lstOpportunity, Map<Id, Opportunity> NewMapOpportunity, Map<Id, Opportunity> oldMapOpportunity) {
		Set<Id> setAcctIdsForAcctCon = new Set<Id>();
		
		//Commented out by sathish - ATP-4481
		/*Set<Id> setCanvassIdsForTelcoPart = new Set<Id>();
		Set<Id> setAcctIdsForTelcoPart = new Set<Id>();
		list<Opportunity> listOpptyForTelcoPart = new list<Opportunity>();*/
		
		set<Id> setOwnerIDs = new set<Id>();
		list<Opportunity> listOpptyForAcctCon = new list<Opportunity>();
		list<Opportunity> listOpptyForOwner = new list<Opportunity>();
		list<Opportunity> listOpptyForRecType = new list<Opportunity>();
		
        ID nRecordTypeID = CommonMethods.getRedordTypeIdByName(CommonMessages.opportunityNationalRT, CommonMessages.opportunityObjectName);
        
        for(Opportunity iteratorOpp : lstOpportunity) {
			Opportunity oldOppty = oldMapOpportunity.get(iteratorOpp.Id);
			if(String.isNotBlank(iteratorOpp.AccountId) && iteratorOpp.AccountId != oldOppty.AccountId) {
				setAcctIdsForAcctCon.add(iteratorOpp.AccountId);
				listOpptyForAcctCon.add(iteratorOpp);
			}
			
			if(nRecordTypeID != iteratorOpp.RecordTypeId) {
				//Commented out by sathish - ATP-4481
				/*if(iteratorOpp.billing_defaults_set__c != True) {
					if(oldOppty.Account_Primary_Canvass__c != iteratorOpp.Account_Primary_Canvass__c) {
						setCanvassIdsForTelcoPart.add(iteratorOpp.Account_Primary_Canvass__c);
						listOpptyForTelcoPart.add(iteratorOpp);
					}
				}*/
				if(iteratorOpp.OwnerId != oldOppty.OwnerId) {
					if(iteratorOpp.OwnerId != null) {
						setOwnerIDs.add(iteratorOpp.OwnerId);
						listOpptyForOwner.add(iteratorOpp);
					}
					else {
						setOwnerIDs.add(UserInfo.getUserId());
						listOpptyForOwner.add(iteratorOpp);
					}
				}				
				if(oldOppty.isLocked__c == false) {
					listOpptyForRecType.add(iteratorOpp);
				}
			}
			
			/* Commented as per CC-3136
			if(oldOppty.StageName != CommonMessages.closedOwnStageOpp && iteratorOpp.StageName == CommonMessages.closedOwnStageOpp && iteratorOpp.CORE_Migration_ID__c == null && iteratorOpp.CreatedById != System.Label.MigrationUserID) {
				iteratorOpp.Close_Date__c = Datetime.now();
			}*/
		}
		
		if(setAcctIdsForAcctCon.size() > 0) {
			populateAccountAndBillingContactInfo(listOpptyForAcctCon, setAcctIdsForAcctCon);
		}
		
		//Commented out by sathish - ATP-4481
		/*if(setCanvassIdsForTelcoPart.size() > 0) {
			defaultBillingTelcoPartner(listOpptyForTelcoPart, setAcctIdsForTelcoPart, setCanvassIdsForTelcoPart);
		}*/
		
		if(setOwnerIDs.size() > 0) {
			populateOpportunityOwnerFields(listOpptyForOwner, setOwnerIDs);
		}
		
		if(listOpptyForRecType.size() > 0) {
			updateRecordType(listOpptyForRecType, NewMapOpportunity, oldMapOpportunity);
		}
    }
    
    public static void onAfterUpdate(list<Opportunity> lstOpportunity, Map<Id, Opportunity> oldMapOpportunity, Map<Id, Opportunity> newMapOpportunity) {
		Set<Id> setOpptyIdsForTalus = new Set<Id>();
		Set<Id> setOpptyIdsForLateOrder = new Set<Id>();
        ID nRecordTypeID = CommonMethods.getRedordTypeIdByName(CommonMessages.opportunityNationalRT, CommonMessages.opportunityObjectName);
		
        for(Opportunity iteratorOpp : lstOpportunity) {
			if(nRecordTypeID != iteratorOpp.RecordTypeId) {
				if(iteratorOpp.CORE_Migration_ID__c == null) {
					if(oldMapOpportunity.get(iteratorOpp.id).StageName!='Closed Won' && 
                        newMapOpportunity.get(iteratorOpp.id).StageName=='Closed Won') {
						setOpptyIdsForTalus.add(iteratorOpp.Id);
					}
				}
			}
			
			if(iteratorOpp.Late_Print_Order_Approved__c && iteratorOpp.Late_Print_Order_Approved__c != oldMapOpportunity.get(iteratorOpp.Id).Late_Print_Order_Approved__c) {
				setOpptyIdsForLateOrder.add(iteratorOpp.Id);
	    	}
		}
		
		if(setOpptyIdsForTalus.size() > 0) {
			//ATP-3754 - to deactive the topMOLI
			//pendingMOLI.deactiveTOPMOLI(setOpptyIdsForTalus);			
			createTalusAccount(setOpptyIdsForTalus);
		}
		
		if(setOpptyIdsForLateOrder.size() > 0) {
			populateDELateOrderApproval(setOpptyIdsForLateOrder);
		}
    }
    
    public static void onBeforeDelete(list<Opportunity> lstOpportunity, list<Opportunity> lstOldOpportunity, Map<Id, Opportunity> oldMapOpportunity) {
        ID nRecordTypeID = CommonMethods.getRedordTypeIdByName(CommonMessages.opportunityNationalRT, CommonMessages.opportunityObjectName);
        deleteOpportunity(null,lstOldOpportunity, oldMapOpportunity);
    }
        
    private static void deleteOpportunity(list<Opportunity> lstOpportunity, list<Opportunity> lstOldOpportunity, Map<Id, Opportunity> oldMapOpportunity) {
        for(Opportunity iteratorOpp : lstOldOpportunity) {
            // Checked current opportunity has been won or not
            if(iteratorOpp.isLocked__c == true){
                // If the Opportunity already won through the message in UI
                oldMapOpportunity.get(iteratorOpp.Id).addError(CommonMessages.opportunityModifyorDelete);
            }
        }
    }    
    
    public static String checkOpportunityRecordTypeNeedChange(Opportunity newoldOpportunity, Opportunity oldOpportunity, BigMachines__Quote__c objBMIQuote) {  
        if(oldOpportunity.isLocked__c == false && newoldOpportunity.isLocked__c == true) {                    
            return 'Lock';
        }
        
        if(newoldOpportunity.isLocked__c == false && objBMIQuote != null) {
            list<BigMachines__Quote_Product__c> lstBMIQuoteProduct = objBMIQuote.BigMachines__BigMachines_Quote_Products__r;
            //system.debug('**********BMI Quote Line Item ***********'+ lstBMIQuoteProduct.size());
            if(newoldOpportunity.BigMachines__Line_Items__c <= 0) {
            	return 'New';
            }
            if(lstBMIQuoteProduct.size() > 0 && (CommonMethods.checkOpportunityHasClosedWon(newoldOpportunity) || newoldOpportunity.stageName == 'Closed Lost') && CommonMethods.checkSigningMethods(newoldOpportunity,null) && CommonMethods.checkBillingInfo(newoldOpportunity,null)) {
                /*if(objBMIQuote.BigMachines__Is_Primary__c == true && objBMIQuote.Submitted_for_Approval__c == true) {
                	return 'New';
                }
                
                if(objBMIQuote.Approval_Triggers__c != null && objBMIQuote.Approval_Status__c.equals('Pending')) {
                	return 'New';
                }
                
                if(objBMIQuote.Approval_Triggers__c != null && objBMIQuote.Approval_Status__c.equals('Rejected')) {
                	return 'New';
                }*/
                return 'Assign';
            }
            else{
                return 'New';
            }
        }
        return 'Nothing';
     }
	 
	 private static void populateAccountAndBillingContactInfo(list<Opportunity> lstOpportunity, Set<ID> accountID) {
        map<ID, Account> mapAccount;
        if(accountID.size() > 0) {
            // Fetch the account and contact information based on opportunity account id.
            mapAccount = AccountSOQLMethods.getAccountContactByAccountID(accountID);
        }
        
        for(Opportunity iteratorOpp : lstOpportunity) {
            if(iteratorOpp.AccountId != null) {
                // Get the account and contact information from map collection based on account id
                Account objAccount = mapAccount.get(iteratorOpp.AccountId);
                if(String.isNotBlank(objAccount.Tier_Override__c)) {
                	iteratorOpp.Client_Tier__c = objAccount.Tier_Override__c;
                }
                else {
                	iteratorOpp.Client_Tier__c = objAccount.Client_Tier__c;
                }
                // assign the account manager value to opportunity account manager field
                if(iteratorOpp.Account_Manager__c == null) {
                    iteratorOpp.Account_Manager__c = objAccount.Account_Manager__c;
                }
                // Assign the account primary canvas to opportunity canvass.
                if(iteratorOpp.Account_Primary_Canvass__c == null) {
                    iteratorOpp.Account_Primary_Canvass__c = objAccount.Primary_Canvass__c;
                }
                
                // Account Number prepopulated from Account to New opportunity
                if(iteratorOpp.Account_Number__c == null){
                    iteratorOpp.Account_Number__c = objAccount.Account_Number__c;
                }
                                    
                if(iteratorOpp.Billing_Contact__c == null){
                    for(Contact objContact : objAccount.Contacts) {
                        //Opportunity Billing Contact Field is populated from the active billing contact from Account
                        if(objContact.PrimaryBilling__c == True && objContact.IsActive__c) {
                            iteratorOpp.Billing_Contact__c = objContact.ID;                            
                        }
                    }
                }
            }
        }
    }
	
	private static void defaultBillingTelcoPartner(list<Opportunity> lstOpportunity, set<ID> accountID, set<ID> canvassID) {
        set<ID> telcoPartnerId = new set<ID>();
        
        if(canvassID.size() > 0) {
            list<Directory_Mapping__c> lstDL = [SELECT Id, Canvass__c, Telco__c, Telco__r.Account__c FROM Directory_Mapping__c 
                                       WHERE Default_Canvass_Telco__c = true AND Canvass__c IN :canvassID 
                                       AND Telco__c != Null];
            if(lstDL.size() > 0) {
                accountID = new set<ID>();
                for(Directory_Mapping__c iteratorDL : lstDL) {
                    accountID.add(iteratorDL.Telco__r.Account__c);
                }
            }
        }
        
        if(accountID.size() > 0) {
            map<Id, Account> mapAccount = new map<Id, Account>([SELECT Id, Telco_Partner__c FROM Account WHERE id IN :accountID]);
            
            if(mapAccount.size() > 0) {
                map<ID,ID> mapAccountIDTelcoPartnerID = new map<ID,ID>();
                for(Account iterator : mapAccount.values()) {
                    if(iterator.Telco_Partner__c != null) {
                        telcoPartnerId.add(iterator.Telco_Partner__c);
                        mapAccountIDTelcoPartnerID.put(iterator.Id, iterator.Telco_Partner__c);
                    }
                }
                
                if(telcoPartnerId.size() > 0) {
                    List<Telco_Billing_Defaults__c> lstTBDs = Telco_Billing_Defaults__c.getall().values();
                    map<String, Telco_Billing_Defaults__c> mspTBDs = new map<String, Telco_Billing_Defaults__c>();
                    for(Telco_Billing_Defaults__c iterator : lstTBDs) {
                        if(iterator.IsActive__c == True && iterator.Print_Only__c != True) {
                            mspTBDs.put(iterator.Account_ID__c, iterator);
                        }
                    }
                    
                    map<Id, Account> lstTelcoAcc = new map<Id, Account>([SELECT Id, Name FROM Account WHERE Id IN :telcoPartnerId]);
                    
                    for(Opportunity iteratorOpp : lstOpportunity) {
                        ID accTelcoId = mapAccountIDTelcoPartnerID.get(iteratorOpp.AccountId);
                        if(accTelcoId != null) {
                            Telco_Billing_Defaults__c objTBDs = mspTBDs.get(String.valueOf(accTelcoId));
                            Account telcoAccount = lstTelcoAcc.get(accTelcoId);
                            if(objTBDs != null) {
                                iteratorOpp.billing_defaults_set__c = True;
                                if(objTBDs.Telco_should_bill__c == True) {
                                    iteratorOpp.Billing_Partner__c = telcoAccount.Name; 
                                    iteratorOpp.Payment_Method__c = 'Telco Billing';
                                    iteratorOpp.Billing_Locked__c = True;
                                }
                                else if(objTBDs.Berry_Must_Bill__c == True ) {
                                    iteratorOpp.Billing_Partner__c = telcoAccount.Name;
                                    // meet the cc prefered billing requirement
                                    iteratorOpp.Payment_Method__c = 'Credit Card';
                                    // do not allow changes
                                    iteratorOpp.Billing_Locked__c = True;
                                }
                            }
                            else {
                                system.debug('******** never found a default**********');
                                iteratorOpp.Billing_Partner__c = telcoAccount.Name;
                                // meet the cc prefered billing requirement
                                iteratorOpp.Payment_Method__c = 'Credit Card';
                                //allow changes
                                iteratorOpp.Billing_Locked__c = False;
                            }
                        }
                        else {
                            system.debug('******** no telco found reset to defaults **********');
                            iteratorOpp.Billing_Partner__c = 'BERRY';
                            // meet the cc prefered billing requirement
                            iteratorOpp.Payment_Method__c = 'Credit Card';
                            // do not allow changes
                            iteratorOpp.Billing_Locked__c = False;    
                        }
                    }
                }
            }
        }
    }
	
	private static void populateOpportunityOwnerFields(list<Opportunity> lstOpportunity, set<Id> setOwnerIds) {
		map<Id, User> mapUser = new map<Id, User>([SELECT Id, email, phone FROM user where Id IN: setOwnerIds]);
		for(Opportunity iterator : lstOpportunity) {
			User objUser = new User();
			if(iterator.OwnerId != null) {
				objUser = mapUser.get(iterator.OwnerId);
			}
			else {
				objUser = mapUser.get(UserInfo.getUserId());
			}
			iterator.Opportunity_Owner_Email__c = objUser.email;
			iterator.Opportunity_Owner_Phone__c = objUser.phone;
		}
    }
	
	private static void updateRecordType(list<Opportunity> lstOpportunity, Map<Id, Opportunity> NewMapOpportunity, Map<Id, Opportunity> oldMapOpportunity) {
        // Fetching the opportunity record type name and id and stroed in map collection
        AllowEditOpportunity__c allowmodifyOpportunity = AllowEditOpportunity__c.getValues(UserInfo.getProfileID());
        Schema.DescribeSObjectResult R = Opportunity.SObjectType.getDescribe();
        List<Schema.RecordTypeInfo> RT = R.getRecordTypeInfos();
        system.debug('********RT are *****'+RT);
        map<String,Id> mapRTIds = new map<String,Id>();
        for(integer i=0;i<RT.Size();i++){
            mapRTIds.put(RT[i].getName(),RT[i].getRecordTypeId());
        }
        set<Id> opportunityID = new set<Id>();
        for(Opportunity iteratorOpp : lstOpportunity) {
            opportunityID.add(iteratorOpp.Id);                
        }
        //BigMachines__Quote__c
        list<BigMachines__Quote__c> lstBMIQuote = BMIQuoteSOQLMethods.getPrimaryBMIQuoteByOpportunityID(opportunityID);
        map<Id, BigMachines__Quote__c> mapBMIQuote = new map<Id, BigMachines__Quote__c>();
        if(lstBMIQuote != null) {
            for(BigMachines__Quote__c iteratorQuote : lstBMIQuote) {
                mapBMIQuote.put(iteratorQuote.BigMachines__Opportunity__c, iteratorQuote);
            }
        }
        
        //Itererating the modified opportunity datas   Signed_Date__c
        for(Opportunity iteratorOpp : lstOpportunity) {
            // Get current opportunity data before mofified
			Opportunity oldOpportunity = oldMapOpportunity.get(iteratorOpp.Id);
	
			//Get the primary BMI Quote for opportunity
			BigMachines__Quote__c objBMIQuote;
			if(mapBMIQuote.size() > 0) {
				objBMIQuote = mapBMIQuote.get(oldOpportunity.Id);
			}
	
			String strState = checkOpportunityRecordTypeNeedChange(iteratorOpp, oldOpportunity, objBMIQuote);
			system.debug('State : '+ strState);
			if(strState.equals('New')){
				if(!string.valueOf(iteratorOpp.RecordTypeId).equals(String.valueOf(mapRTIds.get(CommonMessages.opportunityNewRT)))) {
					iteratorOpp.RecordTypeId = mapRTIds.get(CommonMessages.opportunityNewRT);
				}
			}
			else if(strState.equals('Assign')) {
				if(!string.valueOf(iteratorOpp.RecordTypeId).equals(String.valueOf(mapRTIds.get(CommonMessages.opportunityAssignedRT)))) {
					iteratorOpp.RecordTypeId = mapRTIds.get(CommonMessages.opportunityAssignedRT);
				}
			}
			else if(strState.equals('Lock')) {
				iteratorOpp.RecordTypeId = mapRTIds.get(CommonMessages.opportunityLockRTName);
			}
	
			//Checked opportunity already closed won and display the message in UI
			if(oldOpportunity.isLocked__c == true) {
				if(allowmodifyOpportunity == null) {
					if(!Test.isRunningTest()) {
						NewMapOpportunity.get(iteratorOpp.Id).addError(CommonMessages.opportunityModifyorDelete);
					}
				}
				else if((iteratorOpp.IsWon == false && iteratorOpp.IsClosed == false)
				  || iteratorOpp.StageName == 'Closed Lost') {
					iteratorOpp.RecordTypeId = mapRTIds.get(CommonMessages.opportunityAssignedRT);
					/*******Here sathish - unlocking the opportunity by sales managers**********/
					//iteratorOpp.isLocked__c = false;
				}
			}
        }
    }
	
    private static void createTalusAccount(set<Id> oppId) {
        list<Id> lstOppId = new list<Id>();
        list<Opportunity> lstOpp = [SELECT Id, Name, Account.TalusAccountId__c, Account.Is_Child_Account__c FROM Opportunity WHERE id IN:oppId];
        if(lstOpp.size() > 0) {
            for(Opportunity OpptyNew: lstOpp) {                    
                //Check if Oppty Accounts Talus Id is null and send account information for fulfillment
                if(String.isBlank(OpptyNew.Account.TalusAccountId__c)){
                    lstOppId.add(OpptyNew.AccountId);
                }
            }
            if(lstOppId.size() > 0 && lstOppId.size() <= 10 && CommonVariables.inFutureContext  == false) {
                if(!Test.isRunningTest()) {
                    CreateTalusAccount.createAccTalus(lstOppId, Userinfo.getUserId());
                }
            }
        }
    }	
	
	private static void populateDELateOrderApproval(Set<Id> setOpptyIdsForLateOrder) {
		set<Id> setDirectoryID = new set<Id>();
        list<Directory_Edition__c> lstBOTSDE;
        List<OpportunityLineItem> lstOppLIUpdt = new List<OpportunityLineItem>();
        map<Id, Id> mapBOTSDE = new map<Id, Id>();
    	List<OpportunityLineItem> lstOppLI = [SELECT id, Directory__c,CORE_Migration_ID__c,CreatedById from OpportunityLineItem WHERE opportunityid IN :setOpptyIdsForLateOrder and PricebookEntry.Product2.Media_Type__c = :CommonMessages.printMediaType];
    	for(OpportunityLineItem itrOppLI : lstOppLI) {
    		if(itrOppLI.Directory__c != Null && itrOppLI.CORE_Migration_ID__c == null && itrOppLI.CreatedById != System.Label.MigrationUserID) {
                setDirectoryID.add(itrOppLI.Directory__c);
            }
    	}
    	if(setDirectoryID != null && setDirectoryID.size() > 0) {
            lstBOTSDE = [SELECT Id, Directory__c,Book_Status__c from Directory_Edition__c WHERE Book_Status__c = :CommonMessages.bots and Directory__c IN:setDirectoryID];
            if(lstBOTSDE != null && lstBOTSDE.size() > 0) {
	            for(Directory_Edition__c iterator : lstBOTSDE) {
	                mapBOTSDE.put(iterator.Directory__c, iterator.id);
	            }
	            if(mapBOTSDE != null && mapBOTSDE.size() > 0) {
	            	for(OpportunityLineItem itrOppLI : lstOppLI) {
	            		if(mapBOTSDE.get(itrOppLI.Directory__c) != null) {                        	
                            itrOppLI.Directory_Edition__c = mapBOTSDE.get(itrOppLI.Directory__c);
                            lstOppLIUpdt.add(itrOppLI);
                        }
	            	}
	            }
	        }
        }
        if(lstOppLIUpdt != null && lstOppLIUpdt.size() > 0) {
        	update lstOppLIUpdt;
        }
    }
}