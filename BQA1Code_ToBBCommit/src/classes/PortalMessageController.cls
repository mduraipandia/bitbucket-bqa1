/*
    Author - Jina Chetia
    Date - 01/05/2010
    Company - Bluewolf
    
    This is the controller class for the pages
    Message_Centre and My_Messages
*/
public without sharing class PortalMessageController 
{
       
    //Constructor for the controller
    public PortalMessageController()
    {
        
    }
    
    //Get the user details
    public User getUserDetails()
    {
        system.debug('UserInfo.getUserId()========='+UserInfo.getUserId());
        return [select Id, ProfileId, Portal_Info__c, LanguageLocaleKey, UserType 
                    from User where Id= :UserInfo.getUserId()];
    }
    
    
    //Get Individual Messages under My Messages
    public List<Portal_Message__c> getIndividualMessages()
    {
      
       system.debug('getUserDetails().Id======='+getUserDetails().Id);
       if(!Test.isRunningTest()){
        return [Select Portal_User__c, Name, Message_Spanish__c, Message_English__c,  
                                            Id, Apply_to_the_following_portals__c, Active_Message__c 
                                                From Portal_Message__c p where Active_Message__c = 'Active' 
                                                    and Apply_to_the_following_portals__c = null and Portal_User__c = :getUserDetails().Id];
        }
        else
        {
           return [select Portal_User__c, Name, Message_Spanish__c, Message_English__c,  
                                            Id, Apply_to_the_following_portals__c, Active_Message__c 
                                                From Portal_Message__c ];
        }                                            
    }

    //Get the English Individual Messages
    public String getEnglishIndiMessage()
    {
        String message = '';
        if(getIndividualMessages().size()>0)
        {
            message += '<ul style="margin:0;padding:2px 0 1px 10px">';
            for(Portal_Message__c pm: getIndividualMessages())
            {
                message += '<li style="list-style:disc outside none;margin:0;padding:0;margin-left:1.5em;padding-left:0">' + pm.Message_English__c + '</li>';
            }
            message += '</ul>';
        }
        else
        {
            message = 'No messages to display';
        }
        return message;
    }    
    
     //Get the Spanish Individual Messages
    public String getSpanishIndiMessage()
    {
        String message = '';
        if(getIndividualMessages().size()>0)
        {
            message += '<ul style="margin:0;padding:2px 0 1px 10px">';
            for(Portal_Message__c pm: getIndividualMessages())
            {
                message += '<li style="list-style:disc outside none;margin:0;padding:0;margin-left:1.5em;padding-left:0">' + pm.Message_Spanish__c + '</li>';
            }
            message += '</ul>';
        }
        else
        {
            message = 'No hay mensajes para mostrar';
        }
        return message;
    }
    
    //Get Broadcast Messages under Message Centre
    public List<Portal_Message__c> getAllBroadcastMessages()
    {
        List<Portal_Message__c> broadcastMessages = new List<Portal_Message__c>();
        for(Portal_Message__c pm1: [Select Portal_User__c, Name, Message_Spanish__c, Message_English__c,  
                                                    Id, Apply_to_the_following_portals__c, Active_Message__c 
                                                        From Portal_Message__c p where Active_Message__c = 'Active' 
                                                            and Apply_to_the_following_portals__c != null])
        {
            if(pm1.Apply_to_the_following_portals__c != null)
            {
                for(String s:pm1.Apply_to_the_following_portals__c.split(';'))
                {
                    if(s != null && s.equals(getUserDetails().Portal_Info__c))
                    {
                        broadcastMessages.add(pm1);
                    }
                }
            }
        }
        system.debug('broadcastMessages======'+broadcastMessages);
        return broadcastMessages;
    }
    
     
   
   
}