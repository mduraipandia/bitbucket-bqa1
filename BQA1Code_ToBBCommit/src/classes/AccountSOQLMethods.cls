public class AccountSOQLMethods {

    public static map<ID, Account> getAccountContactByAccountID(set<ID> accountIDs) {
        return new Map<ID, Account>([Select Primary_Contact__c,ACC_Primary_Billing_Contact__c,Client_Tier__c, Tier_Override__c, Delinquency_Indicator__c, Name_and_City__c,Primary_Canvass__c, Primary_Canvass__r.Name, Name_and_Address__c, Name, Id, Account_Manager__c, Account_Manager__r.Name, Account_Number__c, (Select Id, LastName, FirstName, Name,PrimaryBilling__c, IsActive__c, Email, Title, Department, Primary_Contact__c From Contacts where isactive__c = true and PrimaryBilling__c = true) From Account where ID in :accountIDs]);
    }
    
    public static list<Account> getAccountByOpportunityAccountID(set<ID> accountID) {
        return [Select Id,account_number__c,Billing_Anniversary_Date__c,Account_Manager__r.FirstName,Account_Manager__r.LastName,Account_Manager__r.Email,Phone, RecordTypeId, 
                Name, OwnerId, (SELECT Id, Name, Billing_Anniversary_Date__c FROM Orders__r)
                From Account where Id IN :accountID];       
    } 
    
    public static map<Id, Account> getContactByAccountID(set<ID> accountID, set<Id> contactID) {
        return new map<Id, Account>([Select Id, (Select Id, AccountId, Name, Email, IsActive__c, Primary_Contact__c, PrimaryBilling__c, Proof_Contact__c From Contacts where Id NOT IN :contactID and (Proof_Contact__c = true OR PrimaryBilling__c = True OR Primary_Contact__c = true)) From Account where Id IN :accountID]);       
    }
    
    public static map<Id, Account> getContactOpportunityByAccountId(Set<Id> AccountId){ 
        return new map<Id, Account>([Select Primary_Canvass__c, Name, Id, BillingStreet, BillingState, BillingPostalCode, BillingCountry, BillingCity, Account_Number__c,Phone, Primary_Canvass__r.co_brand_name__c,  Primary_Canvass__r.Territory_Name__c, Primary_Canvass__r.Terms_and_Conditions__c, Primary_Canvass__r.Logo__c, Primary_Canvass__r.IsActive__c, Primary_Canvass__r.Invoice_Logo__c, Primary_Canvass__r.Billing_Partner__c, Primary_Canvass__r.Name,(Select Id, RecordTypeId, Name, Description, StageName, Amount, Probability, Account_Manager__c, Billing_Contact__c, Payment_Method__c From Opportunities),(Select Id, AccountId, LastName, FirstName, Name, IsActive__c, PrimaryBilling__c From Contacts) From Account where Id IN :AccountId]);
    }
    
    public static list<Account> getAccountByParentId(set<Id> accountId) {
        return [Select Primary_Canvass__c, ParentId, OwnerId, Name, Id, Account_Manager__c, Current_Spend__c, Pending_Spend__c, Total_Spend__c From Account Where ParentId IN :accountId];
    }
    
    public static list<Account> getAccountByAccountName(String accountName, set<Id> accountId) {
        return [Select Account_Number__c,Billing_Anniversary_Date__c,Primary_Canvass__c, ParentId, OwnerId, Name, Id, Account_Manager__c From Account Where Name Like :accountName and Id NOT IN :accountId and ParentId NOT IN :accountId];
    }
    
    public static list<Account> getCMRAccountByNumber(String CMRnum, String cmrRT, String clinetRT) {
        return [SELECT Id, Name, (Select Id, Name, AccountNumber From ChildAccounts where RecordTypeId =:clinetRT) FROM Account WHERE AccountNumber = :CMRnum AND RecordTypeId =:cmrRT limit 1];
    }
    
    public static list<Account> getClientAccountByNumber(String strClientNum, String clinetRT, String strCMRNo) {
        return [Select ID, Name, AccountNumber,Parent.Name, Parent.AccountNumber, Parent.RecordTypeId, Parent.Id, ParentId From Account where RecordTypeId =:clinetRT AND AccountNumber =:strClientNum and parent.AccountNumber =:strCMRNo];
    }
    
   public static Account getClientNatAcctByNumbers(String strClientNum, string CMRnum) {
        System.debug('accountsoql Testing strClientNum '+strClientNum +' testing CMRnum'+ CMRnum);
        String strAccRTId = CommonMethods.getRedordTypeIdByName('National Account', 'Account');
        Account acc = new account();
        //String strAccRTId = CommonMethods.getRedordTypeIdByName('Customer Account', 'Account');AND RecordTypeId = :strAccRTId
        //System.debug('Testingg strAccRTId '+strAccRTId);
        if ( [SELECT count() FROM Account WHERE Client_Number__c = :strClientNum AND CMR_Number__c = :CMRnum AND RecordTypeId = :strAccRTId limit 1] == 1 ){       
            acc = [Select Id, Name from Account WHERE Client_Number__c = :strClientNum AND CMR_Number__c = :CMRnum AND RecordTypeId = :strAccRTId limit 1];
        }
        return acc;
    } 
    
    public static list<Account> getAccountByIDandParentID(set<Id> setAccountID) {
        return [Select Id, Name, Delinquency_Indicator__c,Account_Number__c, Parentid, Parent.Name, Parent.Account_Number__c from Account where id IN :setAccountID OR parentid IN :setAccountID];
    }
    
    public static list<Account> getSetDelinquencyIndicatorAccountByParentID(set<Id> setAccountIds) {
        return [Select Id, Delinquency_Indicator__c from Account where (id IN:setAccountIds OR ParentId IN:setAccountIds) and Delinquency_Indicator__c = false];
    }
    
    public static map<Id, Account> getNationalCommissionById(set<Id> setAccountID) {
        return new map<Id, Account>([Select Id, National_Commission__c, National_Commission__r.Rate__c from Account where Id IN :setAccountID]);
    }
    
    public static list<Account> getAccountWithStatementById(set<Id> setAccountID) {
        return [Select id,(SELECT Account__r.Name,Account__c,Amount_Due__c,Balance_Last_Statement__c,CreatedDate FROM Statement_Header__r Order By CreatedDate DESC) from Account where Id IN:setAccountID];
    }
    
    public static list<Account> getCMRAccountByNumber(set<String> setCMRNumber) {
        return [Select Id, Name, AccountNumber from Account WHERE AccountNumber IN:setCMRNumber AND RecordTypeId =:CommonMessages.cmrAccountRecordTypeID];  
    }
    
    public static list<Account> getClientAccountByNumber(set<String> setClinetNumber) {
        return [Select Id, Name, ParentId, AccountNumber,CMR_Number_Client_Number__c from Account WHERE CMR_Number_Client_Number__c IN : setClinetNumber
                AND RecordTypeId =:CommonMessages.clientAccountRecordTypeID];   
    }
    
    public static list<Account> getAccountByCode(set<String> setCMRCode, set<String> setClientCode, set<String> setPubcoCode) {
        String query = 'Select Id, Name, CMR_Number__c, Client_Number__c, Publication_Code__c, Delinquency_Indicator__c from Account where ';
        String strWhere = '';
        if(setCMRCode.size() > 0) {
            strWhere += (String.isBlank(strWhere) ? '' : ' OR ') + 'CMR_Number__c IN:setCMRCode ';
        }
        
        if(setCMRCode.size() > 0) {
            strWhere += (String.isBlank(strWhere) ? '' : ' OR ') + 'Client_Number__c IN:setClientCode ';
        }
        
        if(setCMRCode.size() > 0) {
            strWhere += (String.isBlank(strWhere) ? '' : ' OR ') + 'Publication_Code__c IN:setPubcoCode ';
        }
        
        if(!String.isBlank(strWhere)) {
            query += strWhere;
            return database.query(query);
        }
        return new list<Account>();
    } 
    
    public static list<Account> getAccountByPubCode(set<String> setPubcoCode) {
        return [SELECT Id, Publication_Code__c FROM Account WHERE Publication_Code__c IN:setPubcoCode and RecordTypeId=:System.label.ActPubcompanyRTID];
    }   
    
    public static Account getAccountByAccountId(Id acctId){
        return [SELECT Id,Account_Number__c, Name, Billing_Anniversary_Date__c FROM Account WHERE Id = : acctId];
    }
    
    public static List<Account> getAccountsByAccountIdWithSensitiveHeadFalse(Set<Id> setAcctIds){
        return [SELECT Id, Name, Sensitive_Heading__c, TalusAccountId__c, Talus_Updated_JSON_Body__c, TalusPhoneId__c, Phone, Berry_ID__c, 
                Account_Manager__r.Name, Account_Manager__r.FirstName, Account_Manager__r.LastName, Account_Manager__r.Phone, Account_Manager__c, CreatedBy.Name, 
                Account_Manager__r.Email, Account_Manager__r.ManagerId, CreatedBy.FirstName, CreatedBy.LastName, CreatedBy.Username, CreatedBy.Email, CreatedBy.Phone
                FROM Account WHERE Id IN : setAcctIds AND Sensitive_Heading__c = false and TalusAccountId__c != NULL];
    }
    
    public static List<Account> getClientForMediaTrax(String strAccRTId){
        return [SELECT Name, Account_Number__c, Client_Number__c, BillingCity, BillingCountry, Phone, BillingState, BillingStreet, 
        BillingPostalCode, P4P_Spending_Cap__c, P4P_Spending_Cap_Type__c, Fax, Media_Trax_Client_ID__c FROM Account 
        where Media_Trax_Client_ID__c = null and Account_Number__c != null and Name != null and RecordTypeId =:strAccRTId and
        BillingCity != null  and BillingCountry != null and Phone != null and BillingState != null and BillingStreet != null and 
        BillingPostalCode != null and P4P_Spending_Cap__c != null and P4P_Spending_Cap_Type__c != null limit 5];
    }
    
    public static List<Account> getClientForMediaTraxUpdate(String strAccRTId){
        return [SELECT Name, Account_Number__c, Client_Number__c, BillingCity, BillingCountry, Phone, BillingState, BillingStreet, BillingPostalCode,
        P4P_Spending_Cap__c, P4P_Spending_Cap_Type__c, Fax, Media_Trax_Client_ID__c, Is_Changed__c FROM Account 
        where Is_Changed__c = true and Media_Trax_Client_ID__c != null and Account_Number__c != null and Name != null and 
        BillingCity != null and BillingCountry != null and Phone != null and BillingState != null and 
        BillingStreet != null and BillingPostalCode != null and P4P_Spending_Cap__c != null and P4P_Spending_Cap_Type__c != null and 
        RecordTypeId =:strAccRTId limit 10];
    }
    
    public static list<Account> getAccountWithFulfillmentProfileByIds(set<Id> setAccIds) { 
        return [SELECT id, Name, Phone, Clone_Phone__c, BillingStreet, BillingCity, BillingState, BillingPostalCode, Website, RecordType.Name,(SELECT Account__c, Name, Default_Profile__c, Automatically_Sync__c, business_name__c
                 from Fulfillment_Profiles__r where Automatically_Sync__c=true) from Account where Id IN:setAccIds];
    }
    
    public static list<Account> getAccountsByAcctId(set<Id> setAccIds){
        return [SELECT Id, Account_Number__c, Name, Open_Claim__c, CMR_Number_Client_Number__c, Billing_Anniversary_Date__c, Last_Billing_Transfer_Date__c 
            FROM Account WHERE Id IN : setAccIds];
    }
    
    public static list<Account> getAccountsByAcctIdAndCSClaimRT(set<Id> setAccIds, Id CSClaimRecId){
        return [SELECT Id,Account_Number__c, Name, Open_Claim__c, (SELECT Id FROM Cases WHERE RecordTypeId = : CSClaimRecId AND IsClosed = false) FROM Account 
               WHERE Id IN : setAccIds];
    }
    
    public static list<account> getAccountIDByName(set<string> accName)
   {
        return [select id,name from account where name IN:accName];
   }
   
   public static list<Account> getAccountsByExternalId(set<String> setExtIds){
        return [SELECT Id,X3l_External_ID__c,Parent_3L_External_ID__c FROM Account WHERE X3l_External_ID__c IN : setExtIds OR Parent_3L_External_ID__c IN: setExtIds];
   }

   public static list<Account> getAccountsByAcctId(set<Id> setAccIds, Set<Id> setContactID){
        return [SELECT Id, Account_Number__c, Name, Open_Claim__c, CMR_Number_Client_Number__c, Billing_Anniversary_Date__c, Last_Billing_Transfer_Date__c,
            (SELECT Id, Name, Billing_Anniversary_Date__c FROM Orders__r), (Select Id, Name, MailingStreet, MailingCity, MailingState, MailingPostalCode, 
            MailingCountry From Contacts where Id IN:setContactID), (select id, Name,Next_Billing_Date__c from Order_Line_Items1__r where Account__c IN:setAccIds and Next_Billing_Date__c != null and Billing_Frequency__c = 'Monthly' limit 1)
            FROM Account WHERE Id IN : setAccIds];
    } 
}