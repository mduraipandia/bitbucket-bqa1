public with sharing class MediaTraxHeadingManualCallOutController {
    
    public MediaTraxHeadingManualCallOutController(Apexpages.Standardcontroller controller) {
        
    }
    
    public void AddHeading() {
        MediaTraxServiceForAddHeadingBatch objCallout = new MediaTraxServiceForAddHeadingBatch();
        database.executebatch(objCallout, 500);
    }
    
    public void AddDirectory() {
        MediaTraxServiceForAddDirectoryBatch objCallout = new MediaTraxServiceForAddDirectoryBatch();
        database.executebatch(objCallout);
    }
    
    public void AddClient() {
        MediaTraxServiceForGetAddClientBatch objCallout = new MediaTraxServiceForGetAddClientBatch();
        database.executebatch(objCallout, 5);
    }
    
    public void GetAddHeading() {
        MediaTraxServiceForGetAddHeadingBatch objCallout = new MediaTraxServiceForGetAddHeadingBatch();
        database.executebatch(objCallout, 500);
    }
    
    public void GetAddDirectory() {
        MediaTraxServiceForGetAddDirectoryBatch objCallout = new MediaTraxServiceForGetAddDirectoryBatch();
        database.executebatch(objCallout,1);
    }
    
    public void GetAddClient() {
        MediaTraxServiceForGetAddClientBatch objCallout = new MediaTraxServiceForGetAddClientBatch();
        database.executebatch(objCallout, 5);
    }
    
    public void UpdateHeading() {
        MediaTraxServiceForUpdateHeadingBatch objCallout = new MediaTraxServiceForUpdateHeadingBatch();
        database.executebatch(objCallout, 1);
    }
    
    public void UpdateDirectory() {
        MediaTraxServiceForUpdateDirectoryBatch objCallout = new MediaTraxServiceForUpdateDirectoryBatch();
        database.executebatch(objCallout);
    }
    
    public void UpdateClient() {
        MediaTraxServiceForUpdateClientBatch objCallout = new MediaTraxServiceForUpdateClientBatch();
        database.executebatch(objCallout, 5);
    }    
    
    public void AuthendationSutureCallout() {
        try{
            MediaTraxHeadingCalloutController_V1 objCallout = new MediaTraxHeadingCalloutController_V1();
            objCallout.AuthendationSutureCallout();
        }catch(Exception objExp){
            System.debug('Exception occured on getting Authentication token - Message '+objExp.getMessage());
            futureCreateErrorLog.createErrorRecord(objExp.getMessage(), objExp.getStackTraceString(),'MediaTrax');
        }
    }
}