/*
    * FinancialForce.com, inc. claims copyright in this software, its screen display designs and
    * supporting documentation. FinancialForce and FinancialForce.com are trademarks of FinancialForce.com, inc.
    * Any unauthorized use, copying or sale of the above may constitute an infringement of copyright and may
    * result in criminal or other legal proceedings.
    *
    * Copyright FinancialForce.com, inc. All rights reserved.
    * Created by Uves Ravat
*/

public class BatchSpeedWithSObject implements Database.Batchable<SObject>, Database.Stateful
{
    private List<String> errorMessages;
    private List<String> successMessages;

    public BatchSpeedWithSObject() 
    {
        initialise();
    }

    private void initialise()
    {
        errorMessages = new List<String>();
        successMessages = new List<String>();      
    }

    public Database.QueryLocator start(Database.BatchableContext BC) 
    {
    	successMessages.add('Start time of Start Method' + System.now());
        return Database.getQueryLocator('Select Id From Account LIMIT 10000');
    }

    public void execute( Database.BatchableContext BC, List<SObject> unTypedScope)
    {
		successMessages.add('Start time of Execute Method' + System.now());

		if(getHeapSizePercentage() > 80)
		{
			sendEmail('uravat@financialforce.com', successMessages, errorMessages);
	        errorMessages = new List<String>();
	        successMessages = new List<String>();
		}
    }

    public void finish( Database.BatchableContext BC )
    {
    	successMessages.add('Start time of Finish Method' + System.now());
        AsyncApexJob batchJob = [
            Select
                Id, 
                Status, 
                NumberOfErrors, 
                ExtendedStatus, 
                JobItemsProcessed, 
                TotalJobItems, 
                CreatedBy.Email 
            From 
                AsyncApexJob 
            Where 
                Id = :bc.getJobId()
        ];
        
		sendEmail( batchJob.CreatedBy.Email, successMessages, errorMessages );
    }

    private void sendEmail( String address, List<String>successMessages, List<String>errorMessages  )
    {   
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses( new String[] { address } );

        String body = '';
        body += 'Batch Speed Test with List of Ids';    
        if( errorMessages.size() > 0 )
        {
            body += 'Errors occurred.\n';
            for( String error : errorMessages )
            {
                body += error +'\n';
            }
            body += '\n';
        }
        else
        {
            body += 'No errors occurred.\n';
            body += '\n';
        }
        for( String success : successMessages )
        {
            body += success +'\n';
        }
        
        mail.setPlainTextBody( body );
        mail.setSubject( 'Batch Speed Test(BatchSpeedWithSObject)' );
        
        Messaging.sendEmail( new Messaging.SingleEmailMessage[] { mail } );
    }

    public static Decimal getHeapSizePercentage()
    {
        decimal currentHeapsize = Limits.getHeapSize();
        decimal heapsizeLimit = Limits.getLimitHeapSize();

        return (currentHeapsize/heapsizeLimit) * 100;
    }
}