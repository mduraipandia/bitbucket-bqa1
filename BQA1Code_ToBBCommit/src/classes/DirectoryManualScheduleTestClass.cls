@IsTest(SeeAllData=true)
private class DirectoryManualScheduleTestClass {
    public static testMethod void DirectoryManualScheduleInsert(){
    Canvass__c c = TestMethodsUtility.createCanvass();
    Account newaccount = new Account(Primary_Canvass__c = c.Id, BillingState = 'AB', BillingCountry = 'US', BillingCity = 'California', Name = 'Test Account', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');
      insert newaccount ;
    Telco__c newTelco = TestMethodsUtility.createTelco(newAccount.Id);
    // Insert a Directory
    
        
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Name = 'Test Dir1';
        objDir.EBD__c = System.Today().addDays(1);
        objDir.DCR_Close__c = System.Today().addDays(5);
        objDir.BOC__c = System.Today().addDays(2);
        objDir.Final_Service_Order_Due_to_Berry__c = System.Today().addDays(1);
        objDir.Sales_Lockout__c = System.Today();
        objDir.Book_Extract_YP__c = System.Today().addDays(3);
        objDir.Ship_Date__c = System.Today().addDays(-2);
        objDir.Pub_Date__c = System.Today().addDays(4);
        objDir.Directory_code__c = '123456';
        objDir.Telco_Provider__c=newTelco.id;
        insert objDir;
        
    DirectoryManualSchedule dirMS = new DirectoryManualSchedule();
        try{
            dirMS.doRun();
        }
        catch(exception e){}
    }
}