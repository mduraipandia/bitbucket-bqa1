<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>Heading Request - New</fullName>
        <actions>
            <name>New_Heading_Request</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>For notifying a user that a new heading request has been created for them to review</description>
        <formula>1=1</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>New_Heading_Request</fullName>
        <assignedTo>elizabeth.cool@theberrycompany.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>A new heading request has been created.</description>
        <dueDateOffset>5</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Heading_Request__c.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>New Heading Request</subject>
    </tasks>
</Workflow>
