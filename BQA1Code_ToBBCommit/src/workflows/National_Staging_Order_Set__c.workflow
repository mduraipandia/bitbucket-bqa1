<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Late_Order_Checkbox</fullName>
        <field>Late_Order__c</field>
        <literalValue>1</literalValue>
        <name>Update Late Order Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Check Late Orders</fullName>
        <actions>
            <name>Update_Late_Order_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When national close date for the directory edition is past today then national  order will be a late order</description>
        <formula>DATEVALUE(CreatedDate) &gt; DATE(YEAR(Directory_Edition__r.National_Close_Date__c),MONTH(Directory_Edition__r.National_Close_Date__c),DAY(Directory_Edition__r.National_Close_Date__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
