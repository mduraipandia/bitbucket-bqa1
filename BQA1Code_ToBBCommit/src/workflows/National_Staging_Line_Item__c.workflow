<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CAPTURE_Original_Directory_Heading_ID</fullName>
        <field>Original_Directory_Heading_SFDC_ID__c</field>
        <formula>Directory_Heading__c</formula>
        <name>CAPTURE Original Directory Heading ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAPTURE_PRIOR_Directory_Heading_ID</fullName>
        <field>Prior_Directory_Heading_SFDC_ID__c</field>
        <formula>PRIORVALUE( Directory_Heading__c )</formula>
        <name>CAPTURE PRIOR Directory Heading ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CAPTURE_PRIOR_UDAC_Group</fullName>
        <field>Prior_UDAC_Group__c</field>
        <formula>PRIORVALUE(Current_UDAC_Group__c)</formula>
        <name>CAPTURE PRIOR UDAC Group</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Capture_ORIGINAL_UDAC_Group</fullName>
        <field>Original_UDAC_Group__c</field>
        <formula>Current_UDAC_Group__c</formula>
        <name>Capture ORIGINAL UDAC Group</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_NAT_STAGE_LI_Caption_Disp_txt</fullName>
        <description>Used to populate the Caption Display Text on National Staging Line Item with the value in the Advertised Data field</description>
        <field>Caption_Display_Text__c</field>
        <formula>Formatted_Advertising_Data__c</formula>
        <name>Populate NAT STAGE LI Caption Disp. txt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_NAT_STAGE_LI_Caption_Indent_Lvl</fullName>
        <description>Used to populate the Caption Indent Level with the SPINS value when DAT = C, I, X, or S.</description>
        <field>Caption_Indent_Level__c</field>
        <formula>VALUE(SPINS__c)</formula>
        <name>Populate NAT STAGE LI Caption Indent Lvl</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_NAT_STAGE_LI_Caption_Phone_Num</fullName>
        <description>Used to populate Caption Phone Number on National Staging Line Item with the value in the Advertised Data field when DAT = T.</description>
        <field>Caption_Phone_Number__c</field>
        <formula>Advertising_Data__c</formula>
        <name>Populate NAT STAGE LI Caption Phone Num</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Is_UDAC_Changed_Field</fullName>
        <field>Is_UDAC_Changed__c</field>
        <literalValue>1</literalValue>
        <name>Update Is UDAC Changed Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Capture Original UDAC Group %26 Directory Heading ID</fullName>
        <actions>
            <name>CAPTURE_Original_Directory_Heading_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Capture_ORIGINAL_UDAC_Group</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to capture the NAT Stage LI&apos;s ORIGINAL UDAC Group and Directory Heading ID</description>
        <formula>OR( Original_Directory_Heading_SFDC_ID__c = NULL,     Original_UDAC_Group__c = NULL )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Capture PRIOR UDAC Group %26 Directory Heading ID</fullName>
        <actions>
            <name>CAPTURE_PRIOR_Directory_Heading_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CAPTURE_PRIOR_UDAC_Group</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to capture the PRIORVALUES of UDAC Group and Directory Heading when the respective fields are updated due to a National Order Change</description>
        <formula>OR( ISCHANGED( Product2__c ),     ISCHANGED( Directory_Heading__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Caption Display Text</fullName>
        <actions>
            <name>Populate_NAT_STAGE_LI_Caption_Disp_txt</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow handles the rules to populate Caption Display Text based on ELITE Technical/Business Rules. Populated if DAT = N, I, X, C, or S OR if UDAC = TE (Trade Extra Line) OR if UDAC = EL (Extra Line).</description>
        <formula>AND( SPINS__c != &quot;A&quot;,  OR(DAT__c = &quot;N&quot;,DAT__c = &quot;I&quot;,DAT__c = &quot;X&quot;,DAT__c = &quot;C&quot;,DAT__c = &quot;S&quot;,
Product2__r.ProductCode = &quot;TE&quot;,
Product2__r.ProductCode = &quot;EL&quot;,
Product2__r.ProductCode = &quot;AL&quot;,
Product2__r.ProductCode = &quot;EMUC&quot;,
Product2__r.ProductCode = &quot;ILUC&quot;,
Product2__r.ProductCode = &quot;WILE&quot;,
Product2__r.ProductCode = &quot;WILW&quot;,
Product2__r.ProductCode = &quot;TEMUC&quot;,
Product2__r.ProductCode = &quot;TILUC&quot;,
Product2__r.ProductCode = &quot;ITEL&quot;,
Product2__r.ProductCode = &quot;WBEIR&quot;,
Product2__r.ProductCode = &quot;WBELE&quot;,
Product2__r.ProductCode = &quot;WBELI&quot;,
Product2__r.ProductCode = &quot;IL&quot;,
Product2__r.ProductCode =&quot;TNE&quot; ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Caption Phone Number</fullName>
        <actions>
            <name>Populate_NAT_STAGE_LI_Caption_Phone_Num</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow handles the rules to populate Caption Phone Number based on ELITE Technical/Business Rules. Populated if DAT = T</description>
        <formula>OR( DAT__c = &quot;T&quot; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Validate UDAC Change</fullName>
        <actions>
            <name>Update_Is_UDAC_Changed_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This WFRule is created so that we can track National Staging Line Item&apos;s UDAC change  and update Is_UDAC_Changed check box value from false to true. This update will help for National Order Process.</description>
        <formula>ISCHANGED( UDAC__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
