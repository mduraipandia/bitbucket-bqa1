<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Art_Work_Complete_Notification</fullName>
        <description>Art Work Complete Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Art_Work_Complete</template>
    </alerts>
    <fieldUpdates>
        <fullName>General_Addon_Status_FU</fullName>
        <field>Status__c</field>
        <formula>&apos;Complete&apos;</formula>
        <name>General Addon Status FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Caption_Display_Text</fullName>
        <field>Caption_Display_Text__c</field>
        <formula>OrderLineItemID__r.National_Staging_Line_Item__r.Caption_Display_Text__c</formula>
        <name>Populate Caption Display Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Caption_Indent_Level</fullName>
        <field>Caption_Indent_Level__c</field>
        <formula>OrderLineItemID__r.National_Staging_Line_Item__r.Caption_Indent_Level__c</formula>
        <name>Populate Caption Indent Level</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_DFF_Date_Received</fullName>
        <description>When the DFF As Status is set to In Progress, set the Date Graphics Received</description>
        <field>Date_Graphics_Received__c</field>
        <formula>Now()</formula>
        <name>Populate DFF Date Received</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Graphics_Complete_Field</fullName>
        <field>Graphics_Complete_Date__c</field>
        <formula>NOW()</formula>
        <name>Populate Graphics Complete Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Indent_Order</fullName>
        <field>Caption_Indent_Order__c</field>
        <formula>OrderLineItemID__r.National_Staging_Line_Item__r.Caption_Indent_Order__c</formula>
        <name>Populate Caption Indent Order</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Print_DFF_Not_Ready_to_Submit_Status</fullName>
        <description>Set the Status to No when not all of the required fields are populated.  This controls the status in the inline VF page, and when the use can successfully clikc on Send to Graphics to transmit the graphics.</description>
        <field>Status__c</field>
        <name>Print DFF Not Ready to Submit Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Print_DFF_Status_FU</fullName>
        <field>Status__c</field>
        <formula>&apos;Complete&apos;</formula>
        <name>Print DFF Status FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RCF_Area_Code_into_Caption_Area_Code</fullName>
        <field>Caption_Phone_Area_Code__c</field>
        <formula>LEFT( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE(RCF__c, &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;), 3)</formula>
        <name>RCF Area Code into Caption Area Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RCF_Exchange_into_Caption_Exchange</fullName>
        <field>Caption_Phone_Exchange__c</field>
        <formula>MID(
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE(RCF__c, &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;),
4, 3)</formula>
        <name>RCF Exchange into Caption Exchange</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RCF_Line_into_Caption_Line</fullName>
        <field>Caption_Phone_Line__c</field>
        <formula>RIGHT( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE(RCF__c, &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;), 4)</formula>
        <name>RCF Line into Caption Line</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RCF_into_Caption_Phone_Number</fullName>
        <description>Copies the full RCF value into the Caption Phone Number field</description>
        <field>Caption_Phone_Number__c</field>
        <formula>RCF__c</formula>
        <name>RCF into Caption Phone Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Bronze_DFF_Ad_Status_New</fullName>
        <description>Set the Bronze DFF Ad Status to New if the PAO Production Notes is not null</description>
        <field>Miles_Status__c</field>
        <literalValue>New</literalValue>
        <name>Set Bronze DFF Ad Status_New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_DFF_NA_Ad_Status</fullName>
        <description>Set Ad Status to N/A for Bronze DFF and iYP Mobile record types if the Production Notes for PAO is null for Bronze. In this case no YPC Graphics record is created, so the Ad Status will not change.</description>
        <field>Miles_Status__c</field>
        <literalValue>N/A</literalValue>
        <name>Set DFF NA Ad Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Fulfillment_Submit_Status_Complete</fullName>
        <description>Set the Fulfillment Submit Status to Complete when the Print or Multi-Ad DFF is initially sent to graphics.</description>
        <field>Fulfillment_Submit_Status__c</field>
        <formula>&apos;Complete&apos;</formula>
        <name>Set Fulfillment Submit Status Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Singup_Url_FU</fullName>
        <field>signup_url__c</field>
        <formula>&apos;http://www.theberrycompany.com&apos;</formula>
        <name>Singup_Url_FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Advertised_Phone_Area_Code</fullName>
        <description>Used to capture the (Listing) Phone Number&apos;s Area Code</description>
        <field>Advertised_Phone_Area_Code__c</field>
        <formula>LEFT( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE( business_phone_number_office__c , &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;), 3)</formula>
        <name>Update Advertised Phone Area Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Advertised_Phone_Exchange</fullName>
        <description>Used to capture the (Listing) Phone Number&apos;s Exchange</description>
        <field>Advertised_Phone_Exchange__c</field>
        <formula>MID( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE( business_phone_number_office__c , &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;), 4, 3)</formula>
        <name>Update Advertised Phone Exchange</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Advertised_Phone_Line</fullName>
        <description>Used to update the (Listing) Phone Number&apos;s Line</description>
        <field>Advertised_Phone_Line__c</field>
        <formula>RIGHT( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE( business_phone_number_office__c , &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;), 4)</formula>
        <name>Update Advertised Phone Line</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Caption_Phone_Area_Code</fullName>
        <description>Used to capture the Caption Member Phone&apos;s Area Code</description>
        <field>Caption_Phone_Area_Code__c</field>
        <formula>LEFT( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE(Caption_Phone_Number__c, &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;), 3)</formula>
        <name>Update Caption Phone Area Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Caption_Phone_Exchange</fullName>
        <description>Used to capture the Caption Member Phone&apos;s Exchange</description>
        <field>Caption_Phone_Exchange__c</field>
        <formula>MID(
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE(Caption_Phone_Number__c, &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;),
4, 3)</formula>
        <name>Update Caption Phone Exchange</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Caption_Phone_Line</fullName>
        <description>Used to capture the Caption Member Phone&apos;s Line</description>
        <field>Caption_Phone_Line__c</field>
        <formula>RIGHT( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE( 
SUBSTITUTE(Caption_Phone_Number__c, &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;), 4)</formula>
        <name>Update Caption Phone Line</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>iYP_Metal_Status_FU</fullName>
        <field>Status__c</field>
        <formula>&apos;Complete&apos;</formula>
        <name>iYP Metal Status FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>iYP_Metal_VPAO_1_FU</fullName>
        <field>udac_points__c</field>
        <formula>SUBSTITUTE( udac_points__c , &apos;VPAO;&apos;, null)</formula>
        <name>iYP Metal VPAO_1 FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>iYP_Metal_VPAO_FU</fullName>
        <field>udac_points__c</field>
        <formula>CASE( Production_Notes_for_PrintAdOrderGraphic__c , null, &apos;VPAO;&apos; &amp; udac_points__c, null)</formula>
        <name>iYP Metal VPAO FU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Capture Caption %26 Advertised Phone Area Code%2C Exchange%2C Line</fullName>
        <actions>
            <name>Update_Advertised_Phone_Area_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Advertised_Phone_Exchange</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Advertised_Phone_Line</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Caption_Phone_Area_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Caption_Phone_Exchange</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Caption_Phone_Line</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Used to capture the Caption Member and/or Advertised Phone&apos;s Area Code, Exchange, and Line into their respective fields.</description>
        <formula>OR( ISCHANGED( Caption_Phone_Number__c ),      ISCHANGED ( business_phone_number_office__c ),           Caption_Phone_Area_Code__c = NULL,      Caption_Phone_Exchange__c = NULL,      Caption_Phone_Line__c = NULL,     Advertised_Phone_Area_Code__c = NULL,        Advertised_Phone_Exchange__c = NULL,       Advertised_Phone_Line__c = NULL )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Completed DFF Ad</fullName>
        <actions>
            <name>Art_Work_Complete</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.Miles_Status__c</field>
            <operation>equals</operation>
            <value>Complete,Paper Proof,Web Proof</value>
        </criteriaItems>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Print Graphic,Multi Ad,Print Listing Content</value>
        </criteriaItems>
        <description>When the Graphic artist completes the DFF Ad, several work flow actions occur: Create a task for the Sales Rep to alert them the ad is complete; update the links to see the High/Low res image.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>General Addon Status</fullName>
        <actions>
            <name>General_Addon_Status_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.DFF_RecordType_Name__c</field>
            <operation>equals</operation>
            <value>General Add-on</value>
        </criteriaItems>
        <description>WF to set Ready To Submit Status as Green YES on General Add-on Dffs</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>In Progress DFF Request</fullName>
        <actions>
            <name>Populate_DFF_Date_Received</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.Miles_Status__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Print Graphic,Multi Ad</value>
        </criteriaItems>
        <description>When a Miles33 artist begins work on the graphics, the status is set to In Progress, when the DFF status changes, set the Date Graphics Received</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Caption Information on DFF</fullName>
        <actions>
            <name>Populate_Caption_Display_Text</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_Caption_Indent_Level</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_Indent_Order</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Populate Caption Information on DFF</description>
        <formula>CONTAINS(OrderLineItemID__r.RecordType.Name , &quot;National&quot;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Populate DFF Submitted</fullName>
        <actions>
            <name>Set_Fulfillment_Submit_Status_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.Miles_Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Print Graphic,Multi Ad</value>
        </criteriaItems>
        <description>Set Submitted to True when the Date Submitted date (Send to Graphics) is changed for Print and Multi-Ad DFF record types.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Print DFF Not Ready to Submit Status</fullName>
        <actions>
            <name>Print_DFF_Not_Ready_to_Submit_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3 OR 4 OR 5 OR 6 OR 7 OR 8 )</booleanFilter>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Print Graphic,Multi Ad</value>
        </criteriaItems>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.Proof__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.Paper_or_Email__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.Product_Name__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.Enterprise_Customer_ID__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.business_phone_number_office__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.DFF_Sales_Rep_NickName__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.Sales_Rep_Email__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Set the Status that controls the Ready to Submit status to No in the inline VF page, when the conditions are met.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Print Graphic Ready to Submit Status</fullName>
        <actions>
            <name>Print_DFF_Status_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7 AND 8</booleanFilter>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Print Graphic,Multi Ad</value>
        </criteriaItems>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.Proof__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.Paper_or_Email__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.Product_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.Enterprise_Customer_ID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.business_phone_number_office__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.DFF_Sales_Rep_NickName__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.Sales_Rep_Email__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Set the Status that controls the Ready to Submit status to Yes in the inline VF page for Print Graphic and Multi-Ad DFFs.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Print Listing Content No Ready to Submit Status</fullName>
        <actions>
            <name>Print_DFF_Not_Ready_to_Submit_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Set the Status that controls the Ready to Submit status to No in the inline VF page for Print Listing No Content, when Caption Display Text is changed to null</description>
        <formula>AND(ISCHANGED( Caption_Display_Text__c ),  ISBLANK( Caption_Display_Text__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Print Listing Content Ready to Submit Status</fullName>
        <actions>
            <name>Print_DFF_Status_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Fulfillment_Submit_Status_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Print Listing Content</value>
        </criteriaItems>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.Caption_Display_Text__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.Caption_Phone_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Set the Status that controls the Ready to Submit status to Yes in the inline VF page, for Print Listing Content when the Caption Display Text has been populated</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Print Listing Content Request Content</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Print Listing Content</value>
        </criteriaItems>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.Caption_Display_Text__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>User.CommunityNickname</field>
            <operation>notEqual</operation>
            <value>devbatchuser</value>
        </criteriaItems>
        <description>For Print Listing Content, if the Caption Display Text has not been populated within 24 hours of the Print Listing Content creation, send task for Sales Rep to provide the content.</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Print_Listing_Content_Enter_Content</name>
                <type>Task</type>
            </actions>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Print Listing No Content Ready to Submit Status</fullName>
        <actions>
            <name>Print_DFF_Status_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Fulfillment_Submit_Status_Complete</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Print Listing No Content</value>
        </criteriaItems>
        <description>Set the Status that controls the Ready to Submit status to Yes in the inline VF page for Print Listing No Content DFF.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>RCF Phone Number Handler</fullName>
        <actions>
            <name>RCF_Area_Code_into_Caption_Area_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>RCF_Exchange_into_Caption_Exchange</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>RCF_Line_into_Caption_Line</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>RCF_into_Caption_Phone_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Used to copy a user inputted RCF Phone Number, from the RCF phone number field, into the Caption Phone Number fields</description>
        <formula>AND( ISCHANGED(RCF__c),      LEN( SUBSTITUTE(            SUBSTITUTE(            SUBSTITUTE(            SUBSTITUTE( RCF__c , &quot;(&quot;, &quot;&quot;), &quot;)&quot;, &quot;&quot;), &quot; &quot;, &quot;&quot;), &quot;-&quot;, &quot;&quot;)           ) = 10 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Bronze DFF Ad Status</fullName>
        <actions>
            <name>Set_DFF_NA_Ad_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>iYP Bronze,iYP Bronze 5 Heading</value>
        </criteriaItems>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.Production_Notes_for_PrintAdOrderGraphic__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Set the Ad Status to N/A for DFF Bronze record types.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Bronze DFF Ad Status_New</fullName>
        <actions>
            <name>Set_Bronze_DFF_Ad_Status_New</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>iYP Bronze,iYP Bronze 5 Heading</value>
        </criteriaItems>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.Production_Notes_for_PrintAdOrderGraphic__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.Miles_Status__c</field>
            <operation>equals</operation>
            <value>N/A</value>
        </criteriaItems>
        <description>Set the Ad Status to New for DFF Bronze record types if the PAO production Notes have been entered.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set YPC MPL Ad Status NA</fullName>
        <actions>
            <name>Set_DFF_NA_Ad_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>iYP Mobile</value>
        </criteriaItems>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.UDAC__c</field>
            <operation>equals</operation>
            <value>MPL</value>
        </criteriaItems>
        <description>Set the Ad Status to N/A when UDAC=MPL for iYP Mobile record type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>iYP Metal VPAO</fullName>
        <actions>
            <name>iYP_Metal_VPAO_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.DFF_RecordType_Name__c</field>
            <operation>contains</operation>
            <value>iYP</value>
        </criteriaItems>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.Production_Notes_for_PrintAdOrderGraphic__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.udac_points__c</field>
            <operation>notContain</operation>
            <value>VPAO</value>
        </criteriaItems>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>iYP Mobile</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>iYP Metal VPAO_1</fullName>
        <actions>
            <name>iYP_Metal_VPAO_1_FU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.Production_Notes_for_PrintAdOrderGraphic__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.udac_points__c</field>
            <operation>contains</operation>
            <value>VPAO;</value>
        </criteriaItems>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.DFF_RecordType_Name__c</field>
            <operation>contains</operation>
            <value>iYP</value>
        </criteriaItems>
        <criteriaItems>
            <field>Digital_Product_Requirement__c.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>iYP Mobile</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Art_Work_Complete</fullName>
        <assignedToType>owner</assignedToType>
        <description>The art work you submitted to the Graphics Department is complete and ready for review.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Art Work Complete</subject>
    </tasks>
    <tasks>
        <fullName>Print_Listing_Content_Enter_Content</fullName>
        <assignedToType>owner</assignedToType>
        <description>Please complete the Print Listing Content field on the Print Listing record.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Print Listing - Enter Content</subject>
    </tasks>
</Workflow>
