<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_On_Screen_Spec_Ad_Link_Hidden</fullName>
        <description>http://67.211.229.244/LoResCopy/  (244) is location for Miles Test</description>
        <field>OnScreenSpecAdLink_Hidden__c</field>
        <formula>&quot;http://67.211.229.244/LoResCopy/&quot;+ Name +&quot;.PDF&quot;</formula>
        <name>Populate On Screen Spec Ad Link Ad</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Spec_Art_Completed_URL</fullName>
        <description>Populate Spec Art Completed when the Spec Ad Status is Complete
&apos;http://67.211.229.244/HiResCopy/&apos;+ Name +&apos;.PDF&apos;  (244 is Miles Test)</description>
        <field>Link_to_Image_Preview__c</field>
        <formula>&apos;http://67.211.229.244/HiResCopy/&apos;+ Name +&apos;.PDF&apos;</formula>
        <name>Populate Spec Art Completed URL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Spec_Art_Date_Graphics_Complete</fullName>
        <description>Populate the date the graphic artist completes the Spec Ad where the Spec Ad status is Complete.</description>
        <field>Date_Graphics_Complete__c</field>
        <formula>Now()</formula>
        <name>Populate Spec Art Date Graphics Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Spec_Art_Received_Date</fullName>
        <description>When the Spec Art Ad Status is set to In Progress, set the Date Graphics  Received.</description>
        <field>Date_Received__c</field>
        <formula>Now()</formula>
        <name>Populate Spec Art Received Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Telephone_Area_Code</fullName>
        <description>Populate the Telephone Area Code that is passed to Miles33.  The Advertised Telephone Number is broken down into Area Code and Telephone Number.</description>
        <field>Telephone_Areacode__c</field>
        <formula>MID(Advertised_Listed_Phone_Number__c, 2,3)</formula>
        <name>Populate Telephone Area Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Telephone_Number</fullName>
        <description>Populate the Telephone Number from the Advertised Listed Phone Number; field is passed to Miles33.</description>
        <field>Telephone_Number__c</field>
        <formula>TRIM(SUBSTITUTE(RIGHT( Advertised_Listed_Phone_Number__c ,8), &quot;-&quot;,&quot;&quot;))</formula>
        <name>Populate Telephone Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>SpecAd_Graphic_file_upload_to_Miles</fullName>
        <apiVersion>30.0</apiVersion>
        <description>Used to upload the SpecAd Graphic file to PT</description>
        <endpointUrl>https://app.informaticaondemand.com/saas/api/1/salesforceoutboundmessage/n4xEE5zdScVxkyeePD53NkT4cpaKFff5</endpointUrl>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>kristen.maldonado@berrysfdc2.com</integrationUser>
        <name>SpecAd Graphic file upload to Miles</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Completed Spec Ad</fullName>
        <actions>
            <name>Spec_Art_Work_Completed</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Spec_Art_Request__c.Miles_Status__c</field>
            <operation>contains</operation>
            <value>Complete</value>
        </criteriaItems>
        <description>When the Graphic artist completes the Spec Ad, several work flow actions occur: Create a task for the Sales Rep to alert them the ad is complete; update the links to see the High/Low res images; populate the Date Graphics Completed date.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>In Progress Spec Art Request</fullName>
        <actions>
            <name>Populate_Spec_Art_Received_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Spec_Art_Request__c.Miles_Status__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <description>When a Miles33 artist begins work on the Spec Art Request, the status is set to In Progress, when the status changes, set the Date Graphics Received</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Telephone  Area Number</fullName>
        <actions>
            <name>Populate_Telephone_Area_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_Telephone_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Spec_Art_Request__c.Advertised_Listed_Phone_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Populate the Telephone Area and Number fields, from the Advertised Listed Phone Number; these fields are passed to Miles33.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Upload Graphics to Miles33 PT</fullName>
        <actions>
            <name>SpecAd_Graphic_file_upload_to_Miles</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <description>Used to Upload the graphic files to Miles PT</description>
        <formula>AND( ISCHANGED( Migrate_to_Miles_33__c),Migrate_to_Miles_33__c &lt;&gt; null)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Spec_Art_Work_Completed</fullName>
        <assignedToType>owner</assignedToType>
        <description>The Spec Request you submitted to PT is Completed and ready for review.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Spec Art Work Completed</subject>
    </tasks>
</Workflow>
