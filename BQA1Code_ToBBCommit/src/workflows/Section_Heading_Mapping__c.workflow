<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CHECK_Blackout_App_Count_Indicator</fullName>
        <field>Blackout_Appearance_Count_Indicator__c</field>
        <literalValue>1</literalValue>
        <name>CHECK Blackout App Count Indicator</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CHECK_Blackout_IBUN_App_Count_Indicator</fullName>
        <field>Blackout_IBUN_Appearance_Count_Indicator__c</field>
        <literalValue>1</literalValue>
        <name>CHECK Blackout IBUN App Count Indicator</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Decrease_Appearance_Count_By_1</fullName>
        <description>Used to decrease the Appearance Count by 1</description>
        <field>Appearance_Count__c</field>
        <formula>Appearance_Count__c - 1</formula>
        <name>Decrease Appearance Count By 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Increase_Appearance_Count_By_1</fullName>
        <description>This Field Update will increase the Appearance Count on Section Heading Mapping by 1</description>
        <field>Appearance_Count__c</field>
        <formula>IF( TEXT(Appearance_Count__c) = NULL, 1, Appearance_Count__c + 1)</formula>
        <name>Increase Appearance Count By 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SHM_Name_Field_Update</fullName>
        <field>Name</field>
        <formula>Directory_Section__r.Section_Code__c &amp;&quot;-&quot;&amp;Directory_Heading__r.code__c</formula>
        <name>SHM Name Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UNCHCK_Blackout_IBUN_App_Count_Indicator</fullName>
        <field>Blackout_IBUN_Appearance_Count_Indicator__c</field>
        <literalValue>0</literalValue>
        <name>UNCHCK Blackout IBUN App Count Indicator</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UNCHECK_Blackout_App_Count_Indicator</fullName>
        <field>Blackout_Appearance_Count_Indicator__c</field>
        <literalValue>0</literalValue>
        <name>UNCHECK Blackout App Count Indicator</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateSec_Head_Mapping_Name_Unique_Check</fullName>
        <description>Used to ensure that Section Heading Mapping name is UNIQUE</description>
        <field>Sec_Head_Mapping_Name_Unique_Check__c</field>
        <formula>Directory_Section__r.Name &amp; &quot;-&quot; &amp; Directory_Heading__r.Name</formula>
        <name>UpdateSec Head Mapping Name Unique Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Disclaimer_Text</fullName>
        <field>Disclaimer_Text__c</field>
        <formula>Heading_Disclaimer__r.Disclaimer_Text__c</formula>
        <name>Update Disclaimer Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_External_ID</fullName>
        <description>Used to populate and format the External ID field for dupe checking and for Upserting purposes</description>
        <field>External_ID__c</field>
        <formula>Section_Code__c &amp; &quot;-&quot; &amp; Heading_Code__c</formula>
        <name>Update External ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Pagination_Appearance_Count</fullName>
        <description>Used to copy the Appearance Count field to the Pagination Appearance Count field.</description>
        <field>Pagination_Appearance_Count__c</field>
        <formula>Appearance_Count__c</formula>
        <name>Update Pagination Appearance Count</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Pagination_IBUN_Appearance_Count</fullName>
        <description>Used to copy the Internet Bundle Appearances field to the Pagination IBUN Appearance Count field.</description>
        <field>Pagination_IBUN_Appearance_Count__c</field>
        <formula>Internet_Bundle_Appearances__c</formula>
        <name>Update Pagination IBUN Appearance Count</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Section_Heading_ID</fullName>
        <description>Used to Concatenate the 18 Character CASESAFEID of Directory Section and Directory Heading to ensure no Duplicate Section Heading Mapping records exist</description>
        <field>Section_Heading_ID__c</field>
        <formula>CASESAFEID(Directory_Section__c) &amp; CASESAFEID(Directory_Heading__c)</formula>
        <name>Update Section Heading ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Blackout Period Appearance Count Handler</fullName>
        <actions>
            <name>CHECK_Blackout_App_Count_Indicator</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This Workflow Rule is used to copy the Appearance Count field into the Pagination Appearance Count field. The value is only copied in if the current date is NOT within the Sales Blackout Period.</description>
        <formula>AND(Pagination_Appearance_Count__c != Appearance_Count__c,             TODAY()  &gt;=  BOTS_Sales_Lockout_Date__c,     TODAY()  &lt;=  BOTS_Ship_Date__c  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>UNCHECK_Blackout_App_Count_Indicator</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Section_Heading_Mapping__c.BOTS_Ship_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Blackout Period Pagination IBUN Appearance Count Handler</fullName>
        <actions>
            <name>CHECK_Blackout_IBUN_App_Count_Indicator</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(Pagination_IBUN_Appearance_Count__c !=         Internet_Bundle_Appearances__c,             TODAY()  &gt;=  BOTS_Sales_Lockout_Date__c,     TODAY()  &lt;=  BOTS_Ship_Date__c  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>UNCHCK_Blackout_IBUN_App_Count_Indicator</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Section_Heading_Mapping__c.BOTS_Ship_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Decrease Appearance Count By 1</fullName>
        <actions>
            <name>Decrease_Appearance_Count_By_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Used to Decrease Appearance Count By 1 if Heading is Forced to Print but then updated to NOT be Forced</description>
        <formula>AND( NOT(ISNEW()),      ISCHANGED(Force_Heading_To_Appear__c),      Force_Heading_To_Appear__c = FALSE )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Force Section Heading to Appear</fullName>
        <actions>
            <name>Increase_Appearance_Count_By_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( Force_Heading_To_Appear__c = TRUE, OR(Appearance_Count__c &gt; 0, ISNULL(Appearance_Count__c)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>IBUN Pagination Appearance Count Handler</fullName>
        <actions>
            <name>Update_Pagination_IBUN_Appearance_Count</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This Workflow Rule is used to copy the Internet Bundle Appearances field into the Pagination IBUN Appearance Count field. The value is only copied in if the current date is NOT within the Sales Blackout Period.</description>
        <formula>AND( ISCHANGED(Internet_Bundle_Appearances__c),      OR( TODAY()  &lt;  BOTS_Sales_Lockout_Date__c,          TODAY()  &gt;  BOTS_Ship_Date__c )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Increase Appearance Count If Forced And Zero</fullName>
        <actions>
            <name>Increase_Appearance_Count_By_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Fired if a Section Heading is forced to print but the Appearance Count drops to 0</description>
        <formula>Force_Heading_To_Appear__c = TRUE &amp;&amp; Appearance_Count__c = 0</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Pagination Appearance Count Handler</fullName>
        <actions>
            <name>Update_Pagination_Appearance_Count</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This Workflow Rule is used to copy the Appearance Count field into the Pagination Appearance Count field. The value is only copied in if the current date is NOT within the Sales Blackout Period.</description>
        <formula>AND( ISCHANGED(Appearance_Count__c),      OR( TODAY()  &lt;  BOTS_Sales_Lockout_Date__c,          TODAY()  &gt;  BOTS_Ship_Date__c )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Disclaimer Text</fullName>
        <actions>
            <name>Update_Disclaimer_Text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Section_Heading_Mapping__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This Rule will populate Disclaimer Text in Section heading Mapping Record</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Section Heading Mapping Uniqueness</fullName>
        <actions>
            <name>SHM_Name_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>UpdateSec_Head_Mapping_Name_Unique_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_External_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Section_Heading_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This Workflow runs a series of Field Updates on fields marked as UNIQUE to ensure that there are NO Duplicate Section Heading Mapping records. It also formats the NAME field to the appropriate Naming Convention.</description>
        <formula>OR( ISNEW(),  ISCHANGED(Name),  ISCHANGED(Directory_Section__c),  ISCHANGED(Directory_Heading__c),  ISCHANGED(External_ID__c),  ISCHANGED(Section_Heading_ID__c),  ISCHANGED(Sec_Head_Mapping_Name_Unique_Check__c),  ISNULL(External_ID__c),  ISNULL(Section_Heading_ID__c),  ISNULL(Sec_Head_Mapping_Name_Unique_Check__c)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
