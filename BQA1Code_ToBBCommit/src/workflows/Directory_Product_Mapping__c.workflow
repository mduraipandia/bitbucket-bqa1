<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Format_NAME</fullName>
        <description>Used to ensure that the DPM record ALWAYS has the appropriate naming convention</description>
        <field>Name</field>
        <formula>Directory__r.Directory_Code__c &amp; &quot;-&quot; &amp; Product2__r.ProductCode</formula>
        <name>Format NAME</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_DPM_Dir_Prod_External_ID</fullName>
        <description>Used to update DPM Prod Dir External ID. Concatenates the full 18 Character SFDC IDs for Directory and Product</description>
        <field>DPM_Dir_Prod_External_ID__c</field>
        <formula>CASESAFEID(Directory__c) &amp; CASESAFEID(Product2__c)</formula>
        <name>Update DPM Dir Prod External ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_DPM_External_ID</fullName>
        <description>Used to update the DPM External ID. Concatenates the Directory Code &amp; Product Code together</description>
        <field>DPM_External_ID__c</field>
        <formula>Directory__r.Directory_Code__c &amp; &quot;-&quot; &amp;  Product2__r.ProductCode</formula>
        <name>Update DPM External ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Directory Product Mapping Uniqueness</fullName>
        <actions>
            <name>Format_NAME</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_DPM_Dir_Prod_External_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_DPM_External_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This Workflow is fired upon certain fields being edited and/or created to handle field updates and Name formatting to ensure there are no duplicate DPM records</description>
        <formula>OR( ISNEW(),  ISCHANGED(Name),  ISCHANGED(Directory__c),  ISCHANGED(Product2__c),  ISCHANGED(DPM_External_ID__c),  ISCHANGED(DPM_Dir_Prod_External_ID__c),  ISNULL(DPM_External_ID__c),  ISNULL(DPM_Dir_Prod_External_ID__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
