<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Scoping_Total_All_Groups</fullName>
        <field>Scoping_Total_All_Groups__c</field>
        <formula>Requirements_Total_All_Groups__c + Development_Total_All_Groups__c + Testing_Total_All_Groups__c + Deployment_Totals_All_Groups__c</formula>
        <name>Scoping Total-All Groups</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Scoping Total-All Groups</fullName>
        <actions>
            <name>Scoping_Total_All_Groups</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>1=1</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
