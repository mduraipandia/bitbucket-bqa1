<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Capture_Date_of_Claim_Reversal_OLID</fullName>
        <field>Date_of_Reversal__c</field>
        <formula>DATEVALUE( LastModifiedDate )</formula>
        <name>Capture Date of Claim Reversal OLID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Capture Date of Claim Reversal</fullName>
        <actions>
            <name>Capture_Date_of_Claim_Reversal_OLID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order_Line_Item_Discount__c.claim_reversed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
