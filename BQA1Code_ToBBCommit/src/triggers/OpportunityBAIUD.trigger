trigger OpportunityBAIUD on Opportunity (before insert, before update, before delete,after Insert, after update, after Delete) {
	if(Test.isRunningtest() || !CommonMethods.skipTriggerLogic(CommonMessages.opportunityObjectName)) {
		if(trigger.isBefore) {      
			if(trigger.isInsert) {
				OpportunityHandlerController.onBeforeInsert(trigger.new);
			}
			else if(trigger.isUpdate) {
				OpportunityHandlerController.onBeforeUpdate(trigger.new, trigger.newMap, trigger.oldMap);           
			}
			else if(trigger.isDelete) {
				OpportunityHandlerController.onBeforeDelete(trigger.new, trigger.old, trigger.oldMap);
			}
		}
		if(trigger.isAfter) {
			if(trigger.isUpdate) {
				OpportunityHandlerController.onAfterUpdate(trigger.new, trigger.oldMap, trigger.newMap);
			}
		}
	}
}