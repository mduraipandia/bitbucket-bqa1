trigger NationalStagingLineItem_BAIUD on National_Staging_Line_Item__c (before insert, before update, after insert, after update) {
    if (trigger.isBefore) {      
        if (trigger.isInsert) {
            NationalStagingLineItemHandlerController.onBeforeInsert(trigger.new);
        }
        else if(trigger.isUpdate) {
            NationalStagingLineItemHandlerController.onBeforeUpdate(trigger.new, trigger.oldMap);
        }
    }
    
    if (trigger.isAfter) {      
        if (trigger.isInsert) {
            NationalStagingLineItemHandlerController.onAfterInsert(trigger.new);
        }
        else if(trigger.isUpdate) {
            NationalStagingLineItemHandlerController.onAfterUpdate(trigger.new, trigger.oldMap);
        }
    }
}