trigger Account_BIUAIUD on Account (before insert, before update,after insert, after update, after delete) {
    if(Test.isRunningtest() || !CommonMethods.skipTriggerLogic(CommonMessages.accountObjectName)) { 
        if(trigger.isBefore) {
            if(trigger.isInsert) {
                AccountTriggerHandlerController.onBeforeInsert(trigger.new);            
            } else if(trigger.isUpdate) {
                AccountTriggerHandlerController.onBeforeUpdate(trigger.new, trigger.oldMap);      
            }
        }
        
        if (trigger.isAfter) {
            if (trigger.isInsert) {
                AccountTriggerHandlerController.onAfterInsert(trigger.new, trigger.newMap);            
            } else if (trigger.isUpdate) {
                AccountTriggerHandlerController.onAfterUpdate(trigger.new, trigger.oldMap, trigger.newMap);
            }
        }
    }
}