trigger SpecArtRequest_BAIU on Spec_Art_Request__c (before insert, before update) {
	for(Spec_Art_Request__c iterator : Trigger.New) {
		if(Trigger.OldMap != null) {
			if(iterator.Advertised_Listed_Phone_Number__c != null && iterator.Advertised_Listed_Phone_Number__c != Trigger.OldMap.get(iterator.Id).Advertised_Listed_Phone_Number__c) {
				string strPhoneNumber = iterator.Advertised_Listed_Phone_Number__c;
				strPhoneNumber = strPhoneNumber.replaceAll('[()-]', '');
				iterator.Telephone_Areacode__c = strPhoneNumber.Left(3);
				iterator.Telephone_Number__c = strPhoneNumber.Right(7);
			}
		}
		else {
			if(iterator.Advertised_Listed_Phone_Number__c != null) {
				string strPhoneNumber = iterator.Advertised_Listed_Phone_Number__c;
				strPhoneNumber = strPhoneNumber.replaceAll('[()-]', '');
				iterator.Telephone_Areacode__c = strPhoneNumber.Left(3);
				iterator.Telephone_Number__c = strPhoneNumber.Right(7);
			}
		}
	}
}