trigger SmartStreetContactVerification on Contact (before insert,before update,after insert,after update) {
 
  
  if(trigger.isBefore){
      if(trigger.isupdate){
      for(Contact con:Trigger.New){
          if(updateHelper.inFutureContext==False){
          if(con.mailingstate!=trigger.oldmap.get(con.id).mailingstate || con.mailingcity!=trigger.oldmap.get(con.id).mailingcity || con.mailingStreet!=trigger.oldmap.get(con.id).mailingStreet || con.mailingpostalcode!=trigger.oldmap.get(con.id).mailingpostalcode ){
                
                con.Street_Status__c ='';
               
                con.Latitude__latitude__s=Double.valueOf(00.000); 
                con.Latitude__longitude__s=Double.valueOf(00.000);
                con.County_FIPS_Code__c= '';
                con.County_Name__c ='';
                con.Geolocation_Precision__c ='';
                con.Daylight_Savings_Observed__c ='' ;
                con.Delivery_Point_Validation_Code__c = '';
                con.Type_of_Zip_Code__c='';
                con.Delivery_Point_Vacant__c='';
                con.Address_Changes_Made_Codes__c ='';
                con.Residential_Delivery_Indicator__c=''; 
                con.LACSLink_Code__c='';
                con.LACSLink_Match_Indicator__c='';
                con.SuiteLink_Match__c='';
                con.Building_Default_Indicator__c='';
                con.POSTNET_Barcode__c='';
                con.Private_Mailbox_Unit_Designator__c='';
                con.Private_Mailbox_Number__c='';
                con.Uses_Commercial_Mail_Receiving_Agency__c='';
                con.DPV_Reason_Code_s__c='';
                con.Type_of_Address__c='';
                con.Address_is_Active__c ='';
                con.Address_Not_Ready_for_Mail__c='';
                con.UTC_Offset__c ='';
                con.Carrier_Route__c = '';
               con.Time_Zone__c='';
                
              System.debug('Vertex-SmartyStreets **************isUpdate1*******************'+Trigger.New);
          }
          else
          {
              
             if(trigger.oldmap.get(con.id).Do_Not_Validate_Address__c==true){
             
             }
               else{
              
                if(con.Do_Not_Validate_Address__c==true){
                con.Street_Status__c ='';
                
                con.Latitude__latitude__s=Double.valueOf(00.000); 
                con.Latitude__longitude__s=Double.valueOf(00.000);
                con.County_FIPS_Code__c= '';
                con.County_Name__c ='';
                con.Geolocation_Precision__c ='';
                con.Daylight_Savings_Observed__c ='' ;
                con.Delivery_Point_Validation_Code__c = '';
                con.Type_of_Zip_Code__c='';
                con.Delivery_Point_Vacant__c='';
                con.Address_Changes_Made_Codes__c ='';
                con.Residential_Delivery_Indicator__c=''; 
                con.LACSLink_Code__c='';
                con.LACSLink_Match_Indicator__c='';
                con.SuiteLink_Match__c='';
                con.Building_Default_Indicator__c='';
                con.POSTNET_Barcode__c='';
                con.Private_Mailbox_Unit_Designator__c='';
                con.Private_Mailbox_Number__c='';
                con.Uses_Commercial_Mail_Receiving_Agency__c='';
                con.DPV_Reason_Code_s__c='';
                con.Type_of_Address__c='';
                con.Address_is_Active__c ='';
                con.Address_Not_Ready_for_Mail__c='';
                con.UTC_Offset__c ='';
                con.Carrier_Route__c = ''; 
                con.Time_Zone__c=''; 
                }
                }
              System.debug('Vertex-SmartyStreets **************isUpdate2--********************'+Trigger.New);
          }
          }
        }
      }
  }
  
   if(trigger.isAfter){
    if(trigger.isInsert){
    
       if(updateHelper.inFutureContext==False){
        for(Contact con: Trigger.New){
            System.debug('Vertex-SmartyStreets ***************Dont validdate trigger********************'+con.Do_Not_Validate_Address__c);
           
            if(con.Do_Not_Validate_Address__c==false){
             SmartStreetContactVerification.updateContact(con.Id);
            System.debug('Vertex-SmartyStreets ***************Dont validdate trigger********************'+con.Do_Not_Validate_Address__c);
            }
            
        }
        System.debug('Vertex-SmartyStreets***************isAfter1-isInsert********************'+Trigger.New);
        }
    }
    if(trigger.isUpdate){
    
     if(updateHelper.inFutureContext==False){
        for(Contact con: Trigger.New){
           		System.debug('Testingg con.Do_Not_Validate_Address__c '+con.Do_Not_Validate_Address__c);
                if(con.Do_Not_Validate_Address__c==false){
					  System.debug('Testingg con.mailingstate '+con.mailingstate);
                      if(con.mailingstate!=trigger.oldmap.get(con.id).mailingstate || con.mailingcity!=trigger.oldmap.get(con.id).mailingcity || con.mailingStreet!=trigger.oldmap.get(con.id).mailingStreet || con.mailingpostalcode!=trigger.oldmap.get(con.id).mailingpostalcode ){
                                System.debug('Testingg SmartStreetContactVerification call ');
                                SmartStreetContactVerification.updateContact(con.Id);
                                System.debug('Vertex-SmartyStreets*************isUpdated-if******************');
                                            
                       }
                       else
                       {
                               System.debug('Vertex-SmartyStreets*************isUpdated-else******************'+trigger.oldmap.get(con.id).Do_Not_Validate_Address__c);
                               if(trigger.oldmap.get(con.id).Do_Not_Validate_Address__c==true){                                 
                               System.debug('Testingg SmartStreetContactVerification call 2 ');
                               SmartStreetContactVerification.updateContact(con.Id);
                               System.debug('Vertex-SmartyStreets*************isUpdated-else******************');
                                 
                                 }   
                       
                       }
                
               
               }
           
        }
        
        System.debug('Vertex-SmartyStreets***************isAfter2-isUpdate********************'+Trigger.New);
        }            
      }
  }
  
  
}