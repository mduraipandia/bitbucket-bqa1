trigger TransLineVersioningTrigger on c2g__codaTransactionLineItem__c (before update) 
{
    for(c2g__codaTransactionLineItem__c transLine : Trigger.New)
    {
        if(TransactionLineContextMap.transLineMap != null && transactionLineContextMap.transLineMap.containsKey(transLine.Id))
        {
            System.debug('Outstanding Balance '+transLine.c2g__AccountOutstandingValue__c);
            System.debug('Outstanding Balance '+(Decimal)Trigger.oldMap.get(transLine.Id).get('c2g__AccountOutstandingValue__c'));
            System.debug('New version '+transLine.Version__c);
            System.debug('Old version '+(Decimal)Trigger.oldMap.get(transLine.Id).get('Version__c'));
            if((Decimal)Trigger.oldMap.get(transLine.Id).get('Version__c') != transactionLineContextMap.transLineMap.get(transLine.Id).Version__c)
            {
                transLine.addError('This record has not updated due to a locking issue.');
            }
            if (transLine.Version__c != (Decimal)Trigger.oldMap.get(transLine.Id).get('Version__c'))
            {
                transLine.addError('This record has not updated due to a locking issue.');
            }
            else
            {
                transLine.Version__c = ((Decimal)Trigger.oldMap.get(transLine.Id).get('Version__c') == null) ? 1 : transline.Version__c + 1;
            }
        }
    }    
}