trigger SectionHeadingMapping_BIUDAIUD on Section_Heading_Mapping__c (before insert, before update) {
    if(Test.isRunningtest() || !CommonMethods.skipTriggerLogic('Section_Heading_Mapping__c')) {
        if(trigger.isBefore) {
            if(trigger.isInsert) {
                SectionHeadingMappingHandlerController.onBeforeInsert(trigger.new);
            }
            else if(trigger.isUpdate) {
                SectionHeadingMappingHandlerController.onBeforeUpdate(trigger.new, trigger.oldMap);
            }
        }
    }
}