trigger synchronizeWithJIRA_CaseComment on CaseComment (after Insert, after Update) {
    Map<String, JIRAConnector__c> mapJIRAConnector = JIRAConnector__c.getAll();    
    String userEmailid = UserInfo.getUserEmail();
    if(userEmailid <> mapJIRAConnector.values().get(0).Skipping_Email_Address__c) {
        if(trigger.isAfter) {
            if(trigger.isInsert) {
                CaseCommentHandlerController.onAfterInsert(trigger.new);
            } else if(trigger.isUpdate) {
                CaseCommentHandlerController.onAfterUpdate(trigger.new, trigger.oldMap);
            }
        }
    }
    
    
   /*ID BugRecordType;
    ID EnhancementRecordType;

    for (RecordType rt :[SELECT Id, Name,DeveloperName FROM RecordType WHERE SObjectType = 'Case' AND DeveloperName IN ('Bug', 'Enhancement')])
    {
            if (rt.DeveloperName == 'Bug') BugRecordType = rt.Id;
            if (rt.DeveloperName == 'Enhancement') EnhancementRecordType = rt.Id;

    } 
             
    system.debug('BugRecordType======='+BugRecordType+'==='+EnhancementRecordType);   
    
    Set<Id> parentCase=new Set<Id>();
    Map<Id,Case> mapCase=new Map<Id,Case>();
    for (CaseComment t: Trigger.new)
    {
        parentCase.add(t.ParentId);
    }
    system.debug('parentCase========'+parentCase);
    List<Case> lstCase=[Select Id,RecordTypeId,ParentId from case where Id in :parentCase ];
    
    system.debug('lstCase========'+lstCase);
    if(lstCase!=null && lstCase.size()>0)
    {
        for(Case c :lstCase)
        {
            mapCase.put(c.Id,c);
        }
    }
    String userEmailid='';
    User current_user=[SELECT Email FROM User WHERE Id= :UserInfo.getUserId()] ;
    userEmailid = current_user.Email;
    system.debug('userEmailid======'+userEmailid);
    if(userEmailid <> 'beth.starr@theberrycompany.com')
    {
        system.debug('mapCase========='+mapCase);
        for (CaseComment t: Trigger.new)
        {
            if(!mapCase.isEmpty() && mapCase.containskey(t.ParentId))
            {
                String objectId = mapCase.get(t.ParentId).id;
                system.debug('objectId=========='+objectId);
                if(mapCase.get(t.ParentId).RecordTypeId ==BugRecordType || mapCase.get(t.ParentId).RecordTypeId ==EnhancementRecordType)
                {
                    JIRAConnectorWebserviceCalloutSync.synchronizeWithJIRAIssue(objectId);    
                }
            }
        }
    }*/
}