trigger SmartStreetAccountVerification on Account (before insert,before update,after insert,after update) {
  
 
  if(trigger.isBefore){
   if(trigger.isupdate){
      for(Account acc:Trigger.New){
           System.debug('Vertex-SmartyStreets**stret_status__c***********'+acc.billingStreet+'trigger.oldmap.get(acc.id).billingStreet***'+trigger.oldmap.get(acc.id).billingStreet);
          if(updateHelper.inFutureContext==False){
           System.debug('Vertex-SmartyStreets***stret_status__c***********'+acc.billingStreet+'trigger.oldmap.get(acc.id).billingStreet***'+trigger.oldmap.get(acc.id).billingStreet);
           if(acc.billingstate!=trigger.oldmap.get(acc.id).billingstate || acc.billingcity!=trigger.oldmap.get(acc.id).billingcity || acc.billingStreet!=trigger.oldmap.get(acc.id).billingStreet || acc.billingpostalcode!=trigger.oldmap.get(acc.id).billingpostalcode ){
                acc.Street_Status__c ='';
                acc.Latitude__latitude__s=Double.valueOf(00.000);
                acc.Latitude__longitude__s=Double.valueOf(00.000);
                acc.County_FIPS_Code__c= '';
                acc.County_Name__c ='';
                acc.Geolocation_Precision__c ='';
                acc.Daylight_Savings_Observed__c = '';
                acc.Delivery_Point_Validation_Code__c = '';
                acc.Type_of_Zip_Code__c='';
                acc.Delivery_Point_Vacant__c='';
                acc.Address_Changes_Made_Codes__c ='';
                acc.Residential_Delivery_Indicator__c='';
                acc.LACSLink_Code__c='';
                acc.LACSLink_Match_Indicator__c='';
                acc.SuiteLink_Match__c='';
                acc.Building_Default_Indicator__c='';
                acc.POSTNET_Barcode__c='';
                acc.Private_Mailbox_Number__c='';
                acc.Private_Mailbox_Unit_Designator__c='';
                acc.Uses_Commercial_Mail_Receiving_Agency__c='';
                acc.DPV_Reason_Code__c='';
                acc.Type_of_Address__c='';
                acc.Address_is_Active__c ='';
                acc.UTC_Offset__c ='';
                acc.Carrier_Route__c = ''; 
                acc.Time_Zone__c='';
                
                System.debug('Vertex-SmartyStreets**************isUpdate1*******************'+Trigger.New);
          }
          else{
               if(trigger.oldmap.get(acc.id).Do_Not_Validate_Address__c==true){
               
                }
                else
                {
                if(acc.Do_Not_Validate_Address__c==true){
                acc.Street_Status__c ='';
                
                // smart street fields
                acc.Latitude__latitude__s=Double.valueOf(00.000);
                acc.Latitude__longitude__s=Double.valueOf(00.000);
                acc.County_FIPS_Code__c= '';
                acc.County_Name__c ='';
                acc.Geolocation_Precision__c ='';
                acc.Daylight_Savings_Observed__c = '';
                acc.Delivery_Point_Validation_Code__c = '';
                acc.Type_of_Zip_Code__c='';
                acc.Delivery_Point_Vacant__c='';
                acc.Address_Changes_Made_Codes__c ='';
                acc.Residential_Delivery_Indicator__c='';
                acc.LACSLink_Code__c='';
                acc.LACSLink_Match_Indicator__c='';
                acc.SuiteLink_Match__c='';
                acc.Building_Default_Indicator__c='';
                acc.POSTNET_Barcode__c='';
                acc.Private_Mailbox_Number__c='';
                acc.Private_Mailbox_Unit_Designator__c='';
                acc.Uses_Commercial_Mail_Receiving_Agency__c='';
                acc.DPV_Reason_Code__c='';
                acc.Type_of_Address__c='';
                acc.Address_is_Active__c ='';
                acc.UTC_Offset__c ='';
                acc.Carrier_Route__c = ''; 
                acc.Time_Zone__c='';
                }
                }
              System.debug('Vertex-SmartyStreets**************isUpdate2--********************'+Trigger.New);
          }
          }
        }
        
      }
  }
  
   if(trigger.isAfter){
    if(trigger.isInsert){
    if(updateHelper.inFutureContext==False){
        for(Account acc : Trigger.New){
            //System.debug('Vertex-SmartyStreets***************Dont validdate trigger********************'+newvalidvalues);
           
            if(acc.Do_Not_Validate_Address__c==false){
           SmartStreetAccountVerification.updateAccount(acc.Id);
            System.debug('Vertex-SmartyStreets***************Dont validdate trigger********************'+acc.Do_Not_Validate_Address__c);
            }
        }
        System.debug('Vertex-SmartyStreets***************isAfter1-isInsert********************'+Trigger.New);
        }
    }
    if(trigger.isUpdate){
        System.debug('Testingg Inside Update updateHelper.inFutureContext '+updateHelper.inFutureContext);
     if(updateHelper.inFutureContext==False){
        for(Account acc : Trigger.New){
                //System.debug('oldvalidbillingaddres**************'+oldvalidbillingaddres);
                if(acc.Do_Not_Validate_Address__c==false){
                if(acc.billingstate!=trigger.oldmap.get(acc.id).billingstate || acc.billingcity!=trigger.oldmap.get(acc.id).billingcity || acc.billingStreet!=trigger.oldmap.get(acc.id).billingStreet || acc.billingpostalcode!=trigger.oldmap.get(acc.id).billingpostalcode ){
                     System.debug('Vertex-SmartyStreets***************isAfter2-isUpdate********************'+Trigger.New);
                           System.debug('Testingg SmartStreetAccountVerification call 1 ');
                           SmartStreetAccountVerification.updateAccount(acc.Id);
                }
                else
                {
                 if(trigger.oldmap.get(acc.id).Do_Not_Validate_Address__c==true){    
                           System.debug('Testingg SmartStreetAccountVerification call 2 ');
                           SmartStreetAccountVerification.updateAccount(acc.Id);  
                           System.debug('Vertex-SmartyStreets***************isAfter2-isUpdate********************'+Trigger.old);  
                       }
                
                }
                
               
           }
          
        }
        
        System.debug('Vertex-SmartyStreets***************isAfter2-isUpdate********************'+Trigger.New);
        }
        
       
  }
  
  
}
}