trigger EmailMessageDelete on EmailMessage (before delete){

Id sysAdminId = [Select Id, Name from Profile where name = 'System Administrator' LIMIT 1].Id;

for (EmailMessage EmMsg : trigger.old)
if (UserInfo.getProfileId() != sysAdminId)
EmMsg.addError('Emails on Cases may not be deleted');
}