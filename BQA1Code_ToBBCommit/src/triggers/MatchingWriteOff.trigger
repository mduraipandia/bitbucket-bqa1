trigger MatchingWriteOff on ffps_ct__MatchingWriteOff__c (before insert, before update) {
    if(Trigger.isBefore) {
        set<Id> setFlatAccoountID = new set<ID>();
        for(ffps_ct__MatchingWriteOff__c iterator : Trigger.New) {
            if(iterator.ffps_ct__FlatTransaction__c != null) {
                setFlatAccoountID.add(iterator.ffps_ct__FlatTransaction__c);
            }
        }
        if(setFlatAccoountID.size() > 0) {
            map<Id, ffps_ct__FlatTransaction__c> mapFlatRecord = new map<Id, ffps_ct__FlatTransaction__c>([SELECT Id, ffps_ct__Account__c,ffps_ct__DocumentNumber__c FROM ffps_ct__FlatTransaction__c where Id IN:setFlatAccoountID]);
            if(mapFlatRecord.size() > 0) {
                for(ffps_ct__MatchingWriteOff__c iterator : Trigger.New) {
                    if(mapFlatRecord.get(iterator.ffps_ct__FlatTransaction__c) != null) {
                        String strDocNumber = mapFlatRecord.get(iterator.ffps_ct__FlatTransaction__c).ffps_ct__DocumentNumber__c;
                        if(strDocNumber.startsWith('SIN')) {
                            iterator.MWO_Sales_Invoice_Indicator__c = true;
                        }
                        iterator.Account__c = mapFlatRecord.get(iterator.ffps_ct__FlatTransaction__c).ffps_ct__Account__c;
                    }
                }
            }
        }
    }
}