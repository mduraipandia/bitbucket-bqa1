<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Billing history/tracking object for P4P Order Line Items. Used to track clicks per billing periods and clicks per month, and the resulting price for that month or billing period.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>BP_P4P_Total_Cost__c</fullName>
        <externalId>false</externalId>
        <formula>P4P_Quantity_Per_BP__c  *  Price_Per_Click_Lead_Call__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>BP P4P Total Cost</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Billing_Period_End__c</fullName>
        <description>The end of the Billing Period in the Usage record</description>
        <externalId>false</externalId>
        <inlineHelpText>The end of the Billing Period in the Usage record</inlineHelpText>
        <label>Billing Period End</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Billing_Period_Start__c</fullName>
        <description>The beginning of the Billing Period in the Usage record</description>
        <externalId>false</externalId>
        <inlineHelpText>The beginning of the Billing Period in the Usage record</inlineHelpText>
        <label>Billing Period Start</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>End_Of_Month_Date__c</fullName>
        <description>The date field used to provide the month and year for amounts of clicks per a given month in a given year.</description>
        <externalId>false</externalId>
        <inlineHelpText>The date field used to provide the month and year for amounts of clicks per a given month in a given year.</inlineHelpText>
        <label>End of Month Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Month__c</fullName>
        <description>The Month and Year for when tracking Clicks/Leads/Calls per Month.</description>
        <externalId>false</externalId>
        <formula>CASE(MONTH( End_Of_Month_Date__c ), 
1, &quot;January&quot;, 
2, &quot;February&quot;, 
3, &quot;March&quot;, 
4, &quot;April&quot;, 
5, &quot;May&quot;, 
6, &quot;June&quot;, 
7, &quot;July&quot;, 
8, &quot;August&quot;, 
9, &quot;September&quot;, 
10, &quot;October&quot;, 
11, &quot;November&quot;, 
12, &quot;December&quot;, 
&quot;&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>The Month and Year for when tracking Clicks/Leads/Calls per Month.</inlineHelpText>
        <label>Month</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Monthly_P4P_Total_cost__c</fullName>
        <description>When tracking Monthly, Amount of Clicks per Month x Cost Per Click/Lead/Call</description>
        <externalId>false</externalId>
        <formula>P4P_Quantity_Per_Month__c  *  Price_Per_Click_Lead_Call__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>When tracking Monthly, Amount of Clicks per Month x Cost Per Click/Lead/Call</inlineHelpText>
        <label>Monthly P4P Total Cost</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Order_Line_Item__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>The Order Line Item the P4P History record is referring to.</description>
        <externalId>false</externalId>
        <inlineHelpText>The Order Line Item the P4P History record is referring to.</inlineHelpText>
        <label>Order Line Item</label>
        <referenceTo>Order_Line_Items__c</referenceTo>
        <relationshipName>P4P_Order_Line_Item_History</relationshipName>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>P4P_Quantity_Per_BP__c</fullName>
        <description>The number of Clicks/Leads/Calls when tracking per Billing Period</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of Clicks/Leads/Calls when tracking per Billing Period</inlineHelpText>
        <label>P4P Quantity Per BP</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>P4P_Quantity_Per_Month__c</fullName>
        <description>The number of Clicks/Leads/Calls when tracking per Month</description>
        <externalId>false</externalId>
        <inlineHelpText>The number of Clicks/Leads/Calls when tracking per Month</inlineHelpText>
        <label>P4P Quantity Per Month</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Price_Per_Click_Lead_Call__c</fullName>
        <description>The Price per Click/Lead/Call</description>
        <externalId>false</externalId>
        <inlineHelpText>The Price per Click/Lead/Call</inlineHelpText>
        <label>Price Per Click/Lead/Call</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Year__c</fullName>
        <externalId>false</externalId>
        <formula>TEXT(YEAR(End_Of_Month_Date__c))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Year</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>P4P Billing History</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Price_Per_Click_Lead_Call__c</columns>
        <columns>Billing_Period_Start__c</columns>
        <columns>Billing_Period_End__c</columns>
        <columns>P4P_Quantity_Per_BP__c</columns>
        <columns>BP_P4P_Total_Cost__c</columns>
        <columns>Month__c</columns>
        <columns>Year__c</columns>
        <columns>P4P_Quantity_Per_Month__c</columns>
        <columns>Monthly_P4P_Total_cost__c</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>P4P Billing History Name</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>P4P Billing History</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
