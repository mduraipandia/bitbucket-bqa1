<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>ComposeGmail</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>LogCall</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>MailMerge</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>RequestUpdate</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SendEmail</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>ViewAll</actionName>
        <type>Default</type>
    </actionOverrides>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>Callers_Phone__c</fullName>
        <description>Phone Number of Caller (ANI)-- populated automatically for inContact Calls</description>
        <externalId>false</externalId>
        <inlineHelpText>Phone Number of Caller (ANI)</inlineHelpText>
        <label>Caller&apos;s Phone</label>
        <required>false</required>
        <type>Phone</type>
    </fields>
    <fields>
        <fullName>DB_Activity_Type__c</fullName>
        <description>This is a field that is installed by and used with the Adoption Dashboard AppExchange package.  If your org already has a similar field, you can change the reports that are part of the Adoption Dashboard package to use your custom field and then delete this field.</description>
        <externalId>false</externalId>
        <formula>IF(CONTAINS( Subject , &quot;Email&quot;), &quot;Email&quot;, IF(CONTAINS( Subject , &quot;Cold Call&quot;), &quot;Cold Call&quot;, IF(CONTAINS( Subject , &quot;Call&quot;), &quot;Call&quot;, IF(CONTAINS( Subject , &quot;Meeting&quot;), &quot;Meeting&quot;, IF(CONTAINS( Subject , &quot;On Site Visit&quot;), &quot;Site Visit&quot;, IF(CONTAINS( Subject , &quot;Quote&quot;), &quot;Send Letter/Quote&quot;, &quot;Other&quot;))))))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>DB Activity Type</label>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Directory__c</fullName>
        <description>For tasks related to a directory</description>
        <externalId>false</externalId>
        <label>Directory</label>
        <length>25</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Due_Date__c</fullName>
        <externalId>false</externalId>
        <formula>ActivityDate</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Due Date</label>
        <required>false</required>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Redirect_Reason__c</fullName>
        <description>Visible only for Customer Service (and Admin) users.  This field is used by CS to record incoming calls that are not related to Berry publications and/ or advertising.</description>
        <externalId>false</externalId>
        <inlineHelpText>Choose a reason for calls unrelated to Berry publications and/or advertising.</inlineHelpText>
        <label>Redirect Reason</label>
        <picklist>
            <picklistValues>
                <fullName>Wrong Number</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Misdirected Telco Call</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Directory Assistance</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Other</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>TAS_inContact_ContactID__c</fullName>
        <description>captures ContactID of call.  Contact ID is used by inContact to troubleshoot call.  Will only be populated for calls coming through inContact</description>
        <externalId>true</externalId>
        <inlineHelpText>Used by inContact to troubleshoot</inlineHelpText>
        <label>TAS inContact ContactID</label>
        <length>15</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Task_Owner_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Owner:User.FirstName &amp; &quot; &quot; &amp; Owner:User.LastName</formula>
        <label>Task Owner Name</label>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>pymt__Event_Code__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Code used to identify each type of PaymentConnect event notification task</inlineHelpText>
        <label>Event Code</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <listViews>
        <fullName>MyActivities</fullName>
        <filterScope>Mine</filterScope>
        <label>My Activities</label>
    </listViews>
    <listViews>
        <fullName>MyDelegatedActivities</fullName>
        <filterScope>Delegated</filterScope>
        <label>My Delegated Activities</label>
    </listViews>
    <listViews>
        <fullName>MyTeamsActivities</fullName>
        <filterScope>Team</filterScope>
        <label>My Team&apos;s Activities</label>
    </listViews>
    <listViews>
        <fullName>MyTeamsTasksToday</fullName>
        <filterScope>Team</filterScope>
        <filters>
            <field>TASK.CLOSED</field>
            <operation>equals</operation>
            <value>0</value>
        </filters>
        <filters>
            <field>ACTIVITY.TASK</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <filters>
            <field>TASK.DUE_DATE</field>
            <operation>lessThan</operation>
            <value>TOMORROW</value>
        </filters>
        <label>My Team&apos;s Tasks Today</label>
    </listViews>
    <listViews>
        <fullName>TodaysTasks</fullName>
        <filterScope>Mine</filterScope>
        <filters>
            <field>TASK.CLOSED</field>
            <operation>equals</operation>
            <value>0</value>
        </filters>
        <filters>
            <field>ACTIVITY.TASK</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <filters>
            <field>TASK.DUE_DATE</field>
            <operation>lessThan</operation>
            <value>TOMORROW</value>
        </filters>
        <label>Today&apos;s Tasks</label>
    </listViews>
    <listViews>
        <fullName>UpcomingEvents</fullName>
        <filterScope>Mine</filterScope>
        <filters>
            <field>ACTIVITY.TASK</field>
            <operation>equals</operation>
            <value>0</value>
        </filters>
        <filters>
            <field>TASK.DUE_DATE</field>
            <operation>greaterThan</operation>
            <value>YESTERDAY</value>
        </filters>
        <label>Upcoming Events</label>
    </listViews>
    <searchLayouts>
        <searchFilterFields>TASK.SUBJECT</searchFilterFields>
        <searchFilterFields>TASK.WHO_NAME</searchFilterFields>
        <searchFilterFields>TASK.WHAT_NAME</searchFilterFields>
        <searchFilterFields>TASK.DUE_DATE</searchFilterFields>
        <searchFilterFields>CORE.USERS.FULL_NAME</searchFilterFields>
        <searchResultsAdditionalFields>TASK.SUBJECT</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>TASK.WHO_NAME</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>TASK.WHAT_NAME</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>TASK.DUE_DATE</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>CORE.USERS.FULL_NAME</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>Private</sharingModel>
</CustomObject>
