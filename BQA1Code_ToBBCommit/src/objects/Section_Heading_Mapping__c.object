<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object is a Joint Object that controls what Headings can be in what Sections</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>true</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Appearance_Count__c</fullName>
        <defaultValue>0</defaultValue>
        <description>Number of appearances in this particular heading</description>
        <externalId>false</externalId>
        <label>Appearance Count</label>
        <precision>10</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>BOTS_Sales_Lockout_Date__c</fullName>
        <description>The Directory&apos;s current BOTS Edition&apos;s Sales Lockout Date</description>
        <externalId>false</externalId>
        <formula>Directory_Section__r.Directory__r.BOTS_Sales_Lockout_Date__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>The Directory&apos;s current BOTS Edition&apos;s Sales Lockout Date</inlineHelpText>
        <label>BOTS Sales Lockout Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>BOTS_Ship_Date__c</fullName>
        <description>The Director&apos;s current BOTS Edition&apos;s Ship Date</description>
        <externalId>false</externalId>
        <formula>Directory_Section__r.Directory__r.BOTS_Ship_Date__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>The Director&apos;s current BOTS Edition&apos;s Ship Date</inlineHelpText>
        <label>BOTS Ship Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Blackout_Appearance_Count_Indicator__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Blackout Appearance Count Indicator</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Blackout_IBUN_Appearance_Count_Indicator__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Blackout IBUN Appearance Count Indicator</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>DirSecDirHeadIds__c</fullName>
        <externalId>false</externalId>
        <formula>CASESAFEID( Directory_Section__c ) &amp;  CASESAFEID( Directory_Heading__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>DirSecDirHeadIds</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Directory_Code__c</fullName>
        <externalId>false</externalId>
        <label>Directory Code</label>
        <length>10</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Directory_Guide__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Directory Guide</label>
        <referenceTo>Directory_Guide__c</referenceTo>
        <relationshipLabel>Section Heading Mappings</relationshipLabel>
        <relationshipName>Section_Heading_Mappings</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Directory_Heading_Normalized_Sort_Name__c</fullName>
        <externalId>false</externalId>
        <formula>IF( ISPICKVAL(Directory_Section__r.Language__c, &quot;ESP&quot;),
    Directory_Heading__r.Directory_Heading_ESP_Normalized_Name__c,
    Directory_Heading__r.Directory_Heading_Normalized_Name__c
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Directory Heading Normalized Sort Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Directory_Heading__c</fullName>
        <externalId>false</externalId>
        <label>Directory Heading</label>
        <referenceTo>Directory_Heading__c</referenceTo>
        <relationshipLabel>Section Heading Mappings</relationshipLabel>
        <relationshipName>Section_Heading_Mappings1</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>true</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Directory_SFDC_ID__c</fullName>
        <externalId>false</externalId>
        <formula>CASESAFEID(Directory_Section__r.Directory__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Directory SFDC ID</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Directory_Section__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>The Directory Section the Directory Heading on the record is in</description>
        <externalId>false</externalId>
        <inlineHelpText>The Directory Section the Directory Heading on the record is in</inlineHelpText>
        <label>Directory Section</label>
        <referenceTo>Directory_Section__c</referenceTo>
        <relationshipLabel>Section Heading Mappings</relationshipLabel>
        <relationshipName>Section_Heading_Mappings</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Disclaimer_Color__c</fullName>
        <externalId>false</externalId>
        <formula>Heading_Disclaimer__r.Disclaimer_Color__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Disclaimer Color</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Disclaimer_Font__c</fullName>
        <externalId>false</externalId>
        <formula>Heading_Disclaimer__r.Disclaimer_Font__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Disclaimer Font</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Disclaimer_Text__c</fullName>
        <externalId>false</externalId>
        <label>Disclaimer Text</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>External_ID__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>This is a Concatenation of the Directory Section&apos;s 8 Character Section Code and the Directory Heading&apos;s Code. Used for dupe checking and for Upserting</description>
        <externalId>true</externalId>
        <inlineHelpText>This is a Concatenation of the Directory Section&apos;s 8 Character Section Code and the Directory Heading&apos;s Code. Used for dupe checking and for Upserting</inlineHelpText>
        <label>External ID</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Force_Heading_To_Appear__c</fullName>
        <defaultValue>false</defaultValue>
        <description>If checked, this will increase the appearance count by ONE which will force it to appear as Appearance Count distinguishes if we print a Heading within a YP Section or not.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked, this will increase the appearance count by ONE which will force it to appear as Appearance Count distinguishes if we print a Heading within a YP Section or not.</inlineHelpText>
        <label>Force Heading To Appear</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Guide_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Directory_Guide__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Guide Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Guide_Telltale__c</fullName>
        <externalId>false</externalId>
        <label>Guide Telltale</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Heading_Code__c</fullName>
        <externalId>false</externalId>
        <formula>Directory_Heading__r.code__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Heading Code</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Heading_Disclaimer__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>The Disclaimer for this Heading within this Section</description>
        <externalId>false</externalId>
        <inlineHelpText>The Disclaimer for this Heading within this Section</inlineHelpText>
        <label>Heading Disclaimer</label>
        <referenceTo>Heading_Disclaimer__c</referenceTo>
        <relationshipLabel>Section Heading Mappings</relationshipLabel>
        <relationshipName>Section_Heading_Mappings</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Heading_Name_Formatted__c</fullName>
        <externalId>false</externalId>
        <formula>IF( ISPICKVAL(Directory_Section__r.Language__c, &quot;ESP&quot;), 
IF(Directory_Heading__r.Spanish_Heading__c = NULL, 
&quot;AAA - MISSING TRANSLATION&quot; &amp; Heading_Code__c , 
Directory_Heading__r.Spanish_Heading__c ) 
, 
Directory_Heading__r.Name 
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Heading Name Formatted</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Heading_Telltale_Formatted__c</fullName>
        <externalId>false</externalId>
        <formula>IF( ISPICKVAL(Directory_Section__r.Language__c, &quot;ESP&quot;), 
IF(Directory_Heading__r.Spanish_Heading__c = NULL, 
&quot;AAA - MISSING TRANSLATION&quot; &amp; Heading_Code__c , 
Directory_Heading__r.Spanish_Heading_Telltale__c ) 
, 
Directory_Heading__r.Heading_Telltale__c 
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Heading Telltale Formatted</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Heading_XREF_Seetext_And__c</fullName>
        <externalId>false</externalId>
        <formula>IF( ISPICKVAL(Directory_Section__r.Language__c, &quot;ESP&quot;), &quot;y&quot;, &quot;And&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Heading XREF Seetext And</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Heading_XREF_Seetext_See__c</fullName>
        <externalId>false</externalId>
        <formula>IF( ISPICKVAL(Directory_Section__r.Language__c, &quot;ESP&quot;), &quot;Ver&quot;, &quot;See&quot;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Heading XREF Seetext See</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Internet_Bundle_Appearances__c</fullName>
        <description>The amount of Internet Bundle advertisements within this Section Heading</description>
        <externalId>false</externalId>
        <inlineHelpText>The amount of Internet Bundle advertisements within this Section Heading</inlineHelpText>
        <label>Internet Bundle Appearances</label>
        <precision>10</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Internet_Bundle_Artwork__c</fullName>
        <description>This is the Internet Bundle Artwork URN ID for this Directory Section</description>
        <externalId>false</externalId>
        <formula>IF( Internet_Bundle_Appearances__c &gt;= 1, Directory_Section__r.Internet_Bundle_Artwork__c, NULL)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>This is the Internet Bundle Artwork URN ID for this Directory Section</inlineHelpText>
        <label>Internet Bundle Artwork#</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Internet_Bundle_Text__c</fullName>
        <externalId>false</externalId>
        <formula>IF( Internet_Bundle_Appearances__c &gt;= 1, &quot;Visit These Businesses Online&quot; , NULL)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Internet Bundle Text</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Internet_Bundle_UDAC__c</fullName>
        <description>ITM is the UDAC for ALL Internet Bundle Artwork Containers</description>
        <externalId>false</externalId>
        <formula>IF( Internet_Bundle_Appearances__c &gt;= 1, &quot;ITM&quot; , NULL)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>ITM is the UDAC for ALL Internet Bundle Artwork Containers</inlineHelpText>
        <label>Internet Bundle UDAC</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Leader_Ads_Remaining__c</fullName>
        <externalId>false</externalId>
        <formula>IF( Directory_Section__r.Directory__r.Eligible_for_Leader_Ad_Sales__c = TRUE,
Max_Leader_Ad_Quantity__c -  Leader_Ads_Sold__c,
0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Leader Ads Remaining</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Leader_Ads_Sold__c</fullName>
        <description>Number of Non National Leader Ads sold within this Heading within this Section</description>
        <externalId>false</externalId>
        <inlineHelpText>Number of Non National Leader Ads sold within this Heading within this Section</inlineHelpText>
        <label>Leader Ads Sold</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Max_Leader_Ad_Quantity__c</fullName>
        <defaultValue>2</defaultValue>
        <description>The Max amount of Non National Leader Ads that can be sold into this Heading within this Section</description>
        <externalId>false</externalId>
        <inlineHelpText>The Max amount of Non National Leader Ads that can be sold into this Heading within this Section</inlineHelpText>
        <label>Max Leader Ad Quantity</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Max_National_Leader_Ad_Quantity__c</fullName>
        <defaultValue>1</defaultValue>
        <description>The Max amount of National Leader Ads that can be sold into this Heading within this Section</description>
        <externalId>false</externalId>
        <inlineHelpText>The Max amount of National Leader Ads that can be sold into this Heading within this Section</inlineHelpText>
        <label>Max National Leader Ad Quantity</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Migration_ID__c</fullName>
        <description>used for migration of metadata</description>
        <externalId>false</externalId>
        <label>Migration ID</label>
        <length>30</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>National_Leader_Ads_Remaining__c</fullName>
        <externalId>false</externalId>
        <formula>IF( Directory_Section__r.Directory__r.Eligible_for_Leader_Ad_Sales__c = TRUE,
Max_National_Leader_Ad_Quantity__c - National_Leader_Ads_Sold__c,
0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>National Leader Ads Remaining</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>National_Leader_Ads_Sold__c</fullName>
        <description>Number of National Leader Ads sold within this Heading within this Section</description>
        <externalId>false</externalId>
        <inlineHelpText>Number of National Leader Ads sold within this Heading within this Section</inlineHelpText>
        <label>National Leader Ads Sold</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Non_Published_Heading__c</fullName>
        <description>This checkbox is used to identify if a Section Heading Mapping record is setup to include a Non Published Heading within a given Yellow Page Directory Section. Non Published Heading SHM records will not be processed via Informatica for Pagination</description>
        <externalId>false</externalId>
        <formula>Directory_Heading__r.Non_Published_Heading__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>This checkbox is used to identify if a Section Heading Mapping record is setup to include a Non Published Heading within a given Yellow Page Directory Section. Non Published Heading SHM records will not be processed via Informatica for Pagination</inlineHelpText>
        <label>Non Published Heading</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Pagination_Appearance_Count__c</fullName>
        <description>The Appearance Count field used by Informatica to determine if a Heading needs to be paginated or not.</description>
        <externalId>false</externalId>
        <inlineHelpText>The Appearance Count field used by Informatica to determine if a Heading needs to be paginated or not.</inlineHelpText>
        <label>Pagination Appearance Count</label>
        <precision>10</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Pagination_IBUN_Appearance_Count__c</fullName>
        <description>The field Informatica looks at to determine if any IBUN type products have been sold within a given Heading within a given YP Section</description>
        <externalId>false</externalId>
        <inlineHelpText>The field Informatica looks at to determine if any IBUN type products have been sold within a given Heading within a given YP Section</inlineHelpText>
        <label>Pagination IBUN Appearance Count</label>
        <precision>10</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SHM_Advertising_Content_Not_Available__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Advertising Content Not Available</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>SHM_strDirectorySecAndHeading__c</fullName>
        <description>Combination of directory section and directory heading (18+18 digit Id). This is used for internetbundle, SOS Fallout logic.</description>
        <externalId>false</externalId>
        <label>strDirectorySecAndHeading</label>
        <length>36</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Sec_Head_Mapping_Name_Unique_Check__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>This field is marked as UNIQUE and is populated via a Workflow to ensure that the Section Heading Mapping record name, with the appropriate naming convention, is Unique.</description>
        <externalId>false</externalId>
        <inlineHelpText>This field is marked as UNIQUE and is populated via a Workflow to ensure that the Section Heading Mapping record name, with the appropriate naming convention, is Unique.</inlineHelpText>
        <label>Sec Head Mapping Name Unique Check</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>SectionHeadingMappingExternalId__c</fullName>
        <externalId>true</externalId>
        <label>SectionHeadingMappingExternalId</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Section_Code__c</fullName>
        <externalId>false</externalId>
        <formula>Directory_Section__r.Section_Code__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Section Code</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Section_Heading_ID__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>Concatenates the 18 Character CASESAFEID for Directory Section and Directory Heading. Updated via a Workflow and is Unique to disallow duplicate Headings within a Section</description>
        <externalId>false</externalId>
        <inlineHelpText>Concatenates the 18 Character CASESAFEID for Directory Section and Directory Heading. Updated via a Workflow and is Unique to disallow duplicate Headings within a Section</inlineHelpText>
        <label>Section Heading ID</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Spanish_Only_Heading__c</fullName>
        <externalId>false</externalId>
        <formula>Directory_Heading__r.Spanish_Heading_Only__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Spanish Only Heading</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Suppress__c</fullName>
        <externalId>false</externalId>
        <label>Suppress</label>
        <picklist>
            <picklistValues>
                <fullName>Yes</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>is_Active__c</fullName>
        <defaultValue>true</defaultValue>
        <description>To indicate when a heading is no longer available in a given section</description>
        <externalId>false</externalId>
        <inlineHelpText>To indicate when a heading is no longer available in a given section</inlineHelpText>
        <label>is Active</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>strBundleAppearenceCombo__c</fullName>
        <externalId>false</externalId>
        <formula>Directory_Section__c &amp; Directory_Heading__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>strBundleAppearenceCombo</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Section Heading Mapping</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Directory_Code__c</columns>
        <columns>External_ID__c</columns>
        <columns>Directory_Heading__c</columns>
        <columns>Directory_Section__c</columns>
        <columns>Directory_Guide__c</columns>
        <columns>Guide_Telltale__c</columns>
        <columns>Heading_Disclaimer__c</columns>
        <columns>Appearance_Count__c</columns>
        <columns>Force_Heading_To_Appear__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>UPDATEDBY_USER.ALIAS</field>
            <operation>equals</operation>
            <value>dbatc</value>
        </filters>
        <filters>
            <field>Directory_Code__c</field>
            <operation>notEqual</operation>
        </filters>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>JC_Section_Headings1</fullName>
        <columns>NAME</columns>
        <columns>Directory_Code__c</columns>
        <columns>Section_Code__c</columns>
        <columns>External_ID__c</columns>
        <columns>Directory_Heading__c</columns>
        <columns>Directory_Section__c</columns>
        <columns>Directory_Guide__c</columns>
        <columns>Guide_Telltale__c</columns>
        <columns>Heading_Disclaimer__c</columns>
        <columns>Appearance_Count__c</columns>
        <columns>Force_Heading_To_Appear__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Section_Code__c</field>
            <operation>contains</operation>
            <value>05654830</value>
        </filters>
        <label>JC Section Headings</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <label>Section Heading Mapping Name</label>
        <trackHistory>true</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Section Heading Mappings</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Directory_Heading__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Heading_Code__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Directory_Section__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Section_Code__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>External_ID__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Directory_Heading__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Heading_Code__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Directory_Section__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Section_Code__c</lookupDialogsAdditionalFields>
        <searchResultsAdditionalFields>Directory_Section__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Directory_Heading__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>External_ID__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Directory_Guide__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Guide_Telltale__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Heading_Disclaimer__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Force_Heading_To_Appear__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Appearance_Count__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Suppress__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
