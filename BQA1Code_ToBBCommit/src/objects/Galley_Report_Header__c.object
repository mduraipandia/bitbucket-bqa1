<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object is used for Galley Report Customization. This will avoid Crystal Pro limitation issue.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>GRH_Galley_Staging_Job_Id__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Galley Staging Job</label>
        <referenceTo>Galley_Staging_Job__c</referenceTo>
        <relationshipLabel>Galley Report Headers</relationshipLabel>
        <relationshipName>Galley_Report_Headers</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>GRH_Informatica_Run_Instance_Id__c</fullName>
        <externalId>false</externalId>
        <label>Informatica Run Instance Id</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>GR_Business_Type__c</fullName>
        <externalId>false</externalId>
        <label>Business Type</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>GR_Directory_Code__c</fullName>
        <description>Store Selected Directory Code for this header</description>
        <externalId>false</externalId>
        <label>Directory Code</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>GR_Directory_Edition__c</fullName>
        <description>Selected directory edition in the galley parameter screen</description>
        <externalId>false</externalId>
        <label>Directory Edition</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>GR_Directory_Section__c</fullName>
        <description>Selected Section on Parameter Screen</description>
        <externalId>false</externalId>
        <label>Directory Section</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>GR_Directroy_Name__c</fullName>
        <externalId>false</externalId>
        <label>Directory Name</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>GR_Informatica_Auto_Number__c</fullName>
        <description>This field is used in Informatica for maintaining the relationship between Galley Report Header and Galley Report Stage records</description>
        <externalId>false</externalId>
        <label>Informatica Auto Number</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>GR_Selected_Year__c</fullName>
        <description>Years selected in report screen</description>
        <externalId>false</externalId>
        <label>Selected Year</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>GR_Telco__c</fullName>
        <description>Selected Telco on Parameter Screen</description>
        <externalId>false</externalId>
        <label>Telco</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>GR_User_Name__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Report Header Owner</description>
        <externalId>false</externalId>
        <label>User Name</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Galley_Report_Headers</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Galley Report Header</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>GR_User_Name__c</columns>
        <columns>GR_Directory_Code__c</columns>
        <columns>GR_Directroy_Name__c</columns>
        <columns>GR_Directory_Edition__c</columns>
        <columns>GR_Directory_Section__c</columns>
        <columns>GR_Selected_Year__c</columns>
        <columns>GR_Telco__c</columns>
        <columns>GR_Business_Type__c</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>My_Galley_Report_Headers</fullName>
        <columns>NAME</columns>
        <columns>GR_Directroy_Name__c</columns>
        <columns>GR_Directory_Code__c</columns>
        <columns>GR_Selected_Year__c</columns>
        <columns>GR_Directory_Edition__c</columns>
        <columns>GR_Directory_Section__c</columns>
        <columns>GR_Telco__c</columns>
        <columns>GR_Business_Type__c</columns>
        <columns>GR_User_Name__c</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Mine</filterScope>
        <label>My Galley Report Headers</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>GR{000000}</displayFormat>
        <label>Galley Report Number</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Galley Report Headers</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>GR_Directroy_Name__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>GR_Directory_Code__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>GR_Selected_Year__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>GR_Directory_Edition__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>GR_Directory_Section__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>GR_Telco__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>GR_Business_Type__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>GR_User_Name__c</customTabListAdditionalFields>
        <excludedStandardButtons>New</excludedStandardButtons>
        <excludedStandardButtons>ChangeOwner</excludedStandardButtons>
        <excludedStandardButtons>Accept</excludedStandardButtons>
        <lookupDialogsAdditionalFields>GR_Directroy_Name__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>GR_Directory_Code__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>GR_Selected_Year__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>GR_Directory_Edition__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>GR_Directory_Section__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>GR_Telco__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>GR_Business_Type__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>GR_User_Name__c</lookupDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>GR_Directroy_Name__c</searchFilterFields>
        <searchFilterFields>GR_Directory_Code__c</searchFilterFields>
        <searchFilterFields>GR_Selected_Year__c</searchFilterFields>
        <searchFilterFields>GR_User_Name__c</searchFilterFields>
        <searchFilterFields>CREATED_DATE</searchFilterFields>
        <searchFilterFields>LAST_UPDATE</searchFilterFields>
        <searchResultsAdditionalFields>GR_Directroy_Name__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>GR_Directory_Code__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>GR_Selected_Year__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>GR_Directory_Edition__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>GR_Directory_Section__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>GR_Telco__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>GR_Business_Type__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>GR_User_Name__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <webLinks>
        <fullName>Galley_Report</fullName>
        <availability>online</availability>
        <description>Navigate to the Galley Report VF page</description>
        <displayType>link</displayType>
        <linkType>page</linkType>
        <masterLabel>Galley Report</masterLabel>
        <openType>replace</openType>
        <page>GalleyReport_V1</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>Galley_Validation_Report</fullName>
        <availability>online</availability>
        <displayType>link</displayType>
        <encodingKey>UTF-8</encodingKey>
        <hasMenubar>false</hasMenubar>
        <hasScrollbars>true</hasScrollbars>
        <hasToolbar>false</hasToolbar>
        <height>600</height>
        <isResizable>true</isResizable>
        <linkType>url</linkType>
        <masterLabel>Galley Validation Report</masterLabel>
        <openType>newWindow</openType>
        <position>none</position>
        <protected>false</protected>
        <showsLocation>false</showsLocation>
        <showsStatus>false</showsStatus>
        <url>/{!$Label.Galley_Validation_Report_Id}?pv0={!Galley_Report_Header__c.Name}</url>
    </webLinks>
</CustomObject>
