<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <tab>standard-Chatter</tab>
    <tab>standard-Workspace</tab>
    <tab>standard-ContentSearch</tab>
    <tab>standard-ContentSubscriptions</tab>
    <tab>standard-ContentFavorites</tab>
    <tab>Payment_History</tab>
    <tab>Directory_Guide__c</tab>
    <tab>Radio_Button_Background_Matching</tab>
    <tab>ffps_ct__FlatTransaction__c</tab>
    <tab>Galley_Parameter_N</tab>
    <tab>ffps_ct__MatchingWriteOff__c</tab>
    <tab>Tabular_Custom_Data_Table_Tax__c</tab>
    <tab>ffps_ct__StagingTax__c</tab>
    <tab>Billing_Transfer_Status__c</tab>
</CustomApplication>
